#include "image_processing.h"
#if 0
#include <QGuiApplication>
#include <QApplication>
#include <QScreen>
#include <QDebug>
#include <QRect>
#include <QGuiApplication>
#include <QApplication>
#include <QScreen>
#include <QDebug>
#include <QRect>
#include <QBuffer>
#include <QSettings>
#include <QDir>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "traffic_enforcement.h"
#include "common.h"
#include "image_common.h"
#include "mainwindow.h"
#include "detection_lanecovered.h"
#include "sobel.h"
#include "quick_sort.h"
#include "traffic_enforcement.h"
#include "parameters_table.h"
//#include "face_detection_CSIM.h"
#include "pythonwrapper.h"


using namespace cv;
using namespace std;
QElapsedTimer timer3;
QElapsedTimer timer;
qint64 g_totalTime = 0;
qint32 g_cnt = 0;
//opencv version > 3
//#define GET_MAT_REFCOUNT(_x_) ((_x_).u ? (*((_x_).u)->refcount) : 0)
//opencv version < 3
#define GET_MAT_REFCOUNT(_x_) ((!_x_.empty()) ? (*(_x_.refcount)) : -1)
#define USE_MORPHOLOGY_CLOSE_WHEN_CANNY
//#define SAVE_MODIFIED_PIC

typedef void (*FnThresholdCallback)(int, void*);

extern int g_numLines;
extern int g_numCars;

Mat g_matHLSInF;
Mat g_matHLSOut;
Mat g_matHLSOutH;
Mat g_matHLSOutL;
Mat g_matHLSOutS;
Mat g_matHSVInF;
Mat g_matHSVOut;
Mat g_matHSVOutH;
Mat g_matHSVOutS;
Mat g_matHSVOutV;
Mat g_matMixIn;

Mat g_matCannyIn;

Mat g_matSobelIn;
Mat g_matSobelOut;

Mat g_matMaskSobelX;
Mat g_matMaskSobelY;

Mat g_matCarIn;

//#define USE_MANUAL_PIC
//#define USE_DRIVING_RECORDER_PIC
CascadeClassifier cascade;
CascadeClassifier cascade1;
CascadeClassifier cascade2;
CascadeClassifier cascade3;
CascadeClassifier cascade4;


////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//int g_maskMixMode = mask_hls_and_hsv_or_sobel;
//HLS//////////
int g_hlsThresholdValueH = 0;
int g_hlsThresholdValueL = 0;
int g_hlsThresholdValueS = 170;
int g_hlsThresholdValueHMax = k_hls_h_max;
int g_hlsThresholdValueLMax = k_hls_l_max;
int g_hlsThresholdValueSMax = k_hls_s_max;
//HSV//////////
//Hue, Saturation, Value
int g_hsvThresholdValueH = 0;
int g_hsvThresholdValueS = 0;
int g_hsvThresholdValueV = 0;//50; //new
int g_hsvThresholdValueHMax = k_hsv_h_max;
int g_hsvThresholdValueSMax = k_hsv_s_max;
int g_hsvThresholdValueVMax = k_hsv_v_max;
//Sobel//////////
int g_sobelDxyMode = 3;
int g_sobelKSizeX = 3; //0~3, x * 2 + 1
int g_sobelKSizeY = 1; //0~3, x * 2 + 1
int g_sobelThresholdXMin = 3;
int g_sobelThresholdXMax = 255;//80;
int g_sobelThresholdYMin = 5; // DSCF4447.avi
int g_sobelThresholdYMax = 255;
int g_sobelThresholdType = 0;

//Canny//////////
int g_cannyThreshold1 = 74;// 4;
int g_cannyThreshold2 = 306;// 236;//for test
int g_cannyApertureSizeDiv2 = 0;
int g_cannyIsgradient = 0;// 1;
int g_houghRho = 0;
int g_houghTheta = 150;// 325; //test
int g_houghLineThreshold = 214;// 219;
int g_houghMinLineLength = 215;// 250;
int g_houghMaxLineGap = 515;// 600;

							//Car//////////
int carClassifierScaleFactor = 0;
int carClassifierMinNeighbors = 1;
int carClassifierFlags = 0;
int carClassifierMinSize = 40;
int carClassifierMaxSize = 500;

int g_numOfLines = 0;
AutoGeneratorLine *g_pAutoGeneratorLines = nullptr;
int g_numOfCars = 0;
AutoGeneratorRect *g_pAutoGeneratorRects = nullptr;

ImageProcessing *g_pImageProcessing = nullptr;

void remove_extension(char* s) {
	char* dot = 0;
	while (*s) {
		if (*s == '.') dot = s;  // last dot
		else if (*s == '/' || *s == '\\') dot = 0;  // ignore dots before path separators
		s++;
	}
	if (dot) *dot = '\0';
}

ImageProcessing::ImageProcessing(QObject *parent)
	: ImageProcessingThread(parent)
    //: QObject(parent)
    , m_isFistTimeToShow(true)
    , m_isRatInMaze(true)
    , m_isWindowExisted(false)
	, m_isEngineeringMode(false)
    //, m_CheckLaneDetectionAndCalibration(true)
    , m_isReadyToDetectVehicle(false)
	, m_currentFrame(0)
    , m_state(e_imageprocessing_state_stopped)
	, m_approach(e_imageprocessing_common_preview)
    , m_pPythonWrapper(nullptr)
    , m_pPythonWrapperModel(nullptr)
    , m_pScene(nullptr)
{
    g_pImageProcessing = this;

	memset(&m_videoConfigFromSource, 0, sizeof(m_videoConfigFromSource));
	timer3.start();
}

ImageProcessing::~ImageProcessing()
{
    Release();
}

//void ImageProcessing::InitParameters(const char *strFileName)
//{
//	int i;
//    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
//	for (i = 0; i < DIM(k_parameters_table); ++i)
//	{
//		//qDebug("11 %s", strFileName);
//		//qDebug("22 %s", k_parameters_table[i].file_name);
//		if (0 == strcmp(strFileName, k_parameters_table[i].file_name))
//		{
//			//qDebug("%s", strFileName);
//            pCache->lanedetection_maskmix_mode = k_parameters_table[i].mix_mode;
//			g_hlsThresholdValueH = k_parameters_table[i].hls.threshold_value_h;
//			g_hlsThresholdValueL = k_parameters_table[i].hls.threshold_value_l;
//			g_hlsThresholdValueS = k_parameters_table[i].hls.threshold_value_s;
//			g_hlsThresholdValueHMax = k_parameters_table[i].hls.threshold_value_h_max;
//			g_hlsThresholdValueLMax = k_parameters_table[i].hls.threshold_value_l_max;
//			g_hlsThresholdValueSMax = k_parameters_table[i].hls.threshold_value_s_max;
//
//			//HSV//////////
//			//Hue, Saturation, Value
//			g_hsvThresholdValueH = k_parameters_table[i].hsv.threshold_value_h;
//			g_hsvThresholdValueS = k_parameters_table[i].hsv.threshold_value_s;
//			g_hsvThresholdValueV = k_parameters_table[i].hsv.threshold_value_v;
//			g_hsvThresholdValueHMax = k_parameters_table[i].hsv.threshold_value_h_max;
//			g_hsvThresholdValueSMax = k_parameters_table[i].hsv.threshold_value_s_max;
//			g_hsvThresholdValueVMax = k_parameters_table[i].hsv.threshold_value_v_max;
//			//Sobel//////////
//			g_sobelDxyMode = k_parameters_table[i].sobel.dxymode;
//			g_sobelKSizeX = k_parameters_table[i].sobel.ksize_x;
//			g_sobelKSizeY = k_parameters_table[i].sobel.ksize_y;
//			g_sobelThresholdXMin = k_parameters_table[i].sobel.threshold_x_min;
//			g_sobelThresholdXMax = k_parameters_table[i].sobel.threshold_x_max;
//			g_sobelThresholdYMin = k_parameters_table[i].sobel.threshold_y_min;
//			g_sobelThresholdYMax = k_parameters_table[i].sobel.threshold_y_max;
//			g_sobelThresholdType = k_parameters_table[i].sobel.threshold_type;
//
//			//Canny//////////
//			g_cannyThreshold1 = k_parameters_table[i].canny.threshold_1;
//			g_cannyThreshold2 = k_parameters_table[i].canny.threshold_2;
//			g_cannyApertureSizeDiv2 = k_parameters_table[i].canny.aperture_sizediv2;
//			g_cannyIsgradient = k_parameters_table[i].canny.is_gradient;
//			g_houghRho = k_parameters_table[i].hough.rho;
//			g_houghTheta = k_parameters_table[i].hough.theta;
//			g_houghLineThreshold = k_parameters_table[i].hough.line_threshold;
//			g_houghMinLineLength = k_parameters_table[i].hough.minline_length;
//			g_houghMaxLineGap = k_parameters_table[i].hough.maxline_gap;
//
//            m_lanelineLeft[0] = k_parameters_table[i].line_left.x1;
//            m_lanelineLeft[1] = k_parameters_table[i].line_left.y1;
//            m_lanelineLeft[2] = k_parameters_table[i].line_left.x2;
//            m_lanelineLeft[3] = k_parameters_table[i].line_left.y2;
//
//            //DSG(1, "ggggg m_lanelineLeft= i=%d %d %d %d %d", i
//            //    , m_lanelineLeft[0]
//            //    , m_lanelineLeft[1]
//            //    , m_lanelineLeft[2]
//            //    , m_lanelineLeft[3]);
//
//            m_lanelineRight[0] = k_parameters_table[i].line_right.x1;
//            m_lanelineRight[1] = k_parameters_table[i].line_right.y1;
//            m_lanelineRight[2] = k_parameters_table[i].line_right.x2;
//            m_lanelineRight[3] = k_parameters_table[i].line_right.y2;
//
//			//Car//////////
//			carClassifierScaleFactor = k_parameters_table[i].car_classifier.scale_factor;
//			carClassifierMinNeighbors = k_parameters_table[i].car_classifier.min_heighbors;
//			carClassifierFlags = k_parameters_table[i].car_classifier.flags;
//			carClassifierMinSize = k_parameters_table[i].car_classifier.min_size;
//			carClassifierMaxSize = k_parameters_table[i].car_classifier.max_size;
//			break;
//		}
//	}
//
//	if (DIM(k_parameters_table) == i)
//	{
//        pCache->lanedetection_maskmix_mode = mask_gray_equalizehist_only;
//	}
//}

int  ImageProcessing::Probe(cv::Mat &matScaledOrgClone, DrawableGraphicsScene *pScene)
{
    if (!pScene)
        return -1;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    m_pScene = pScene;

	//m_matScaledImageList.enqueue(matScaledOrgClone);

	m_matScaledImagePrevious.release();
	m_matScaledImage.release();
	m_matScaledImageF.release();
	m_matScaledImageGray.release();

	m_matScaledImagePrevious = matScaledOrgClone.clone();
    m_matScaledImage = matScaledOrgClone.clone();
    matScaledOrgClone.convertTo(m_matScaledImageF, CV_32FC3, 1.0 / 255, 0);
	cv::cvtColor(matScaledOrgClone, m_matScaledImageGray, COLOR_BGR2GRAY);

    ELaneDetectionApproach approach = pCache->lanedetection_approach;
    if (e_lanedetection_hls == approach)
    {
        PrepareImageForLaneDetectionHLS(m_matScaledImageF);
    }
    else if (e_lanedetection_hsv == approach)
    {
        PrepareImageForLaneDetectionHSV(m_matScaledImageF);
    }
    else if (e_lanedetection_sobel == approach)
    {
		PrepareImageForLaneDetectionHLS(m_matScaledImageF); // g_sobelDxyMode == 3
        PrepareImageForLaneDetectionSobel(m_matScaledImage);
    }
    else if (e_lanedetection_canny == approach)
    {
        PrepareImageForLaneDetectionCanny(m_matScaledImage);
    }
    else if (e_lanedetection_mix_hls_hsv_canny == approach)
    {
        PrepareImageForLaneDetectionHLS(m_matScaledImageF);
        PrepareImageForLaneDetectionHSV(m_matScaledImageF);
        PrepareImageForLaneDetectionMix(m_matScaledImage);
        PrepareImageForLaneDetectionCanny(m_matScaledImage);
    }
    else if (e_lanedetection_mix_hls_hsv_sobel_canny == approach)
    {
        PrepareImageForLaneDetectionHLS(m_matScaledImageF);
        PrepareImageForLaneDetectionHSV(m_matScaledImageF);	
        PrepareImageForLaneDetectionSobel(m_matScaledImage);	
        PrepareImageForLaneDetectionMix(m_matScaledImage);	
        PrepareImageForLaneDetectionCanny(m_matScaledImage);	
    }

    PrepareImageForCarDetection(m_matScaledImage);
	
    return 0;
}

void ImageProcessing::SyncProcess()
{
	//qDebug("Process will ShowTrackbarWindow");
    ShowTrackbarWindow(e_trackbar_force_normal);
	//qDebug("Process will ThresholdCallbackAll");
    ThresholdCallbackAll(0, 0);
    //HandleThresholdCallback(false);
//    while (true) {
//        char key = (char) waitKey(30);
//        if (key == 'q' || key == 27)
//        {
//            break;
//        }
//    }
}

void ImageProcessing::AsyncProcess(const cv::Mat &matScaledOrgClone)
{
    m_matScaledImagePrevious.release();
    m_matScaledImage.release();
    m_matScaledImageF.release();
    m_matScaledImageGray.release();

    m_matScaledImagePrevious = matScaledOrgClone.clone();
    m_matScaledImage = matScaledOrgClone.clone();
    matScaledOrgClone.convertTo(m_matScaledImageF, CV_32FC3, 1.0 / 255, 0);
    cv::cvtColor(matScaledOrgClone, m_matScaledImageGray, COLOR_BGR2GRAY);

    ShowTrackbarWindow(e_trackbar_force_normal);

    ThresholdCallbackAllAsync(0, 0);
}

void ImageProcessing::Release()
{
    qDebug("ImageProcessing::Release");
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

	ShowTrackbarWindow(e_trackbar_force_off);
	ReleaseLanelinesInfo(true);

	if (e_vd_model_kneron == pCache->vehicledetection_model)
	{
        if (m_pPythonWrapperModel)
        {
            delete m_pPythonWrapperModel;
            m_pPythonWrapperModel = nullptr;
        }
        if (m_pPythonWrapper)
        {
            delete m_pPythonWrapper;
            m_pPythonWrapper = nullptr;
        }
        //PythonWrapperFree();
	}
	else
	{
	}

    g_matCarIn.release();
    g_matCannyIn.release();
    g_matMixIn.release();
    g_matSobelIn.release();
    g_matSobelOut.release();
    g_matHSVInF.release();
    g_matHSVOut.release();
    g_matHSVOutH.release();
    g_matHSVOutS.release();
    g_matHSVOutV.release();
    g_matHLSInF.release();
    g_matHLSOut.release();
    g_matHLSOutH.release();
    g_matHLSOutL.release();
    g_matHLSOutS.release();

	m_matDisplay.release();
	m_matCannyOut.release();
	m_matMixOut.release();
	m_matMaskMix.release();
	m_matMaskSobel.release();
	m_matMaskHSV_H.release();
	m_matMaskHSV_S.release();
	m_matMaskHSV_V.release();
	m_matMaskHSV.release();
	m_matMaskHLS_H.release();
	m_matMaskHLS_L.release();
	m_matMaskHLS_S.release();
	m_matMaskHLS.release();
	m_matHLSChannel.clear();
	m_matScaledImageGray.release();
    m_matScaledImage.release();
    m_matScaledImageF.release();
	m_matScaledImagePrevious.release();

	//m_matScaledImageList.clear();
	//QQueue<Mat> zerom_matScaledOrgImageList;
	//m_matScaledImageList.swap(zerom_matScaledOrgImageList);

    DeleteCarAgent();

    cv::destroyAllWindows();
}

void ImageProcessing::EmitMaskHLSImageReady(const cv::Mat &matImage)
{
    DSG(1, "EmitMaskHLSImageReady");
	emit maskHLSImageReady(matImage);
}

void ImageProcessing::EmitMaskHSVImageReady(const cv::Mat &matImage)
{
    DSG(1, "EmitMaskHSVImageReady");
	emit maskHSVImageReady(matImage);
}

void ImageProcessing::EmitMaskSobelImageReady(const cv::Mat &matImage)
{
    DSG(1, "EmitMaskSobelImageReady");
	emit maskSobelImageReady(matImage);
}

void ImageProcessing::EmitMixOutImageReady(const cv::Mat &matImage)
{
    DSG(1, "EmitMixOutImageReady");
	emit mixOutImageReady(matImage);
}

void ImageProcessing::EmitCannyOutImageReady(const cv::Mat &matImage)
{
    DSG(1, "EmitCannyOutImageReady");
	emit cannyOutImageReady(matImage);
}

void ImageProcessing::EmitDisplayImageReady(const cv::Mat &matImage)
{
	//qDebug("ShowWindowCarClassifier 1");
    //emit displayImageReady(matImage);
}

void ImageProcessing::EmitUpdateLaneline(std::vector<cv::Vec4i> *pLanelineArray)
{
    //qDebug("ImageProcessing::EmitUpdateLaneline");
    emit updateLaneline(pLanelineArray);
}

void ImageProcessing::EmitUpdateVehicleInfo(std::vector<cv::Rect> *pVehicleArray)
{
	//qDebug("ImageProcessing::EmitUpdateVehicleInfo");
	emit updateVehicleInfo(pVehicleArray);
}

void ImageProcessing::ReactFromTrackbarHLS(const Mat &in, cv::Mat *lpMaskH, cv::Mat *lpMaskL, cv::Mat *lpMaskS)
{

    //std::vector<cv::Mat> zeroMat;
    //m_matHLSChannel.swap(zeroMat);
    m_matHLSChannel.clear();
    m_matHLSChannel.shrink_to_fit();

    split(in, m_matHLSChannel);
    //inRange(in, Scalar(g_hlsThresholdValueH,
    //                   g_hlsThresholdValueL / float(k_hls_l_max),
    //                   g_hlsThresholdValueS / float(k_hls_s_max)),
    //            Scalar(k_hls_h_max,
    //                   k_hls_l_max / float(k_hls_l_max),
    //                   k_hls_s_max / float(k_hls_s_max)), mask);

	//std::cout << m_matHLSChannel[0] << std::endl;
    //m_matHLSChannel[0]: 0 ~ 360
	inRange(m_matHLSChannel[0], Scalar(g_hlsThresholdValueH), Scalar(g_hlsThresholdValueHMax), *lpMaskH);
	inRange(m_matHLSChannel[1], Scalar(g_hlsThresholdValueL / float(k_hls_l_max)), Scalar(g_hlsThresholdValueLMax / float(k_hls_l_max)), *lpMaskL);
	inRange(m_matHLSChannel[2], Scalar(g_hlsThresholdValueS / float(k_hls_s_max)), Scalar(g_hlsThresholdValueSMax / float(k_hls_s_max)), *lpMaskS);
}

//void ImageProcessing::ReactFromTrackbarHSV(const Mat &in, Mat &mask)
void ImageProcessing::ReactFromTrackbarHSV(const cv::Mat &in, cv::Mat *lpMaskH, cv::Mat *lpMaskS, cv::Mat *lpMaskV)
{
	vector<Mat> chHSV;
	split(in, chHSV);

	inRange(chHSV[0], Scalar(g_hsvThresholdValueH), Scalar(g_hsvThresholdValueHMax), *lpMaskH);
	inRange(chHSV[1], Scalar(g_hsvThresholdValueS / float(k_hsv_s_max)), Scalar(g_hsvThresholdValueSMax / float(k_hsv_s_max)), *lpMaskS);
	inRange(chHSV[2], Scalar(g_hsvThresholdValueV / float(k_hsv_v_max)), Scalar(g_hsvThresholdValueVMax / float(k_hsv_v_max)), *lpMaskV);

	//inRange(in, Scalar(g_hsvThresholdValueH,
	//	g_hsvThresholdValueS / float(k_hsv_s_max),
	//	g_hsvThresholdValueV / float(k_hsv_v_max)),
	//	Scalar(k_hsv_h_max,
	//		k_hsv_s_max / float(k_hsv_s_max),
	//		k_hsv_v_max / float(k_hsv_v_max)), mask);
}

void ImageProcessing::ReactFromTrackbarSobel(const cv::Mat &in, cv::Mat &maskX, cv::Mat &maskY)
{    
    //in: g_matSobelIn CV_RGB2GRAY
    Mat dstX, dstY;
    Mat absDstX, absDstY;
    /* 0: Binary
       1: Binary Inverted
       2: Threshold Truncated
       3: Threshold to Zero
       4: Threshold to Zero Inverted
     */
    int threadTypeX = g_sobelThresholdType;
    int threadTypeY = g_sobelThresholdType;

    if (0 == g_sobelDxyMode) //Gradient X
    {
        Sobel(in, dstX, CV_16S, 1, 0, (g_sobelKSizeX * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstX, absDstX); //convert our partial results back to CV_8U
		inRange(absDstX, Scalar(g_sobelThresholdXMin), Scalar(g_sobelThresholdXMax), maskX);
        //inRange(absDstX, Scalar(g_sobelThresholdXMin, g_sobelThresholdXMin, g_sobelThresholdXMin),
        //                 Scalar(g_sobelThresholdXMax, g_sobelThresholdXMax, g_sobelThresholdXMax),
        //                 maskX);
        //if (absDstX.type() == CV_8UC1)
        //    threadTypeX |= THRESH_OTSU;
        //threshold(absDstX, maskX, g_sobelThresholdXMin, g_sobelThresholdXMax, threadTypeX);
    }
    else if (1 == g_sobelDxyMode) // Gradient Y
    {
        Sobel(in, dstY, CV_16S, 0, 1, (g_sobelKSizeY * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstY, absDstY); //convert our partial results back to CV_8U
		inRange(absDstY, Scalar(g_sobelThresholdYMin), Scalar(g_sobelThresholdYMax), maskY);
        //inRange(absDstY, Scalar(g_sobelThresholdYMin, g_sobelThresholdYMin, g_sobelThresholdYMin),
        //                 Scalar(g_sobelThresholdYMax, g_sobelThresholdYMax, g_sobelThresholdYMax),
        //                 maskY);
        //if (absDstX.type() == CV_8UC1)
        //    threadTypeY |= THRESH_OTSU;
        //threshold(absDstY, maskY, g_sobelThresholdYMin, g_sobelThresholdYMax, threadTypeY);
    }
    else if (2 == g_sobelDxyMode)
    {
#if 1
        Sobel(in, dstX, CV_64F, 1, 0, (g_sobelKSizeX * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstX, absDstX); //convert our partial results back to CV_8U

        //qDebug("aaa1 %d", in.at<Vec3b>(200, 200)[0]);
        //qDebug("aaa2 %d", in.at<Vec3b>(200, 200)[1]);
        //qDebug("aaa3 %d", in.at<Vec3b>(200, 200)[2]);
        //inRange(in, Scalar(g_sobelThresholdXMin, g_sobelThresholdXMin, g_sobelThresholdXMin),
        //            Scalar(g_sobelThresholdXMax, g_sobelThresholdXMax, g_sobelThresholdXMax), maskX);
        inRange(absDstX, Scalar(g_sobelThresholdXMin), Scalar(g_sobelThresholdXMax), maskX);
        //qDebug("bbb1 %d", maskX.at<uchar>(200, 200));
//        if (absDstX.type() == CV_8UC1)
//            threadTypeX |= THRESH_OTSU;
        //threshold(absDstX, maskX, g_sobelThresholdXMin, g_sobelThresholdXMax, threadTypeX);
        //imshow("maskX", maskX);
        Sobel(in, dstY, CV_16S, 0, 1, (g_sobelKSizeY * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstY, absDstY); //convert our partial results back to CV_8U

        //qDebug("aaa1 %d", in.at<Vec3b>(200, 200)[0]);
        //qDebug("aaa2 %d", in.at<Vec3b>(200, 200)[1]);
        //qDebug("aaa3 %d", in.at<Vec3b>(200, 200)[2]);
        //inRange(in, Scalar(g_sobelThresholdYMin, g_sobelThresholdYMin, g_sobelThresholdYMin),
        //            Scalar(g_sobelThresholdYMax, g_sobelThresholdYMax, g_sobelThresholdYMax), maskY);
        inRange(absDstY, Scalar(g_sobelThresholdYMin), Scalar(g_sobelThresholdYMax), maskY);
        //qDebug("bbb1 %d", maskY.at<uchar>(200, 200));
//        if (absDstY.type() == CV_8UC1)
//            threadTypeY |= THRESH_OTSU;
//imshow("maskY", maskY);
        //threshold(absDstY, maskY, g_sobelThresholdYMin, g_sobelThresholdYMax, threadTypeY);
#else
        Sobel(in, dstX, CV_16S, 1, 0, (g_sobelKSizeX * 2) + 1, 1, 0, BORDER_DEFAULT);
        //qDebug("ReactFromTrackbarSobel in=%d %d %d",
        //       in.type(), in.cols, in.rows);//5 864 576
        //qDebug("ReactFromTrackbarSobel dstX=%d %d %d",
        //       dstX.type(), dstX.cols, dstX.rows);//3 864 576
        convertScaleAbs(dstX, absDstX); //convert our partial results back to CV_8U
        //qDebug("ReactFromTrackbarSobel absDstX=%d", absDstX.type());//0 864 576
        Sobel(in, dstY, CV_16S, 0, 1, (g_sobelKSizeY * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstY, absDstY); //convert our partial results back to CV_8U
        // Total Gradient
        Mat matTmp;
        addWeighted(absDstX, 0.5, absDstY, 0.5, 0, matTmp);

        //qDebug("ReactFromTrackbarSobel matTmp=%d %d %d",
        //       matTmp.type(), matTmp.cols, matTmp.rows);        //0 864 576
        if (matTmp.type() == CV_8UC1)
            threshold(matTmp, mask, g_sobelThresholdXMin, k_sobel_threshold_x_max, g_sobelThresholdType | THRESH_OTSU);
        else
            threshold(matTmp, mask, g_sobelThresholdYMin, k_sobel_threshold_y_max, g_sobelThresholdType);
#endif
    }
	else if (3 == g_sobelDxyMode)
	{
		Sobel(in, dstX, CV_64F, 1, 0, (g_sobelKSizeX * 2) + 1, 1, 0, BORDER_DEFAULT);

		//dstY = cv::Mat::zeros(cv::Size(dstX.cols, dstX.rows), CV_64F);
		
		//imwrite("sobelx_1_good.bmp", dstX);

		Mat sobelpow2X, sobelpow2Y;
		//sobelpow2Y = dstY;
		cv::pow(dstX, 2, sobelpow2X);
		//cv::pow(dstY, 2, sobelpow2Y);

		Mat sobel_abs = cv::abs(sobelpow2X);// +sobelpow2Y);

		//imwrite("sobel_abs1.jpg", sobel_abs);

		double minv = 0.0, maxv = 0.0;
		minMaxIdx(sobel_abs, &minv, &maxv);

		Mat sobel_abs_norm = sobel_abs.mul(255.0 / maxv);
		//imwrite("sobel_abs2.jpg", sobel_abs_norm);

		cv::inRange(sobel_abs_norm, Scalar(g_sobelThresholdXMin), Scalar(g_sobelThresholdXMax), maskX); 

		//maskY = cv::Mat::zeros(cv::Size(dstX.cols, dstX.rows), CV_8U);

		//imwrite("maskX.bmp", maskX);
	}
}

void ImageProcessing::ReactFromTrackbarCanny(cv::Mat &in, cv::Mat &out)
{
#ifdef USE_MANUAL_PIC

#ifdef USE_MORPHOLOGY_CLOSE_WHEN_CANNY
        Mat tmp;
        Mat dst1;
        Mat dst2;
        Canny(in, tmp, g_cannyThreshold1, g_cannyThreshold2, 3 + (g_cannyApertureSizeDiv2 << 1), (bool)g_cannyIsgradient);
        Mat erodeStruct = getStructuringElement(MORPH_RECT,Size(3,3));
        morphologyEx(tmp, out, MORPH_CLOSE, erodeStruct);
        //morphologyEx(dst1, out, MORPH_OPEN, erodeStruct);
    #else
        Canny(in, out, g_cannyThreshold1, g_cannyThreshold2, 3 + (g_cannyApertureSizeDiv2 << 1), (bool)g_cannyIsgradient);
    #endif
#else
    Canny(in, out, g_cannyThreshold1, g_cannyThreshold2, 3 + (g_cannyApertureSizeDiv2 << 1), (bool)g_cannyIsgradient);
#endif
}

void ImageProcessing::UpdateOutputMatHLS(const Mat &srcF, const Mat &maskH, const Mat &maskL, const Mat &maskS, 
    Mat &mask, cv::Mat &dst)
{
    //qDebug("orgImage=%d %d %d", orgImage.type(), orgImage.cols, orgImage.rows);
    //qDebug("mask=%d %d %d", mask.type(), mask.cols, mask.rows);
	Mat matTmpDst = Mat::zeros(srcF.size(), CV_32FC3);
	//qDebug("srcF=%d %d %d", srcF.type(), srcF.cols, srcF.rows);
	//qDebug("matTmpDst=%d %d %d", matTmpDst.type(), matTmpDst.cols, matTmpDst.rows);
	//qDebug("-======================");
	//cout << maskL << endl;
    //static int aa = 0;
    for (int rowIdx = 0; rowIdx < srcF.rows; ++rowIdx)
    {
        for (int colIdx = 0; colIdx < srcF.cols; ++colIdx)
        {
            if ((255 == maskH.at<uchar>(rowIdx, colIdx)) &&
				(255 == maskL.at<uchar>(rowIdx, colIdx)) &&
				(255 == maskS.at<uchar>(rowIdx, colIdx))) // the pixel pass the mask detection
            {
                //if (aa > 10)
                //{
                //    DSG(1, "YYYYYEEEEESSSSS");
                //}
                //else
                //    aa++;
				mask.at<uchar>(rowIdx, colIdx) = 255;
            //#ifdef USE_CV_32FC3
				matTmpDst.at<Vec3f>(rowIdx, colIdx) = srcF.at<Vec3f>(rowIdx, colIdx);
            //#else
			//	matTmpDst.at<Vec3b>(rowIdx, colIdx) = srcF.at<Vec3b>(rowIdx, colIdx);
            //#endif
            }
            else
            {
				mask.at<uchar>(rowIdx, colIdx) = 0;
            //#ifdef USE_CV_32FC3
				matTmpDst.at<Vec3f>(rowIdx, colIdx) = 0;
            //#else
            //    matTmpDst.at<Vec3b>(rowIdx, colIdx) = 0;
            //#endif
            }
        }
    }

	matTmpDst.convertTo(dst, CV_8UC3, 255, 0);
}

//void ImageProcessing::UpdateOutputMatHSV(const Mat &orgImage, const Mat &mask, cv::Mat &dst)
void ImageProcessing::UpdateOutputMatHSV(const Mat &srcF, const Mat &maskH, const Mat &maskS, const Mat &maskV, Mat &mask, cv::Mat &dst)
{
#ifdef HSV_INDIVIDUAL
    g_matHSVOut.release();
    inRange(g_matHSVInF, Scalar(g_hsvThresholdValueH,
                               g_hsvThresholdValueS,
                               g_hsvThresholdValueV),
                        Scalar(k_hsv_h_max,
                               k_hsv_s_max,
                               k_hsv_v_max), g_matHSVOut);

    //Open - denoise
    //Mat element = getStructuringElement(MORPH_RECT, Size(5, 5));
    //morphologyEx(g_matHSVOut, g_matHSVOut, MORPH_OPEN, element);

    //Close - connect
    //morphologyEx(g_matHSVOut, g_matHSVOut, MORPH_CLOSE, element);
#else
	Mat matTmpDst = Mat::zeros(srcF.size(), CV_32FC3);
    for (int rowIdx = 0; rowIdx < srcF.rows; ++rowIdx)
    {
        for (int colIdx = 0; colIdx < srcF.cols; ++colIdx)
        {
			if ((255 == maskH.at<uchar>(rowIdx, colIdx)) &&
				(255 == maskS.at<uchar>(rowIdx, colIdx)) &&
				(255 == maskV.at<uchar>(rowIdx, colIdx)))
			{
				mask.at<uchar>(rowIdx, colIdx) = 255;
				matTmpDst.at<Vec3f>(rowIdx, colIdx) = srcF.at<Vec3f>(rowIdx, colIdx);
			}
			else
			{
                //2018.1004
                mask.at<uchar>(rowIdx, colIdx) = 0;// 255;
				matTmpDst.at<Vec3f>(rowIdx, colIdx) = 0;
			}
        }
    }
#endif

	matTmpDst.convertTo(dst, CV_8UC3, 255, 0);
}

void ImageProcessing::UpdateOutputMatSobel(const Mat &src, const Mat &mask, Mat &dst)//Mat &maskX, Mat &maskY, Mat &dst)
{
    //int numsOfX = 0;
    //int numsOfY = 0;
    //int numsOfAll = 0;
    //int numsBlack = 0;
    //int numsOfAll2 = 0;
    //qDebug("UpdateOutputMatSobel orgImage=%d %d %d", orgImage.type(), orgImage.cols, orgImage.rows);//16//21
    //qDebug("UpdateOutputMatSobel maskX=%d %d %d", maskX.type(), maskX.cols, maskX.rows);//0
    //qDebug("UpdateOutputMatSobel maskY=%d %d %d", maskY.type(), maskY.cols, maskY.rows);//0
    //qDebug("UpdateOutputMatSobel dst=%d %d %d", dst.type(), dst.cols, dst.rows);//16
	//Mat matTmpDst = Mat::zeros(src.size(), CV_8UC3);
    for (int rowIdx = 0; rowIdx < src.rows; ++rowIdx)
    {
        for (int colIdx = 0; colIdx < src.cols; ++colIdx)
        {
#if 1
            //if (255 == maskX.at<uchar>(rowIdx, colIdx))
            //    ++numsOfX;
            //if (255 == maskY.at<uchar>(rowIdx, colIdx))
            //    ++numsOfY;
            //if (255 == m_matMaskSobel.at<uchar>(rowIdx, colIdx))
            //    ++numsOfAll2;

            //if ((255 == maskX.at<uchar>(rowIdx, colIdx)) ||
            //    (255 == maskY.at<uchar>(rowIdx, colIdx)))
            if (255 == mask.at<uchar>(rowIdx, colIdx))
#else
            if (g_sobelThresholdXMax == maskX.at<uchar>(rowIdx, colIdx))
                ++numsOfX;
            if (g_sobelThresholdYMax == maskY.at<uchar>(rowIdx, colIdx))
                ++numsOfY;
            if ((g_sobelThresholdXMax == maskX.at<uchar>(rowIdx, colIdx)) ||
                (g_sobelThresholdYMax == maskY.at<uchar>(rowIdx, colIdx))) // the pixel pass the mask detection
#endif
            {
                //++numsOfAll;
            //#ifdef USE_CV_32FC3
            //    dst.at<Vec3f>(rowIdx, colIdx) = orgImage.at<Vec3f>(rowIdx, colIdx);
            //#else
                dst.at<Vec3b>(rowIdx, colIdx) = src.at<Vec3b>(rowIdx, colIdx);
            //#endif
            }
            //else
            //{
            //    //++numsBlack;
            ////#ifdef USE_CV_32FC3
            ////    dst.at<Vec3f>(rowIdx, colIdx) = 0;
            ////#else
                dst.at<Vec3b>(rowIdx, colIdx) = 0;
            ////#endif
            //}
        }
    }
    //qDebug("numsOfX=%d numsOfY=%d numsOfAll=%d numsOfAll2=%d numsBlack=%d",
    //numsOfX,numsOfY,numsOfAll,numsOfAll2,numsBlack);
}

void ImageProcessing::FindLanes(const cv::Mat &in, std::vector<cv::Vec4i> *lpDetectedLaneLineArray)
{
    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();
	std::vector<cv::Vec4i> *pLineArray = lpDetectedLaneLineArray;
	//qDebug("lpDetectedLaneLineArray=%p", lpDetectedLaneLineArray);

	if (e_operation_static == pCache->operation_mode)
	{
		HoughLinesP(in, *pLineArray, 1 + g_houghRho, CV_PI / double(g_houghTheta), g_houghLineThreshold, g_houghMinLineLength, g_houghMaxLineGap);
	}
	else if (e_operation_live == pCache->operation_mode)
	{
		bool div2Needed = false;
		if (e_lanedetection_cycletime_always == pCache->lanedetecion_cycletime)
		{
			Mat out;
			CVImageResize2ByWH(in, out, in.cols / 2, in.rows / 2);
			HoughLinesP(out, *pLineArray, 1 + g_houghRho, CV_PI / double(g_houghTheta), g_houghLineThreshold, g_houghMinLineLength, g_houghMaxLineGap);
			div2Needed = true;
		}
		else if (e_lanedetection_cycletime_10s == pCache->lanedetecion_cycletime)
		{
			Mat out;
			CVImageResize2ByWH(in, out, in.cols / 2, in.rows / 2);
			HoughLinesP(out, *pLineArray, 1 + g_houghRho, CV_PI / double(g_houghTheta), g_houghLineThreshold, g_houghMinLineLength, g_houghMaxLineGap);
			div2Needed = true;
		}	

		if (div2Needed)
		{
			std::vector<cv::Vec4i>::iterator it = (*pLineArray).begin();
			while (it != (*pLineArray).end()) {
				//qDebug("GHH1 [%d] %d %d %d %d", countIdx, (*it)[0], (*it)[1], (*it)[2], (*it)[3]);
				(*it) *= 2;
				//qDebug("GHH2 [%d] %d %d %d %d", countIdx, (*it)[0], (*it)[1], (*it)[2], (*it)[3]);
				++it;
			}
		}
	}

	qDebug("FindLanes 1 size=%d", (*pLineArray).size());
	CVRemoveLinesOfInconsistentOrientationsAdvanced(*pLineArray, m_matDetectedLane, 0.1, k_angel15);
	qDebug("FindLanes 2 size=%d", (*pLineArray).size());

    QuickSortVec4i(*pLineArray);

    std::vector<cv::Vec4i>::iterator it2 = (*pLineArray).begin();
    int countIdx = 0;
    int x1, y1, x2, y2;
    int oldX = INT_MAX, newX;
    int totalX1 = 0, totalY1 = 0, totalX2 = 0, totalY2 = 0;
    int avgX1 = 0, avgY1 = 0, avgX2 = 0, avgY2 = 0;
    int numsOfLines;
    Vec4i avg;
    vector<Vec4i> realLines;
    vector<Vec4i> currLines;
    while (it2 != (*pLineArray).end()) {
        // end points
        x1 = (*it2)[0];
        y1 = (*it2)[1];
        x2 = (*it2)[2];
        y2 = (*it2)[3];
       // qDebug("GHH2 [%d] %d %d %d %d", countIdx, x1, y1, x2, y2);
        if (y1 > y2)
            newX = x1;
        else
            newX = x2;
        countIdx++;
		//qDebug("(newX - oldX)=%d", (newX - oldX));
        if ((newX - oldX) > k_cluster_distance)
        {
            totalX1 = totalY1 = totalX2 = totalY2 = 0;
            numsOfLines = currLines.size();
            //qDebug("currLines.size()1=%d", currLines.size());
            for (int i = 0; i < numsOfLines; ++i)
            {
                totalX1 += currLines[i][0];
                totalY1 += currLines[i][1];
                totalX2 += currLines[i][2];
                totalY2 += currLines[i][3];
            }

            avg[0] = totalX1 / numsOfLines;
            avg[1] = totalY1 / numsOfLines;
            avg[2] = totalX2 / numsOfLines;
            avg[3] = totalY2 / numsOfLines;
//qDebug("111 avg %d %d %d %d", avg[0], avg[1], avg[2], avg[3]);
            currLines.clear();

			if (it2 == ((*pLineArray).end() - 1)) // coincidence
			{
				realLines.push_back(*it2);
			}
			else
				currLines.push_back(*it2);
            realLines.push_back(avg);
        }
        else if (it2 == ((*pLineArray).end() - 1))
        {
            totalX1 = totalY1 =totalX2 = totalY2 = 0;

            currLines.push_back(*it2);

            numsOfLines = currLines.size();
            //qDebug("currLines.size()2=%d", currLines.size());
            for (int i = 0; i < numsOfLines; ++i)
            {
                totalX1 += currLines[i][0];
                totalY1 += currLines[i][1];
                totalX2 += currLines[i][2];
                totalY2 += currLines[i][3];
            }

            avg[0] = totalX1 / numsOfLines;
            avg[1] = totalY1 / numsOfLines;
            avg[2] = totalX2 / numsOfLines;
            avg[3] = totalY2 / numsOfLines;
//qDebug("222 %d %d %d", avg[0], avg[1], avg[2]);
            realLines.push_back(avg);
            break;
        }
        else
        {
            currLines.push_back(*it2);
            //qDebug("333 [%d] %d %d %d %d", countIdx, x1, y1, x2, y2);
        }
        oldX = newX;

        ++it2;
    }

    //qDebug("realLines.size()=%d", realLines.size());
    if (realLines.size() > 0)
    {
        pLineArray->clear();
        for (int i = 0; i < realLines.size(); ++i)
        {
            pLineArray->push_back(realLines[i]);
        }
    }
    //qDebug("FindLanes 32 size=%d", (*pLineArray).size());
}

bool ImageProcessing::FindDiffBetweenMatrix(Mat &matBg, Mat &matScaled, SLanelinesInfo *pLaneLinesInfo, float delta)
{
    //qDebug("pLaneLinesInfo %p", pLaneLinesInfo);
	bool ret = false;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList;
    ppWinInfo2DimList = &(pLaneLinesInfo->sliding_win_info_list);
    //qDebug("ppWinInfo2DimList %p", ppWinInfo2DimList);
//    imshow("matBg", matBg);
//    imshow("matScaled", matScaled);
    Mat matDiff;
//qDebug("diff matBg %d %d %d", matBg.type(), matBg.cols, matBg.rows);
//qDebug("diff matScaled %d %d %d", matScaled.type(), matScaled.cols, matScaled.rows);
#ifdef USE_HSV_TO_COMPARE
    Mat matBgHSV;
    Mat matScaledHSV;
    cv::cvtColor(matBg, matBgHSV, CV_BGR2HSV);

    cv::cvtColor(matScaled, matScaledHSV, CV_BGR2HSV);
    cv::absdiff(matBgHSV, matScaledHSV, matDiff);
#else
    cv::absdiff(matBg, matScaled, matDiff);
#endif
	//qDebug("diff matDiff %d %d %d", matScaled.type(), matScaled.cols, matScaled.rows);
    //imshow("matDiff", matDiff);
    float threshold = 66.0;  // 0
    float dist;
    //Mat mask(matBg.size(), CV_8UC1);

    int total = 0;
    int total2 = 0;
    int maskcount = 0;
    cv::Mat mask = cv::Mat::zeros(matDiff.rows, matDiff.cols, CV_8UC1);
#if 1 // more precise
    {
        int previousX, previousY;
        int minX, maxX;

        vector<vector<SSlidingWindowInfo>>::iterator itWindow = ppWinInfo2DimList->begin();
        std::vector<SSlidingWindowInfo> *pCurrentWindowInfoList;
        SSlidingWindowInfo *pInfo;
        //qDebug("ppWinInfo2DimList size=%d", ppWinInfo2DimList->size());
        while (itWindow != ppWinInfo2DimList->end())
        //for (int i = 0; i < ppWinInfo2DimList.size(); ++i)
        {
            //vector<SSlidingWindowInfo> *pCurrentWindowInfoList = &(*itWindow);
            //vector<SSlidingWindowInfo>::iterator itChild = pCurrentWindowInfoList->begin();
            pCurrentWindowInfoList = &(*itWindow);
            //qDebug("vvv points=%d", pCurrentWindowInfoList->size());
            for (int j = 0; j < pCurrentWindowInfoList->size(); ++j)
            {
                total++;
                pInfo = &(pCurrentWindowInfoList->at(j));
                for (int x = pInfo->min_x; x < pInfo->max_x; ++x)
                {
                    total2++;
                    cv::Vec3b pix = matDiff.at<cv::Vec3b>(pInfo->y, x);
                    dist = (pix[0]*pix[0] + pix[1]*pix[1] + pix[2]*pix[2]);
                    dist = sqrt(dist);

                    if(dist > threshold){
                        maskcount++;
                        //qDebug("lineIdx=%d ptIdx=%d x=%d y=%d", i, j, x, pInfo->y);
                        mask.at<unsigned char>(pInfo->y, x) = 255;
                    }
                    //qDebug("[%d] %u %u %u %u", j, pInfo->min_x, pInfo->max_x, pInfo->centroid, pInfo->y);
                }
            }
            ++itWindow;
        }
    }
	//qDebug("m_processingSetting.getVD1PassDiffThreshold()=%d", m_processingSetting.getVD1PassDiffThreshold());
	if ((maskcount / float(total2)) > ((float)pCache->vd_1pass_diffthreshold_idx))
		ret = true;
    //qDebug("total=%d total2=%d total=%d", total, total2, maskcount);
#else
//    for(int j = 0; j < matDiff.rows; ++j) {
//        for(int i = 0; i < matDiff.cols; ++i){
//            cv::Vec3b pix = matDiff.at<cv::Vec3b>(j, i);
//            //int val = (pix[0] + pix[1] + pix[2]);
//            dist = (pix[0]*pix[0] + pix[1]*pix[1] + pix[2]*pix[2]);
//            dist = sqrt(dist);
//            if(dist > threshold){
//                //mask.at<unsigned char>(j, i) = 255;
//            }
//        }
//    }
#endif
    Mat res;
    bitwise_and(matScaled, matScaled, res, mask);

    // display
    //imshow("maskwhite", mask);
	emit diffImageReady(res);
    //imshow("res", res);
    //cv::threshold(motion, motion, 80, 255, cv::THRESH_BINARY);
    //cv::erode(motion, motion, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3,3)));
	return ret;
}

void ImageProcessing::InitRelatedLandLineInfos(Mat &src, SLanelinesInfo *pLaneLinesInfo, bool bBackupSWInfoFile, QString swInfoFileName)
{
    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();

	//std::vector<bool> *pLaneInROI = &(pLaneLinesInfo->in_roi);
	std::vector<cv::Vec4i> *pLaneArray = &(pLaneLinesInfo->array);
	std::vector<cv::Point2f> *pLaneWinTl = &(pLaneLinesInfo->sliding_win_tl);
	std::vector<cv::Point2f> *pLaneWinBr = &(pLaneLinesInfo->sliding_win_br);
	//std::vector<cv::Rect> *pLaneWinRect;
	std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList =
		&(pLaneLinesInfo->sliding_win_info_list);

    //int idx = 0;
	int roiLeft, roiRight, roiTop, roiBottom;
	//vector<Point2f> roiContours;
	if (pCache->roi_enabled)
	{
		int roiLeft = roiRight = roiTop = roiBottom = 0;
		QVector<DrawableGraphicsItem<QGraphicsLineItem, QPushButton>*> *pROILines;
		pROILines = GetScene()->getROILines();
		for (size_t i = 0; i < pROILines->size(); ++i)
		{
			DrawableGraphicsItem<QGraphicsLineItem, QPushButton> *pROILine = pROILines->at(i);
			QGraphicsLineItem *pLine = static_cast<QGraphicsLineItem*>(pROILine->getItem());
			if (0 == i) roiTop = pLine->pos().y();
			else if (1 == i) roiBottom = pLine->pos().y();
			else if (2 == i) roiLeft = pLine->pos().x();
			else if (3 == i) roiRight = pLine->pos().x();
		}
	//	roiContours.push_back(Point2f(roiLeft, roiTop));
	//	roiContours.push_back(Point2f(roiRight, roiTop));
	//	roiContours.push_back(Point2f(roiRight, roiBottom));
	//	roiContours.push_back(Point2f(roiLeft, roiBottom));
	//	//roiLeft -= 10; roiRight += 10; roiTop -= 10; roiBottom += 10;
	qDebug("roi %d %d %d %d", roiLeft, roiRight, roiTop, roiBottom);
	}

    //if (bBackupSWInfoFile)
    {
        int previousX, previousY;
        int minX, maxX;
        vector<Vec4i>::iterator it = pLaneArray->begin();
		
    //    while (it != pLaneArray->end())
    //    {
    //        qDebug("line[%d] %d %d %d %d", idx, (*it)[0], (*it)[1], (*it)[2], (*it)[3]);
    //        ++it;
    //        idx++;
    //    }
	//	it = pLaneArray->begin();
		if (bBackupSWInfoFile)
			OpenAndWriteSlidingWindowsFile(swInfoFileName);

        if (bBackupSWInfoFile)
            SaveSlidingWindowsNumsOfLines(pLaneArray->size());

		int p1x, p1y, p2x, p2y;
		int lineleft, lineright;
		int rcMinX, rcMaxX, rcMinY, rcMaxY;
        while (it != pLaneArray->end())
        {
			// get the info about the line is valid or not. //////////////
			Point ptLeft;
			Point ptRight;
			Vec4i line = (*it);//[0];

			if (line[0] > line[2])
			{
				ptLeft.x = line[2];
				ptLeft.y = line[3];
				ptRight.x = line[0];
				ptRight.y = line[1];
			}
			else
			{
				ptLeft.x = line[0];
				ptLeft.y = line[1];
				ptRight.x = line[2];
				ptRight.y = line[3];
			}

//			if (pCache->roi_enabled)
//			{
//				lineleft = line[0];
//				lineright = line[2];
//				if (lineleft > lineright)
//				    std::swap(lineleft, lineright);
//				//qDebug("roi checking %d %d %d %d", line[0], line[1], line[2], line[3]);
//				//qDebug("roi checking roiLeft %d %d", roiLeft, roiRight);
//				if ((ptLeft.x <= roiLeft) || (ptRight.x >= roiRight))
//				{
//					pLaneInROI->push_back(false);
//				}
//				else
//				{
//#ifdef DRAW_LANELINE_ON_MAT_TO_TEST
//					cv::line(src, ptLeft, ptRight, Scalar(k_laneline_clr_b, k_laneline_clr_g, k_laneline_clr_r), k_laneline_width, 8);
//#endif		
//					pLaneInROI->push_back(true);
//				}
//			}
//			else
//			{
//				pLaneInROI->push_back(false);
//			}


			//get rect of line //////////////
			{
			cv::Rect rect;
			p1x = (*it)[0];
			p1y = (*it)[1];
			p2x = (*it)[2];
			p2y = (*it)[3];
			//qDebug("%d %d %d %d %d %d %d", p1x, p1y, p2x, p2y, k_sliding_windows_radius, src.cols, src.rows);
				
			CVFindRect(p1x, p1y, p2x, p2y, k_sliding_windows_radius, src.cols, src.rows, rect);
			//qDebug("%d %d %d %d", rect.x, rect.y, rect.width, rect.height);
			pLaneWinTl->push_back(Point2f(rect.br()));
			pLaneWinBr->push_back(Point2f(rect.tl()));
			//pLaneWinRect->push_back(rect);
			}

			//get all point of line //////////////
            LineIterator itLine(src, Point(p1x, p1y), Point(p2x, p2y), 8);
			// do ~ while 
            //qDebug("itLine count=%d", itLine.count);
            previousX = itLine.pos().x;
            previousY = itLine.pos().y;
            minX = itLine.pos().x - k_sliding_windows_radius;
            maxX = itLine.pos().x + k_sliding_windows_radius;
            //qDebug("itLine pt[%d]=(%d,%d)", 0, itLine.pos().x, itLine.pos().y);
            ++itLine;
            vector<SSlidingWindowInfo> currentWindowInfoList;
            if (bBackupSWInfoFile)
                SaveSlidingWindowsNumsOfLines(itLine.count);

            for (int i = 1; i < itLine.count; ++i, ++itLine)
            {
                //qDebug("itLine pt[%d]=(%d,%d)", i, itLine.pos().x, itLine.pos().y);
                if (previousY == itLine.pos().y) // point are moving on the same horizontal axis.
                {
                    //qDebug("itLine same pt[%d]=(%d,%d)", i, itLine.pos().x, itLine.pos().y);
                    if (minX > itLine.pos().x - k_sliding_windows_radius)
                        minX = itLine.pos().x - k_sliding_windows_radius;
                    if (maxX < itLine.pos().x + k_sliding_windows_radius)
                        maxX = itLine.pos().x + k_sliding_windows_radius;

                    if ((i + 1) == itLine.count)
                    {
					#ifdef UPDATE_SLIDINGWINDOW_CONTENT
                        AddSlidingWindowsContent(
                            src, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY, bBackupSWInfoFile);
					#endif				
    //                    info.min_x = minX;
    //                    info.max_x = maxX;
    //                    info.centroid = itLine.pos().x;
    //                    info.y = previousY;
    //                    if (k_sliding_windows_be_erased)
    //                    {
    //                        for (int j = minX; j < maxX; ++j)
    //                        {
    //                            src.at<Vec3b>(previousY, j)[0] = 255;
    //                            src.at<Vec3b>(previousY, j)[1] = 255;
    //                            src.at<Vec3b>(previousY, j)[2] = 255;
    //                        }
    //                    }
                        //currentWindowInfoList.push_back(info);
                    }
                }
                else
                {
    //                info.min_x = minX;
    //                info.max_x = maxX;
    //                info.centroid = itLine.pos().x;
    //                info.y = previousY;
    //                if (k_sliding_windows_be_erased)
    //                {
    //                    for (int j = minX; j < maxX; ++j)
    //                    {
    //                        src.at<Vec3b>(previousY, j)[0] = 255;
    //                        src.at<Vec3b>(previousY, j)[1] = 255;
    //                        src.at<Vec3b>(previousY, j)[2] = 255;
    //                    }
    //                }
				#ifdef UPDATE_SLIDINGWINDOW_CONTENT
                    AddSlidingWindowsContent(
                                src, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY, bBackupSWInfoFile);
				#endif			
                    //currentWindowInfoList.push_back(info);

                    minX = itLine.pos().x - k_sliding_windows_radius;
                    maxX = itLine.pos().x + k_sliding_windows_radius;

                    //preivousX = itLine.pos().x;
                    previousY = itLine.pos().y;

                    if ((i + 1) == itLine.count)
                    {
    //                    info.min_x = minX;
    //                    info.max_x = maxX;
    //                    info.centroid = itLine.pos().x;
    //                    info.y = previousY;
    //                    if (k_sliding_windows_be_erased)
    //                    {
    //                        for (int j = minX; j < maxX; ++j)
    //                        {
    //                            src.at<Vec3b>(previousY, j)[0] = 255;
    //                            src.at<Vec3b>(previousY, j)[1] = 255;
    //                            src.at<Vec3b>(previousY, j)[2] = 255;
    //                        }
    //                    }
					#ifdef UPDATE_SLIDINGWINDOW_CONTENT
                        AddSlidingWindowsContent(
                            src, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY, bBackupSWInfoFile);
					#endif		
                        //currentWindowInfoList.push_back(info);
                    }
                }
            }

            ppWinInfo2DimList->push_back(currentWindowInfoList);
            ++it;
        }

		if (bBackupSWInfoFile)
			CloseAndWriteSlidingWindowsFile();
    }

#ifdef SAVE_MODIFIED_PIC
    imwrite("../withWhite.jpg", src);
#endif

    int idx = 0;
#if 0
    vector<vector<SSlidingWindowInfo>>::iterator itWindow = ppWinInfo2DimList->begin();
    //qDebug("ppWinInfo2DimList size=%d", ppWinInfo2DimList->size());
    while (itWindow != ppWinInfo2DimList->end())
    {
        vector<SSlidingWindowInfo> *pCurrentWindowInfoList = &(*itWindow);
        vector<SSlidingWindowInfo>::iterator itChild = pCurrentWindowInfoList->begin();
        //qDebug("(*itWindow)[%d] size=%d", idx, pCurrentWindowInfoList->size());
        ++idx;
        int idxChild = 0;
        while (itChild != pCurrentWindowInfoList->end())
        {
            SSlidingWindowInfo *pInfo = &(*itChild);
            if (pInfo)
            {
                for (int i = pInfo->min_x; i < pInfo->max_x + 1; ++i)
                {
                    src.at<Vec3b>(pInfo->y, i)[0] = 255;
                    src.at<Vec3b>(pInfo->y, i)[1] = 0;
                    src.at<Vec3b>(pInfo->y, i)[2] = 0;
                }

                //qDebug("(*itChild)[%d] minX=%d maxX=%d centroid=%d y=%d",
                //       idxChild, pInfo->min_x, pInfo->max_x, pInfo->centroid, pInfo->y);
            }
            ++idxChild;
            ++itChild;
        }

        ++itWindow;
    }
#endif
}

void ImageProcessing::CollectSlidingWindowInStaticMode()
{
	qDebug("CollectSlidingWindowInStaticMode");
	SLanelinesInfo* pLaneLinesInfo = GetDetectedLaneLinesInfo();
	QString str = QApplication::applicationDirPath() + "/TrafficEnforcement.ini";
	QSettings settings(str, QSettings::IniFormat);
	settings.setIniCodec("UTF-8");

	bool bBackupSWInfoFile = settings.value(QString(k_str_em_sm_always_backup_info_of_slidingwindows)).toBool();

	settings.beginGroup(QString(k_str_engineeringmode));

	bool useBaseImageFile = settings.value(QString(k_str_emdialog_sm_baseimagefile)).toBool();
	bool useExistedSWInfoFile = settings.value(QString(k_str_emdialog_sm_slidingwindowsfile)).toBool();
	QString swBaseImageName = settings.value(QString(k_str_emdialog_sm_baseimagefilepath)).toString();
	QString swInfoFileName = settings.value(QString(k_str_emdialog_sm_slidingwindowsfilepath)).toString();
	if (!swBaseImageName.isEmpty())
		//qDebug("ThresholdCallbackCanny swBaseImageName=%s", swBaseImageName.toLatin1().constData());
		if (!swInfoFileName.isEmpty())
			//qDebug("ThresholdCallbackCanny swInfoFileName=%s", swInfoFileName.toLatin1().constData());
			//qDebug("useExistedSWInfoFile=%d", useExistedSWInfoFile);
			settings.endGroup();

	if (true == useExistedSWInfoFile)
	{
		OpenAndReadSlidingWindowsFile(swInfoFileName);

		LoadSlidingWindows(pLaneLinesInfo->sliding_win_info_list);

		Mat matBg;
#ifdef USE_SCALED_TO_COMPARE
		Mat matOrg = CVImageOpen(swBaseImageName.toLatin1().constData());
		Mat matBgScaled;
		if ((matOrg.cols * matOrg.rows) > (4096 * 2160))
			CVImageResize(matOrg, matBgScaled, 0.125, 0.125);
		else if ((matOrg.cols * matOrg.rows) > (2560 * 1440))
			CVImageResize(matOrg, matBgScaled, 0.25, 0.25);
		else if ((matOrg.cols * matOrg.rows) > (1920 * 1080))
			CVImageResize(matOrg, matBgScaled, 0.5, 0.5);
		else
			CVImageResize(matOrg, matBgScaled, 1, 1);

		matBg = matBgScaled;
#else
		matBg = CVImageOpen(swBaseImageName.toLatin1().constData());
#endif

		CloseAndReadSlidingWindowsFile();
	}
}

void ImageProcessing::LaneDetouching(ImageProcessing *pImageProcessing, SLanelinesInfo *pLaneLinesInfo)//, Mat &src)
{
#ifdef UNLOCK_TEMPORARILY_0714_1
	if (e_operation_static == pCache->operation_mode)
	{
		pImageProcessing->CollectSlidingWindowInStaticMode();
	}
	else if (e_operation_live == pCache->operation_mode)
#endif
		pImageProcessing->InitRelatedLandLineInfos(m_matDetectedLane, pLaneLinesInfo, false, QString());

	//{ //test region
	//  //qDebug("pLaneWinRect count=%d", pLaneWinRect->size());
	//	std::vector<cv::Point2f>::const_iterator itTl = pLaneWinTl->begin();
	//	std::vector<cv::Point2f>::const_iterator itBr = pLaneWinBr->begin();
	//	while (itTl != pLaneWinTl->end() && itBr != pLaneWinBr->end()) {
	//		//qDebug("(*itRect).a() %d %d", (*itRect).x, (*itRect).y);
	//		//qDebug("(*itRect).b() %d %d", (*itRect).width, (*itRect).height);
	//		//qDebug("(*itRect).br() %d %d", (*itRect).br().x, (*itRect).br().y);
	//		//qDebug("(*itRect).tl() %d %d", (*itRect).tl().x, (*itRect).tl().y);
	//		cv::rectangle(matBackground, (*itTl), (*itBr), Scalar(0, 128, 128), 3, 8, 0);
	//		++itTl;
	//		++itBr;
	//	}
	//}
}

void ImageProcessing::PredictLanes()//Mat &src)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

	const Mat *pSrc = GetScaledImage();
	const Mat *pSrcP = GetScaledImageP();
    Mat *pDst = &m_matDetectedLane;

	SLanelinesInfo* pLaneLinesInfo = GetDetectedLaneLinesInfo();
	//std::vector<bool> *pLaneInROI = &(pLaneLinesInfo->in_roi);
	std::vector<cv::Vec4i> *pLaneArray = &(pLaneLinesInfo->array);
	std::vector<cv::Point2f> *pLaneWinTl = &(pLaneLinesInfo->sliding_win_tl);
	std::vector<cv::Point2f> *pLaneWinBr = &(pLaneLinesInfo->sliding_win_br);
	//std::vector<cv::Rect> *pLaneWinRect;
	std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList =
		&(pLaneLinesInfo->sliding_win_info_list);

	Mat tmp;
	Mat M = estimateRigidTransform(*pSrcP, *pSrc, 0);
	//PrintMatInfo(M);
	//std::cout << M << std::endl;
	warpAffine(*pDst, tmp, M, tmp.size(), INTER_NEAREST | WARP_INVERSE_MAP);

    ResetDetectedLaneImage(this, tmp);
	//for (int rowIdx = 0; rowIdx < M.rows; ++rowIdx)
	//{
	//	for (int colIdx = 0; colIdx < M.cols; ++colIdx)
	//	{
	//		qDebug("[%d,%d] value=%f", rowIdx, colIdx, M.at<double>(rowIdx, colIdx));
	//	}
	//}
	//qDebug("CC %f", M.at<double>(0, 2));
	//qDebug("CC1 %f", M.at<double>(1, 2));
	//qDebug("pLaneLinesInfo->offset_x 1=%f", pLaneLinesInfo->offset_x);
	//qDebug("pLaneLinesInfo->offset_y 1=%f", pLaneLinesInfo->offset_y);
	int offsetX = 0;
	int offsetY = 0;
	pLaneLinesInfo->offset_x += M.at<double>(0, 2);
	pLaneLinesInfo->offset_y += M.at<double>(1, 2);
	//qDebug("pLaneLinesInfo->offset_x 2=%f", pLaneLinesInfo->offset_x);
	//qDebug("pLaneLinesInfo->offset_y 2=%f", pLaneLinesInfo->offset_y);
	if (pLaneLinesInfo->offset_x < -1.0)
	{
		offsetX = int(pLaneLinesInfo->offset_x);
		pLaneLinesInfo->offset_x -= (double)offsetX;
	}
	if (pLaneLinesInfo->offset_x > 1.0)
	{
		offsetX = int(pLaneLinesInfo->offset_x);
		pLaneLinesInfo->offset_x -= (double)offsetX;
	}
	if (pLaneLinesInfo->offset_y < -1.0)
	{
		offsetY = int(pLaneLinesInfo->offset_y);
		pLaneLinesInfo->offset_y -= (double)offsetY;
	}
	if (pLaneLinesInfo->offset_y > 1.0)
	{
		offsetY = int(pLaneLinesInfo->offset_y);
		pLaneLinesInfo->offset_y -= (double)offsetY;
	}

	if ((0 == offsetX) && (0 == offsetY))
		return;

	ReleaseLanelinesInfo(false);

	int previousX, previousY;
	int minX, maxX;
	vector<Vec4i>::iterator it = pLaneArray->begin();

	int p1x, p1y, p2x, p2y;
	int lineleft, lineright;
	int rcMinX, rcMaxX, rcMinY, rcMaxY;

	int idx = 0;
	int roiLeft, roiRight, roiTop, roiBottom;
	//vector<Point2f> roiContours;
	if (pCache->roi_enabled)
	{
		int roiLeft = roiRight = roiTop = roiBottom = 0;
		QVector<DrawableGraphicsItem<QGraphicsLineItem, QPushButton>*> *pROILines;
		pROILines = GetScene()->getROILines();
		for (size_t i = 0; i < pROILines->size(); ++i)
		{
			DrawableGraphicsItem<QGraphicsLineItem, QPushButton> *pROILine = pROILines->at(i);
			QGraphicsLineItem *pLine = static_cast<QGraphicsLineItem*>(pROILine->getItem());
			if (0 == i) roiTop = pLine->pos().y();
			else if (1 == i) roiBottom = pLine->pos().y();
			else if (2 == i) roiLeft = pLine->pos().x();
			else if (3 == i) roiRight = pLine->pos().x();
		}
		//	roiContours.push_back(Point2f(roiLeft, roiTop));
		//	roiContours.push_back(Point2f(roiRight, roiTop));
		//	roiContours.push_back(Point2f(roiRight, roiBottom));
		//	roiContours.push_back(Point2f(roiLeft, roiBottom));
		//	//roiLeft -= 10; roiRight += 10; roiTop -= 10; roiBottom += 10;
		//	//qDebug("roi %d %d %d %d", roiLeft, roiRight, roiTop, roiBottom);
	}

	while (it != pLaneArray->end())
	{
		// get the info about the line is valid or not. //////////////
		Point ptLeft;
		Point ptRight;
		Vec4i *pLine = &(*it);
		Vec4i line2 = (*it);
		//qDebug("predict %d %d %d %d %d %d %d %d", pLine[0][0], pLine[0][1], pLine[0][2], pLine[0][3], line2[0], line2[1], line2[2], line2[3]);
		pLine[0] = { pLine[0][0] + offsetX,
					 pLine[0][1] + offsetY,
					 pLine[0][2] + offsetX,
					 pLine[0][3] + offsetY };

		Vec4i line = (*it);
		//qDebug("predict %d %d %d %d", line[0], line[1], line[2], line[3]);
		if (line[0] > line[2])
		{
			ptLeft.x = line[2];
			ptLeft.y = line[3];
			ptRight.x = line[0];
			ptRight.y = line[1];
		}
		else
		{
			ptLeft.x = line[0];
			ptLeft.y = line[1];
			ptRight.x = line[2];
			ptRight.y = line[3];
		}

		//if (pCache->roi_enabled)
		//{
		//	//lineleft = line[0];
		//	//lineright = line[2];
		//	//if (lineleft > lineright)
		//	//	std::swap(lineleft, lineright);
		//	//qDebug("aa %d %d %d %d", line[0], line[1], line[2], line[3]);
		//	// qDebug("aa roiLeft %d %d", roiLeft, roiRight);
		//	if ((ptLeft.x <= roiLeft) || (ptRight.x >= roiRight))
		//	{
		//		pLaneInROI[0][idx] = false;
		//		pLaneInROI->push_back(false);
		//	}
		//	else
		//	{
		//		//pLaneInROI[0][idx] = true;
		//		pLaneInROI->push_back(true);
		//	}
		//}

		//get rect of line //////////////
		{
			cv::Rect rect;
			p1x = (*it)[0];
			p1y = (*it)[1];
			p2x = (*it)[2];
			p2y = (*it)[3];
			//qDebug("%d %d %d %d %d %d %d", p1x, p1y, p2x, p2y, k_sliding_windows_radius, src.cols, src.rows);

			CVFindRect(p1x, p1y, p2x, p2y, k_sliding_windows_radius, pSrc->cols, pSrc->rows, rect);
			//qDebug("%d %d %d %d", rect.x, rect.y, rect.width, rect.height);
			pLaneWinTl->push_back(Point2f(rect.br()));
			pLaneWinBr->push_back(Point2f(rect.tl()));
		}

		//get all point of line //////////////
		LineIterator itLine(*pDst, Point(p1x, p1y), Point(p2x, p2y), 8);
		// do ~ while 
		//qDebug("itLine count=%d", itLine.count);
		previousX = itLine.pos().x;
		previousY = itLine.pos().y;
		minX = itLine.pos().x - k_sliding_windows_radius;
		maxX = itLine.pos().x + k_sliding_windows_radius;
		//qDebug("itLine pt[%d]=(%d,%d)", 0, itLine.pos().x, itLine.pos().y);
		++itLine;
		vector<SSlidingWindowInfo> currentWindowInfoList;
		for (int i = 1; i < itLine.count; ++i, ++itLine)
		{
			//qDebug("itLine pt[%d]=(%d,%d)", i, itLine.pos().x, itLine.pos().y);
			if (previousY == itLine.pos().y) // point are moving on the same horizontal axis.
			{
				//qDebug("itLine same pt[%d]=(%d,%d)", i, itLine.pos().x, itLine.pos().y);
				if (minX > itLine.pos().x - k_sliding_windows_radius)
					minX = itLine.pos().x - k_sliding_windows_radius;
				if (maxX < itLine.pos().x + k_sliding_windows_radius)
					maxX = itLine.pos().x + k_sliding_windows_radius;

				if ((i + 1) == itLine.count)
				{
#ifdef UPDATE_SLIDINGWINDOW_CONTENT
					AddSlidingWindowsContent(
                        *pDst, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY, false);
#endif				
				}
			}
			else
			{
#ifdef UPDATE_SLIDINGWINDOW_CONTENT
				AddSlidingWindowsContent(
                    *pDst, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY, false);
#endif			
				minX = itLine.pos().x - k_sliding_windows_radius;
				maxX = itLine.pos().x + k_sliding_windows_radius;

				//preivousX = itLine.pos().x;
				previousY = itLine.pos().y;

				if ((i + 1) == itLine.count)
				{
#ifdef UPDATE_SLIDINGWINDOW_CONTENT
					AddSlidingWindowsContent(
                        *pDst, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY, false);
#endif		
				}
			}
		}

		ppWinInfo2DimList->push_back(currentWindowInfoList);
		++it;
	}
}

void ImageProcessing::UpdateOutputMatMix_HLSAndHSV(const Mat &orgImage, const Mat &maskHLS, const Mat &maskHSV, Mat &mixOut)
{
    for (int rowIdx = 0; rowIdx < orgImage.rows; ++rowIdx)
    {
        for (int colIdx = 0; colIdx < orgImage.cols; ++colIdx)
        {
            // the pixel pass the mask detection
            if ((255 == maskHLS.at<uchar>(rowIdx, colIdx)) &&
                (255 == maskHSV.at<uchar>(rowIdx, colIdx)))
            {
            #ifdef USE_CV_32FC3
				mixOut.at<Vec3f>(rowIdx, colIdx) = orgImage.at<Vec3f>(rowIdx, colIdx);
            #else
				mixOut.at<Vec3i>(rowIdx, colIdx) = orgImage.at<Vec3f>(rowIdx, colIdx);
            #endif
            }
            else
            {
            #ifdef USE_CV_32FC3
				mixOut.at<Vec3f>(rowIdx, colIdx) = 0;
            #else
				mixOut.at<Vec3i>(rowIdx, colIdx) = 0;
            #endif
            }
        }
    }
}

void ImageProcessing::UpdateOutputMatMix_HLSAndHSVAndSobel(const Mat &src, const Mat &maskHLS, const Mat &maskHSV, const Mat &maskSobel, Mat &mixMask, Mat &mixOut)
{
//    int numsOfX = 0;
//    int numsOfY = 0;
//    int numsOfSobel = 0;
//    int numsOfAll = 0;
//    int numsBlack = 0;
//    int numsOfSobel2 = 0;
    //bool isSobelEnabled = false;
	//qDebug("UpdateOutputMatMix_HLSAndHSVAndSobel orgImage=%d %d %d", orgImage.type(), orgImage.cols, orgImage.rows);
    //0 864 576
    //qDebug("UpdateOutputMatMix_HLSAndHSVAndSobel maskHLS=%d %d %d", maskHLS.type(), maskHLS.cols, maskHLS.rows);
    //0
    //qDebug("UpdateOutputMatMix_HLSAndHSVAndSobel maskHSV=%d %d %d", maskHSV.type(), maskHSV.cols, maskHSV.rows);
    //0
    //qDebug("UpdateOutputMatMix_HLSAndHSVAndSobel maskSobel=%d %d %d", maskSobel.type(), maskSobel.cols, maskSobel.rows);
    //21
	//qDebug("UpdateOutputMatMix_HLSAndHSVAndSobel mixMask=%d %d %d", mixMask.type(), mixMask.cols, mixMask.rows);
    //qDebug("UpdateOutputMatMix_HLSAndHSVAndSobel mixOut=%d %d %d", mixOut.type(), mixOut.cols, mixOut.rows);
	bool bFound;
    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();

    for (int rowIdx = 0; rowIdx < src.rows; ++rowIdx)
    {
		//qDebug("rowIdx=%d", rowIdx);
        for (int colIdx = 0; colIdx < src.cols; ++colIdx)
        {
#if 1
//            if (255 == maskSobelX.at<uchar>(rowIdx, colIdx))
//                ++numsOfX;
//            if (255 == maskSobelY.at<uchar>(rowIdx, colIdx))
//                ++numsOfY;

//            isSobelEnabled = false;
//            if ((0 == g_sobelDxyMode) && (255 == maskSobelX.at<uchar>(rowIdx, colIdx)))
//                isSobelEnabled = true;
//            else if ((1 == g_sobelDxyMode) && (255 == maskSobelY.at<uchar>(rowIdx, colIdx)))
//                isSobelEnabled = true;
//            else if ((2 == g_sobelDxyMode) && (
//                    ((255 == maskSobelX.at<uchar>(rowIdx, colIdx)) &&
//                     (255 == maskSobelY.at<uchar>(rowIdx, colIdx)))))
//                isSobelEnabled = true;

//            if (isSobelEnabled)
//                ++numsOfSobel;
            // the pixel pass the mask detection
//            if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) &&
//                 (255 == maskHSV.at<uchar>(rowIdx, colIdx))) ||
//                 (isSobelEnabled))
			bFound = false;
            if (mask_hls_and_hsv_and_sobel == pCache->lanedetection_maskmix_mode)
            {
                if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) &&
                    (255 == maskHSV.at<uchar>(rowIdx, colIdx))) &&
                    (255 == maskSobel.at<uchar>(rowIdx, colIdx)))
                    bFound = true;
            }
			else if (mask_hls_and_hsv_or_sobel == pCache->lanedetection_maskmix_mode)
			{
				if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) &&
					 (255 == maskHSV.at<uchar>(rowIdx, colIdx))) ||
					 (255 == maskSobel.at<uchar>(rowIdx, colIdx)))
					bFound = true;
			}
			else if (mask_hls_or_hsv_or_sobel == pCache->lanedetection_maskmix_mode)
			{
				if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) ||
					 (255 == maskHSV.at<uchar>(rowIdx, colIdx))) ||
					 (255 == maskSobel.at<uchar>(rowIdx, colIdx)))
					bFound = true;
			}
			//else if (mask_hls_and_hsv_or_gray_sobel == pCache->lanedetection_maskmix_mode)
			//{
			//	if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) &&
			//		(255 == maskHSV.at<uchar>(rowIdx, colIdx))) ||
			//		(255 == maskSobel.at<uchar>(rowIdx, colIdx)))
			//		bFound = true;
			//}

			if (bFound)
#else
            if (g_sobelThresholdXMax == maskSobelX.at<uchar>(rowIdx, colIdx))
                ++nums1;
            if (g_sobelThresholdYMax == maskSobelY.at<uchar>(rowIdx, colIdx))
                ++nums2;
            // the pixel pass the mask detection
            if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) &&
                 (255 == maskHSV.at<uchar>(rowIdx, colIdx))) ||
               ((g_sobelThresholdXMax == maskSobelX.at<uchar>(rowIdx, colIdx)) &&
                (g_sobelThresholdYMax == maskSobelY.at<uchar>(rowIdx, colIdx))))
#endif
            {
				mixMask.at<uchar>(rowIdx, colIdx) = 255;
                //++numsOfAll;
            //#ifdef USE_CV_32FC3
			//	mixOut.at<Vec3f>(rowIdx, colIdx) = orgImage.at<Vec3f>(rowIdx, colIdx);
            //#else
				mixOut.at<Vec3b>(rowIdx, colIdx) = src.at<Vec3b>(rowIdx, colIdx);
            //#endif
            }
            else
            {
				mixMask.at<uchar>(rowIdx, colIdx) = 0;
                //++numsBlack;
            //#ifdef USE_CV_32FC3
			//	mixOut.at<Vec3f>(rowIdx, colIdx) = 0;
            //#else
				mixOut.at<Vec3b>(rowIdx, colIdx) = 0;
            //#endif
            }
        }
    }
    //qDebug("numsOfX=%d numsOfY=%d numsOfSobel=%d numsOfSobel2=%d numsOfAll=%d numsBlack=%d",
    //       numsOfX,numsOfY,numsOfSobel,numsOfSobel2,numsOfAll,numsBlack);
}

void ImageProcessing::DoImageProcessing(const cv::Mat &matImage)
{
    if (e_imageprocessing_common_preview == m_approach)
        DoImageProcessingForPreview(matImage);
    else if (e_imageprocessing_grabber_preview == m_approach)
        DoImageProcessingForEtronCapture(matImage);
    else if (e_imageprocessing_grabber_capture == m_approach)
        DoImageProcessingForEtronCapture(matImage);
    else if (e_imageprocessing_etron_preview == m_approach)
        DoImageProcessingForEtronPreview(matImage);
    else if (e_imageprocessing_etron_capture == m_approach)
        DoImageProcessingForEtronCapture(matImage);
}

void ImageProcessing::DoImageProcessingForPreview(const cv::Mat &matImage)
{
    if (e_imageprocessing_state_started != m_state)
        return;

    //emit targetImageReady(matImage, m_currentFrame++);
    AsyncProcess(matImage);
}

void ImageProcessing::DoImageProcessingForGrabberPreview(const cv::Mat &matImage)
{
    DoImageProcessingForEtronPreview(matImage);
}

void ImageProcessing::DoImageProcessingForGrabberCapture(const cv::Mat &matImage)
{
    DoImageProcessingForEtronCapture(matImage);
}

void ImageProcessing::DoImageProcessingForEtronPreview(const cv::Mat &matImage)
{
    
}

void ImageProcessing::DoImageProcessingForEtronCapture(const cv::Mat &matImage)
{
    
}

void ImageProcessing::ThresholdCallbackAll(int pos, void *userdata)
{
    DSG(1, "ThresholdCallbackAll");
    ImageProcessing *pImageProcessing;
    if (userdata)
        pImageProcessing = static_cast<ImageProcessing*>(userdata);
    else
        pImageProcessing = g_pImageProcessing;
    if (!pImageProcessing) return;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    ELaneDetectionApproach approach = pCache->lanedetection_approach;
    //DSG(1, "ThresholdCallbackAll approach=%d", approach);
    if (e_lanedetection_hls == approach)
    {
        ThresholdCallbackHLS(pos, userdata);
        ThresholdCallbackMix(pImageProcessing);
		ThresholdCallbackVehicle(pos, userdata);
    }
    else if (e_lanedetection_hsv == approach)
    {
        ThresholdCallbackHSV(pos, userdata);
        ThresholdCallbackMix(pImageProcessing);
		ThresholdCallbackVehicle(pos, userdata);
    }
    else if (e_lanedetection_sobel == approach)
    {
        ThresholdCallbackSobel(pos, userdata, true);
        ThresholdCallbackMix(pImageProcessing);
		ThresholdCallbackVehicle(pos, userdata);
    }
    else if (e_lanedetection_canny == approach)
    {
        ThresholdCallbackMix(pImageProcessing);
        ThresholdCallbackCanny(pos, userdata);
		ThresholdCallbackVehicle(pos, userdata);
    }
    else if (e_lanedetection_mix_hls_hsv_canny == approach)
    {
        ThresholdCallbackHLS(pos, userdata); 
        ThresholdCallbackHSV(pos, userdata); DSG(1, "3");
        ThresholdCallbackMix(pImageProcessing); DSG(1, "4");
        ThresholdCallbackCanny(pos, userdata); DSG(1, "5");
		ThresholdCallbackVehicle(pos, userdata); DSG(1, "6");
    }
    else if (e_lanedetection_mix_hls_hsv_sobel_canny == approach)
    {
		//qDebug("ThresholdCallbackAll e_lanedetection_mix_hls_hsv_sobel_canny will ThresholdCallbackHLS");
#define TEST_TIME
#ifdef TEST_TIME
		QElapsedTimer timer2;
		timer2.start();
#endif
        ThresholdCallbackHLS(pos, userdata);
#ifdef TEST_TIME
		qDebug("ThresholdCallbackHLS timer2 %d", timer2.elapsed());
		timer2.start();
#endif
        ThresholdCallbackHSV(pos, userdata);
#ifdef TEST_TIME
		qDebug("ThresholdCallbackHSV timer2 %d", timer2.elapsed());
		timer2.start();
#endif
        ThresholdCallbackSobel(pos, userdata, false);
#ifdef TEST_TIME
		qDebug("ThresholdCallbackSobel timer2 %d", timer2.elapsed());
		timer2.start();
#endif
        ThresholdCallbackMix(pImageProcessing);
#ifdef TEST_TIME
		qDebug("ThresholdCallbackMix timer2 %d", timer2.elapsed());
		timer2.start();
#endif
        //DSG(1, "ThresholdCallbackCanny");
        ThresholdCallbackCanny(pos, userdata);
#ifdef TEST_TIME
		qDebug("ThresholdCallbackCanny timer2 %d", timer2.elapsed());
		timer2.start();
#endif
		ThresholdCallbackVehicle(pos, userdata);
#ifdef TEST_TIME
		qDebug("ThresholdCallbackVehicle timer2 %d", timer2.elapsed());
#endif
    }
}

void ImageProcessing::ThresholdCallbackAllAsync(int pos, void *userdata)
{
    //DSG(1, "ThresholdCallbackAll");
    ImageProcessing *pImageProcessing;
    if (userdata)
        pImageProcessing = static_cast<ImageProcessing*>(userdata);
    else
        pImageProcessing = g_pImageProcessing;
    if (!pImageProcessing) return;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    ELaneDetectionApproach approach = pCache->lanedetection_approach;
    //DSG(1, "ThresholdCallbackAll approach=%d", approach);
    if (e_lanedetection_hls == approach)
    {
        ThresholdCallbackHLS(pos, userdata);
        ThresholdCallbackMix(pImageProcessing);
        ThresholdCallbackVehicle(pos, userdata);
    }
    else if (e_lanedetection_hsv == approach)
    {
        ThresholdCallbackHSV(pos, userdata);
        ThresholdCallbackMix(pImageProcessing);
        ThresholdCallbackVehicle(pos, userdata);
    }
    else if (e_lanedetection_sobel == approach)
    {
        ThresholdCallbackSobel(pos, userdata, true);
        ThresholdCallbackMix(pImageProcessing);
        ThresholdCallbackVehicle(pos, userdata);
    }
    else if (e_lanedetection_canny == approach)
    {
        ThresholdCallbackMix(pImageProcessing);
        ThresholdCallbackCanny(pos, userdata);
        ThresholdCallbackVehicle(pos, userdata);
    }
    else if (e_lanedetection_mix_hls_hsv_canny == approach)
    {
        ThresholdCallbackHLS(pos, userdata);
        ThresholdCallbackHSV(pos, userdata); DSG(1, "3");
        ThresholdCallbackMix(pImageProcessing); DSG(1, "4");
        ThresholdCallbackCanny(pos, userdata); DSG(1, "5");
        ThresholdCallbackVehicle(pos, userdata); DSG(1, "6");
    }
    else if (e_lanedetection_mix_hls_hsv_sobel_canny == approach)
    {
        //qDebug("ThresholdCallbackAll e_lanedetection_mix_hls_hsv_sobel_canny will ThresholdCallbackHLS");
        //#define TEST_TIME
#ifdef TEST_TIME
        QElapsedTimer timer2;
        timer2.start();
#endif
        ThresholdCallbackHLS(pos, userdata);
#ifdef TEST_TIME
        qDebug("ThresholdCallbackHLS timer2 %d", timer2.elapsed());
        timer2.start();
#endif
        ThresholdCallbackHSV(pos, userdata);
#ifdef TEST_TIME
        qDebug("ThresholdCallbackHSV timer2 %d", timer2.elapsed());
        timer2.start();
#endif
        ThresholdCallbackSobel(pos, userdata, false);
#ifdef TEST_TIME
        qDebug("ThresholdCallbackSobel timer2 %d", timer2.elapsed());
        timer2.start();
#endif
        ThresholdCallbackMix(pImageProcessing);
#ifdef TEST_TIME
        qDebug("ThresholdCallbackMix timer2 %d", timer2.elapsed());
        timer2.start();
#endif
        ThresholdCallbackCanny(pos, userdata);
#ifdef TEST_TIME
        qDebug("ThresholdCallbackCanny timer2 %d", timer2.elapsed());
        timer2.start();
#endif
        ThresholdCallbackVehicle(pos, userdata);
#ifdef TEST_TIME
        qDebug("ThresholdCallbackVehicle timer2 %d", timer2.elapsed());
#endif
    }
    //ThresholdCallbackCanny2(pos, userdata);
    //ThresholdCallbackVehicle(pos, userdata);
}

void ImageProcessing::ThresholdCallbackHLS(int pos, void *userdata)
{
    //DSG(1, "ThresholdCallbackHLS");
    ImageProcessing *pImageProcessing;
    if (userdata)
        pImageProcessing = static_cast<ImageProcessing*>(userdata);
    else
        pImageProcessing = g_pImageProcessing;
    if (!pImageProcessing) return;

    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();
    CQElapsedTimer timer1;
    if (pImageProcessing->IsLaneDetectionEnabled())
	{
		const Mat *pSrcF = pImageProcessing->GetScaledImageF(); //m_matScaledImageF
		const Mat *pSrc = pImageProcessing->GetScaledImage(); //m_matScaledImage
		Mat *pHLSMaskH = (const_cast<Mat*>(pImageProcessing->GetHLSMaskH()));
		Mat *pHLSMaskL = (const_cast<Mat*>(pImageProcessing->GetHLSMaskL()));
		Mat *pHLSMaskS = (const_cast<Mat*>(pImageProcessing->GetHLSMaskS()));
		Mat *pHLSMask = (const_cast<Mat*>(pImageProcessing->GetHLSMask()));
        //PrintMatInfo(*pSrcF);
        //PrintMatInfo(*pSrc);
        //PrintMatInfo(*pHLSMaskH);
        //PrintMatInfo(*pHLSMaskL);
        //PrintMatInfo(*pHLSMaskS);
        //PrintMatInfo(*pHLSMask);
		//QElapsedTimer timer;

		// 4ms
		if (e_operation_live == pCache->operation_mode)
		{
			g_matHLSInF.release();
			//g_matHLSOut.release();

			cv::cvtColor(*pSrcF, g_matHLSInF, COLOR_BGR2HLS);

			//#ifdef USE_CV_32FC3
			//  g_matHLSOut = Mat::zeros(matScaledOrgClone.size(), CV_32FC3);
			//#else
			//  g_matHLSOut = Mat::zeros(pSrc->size(), CV_8UC3);
			//#endif
		}
        timer1.Print("ThresholdCallbackHLS 1");
        timer1.ResetTimer();
		//qDebug("g_matHLSInF=%d %d %d", g_matHLSInF.type(), g_matHLSInF.cols, g_matHLSInF.rows);
		// 9ms
		pImageProcessing->ReactFromTrackbarHLS(g_matHLSInF, pHLSMaskH, pHLSMaskL, pHLSMaskS);
        timer1.Print("ThresholdCallbackHLS 2");
        timer1.ResetTimer();
		//timer.start();
		//Mat matTmp;// = Mat::zeros(g_matHLSInF.size(), CV_8U);
		//pHLSMask->zeros(g_matHLSInF.size(), CV_8U);
		//bitwise_and(*pHLSMaskH, *pHLSMaskL, matTmp);
		//bitwise_and(matTmp, *pHLSMaskS, *pHLSMask);

		//PrintMatInfo(g_matHLSOut);
		//g_matHLSOut = Mat::zeros((*pSrc).size(), CV_8UC3); // maybe this can be removed
		// 10ms
        //Mat dst1, dst2, dst3;
        //pHLSMaskH->convertTo(dst1, CV_8UC3, 255, 0);
        //pHLSMaskL->convertTo(dst2, CV_8UC3, 255, 0);
        //pHLSMaskS->convertTo(dst3, CV_8UC3, 255, 0);
        //DSG(1, "WWWWW");
        //imshow("dst1", dst1);
        //imshow("dst2", dst2);
        //imshow("dst3", dst3);
		pImageProcessing->UpdateOutputMatHLS(*pSrcF, *pHLSMaskH, *pHLSMaskL, *pHLSMaskS, *pHLSMask, g_matHLSOut);
        timer1.Print("ThresholdCallbackHLS 3");
        
		//qDebug("ThresholdCallbackHLS UpdateOutputMatHLS timer %d", timer.elapsed());
	}
    timer1.ResetTimer();
	//timer.start();
	// 9 ms 
    //if (pCache->showtrackbar_enabled)
    {
        pImageProcessing->ShowWindowHLS();
    }
	//qDebug("ThresholdCallbackHLS ShowWindowHLS timer %d", timer.elapsed());
    timer1.Print("ThresholdCallbackHLS 4");
}

void ImageProcessing::ThresholdCallbackHSV(int pos, void *userdata)
{
	QElapsedTimer timer;

    ImageProcessing *pImageProcessing;
    if (userdata)
        pImageProcessing = static_cast<ImageProcessing*>(userdata);
    else
        pImageProcessing = g_pImageProcessing;
    if (!pImageProcessing) return;

    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();

	if (pImageProcessing->IsLaneDetectionEnabled())
	{
		//qDebug("time up go hsv");
		const Mat *pSrcF = pImageProcessing->GetScaledImageF(); //m_matScaledImageF
		const Mat *pSrc = pImageProcessing->GetScaledImage(); //m_matScaledImage
		const Mat *pHSVMaskH = pImageProcessing->GetHSVMaskH();
		const Mat *pHSVMaskS = pImageProcessing->GetHSVMaskS();
		const Mat *pHSVMaskV = pImageProcessing->GetHSVMaskV();
		const Mat *pHSVMask = pImageProcessing->GetHSVMask();

		// 0 ms
		if (e_operation_live == pCache->operation_mode)
		{
			g_matHSVInF.release();
			//g_matHSVOut.release();
			cv::cvtColor((*pSrcF), g_matHSVInF, COLOR_BGR2HSV);
		}

		//timer.start();
		//pHSVMaskH->zeros(g_matHSVInF.size(), CV_8U);
		//pHSVMaskS->zeros(g_matHSVInF.size(), CV_8U);
		//pHSVMaskV->zeros(g_matHSVInF.size(), CV_8U);
        //qDebug("g_matHSVInF=%d %d %d", g_matHLSInF.type(), g_matHLSInF.cols, g_matHLSInF.rows);
        //DSG(1, "%p %p %p ", pHSVMaskH, pHSVMaskS, pHSVMaskV);
		// 3 ~ 4ms
		pImageProcessing->ReactFromTrackbarHSV(g_matHSVInF,
			(const_cast<Mat*>(pHSVMaskH)), (const_cast<Mat*>(pHSVMaskS)), (const_cast<Mat*>(pHSVMaskV)));
		//qDebug("ThresholdCallbackHSV ReactFromTrackbarHSV timer %d", timer.elapsed());

		//timer.start();
		//Mat matTmp;// = Mat::zeros(g_matHSVInF.size(), CV_8U);
		//pHSVMask->zeros(g_matHSVInF.size(), CV_8U);

		//bitwise_and(*pHSVMaskH, *pHSVMaskS, matTmp);
		//bitwise_and(matTmp, *pHSVMaskV, *pHSVMask);

		//g_matHSVOut = Mat::zeros((*pSrc).size(), CV_8UC3); // maybe this can be removed
		// 5ms
		//PrintMatInfo(*pSrcF);
		//PrintMatInfo(*pHSVMaskH);
		//PrintMatInfo(*pHSVMaskS);
		//PrintMatInfo(*pHSVMaskV);
		//PrintMatInfo(*pHSVMask);
		pImageProcessing->UpdateOutputMatHSV(*pSrcF, *pHSVMaskH, *pHSVMaskS, *pHSVMaskV, *(const_cast<Mat*>(pHSVMask)), g_matHSVOut);
		//qDebug("g_matHSVOut=%d %d %d", g_matHSVOut.type(), g_matHSVOut.cols, g_matHSVOut.rows);
		//qDebug("ThresholdCallbackHSV UpdateOutputMatHSV timer %d", timer.elapsed());
	}
	timer.start();
    //if (pCache->showtrackbar_enabled)
    {
        pImageProcessing->ShowWindowHSV();
    }
	//qDebug("ThresholdCallbackHSV ShowWindowHSV timer %d", timer.elapsed());
}

void ImageProcessing::ThresholdCallbackSobel(int pos, void *userdata, bool willUpdateOutputMat)
{
    ImageProcessing *pImageProcessing;
    if (userdata)
        pImageProcessing = static_cast<ImageProcessing*>(userdata);
    else
        pImageProcessing = g_pImageProcessing;
    if (!pImageProcessing) return;
   
    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();

	if (pImageProcessing->IsLaneDetectionEnabled())
	{
		//qDebug("time up go sobel");
		const Mat *pSrc = pImageProcessing->GetScaledImage();
		const Mat *pSobelMask = pImageProcessing->GetSobelMask();
        
		const vector<Mat> *pHLSChannel = pImageProcessing->GetHLSChannel();
        if (!pHLSChannel)
            return;
        //DSG(1, "pHLSChannel.size()=%d", pHLSChannel->size());
        
		//qDebug("pSrc=%d %d %d", pSrc->type(), pSrc->cols, pSrc->rows);
		//qDebug("pSobelMask=%d %d %d", pSobelMask->type(), pSobelMask->cols, pSobelMask->rows);
        
		QElapsedTimer timer;

		timer.start();
		if (e_operation_live == pCache->operation_mode)
		{
            
			g_matSobelIn.release();
			if ((mask_hls_and_hsv_or_sobel == pCache->lanedetection_approach) ||
				(mask_hls_or_hsv_or_sobel == pCache->lanedetection_approach))
			{
				Mat matGray;
				pHLSChannel->at(1).convertTo(matGray, CV_8U, 255, 0);
				g_matSobelIn = matGray;
			}
			else 
			{
				//Mat matTmp;
				//GaussianBlur(*pSrc, matTmp, Size(3, 3), 0, 0, BORDER_DEFAULT);
				//cvtColor(matTmp, g_matSobelIn, COLOR_BGR2GRAY);
                
				cvtColor(*pSrc, g_matSobelIn, COLOR_BGR2GRAY);
			}
		}
        
		//qDebug("ThresholdCallbackSobel getOperationMode timer %lld", timer.elapsed());
        
		//timer.start();
		//19 ms
		g_matMaskSobelX = Mat::zeros(g_matSobelIn.size(), CV_8U);
		g_matMaskSobelY = Mat::zeros(g_matSobelIn.size(), CV_8U);
		pImageProcessing->ReactFromTrackbarSobel(g_matSobelIn, g_matMaskSobelX, g_matMaskSobelY);
		//qDebug("ThresholdCallbackSobel ReactFromTrackbarSobel timer %d", timer.elapsed());

		
		//qDebug("g_sobelThresholdXMin=%d g_sobelThresholdXMax=%d", g_sobelThresholdXMin, g_sobelThresholdXMax);
		//qDebug("g_matMaskSobelX=%d %d %d", g_matMaskSobelX.type(), g_matMaskSobelX.cols, g_matMaskSobelX.rows);//0
		//qDebug("g_matMaskSobelY=%d %d %d", g_matMaskSobelY.type(), g_matMaskSobelY.cols, g_matMaskSobelY.rows);//0
		//timer.start();
		//bitwise_or(g_matMaskSobelX, g_matMaskSobelY, m_matMaskSobel);
		//pSobelMask->zeros(g_matSobelIn.size(), CV_8U);
		bitwise_or(g_matMaskSobelX, g_matMaskSobelY, *(const_cast<Mat*>(pSobelMask)));
		//imwrite("g_matMaskSobelX.bmp", g_matMaskSobelX);
		//imwrite("g_matMaskSobelY.bmp", g_matMaskSobelY);
		//imwrite("pSobelMask.bmp", *pSobelMask);
		//qDebug("ThresholdCallbackSobel bitwise_or timer %d", timer.elapsed());

		//qDebug("pSobelMask=%d %d %d", pSobelMask->type(), pSobelMask->cols, pSobelMask->rows);
		//PrintMatInfo(g_matSobelOut);
		timer.start();
		//1 ms
		//g_matSobelOut = Mat::zeros((*pSrc).size(), CV_8UC3); // maybe this can be removed
		pImageProcessing->UpdateOutputMatSobel(*pSrc, *pSobelMask, g_matSobelOut);
		//qDebug("ThresholdCallbackSobel UpdateOutputMatSobel timer %d", timer.elapsed());

		//qDebug("g_matSobelOut=%d %d %d", g_matSobelOut.type(), g_matSobelOut.cols, g_matSobelOut.rows);//0
	}
    timer.start();
    //if (pCache->showtrackbar_enabled)
        pImageProcessing->ShowWindowSobel();
    //qDebug("ThresholdCallbackSobel ShowWindowSobel timer %d", timer.elapsed());
}

void ImageProcessing::ThresholdCallbackMix(ImageProcessing *pImageProcessing)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

	if (pImageProcessing->IsLaneDetectionEnabled())
	{
		//DSG(1, "ImageProcessing::ThresholdCallbackMix time up go mix");
        const Mat *pSrc = pImageProcessing->GetScaledImage();
		const Mat *pSrcF = pImageProcessing->GetScaledImageF();
		const Mat *pHLSMask = pImageProcessing->GetHLSMask();
		const Mat *pHSVMask = pImageProcessing->GetHSVMask();
		const Mat *pSobelMask = pImageProcessing->GetSobelMask();
		const Mat *pMixMask;
		const Mat *pMixOut;
		ELaneDetectionApproach approach = pCache->lanedetection_approach;
		if (e_lanedetection_hls == approach)
		{
			pMixOut = &g_matHLSOut;
		}
		else if (e_lanedetection_hsv == approach)
		{
			pMixOut = &g_matHSVOut;
		}
		else if (e_lanedetection_sobel == approach)
		{
			pMixOut = &g_matSobelOut;
		}
		else if (e_lanedetection_canny == approach)
		{
			pMixOut = pSrcF;
		}
		else if (e_lanedetection_mix_hls_hsv_canny == approach)
		{
			pMixOut = pImageProcessing->GetMixOutImage();

			pMixOut->zeros((*pSrc).size(), CV_8UC3);
            //PrintMatInfo(*pSrcF);
            //PrintMatInfo(*pHLSMask);
            //PrintMatInfo(*pHSVMask);
            //PrintMatInfo(*pMixOut);
            pImageProcessing->UpdateOutputMatMix_HLSAndHSV(*pSrcF, *pHLSMask, *pHSVMask, *(const_cast<Mat*>(pMixOut)));
            //DSG(1, "ImageProcessing::ThresholdCallbackMix 3 pMixOut=%p", pMixOut);
		}
		else if (e_lanedetection_mix_hls_hsv_sobel_canny == approach)
		{
			//pImageProcessing->UpdateOutputMatMix_HLSAndHSVAndSobel(matSrc, m_matMaskHLS, m_matMaskHSV, g_matMaskSobelX, g_matMaskSobelY, m_matMixOut);
			//pImageProcessing->UpdateOutputMatMix_HLSAndHSVAndSobel(*pSrc, *pHLSMask, *pHSVMask, *pSobelMask, *(const_cast<Mat*>(pMixOut)));

			pMixMask = pImageProcessing->GetMixMask();
			pMixOut = pImageProcessing->GetMixOutImage();
			//pMixMask->zeros((*pSrc).size(), CV_8U);
			//pMixOut->zeros((*pSrc).size(), CV_8UC3);

			//QElapsedTimer timer;
			//timer.start();
			pImageProcessing->UpdateOutputMatMix_HLSAndHSVAndSobel(*pSrc, *pHLSMask, *pHSVMask, *pSobelMask,
				*(const_cast<Mat*>(pMixMask)), *(const_cast<Mat*>(pMixOut)));
			//qDebug("ThresholdCallbackMix UpdateOutputMatMix_HLSAndHSVAndSobel timer %d", timer.elapsed());
			//imwrite("pHLSMask.bmp", *pHLSMask);
			//imwrite("pHSVMask.bmp", *pHSVMask);
			//imwrite("pSobelMask.bmp", *pSobelMask);
			//imwrite("pMixMask.bmp", *pMixMask);
			//imwrite("pMixOut.bmp", *pMixOut);
		}
	}

    //if (pCache->showtrackbar_enabled)
    {
        pImageProcessing->ShowWindowMix();
    }

}

void ImageProcessing::ThresholdCallbackCanny(int pos, void *userdata)
{
    //DSG(1, "XXXXX ThresholdCallbackCanny pos=%d userdata=%p", pos, userdata);
	ImageProcessing *pImageProcessing;
	if (userdata)
		pImageProcessing = static_cast<ImageProcessing*>(userdata);
	else
		pImageProcessing = g_pImageProcessing;
	if (!pImageProcessing) return;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

	const Mat *pSrc = pImageProcessing->GetScaledImage();
	const Mat *pSrcP = pImageProcessing->GetScaledImageP();
	const Mat *pSrcGray = pImageProcessing->GetScaledImageGray();

	SLanelinesInfo *pLaneLinesInfo = pImageProcessing->GetDetectedLaneLinesInfo();
	QElapsedTimer timer;
    //DSG(1, "ThresholdCallbackCanny pSrcGray=%p", pSrcGray);
    pImageProcessing->ResetDetectedLaneImage(pImageProcessing, *pSrc);

	if (pImageProcessing->IsLaneDetectionEnabled())
	{
		int numsOfLines;
		int roiLeft, roiRight, roiTop, roiBottom;
		int lineLeft, lineRight, lineTop, lineBottom;

		const Mat *pMixMask = pImageProcessing->GetMixMask();
		const Mat *pMixOut = pImageProcessing->GetMixOutImage();
		const Mat *pCannyOut = pImageProcessing->GetCannyOutImage();

		if (pCache->roi_enabled)
		{
			roiLeft = roiRight = roiTop = roiBottom = 0;
			QVector<DrawableGraphicsItem<QGraphicsLineItem, QPushButton>*> *pROILines;
			pROILines = pImageProcessing->GetScene()->getROILines();
			for (size_t i = 0; i < pROILines->size(); ++i)
			{
				DrawableGraphicsItem<QGraphicsLineItem, QPushButton> *pROILine = pROILines->at(i);
				QGraphicsLineItem *pLine = static_cast<QGraphicsLineItem*>(pROILine->getItem());

				if (0 == i)
					roiTop = pLine->pos().y();
				else if (1 == i)
					roiBottom = pLine->pos().y();
				else if (2 == i)
					roiLeft = pLine->pos().x();
				else if (3 == i)
					roiRight = pLine->pos().x();
			}
			// To Do ...
			cv::Rect rcROI;
			CVFindRect(roiLeft, roiTop, roiRight, roiBottom, 10, (*pSrc).cols, (*pSrc).rows, rcROI);

			Mat matROISrc(*pSrc, rcROI);

			//g_matCannyIn.release();
			(const_cast<Mat*>(pCannyOut))->release();
			if (mask_gray_equalizehist_only == pCache->lanedetection_approach)
			{
#if 0
				Mat matGaussian;
				cv::GaussianBlur(*pSrcGray, matGaussian, Size(5, 5), 5);
				cv::equalizeHist(matGaussian, g_matCannyIn);
#else
				equalizeHist(*pSrcGray, g_matCannyIn);
				//g_matCannyIn = *pSrcGray;
#endif
			}
			else
			{
				g_matCannyIn = *pMixMask;
				//g_matCannyIn = (*pMixMask)->clone();
			}

			//4~5ms
			//timer.start();
			pImageProcessing->ReactFromTrackbarCanny(g_matCannyIn, *(const_cast<Mat*>(pCannyOut)));
			//qDebug("ReactFromTrackbarCanny timer=%d", timer.elapsed());
		}
		else
		{
			//g_matCannyIn.release();
			(const_cast<Mat*>(pCannyOut))->release();

			if (mask_gray_equalizehist_only == pCache->lanedetection_approach)
			{
#if 0
				Mat matGaussian;
				cv::GaussianBlur(*pSrcGray, matGaussian, Size(5, 5), 5);
				cv::equalizeHist(matGaussian, g_matCannyIn);
#else
				equalizeHist(*pSrcGray, g_matCannyIn);
				//g_matCannyIn = *pSrcGray;
#endif
			}
			else
			{
				g_matCannyIn = *pMixMask;
				//g_matCannyIn = (*pMixMask)->clone();
			}

			//5ms
			timer.start();
			pImageProcessing->ReactFromTrackbarCanny(g_matCannyIn, *(const_cast<Mat*>(pCannyOut)));
			qDebug("ReactFromTrackbarCanny timer=%d", timer.elapsed());
		}

        //PrintMatInfo(*pCannyOut);
		{
			SLanelinesInfo* pLaneLinesInfo = pImageProcessing->GetDetectedLaneLinesInfo();
			//std::vector<bool> *pLaneInROI = &(pLaneLinesInfo->in_roi);
			std::vector<cv::Vec4i> *pLaneArray = &(pLaneLinesInfo->array);
			std::vector<cv::Point2f> *pLaneWinTl = &(pLaneLinesInfo->sliding_win_tl);
			std::vector<cv::Point2f> *pLaneWinBr = &(pLaneLinesInfo->sliding_win_br);
			std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList =
				&(pLaneLinesInfo->sliding_win_info_list);

			pImageProcessing->ReleaseLanelinesInfo(true);

			//84
			timer.start();
			pImageProcessing->FindLanes(*pCannyOut, pLaneArray);
			//qDebug("FindLanes timer=%d", timer.elapsed());

			pImageProcessing->LaneDetouching(pImageProcessing, pLaneLinesInfo);
		}
	}
	else
	{
		if (pCache->video_stability_enabled)
			pImageProcessing->PredictLanes();
	}

    std::vector<cv::Vec4i> *pLaneArrayTmp = &(pLaneLinesInfo->array);
    if (pLaneArrayTmp)
    {
        //pLaneArrayTmp->clear();
		//std::vector<cv::Vec4i> zeroLaneArray;
		//pLaneArrayTmp->clear();
		//pLaneArrayTmp->swap(zeroLaneArray);
        if (pLaneArrayTmp->size() == 0)
        {
            //DSG(1, "pImageProcessing->m_lanelineLeft=%d %d %d %d"
            //    , pImageProcessing->m_lanelineLeft[0]
            //    , pImageProcessing->m_lanelineLeft[1]
            //    , pImageProcessing->m_lanelineLeft[2]
            //    , pImageProcessing->m_lanelineLeft[3]);
            pLaneArrayTmp->push_back(pImageProcessing->m_lanelineLeft);
            pLaneArrayTmp->push_back(pImageProcessing->m_lanelineRight);
        }
    }

	pImageProcessing->EmitUpdateLaneline(&(pLaneLinesInfo->array));
	//timer.start();
	//if (pCache->showtrackbar_enabled)
	{
		pImageProcessing->ShowWindowCanny();
	}
    //DSG(1, "CCC 2");
}

void ImageProcessing::ThresholdCallbackCanny2(int pos, void *userdata)
{
    //DSG(1, "ThresholdCallbackCanny pos=%d userdata=%p", pos, userdata);
    ImageProcessing *pImageProcessing;
    if (userdata)
        pImageProcessing = static_cast<ImageProcessing*>(userdata);
    else
        pImageProcessing = g_pImageProcessing;
    if (!pImageProcessing) return;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    const Mat *pSrc = pImageProcessing->GetScaledImage();
    const Mat *pSrcP = pImageProcessing->GetScaledImageP();
    const Mat *pSrcGray = pImageProcessing->GetScaledImageGray();

    SLanelinesInfo *pLaneLinesInfo = pImageProcessing->GetDetectedLaneLinesInfo();
    QElapsedTimer timer;
    //DSG(1, "ThresholdCallbackCanny pSrcGray=%p", pSrcGray);
    pImageProcessing->ResetDetectedLaneImage(pImageProcessing, *pSrc);

    if (pImageProcessing->IsLaneDetectionEnabled())
    {
        int numsOfLines;
        int roiLeft, roiRight, roiTop, roiBottom;
        int lineLeft, lineRight, lineTop, lineBottom;

        const Mat *pMixMask = pImageProcessing->GetMixMask();
        const Mat *pMixOut = pImageProcessing->GetMixOutImage();
        const Mat *pCannyOut = pImageProcessing->GetCannyOutImage();

        if (pCache->roi_enabled)
        {
            roiLeft = roiRight = roiTop = roiBottom = 0;
            QVector<DrawableGraphicsItem<QGraphicsLineItem, QPushButton>*> *pROILines;
            pROILines = pImageProcessing->GetScene()->getROILines();
            for (size_t i = 0; i < pROILines->size(); ++i)
            {
                DrawableGraphicsItem<QGraphicsLineItem, QPushButton> *pROILine = pROILines->at(i);
                QGraphicsLineItem *pLine = static_cast<QGraphicsLineItem*>(pROILine->getItem());



                if (0 == i)
                    roiTop = pLine->pos().y();
                else if (1 == i)
                    roiBottom = pLine->pos().y();
                else if (2 == i)
                    roiLeft = pLine->pos().x();
                else if (3 == i)
                    roiRight = pLine->pos().x();
            }
            // To Do ...
            cv::Rect rcROI;
            CVFindRect(roiLeft, roiTop, roiRight, roiBottom, 10, (*pSrc).cols, (*pSrc).rows, rcROI);

            Mat matROISrc(*pSrc, rcROI);

            //g_matCannyIn.release();
            (const_cast<Mat*>(pCannyOut))->release();
            if (mask_gray_equalizehist_only == pCache->lanedetection_approach)
            {
#if 0
                Mat matGaussian;
                cv::GaussianBlur(*pSrcGray, matGaussian, Size(5, 5), 5);
                cv::equalizeHist(matGaussian, g_matCannyIn);
#else
                equalizeHist(*pSrcGray, g_matCannyIn);
                //g_matCannyIn = *pSrcGray;
#endif
            }
            else
            {
                g_matCannyIn = *pMixMask;
                //g_matCannyIn = (*pMixMask)->clone();
            }

            //4~5ms
            //timer.start();
            pImageProcessing->ReactFromTrackbarCanny(g_matCannyIn, *(const_cast<Mat*>(pCannyOut)));
            //qDebug("ReactFromTrackbarCanny timer=%d", timer.elapsed());
        }
        else
        {
            //g_matCannyIn.release();
            (const_cast<Mat*>(pCannyOut))->release();

            if (mask_gray_equalizehist_only == pCache->lanedetection_approach)
            {
#if 0
                Mat matGaussian;
                cv::GaussianBlur(*pSrcGray, matGaussian, Size(5, 5), 5);
                cv::equalizeHist(matGaussian, g_matCannyIn);
#else
                equalizeHist(*pSrcGray, g_matCannyIn);
                //g_matCannyIn = *pSrcGray;
#endif
            }
            else
            {
                g_matCannyIn = *pMixMask;
                //g_matCannyIn = (*pMixMask)->clone();
            }

            //5ms
            timer.start();
            pImageProcessing->ReactFromTrackbarCanny(g_matCannyIn, *(const_cast<Mat*>(pCannyOut)));
            qDebug("ReactFromTrackbarCanny timer=%d", timer.elapsed());
        }

        //PrintMatInfo(*pCannyOut);
        {
            SLanelinesInfo* pLaneLinesInfo = pImageProcessing->GetDetectedLaneLinesInfo();
            //std::vector<bool> *pLaneInROI = &(pLaneLinesInfo->in_roi);
            std::vector<cv::Vec4i> *pLaneArray = &(pLaneLinesInfo->array);
            std::vector<cv::Point2f> *pLaneWinTl = &(pLaneLinesInfo->sliding_win_tl);
            std::vector<cv::Point2f> *pLaneWinBr = &(pLaneLinesInfo->sliding_win_br);
            std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList =
                &(pLaneLinesInfo->sliding_win_info_list);

            pImageProcessing->ReleaseLanelinesInfo(true);

            //84
            timer.start();
            pImageProcessing->FindLanes(*pCannyOut, pLaneArray);
            //qDebug("FindLanes timer=%d", timer.elapsed());

            pImageProcessing->LaneDetouching(pImageProcessing, pLaneLinesInfo);
        }
    }
    else
    {
        if (pCache->video_stability_enabled)
            pImageProcessing->PredictLanes();
    }
    //DSG(1, "CCC");

    //{

    //    std::vector<cv::Vec4i> *pLaneArrayTmp = &(pLaneLinesInfo->array);
    //    pLaneArrayTmp->clear();
    //    Vec4i realLine;
    //    realLine[0] = 610;
    //    realLine[1] = 20;
    //    realLine[2] = 610;
    //    realLine[3] = 710;

    //    pLaneArrayTmp->push_back(realLine);

    //    Vec4i realLine1;
    //    realLine1[0] = 720;
    //    realLine1[1] = 20;
    //    realLine1[2] = 1070;
    //    realLine1[3] = 710;

    //    pLaneArrayTmp->push_back(realLine1);
    //}

    pImageProcessing->EmitUpdateLaneline(&(pLaneLinesInfo->array));
    //timer.start();
    //if (pCache->showtrackbar_enabled)
    {
        pImageProcessing->ShowWindowCanny();
    }
    //DSG(1, "CCC 2");
}


void ImageProcessing::ThresholdCallbackVehicle(int pos, void *userdata)
{
    //DSG(1, "ThresholdCallbackVehicle");
	//qDebug("ThresholdCallbackVehicle timer3 %lld", timer3.elapsed());
 
	ImageProcessing *pImageProcessing;
	if (userdata)
	{
		pImageProcessing = static_cast<ImageProcessing*>(userdata);
	} else
		pImageProcessing = g_pImageProcessing;
	if (!pImageProcessing) return;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    bool isCandidate = false;
    int baseline = 0;
    int roiLeft, roiRight, roiTop, roiBottom;
    char szText[1024] = {0};
    Size textSize;

    SLanelinesInfo *pLaneLinesInfo;
    std::vector<cv::Vec4i> *pLaneArray;

    SCarAgent *pCarAgent = pImageProcessing->GetCarAgent();
    const Mat *pLaneDetectedImage = pImageProcessing->GetLaneDetectedImage();
    Mat *pDisplayImage = pImageProcessing->ReassignDisplayImage(*pLaneDetectedImage);
    //DSG(1, "pDisplayImage =%p", pDisplayImage);
	if (!pDisplayImage) return;

	size_t numsOfLines = 0;
	size_t numsOfCars = 0;

	do {
		if (!pCache->vehicledetection_enabled)
			break;

		//QQueue<Mat> *pSrcList = pImageProcessing->GetScaledImageList();
		const Mat *pSrcP = pImageProcessing->GetScaledImageP();
		const Mat *pSrc = pImageProcessing->GetScaledImage();
		const Mat *pSrcGray = pImageProcessing->GetScaledImageGray();
		vector<Rect> tmpVehicleArrary;

		pLaneLinesInfo = pImageProcessing->GetDetectedLaneLinesInfo();
        //DSG(1, "pLaneLinesInfo =%p", pLaneLinesInfo);
		//pAutoGeneratorRects = &(pCarAgent->autogenerator_list);

		bool checkVehicle = 0;
		//checkVehicle = pImageProcessing->FindDiffBetweenMatrix(*(const_cast<Mat*>(pSrcP)), *(const_cast<Mat*>(pSrc)), pLaneLinesInfo, 0.3);
		//checkVehicle = pImageProcessing->FindDiffBetweenMatrix(pSrcList->head(), *(const_cast<Mat*>(pSrc)), pLaneLinesInfo, 0.3); //ektwo
		//qDebug("checkVehicle = %d", checkVehicle); // 0
		//qDebug("pos = %d", pos); // 0
		//qDebug("userdata = %p", userdata); // 0
		// 0 == pos:
		// userdata: when change the setting of car classifier dialog
		if ((checkVehicle) || ((0 == pos) || (userdata)))
		{
			if (e_vd_model_kneron == pCache->vehicledetection_model)
				//#ifdef USE_KNERON_VEHICLE_DETECTION
			{
                if (pImageProcessing->IsReadyToDetectVehicle())
                {
                #if 1
                    //tmpVehicleArrary = detect_obj(utf8_text);

                    static std::vector<cv::Vec4i> tmpVehicleArraryStd;
                    std::vector<cv::Vec4i> tmpVehicleArrary2;

                    //cv::Mat matImage = cv::imread("DSCF4460.JPG", CV_LOAD_IMAGE_COLOR);
                    //cv::Mat matImage = cv::imread("capture_1334.bmp", CV_LOAD_IMAGE_COLOR);
                    //tmpVehicleArrary2 = GetDetectedCars(matImage);
					
					timer.start();
					//Mat mat = pSrc->clone();
                    Mat mat = *pSrc;
					//PythonWrapperInit();
                    if ((g_cnt != 0) && ((g_cnt % 3) == 0))
                    {
                        for (int i = 0; i < tmpVehicleArraryStd.size(); ++i)
                        {
                            tmpVehicleArrary2[i][0] = tmpVehicleArraryStd[i][0];
                            tmpVehicleArrary2[i][1] = tmpVehicleArraryStd[i][1];
                            tmpVehicleArrary2[i][2] = tmpVehicleArraryStd[i][2];
                            tmpVehicleArrary2[i][3] = tmpVehicleArraryStd[i][3];
                        }
                        //memcpy(tmpVehicleArrary2, tmpVehicleArraryStd, sizeof(tmpVehicleArraryStd));
                    }
                    else {
                        CPythonWrapperModel *pKneronModel = pImageProcessing->GetModel();
                        if (pKneronModel)
                        {
                            tmpVehicleArrary2 = pKneronModel->GetDetectedCars(mat);
                            for (int i = 0; i < tmpVehicleArraryStd.size(); ++i)
                            {
                                tmpVehicleArraryStd[i][0] = tmpVehicleArrary2[i][0];
                                tmpVehicleArraryStd[i][1] = tmpVehicleArrary2[i][1];
                                tmpVehicleArraryStd[i][2] = tmpVehicleArrary2[i][2];
                                tmpVehicleArraryStd[i][3] = tmpVehicleArrary2[i][3];
                            }
                        }
                    }
                    //imwrite("ggg.bmp", mat);
                    //DSG(1, "??? g_cnt=%d", g_cnt);
					//GetDetectedCars(mat);
					//PythonWrapperFree();
                    //tmpVehicleArrary2 = GetDetectedCars(*(const_cast<Mat*>(pSrc)));
                    //tmpVehicleArrary2 = GetDetectedCars(*(const_cast<Mat*>(pSrc)));
                    if (g_cnt > 0)
                    {
                        g_totalTime += timer.elapsed();
                        qDebug("ThresholdCallbackVehicle time=%lld cnt=%d avg=%lld", 
                            timer.elapsed(), g_cnt, g_totalTime / (g_cnt));
                    }
                    else
                        qDebug("ThresholdCallbackVehicle time=%lld", timer.elapsed());
                    ++g_cnt;

                    //DSG(1, "tmpVehicleArrary2.size()=%d", tmpVehicleArrary2.size());
                    for (int i = 0; i < tmpVehicleArrary2.size(); i++)
                    {
                        cv::Vec4i rc = tmpVehicleArrary2.at(i);
					
                        cv::Rect rcDst;
                        rcDst.x = rc[1];
                        rcDst.y = rc[0];
                        rcDst.width = rc[3] - rc[1];
                        rcDst.height = rc[2] - rc[0];

                        //DSG(1, "%d (%d %d %d %d)",
                        //    i, rcDst.x,rcDst.y,rcDst.width,rcDst.height);
                        tmpVehicleArrary.push_back(rcDst);
                    }
					numsOfCars = tmpVehicleArrary2.size();// pCarAgent->car_list.size();
                    DSG(1, "numsOfCars=%d", numsOfCars);
                #else
                    //QString s = QApplication::applicationDirPath() + "/storage";
                    QString s = "D:/storage";

                    if (!QDir(s).exists())
                    {
                        //qDebug("exist");
                        QDir().mkdir(s);
                    }

                    char szFileName[256];
                    strcpy(szFileName, pImageProcessing->GetVideoConfig()->filename);
                    remove_extension(szFileName);
                    //qDebug("filename=%s", szFileName);
                    //qDebug("pImageProcessing->getcurrentFrameIdx()=%d", pImageProcessing->getcurrentFrameIdx());

                    QString str;
                    str.sprintf("%s/%s%s%d%s",
                        s.toLatin1().constData(),
                        pImageProcessing->GetVideoConfig()->filename,
                        "_violation_",
                        pImageProcessing->getcurrentFrameIdx(),
                        ".bmp");
                    //qDebug("storage_name=%s", str.toLatin1().constData());
                    std::string utf8_text = str.toUtf8().constData();

                    bool b = imwrite(utf8_text, *pSrc);
                    //vector<Rect> v = detect_obj(utf8_text);
                    //*pCarRects

                    //tmpVehicleArrary = detect_obj(utf8_text);

                    numsOfCars = tmpVehicleArrary.size();// pCarAgent->car_list.size();
                    //qDebug("1 numsOfCars=%d", numsOfCars);
                    //for (int i = 0; i < numsOfCars; ++i)
                    //{
                    //	qDebug("%d %d %d %d",  tmpVehicleArrary[i].x,tmpVehicleArrary[i].y, tmpVehicleArrary[i].width, tmpVehicleArrary[i].height);
                    //}
                #endif
                }
			}
			else if (e_vd_model_common_cascadeclassifier == pCache->vehicledetection_model)
			{
				if (e_operation_live == pCache->operation_mode)
				{
					g_matCarIn.release();
				#if 0
					Mat matGaussian;
					Mat matHist;
					cv::GaussianBlur(*pSrcGray, matGaussian, Size(5, 5), 5);
					cv::equalizeHist(matGaussian, g_matCarIn);
				#else
					g_matCarIn = *pSrcGray;
				#endif
				}

				//imshow("GRGR", g_matCarIn);
				cascade.detectMultiScale(g_matCarIn,
					tmpVehicleArrary, 
					double(carClassifierScaleFactor + 11) / 10.0,
					carClassifierMinNeighbors,
					(1 << carClassifierFlags),// 0 | CV_HAAR_SCALE_IMAGE,
					Size(carClassifierMinSize, carClassifierMinSize),
					Size(carClassifierMaxSize, carClassifierMaxSize));

				numsOfCars = tmpVehicleArrary.size();// pCarAgent->car_list.size();// pCarRects->size();
				//qDebug("2 numsOfCars=%d", numsOfCars);

				///SeeAllRect(&cascade1, g_matCarIn, *pDisplayImage, Scalar(30, 255, 38));
				//SeeAllRect(&cascade2, g_matCarIn, *pDisplayImage, Scalar(30, 200, 38));
				//SeeAllRect(&cascade3, g_matCarIn, *pDisplayImage, Scalar(30, 150, 38));
				//SeeAllRect(&cascade4, g_matCarIn, *pDisplayImage, Scalar(30, 100, 38));
			}
		}
#if 1
		if (numsOfCars <= 0)
			break;

		//vector<Point> *pts = pProcessingSetting->getROIPoints();
		//for (int i = 0; i < pts->size(); ++i)
		//	qDebug("pts (%d,%d)", pts->at(i).x, pts->at(i).y);
        //DSG(1, "numsOfCars 2 =%d", numsOfCars);
		pImageProcessing->DeleteCarAgent();

		pLaneArray = &(pLaneLinesInfo->array);

		numsOfLines = pLaneArray->size();
		DSG(1, "numsOfLines=%d", numsOfLines);

		g_numCars = numsOfCars;
		g_numLines = numsOfLines;

		int x, y, width, height;
		int tolerance = pCache->dwl_tolerance;
		int drawLevel = 0;
		Point pt1;
		Point pt2;
		Point leftWheel;
		Point rightWheel;
		Point2f leftWheelF, rightWheelF;
        //DSG(1, "numsOfCars=%d", numsOfCars);
		for (size_t idx = 0; idx < numsOfCars; ++idx)
		{
			//AutoGeneratorRect autoGeneratorRect;
			//autoGeneratorRect.SetBoundary(QRectF(0, 0, g_matCarIn.cols, g_matCarIn.rows));
			//pCarAgent->autogenerator_list.push_back(autoGeneratorRect);

			pCarAgent->car_list.push_back(tmpVehicleArrary[idx]);

			x = tmpVehicleArrary[idx].x;
			y = tmpVehicleArrary[idx].y;
			width = tmpVehicleArrary[idx].width;
			height = tmpVehicleArrary[idx].height;
			pt1.x = x;
			pt1.y = y;
			pt2.x = pt1.x + width;
			pt2.y = pt1.y + height;
            //DSG(1, "pt1.x=%d %d %d %d", pt1.x, pt1.y, pt2.x, pt2.y);
			leftWheel.x = pt1.x;
			leftWheel.y = pt2.y;
			rightWheel.x = pt2.x;
			rightWheel.y = pt2.y;
			leftWheelF.x = (float)(pt1.x);
			leftWheelF.y = (float)(pt2.y);
			rightWheelF.x = (float)(pt2.x);
			rightWheelF.y = (float)(pt2.y);

			//qDebug("car:[%d] wheel(%d, %d) (%d, %d)", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
            //DSG(1, "numsOfLines 2 =%d", numsOfLines);
        #ifdef CHECK_LANDLINE
			if (numsOfLines <= 0)
				break;

			drawLevel = 0;
            //2018.1005
			//if ((CVCheckPointInROI(*pProcessingSetting->getROIPoints(), leftWheelF)) ||
			//	(CVCheckPointInROI(*pProcessingSetting->getROIPoints(), rightWheelF)))
			{
				++drawLevel;
				//pLaneInROI = &(pLaneLinesInfo->in_roi);

				for (size_t j = 0; j < numsOfLines; ++j)
				{
					if (true == CheckCoveredState(&(pLaneArray->at(j)), leftWheel, rightWheel, tolerance))
					{
						++drawLevel;
						break;
					}
				}
			}
            DSG(1, "drawLevel=%d", drawLevel);
        #else
            drawLevel = 2;
        #endif
			if (2 == drawLevel)
			{
				//snprintf(szText, sizeof(szText) - 1, "car_%d_%d_%d_%d_%d", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
				snprintf(szText, sizeof(szText) - 1, "car_%d", idx);
				textSize = getTextSize(String(szText), k_car_font_face, k_car_font_scale_no, k_car_thickness_no, &baseline);

				rectangle(*pDisplayImage, pt1, Point(pt1.x+ textSize.width, pt1.y+textSize.height), Scalar(60, 60, 60), CV_FILLED, 8, 0);
				putText(*pDisplayImage,
					String(szText),
					Point(leftWheel.x, pt1.y + textSize.height),//leftWheel.y + textSize.height + 4),
					k_car_font_face, k_car_font_scale_no,
					k_car_color_no);
				rectangle(*pDisplayImage, pt1, pt2, Scalar(0, 255, 0), 3, 8, 0);

                DSG(1, "x=%d y=%d width=%d height=%d", x, y, width, height);
            #ifdef OUTPUT_DEBUG_IMG
                static int illegalIdx = 0;
                int newX, newY, newW, newH;
                newX = x - (width / 10);
                if (newX < 0) newX = 0;
                newY = y - (height / 10);
                if (newY < 0) newY = 0;
                newW = width * 1.2;
                if (newW > 1280) newW = 1280;
                newH = height * 1.2;
                if (newH > 720) newH = 720;
                DSG(1, "newX=%d newY=%d newW=%d newH=%d", newX, newY, newW, newH);
                if (newX + newW > (*pSrc).cols)
                {
                    newX = x; newW = width;
                }
                if (newY + newH > (*pSrc).rows)
                {
                    newY = y; newH = height;
                }
                PrintMatInfo(*pSrc);
                Mat matFile = Mat(*pSrc, Rect(newX, newY, newW, newH));
                char sz[256];
                sprintf(sz, "D:/storage/car_%d_illegal.bmp", illegalIdx++);
                imwrite(sz, matFile);

                //emit pImageProcessing->updateImageList();
                DSG(1, "end");
            #endif
			}
			else if (1 == drawLevel)
			{
				snprintf(szText, sizeof(szText) - 1, "car_%d_%d_%d_%d_%d", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
				//snprintf(szText, sizeof(szText) - 1, "car_%d", idx);
				textSize = getTextSize(String(szText), k_car_font_face, k_car_font_scale_no, k_car_thickness_no, &baseline);

				rectangle(*pDisplayImage, pt1, Point(pt1.x + textSize.width, pt1.y + textSize.height), Scalar(60, 60, 60), CV_FILLED, 8, 0);
				putText(*pDisplayImage,
					String(szText),
					Point(leftWheel.x, pt1.y + textSize.height),//leftWheel.y + textSize.height + 4),
					k_car_font_face, k_car_font_scale_no,
					k_car_color_no);

				rectangle(*pDisplayImage, pt1, pt2, Scalar(0, 255, 255), 3, 8, 0);
			}
			else if (0 == drawLevel)
			{
				//snprintf(szText, sizeof(szText) - 1, "car_%d_%d_%d_%d_%d", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
				snprintf(szText, sizeof(szText) - 1, "car_%d", idx);
				textSize = getTextSize(String(szText), k_car_font_face, k_car_font_scale_no, k_car_thickness_no, &baseline);

				rectangle(*pDisplayImage, pt1, Point(pt1.x + textSize.width, pt1.y + textSize.height), Scalar(60, 60, 60), CV_FILLED, 8, 0);
				putText(*pDisplayImage,
					String(szText),
					Point(leftWheel.x, pt1.y + textSize.height),//leftWheel.y + textSize.height + 4),
					k_car_font_face, k_car_font_scale_no,
					k_car_color_no);

				rectangle(*pDisplayImage, pt1, pt2, Scalar(150, 150, 150), 3, 8, 0);
			}
            //imshow(str_win_car_classifier, *pSrc);// m_matDetectedLane);// *pDisplayImage);


		}
		break;

#endif
	} while (0);

	if (numsOfCars > 0)
	{
		//qDebug("CCCCC %d", pCarAgent->car_list.size());
	//	pImageProcessing->EmitUpdateVehicleInfo(&(pCarAgent->car_list));
	}
	
	//if (pCache->showtrackbar_enabled)
		pImageProcessing->ShowWindowCarClassifier(); //ektwo

	//qDebug("matDisplay=%d %d %d", matDisplay.type(), matDisplay.cols, matDisplay.rows);
    //pImageProcessing->EmitDisplayImageReady(matDisplay);

	//timer3.start();
}

void ImageProcessing::ThresholdCallbackCarClassifierEngineeringMode()
{
    ImageProcessing *pImageProcessing = g_pImageProcessing;

    pImageProcessing->ThresholdCallbackVehicle(-1, 0);
}

void ImageProcessing::PrepareImageForLaneDetectionHLS(Mat &matScaledOrgClone)
{
	g_matHLSInF.release();
    g_matHLSOut.release();
	m_matMaskHLS_H.release();
	m_matMaskHLS_L.release();
	m_matMaskHLS_S.release();
	m_matMaskHLS.release();
	m_matHLSChannel.clear();

	m_matMaskHLS_H = Mat::zeros(matScaledOrgClone.size(), CV_8U);
	m_matMaskHLS_L = Mat::zeros(matScaledOrgClone.size(), CV_8U);
	m_matMaskHLS_S = Mat::zeros(matScaledOrgClone.size(), CV_8U);
	m_matMaskHLS = Mat::zeros(matScaledOrgClone.size(), CV_8U);

    //cvtColor(matScaledOrgClone, g_matHLSInF, COLOR_BGR2HLS);
	cv::cvtColor(m_matScaledImageF, g_matHLSInF, COLOR_BGR2HLS);

	split(g_matHLSInF, m_matHLSChannel);

		//imwrite("L.bmp", m_matHLSChannel[1]);
		//imwrite("S.bmp", m_matHLSChannel[2]);

	//imwrite("L2.bmp", aa[1]);
	//imwrite("S2.bmp", aa[2]);
		
//#ifdef USE_CV_32FC3
//    g_matHLSOut = Mat::zeros(matScaledOrgClone.size(), CV_32FC3);
//#else
    g_matHLSOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);
//#endif


#ifdef HLS_INDIVIDUAL
    split(g_matHLSInF, g_chHLS);
    //CV_8UC1
    g_matHLSOutH.release();
    g_matHLSOutH = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
    g_matHLSOutL.release();
    g_matHLSOutL = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
    g_matHLSOutS.release();
    g_matHLSOutS = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
#endif
}

void ImageProcessing::PrepareImageForLaneDetectionHSV(Mat &matScaledOrgClone)
{
    g_matHSVInF.release();
    g_matHSVOut.release();
	m_matMaskHSV_H.release();
	m_matMaskHSV_S.release();
	m_matMaskHSV_V.release();
	m_matMaskHSV.release();

	m_matMaskHSV_H = Mat::zeros(matScaledOrgClone.size(), CV_8U);
	m_matMaskHSV_S = Mat::zeros(matScaledOrgClone.size(), CV_8U);
	m_matMaskHSV_V = Mat::zeros(matScaledOrgClone.size(), CV_8U);
	m_matMaskHSV = Mat::zeros(matScaledOrgClone.size(), CV_8U);

#if 1
	cv::cvtColor(matScaledOrgClone, g_matHSVInF, COLOR_BGR2HSV);

    //2018.1004
//#   ifdef USE_CV_32FC3
//    g_matHSVOut = Mat::zeros(matScaledOrgClone.size(), CV_32FC3);
//#   else
    g_matHSVOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);
//#   endif
#else
	cv::cvtColor(matScaledOrgClone, g_matHSVInF, COLOR_BGR2HSV);

    g_matHSVOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);
#endif

#ifdef HSV_INDIVIDUAL
    split(g_matHSVInF, g_chHSV);

    //CV_8UC1
    g_matHSVOutH.release();
    g_matHSVOutH = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
    g_matHSVOutS.release();
    g_matHSVOutS = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
    g_matHSVOutV.release();
    g_matHSVOutV = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
#endif
}

void ImageProcessing::PrepareImageForLaneDetectionSobel(Mat &matScaledOrgClone)
{
    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();

    g_matSobelIn.release();
    g_matSobelOut.release();
	g_matMaskSobelX.release();
	g_matMaskSobelY.release();
	m_matMaskSobel.release();
    //g_matSobelIn = Mat(matScaledOrgClone.rows, matScaledOrgClone.cols, CV_8UC1, Scalar(0));

	g_matMaskSobelX = Mat::zeros(matScaledOrgClone.size(), CV_8U);
	g_matMaskSobelY = Mat::zeros(matScaledOrgClone.size(), CV_8U);
	m_matMaskSobel = Mat::zeros(matScaledOrgClone.size(), CV_8U);

	if ((mask_hls_and_hsv_or_sobel == pCache->lanedetection_approach) ||
		(mask_hls_or_hsv_or_sobel == pCache->lanedetection_approach))
	{
		Mat matGray;
		m_matHLSChannel[1].convertTo(matGray, CV_8U, 255, 0);
		//PrintMatInfo(matGray);
	#if 1
		Mat matGaussian;
		cv::GaussianBlur(matGray, matGaussian, Size(5, 5), 5);
		cv::equalizeHist(matGaussian, g_matSobelIn);
	#else	
		g_matSobelIn = matGray;
	#endif	
	}
	else
	{
		g_matSobelIn = m_matScaledImageGray;
	}

    //5 864 576
    //qDebug("PrepareImageForLaneDetectionSobel 2 g_matSobelIn=%d %d %d", g_matSobelIn.type(), g_matSobelIn.cols, g_matSobelIn.rows);
//#ifdef USE_CV_32FC3
//   g_matSobelOut = Mat::zeros(matScaledOrgClone.size(), CV_32FC3);
//#else
   g_matSobelOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);
//#endif

   //21 864 576
//qDebug("PrepareImageForLaneDetectionSobel 2 g_matSobelOut=%d %d %d",
//       g_matSobelOut.type(), g_matSobelOut.cols, g_matSobelOut.rows);
}

void ImageProcessing::PrepareImageForLaneDetectionMix(Mat &matScaledOrgClone)
{
    g_matMixIn.release();
    m_matMixOut.release();
	m_matMaskMix.release();

	m_matMaskMix = Mat::zeros(matScaledOrgClone.size(), CV_8U);

//#ifdef USE_CV_32FC3
//   m_matMixOut = Mat::zeros(matScaledOrgClone.size(), CV_32FC3);
//#else
   m_matMixOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);
//#endif
}

void ImageProcessing::PrepareImageForLaneDetectionCanny(Mat &matScaledOrgClone)
{
	g_matCannyIn.release();
	m_matCannyOut.release();

    m_matCannyOut = Mat::zeros(matScaledOrgClone.size(), CV_8U);
}

void ImageProcessing::PrepareImageForCarDetection(Mat &matScaledOrgClone)
{
	g_matCarIn.release();
	{
        TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
		if (e_vd_model_kneron == pCache->vehicledetection_model)
		{
            if (!m_pPythonWrapper)
                m_pPythonWrapper = new CPythonWrapper();
            if (!m_pPythonWrapperModel)
                m_pPythonWrapperModel = new CPythonWrapperModel();
            if (m_pPythonWrapperModel)
            {
                //if (0 == PythonWrapperInit2())
                DSG(1, "m_pPythonWrapperModel->Initialize");
                if (0 == m_pPythonWrapperModel->Initialize())
                    m_isReadyToDetectVehicle = true;
            }
		}
		else if (e_vd_model_common_cascadeclassifier == pCache->vehicledetection_model)
		{
			QString sPath = QCoreApplication::applicationDirPath().append("/casaa.xml");
			QFile::copy(":/models/opensource/cars.xml", sPath);
			cascade.load(sPath.toStdString());
			QFile::remove(sPath);

			//{
			//	QString sPath = QCoreApplication::applicationDirPath().append("/casaa.xml");
			//	QFile::copy(":/models/opensource/cas1.xml", sPath);
			//	cascade1.load(sPath.toStdString());
			//	QFile::remove(sPath);
			//}
			//{
			//	QString sPath = QCoreApplication::applicationDirPath().append("/casaa.xml");
			//	QFile::copy(":/models/opensource/cas2.xml", sPath);
			//	cascade2.load(sPath.toStdString());
			//	QFile::remove(sPath);
			//}
			//{
			//	QString sPath = QCoreApplication::applicationDirPath().append("/casaa.xml");
			//	QFile::copy(":/models/opensource/cas3.xml", sPath);
			//	cascade3.load(sPath.toStdString());
			//	QFile::remove(sPath);
			//}
			//{
			//	QString sPath = QCoreApplication::applicationDirPath().append("/casaa.xml");
			//	QFile::copy(":/models/opensource/cas4.xml", sPath);
			//	cascade4.load(sPath.toStdString());
			//	QFile::remove(sPath);
			//}
		}


	}


#if 0
	Mat matGaussian;
	Mat matHist;
	cv::GaussianBlur(m_matScaledImageGray, matGaussian, Size(5, 5), 5);
	cv::equalizeHist(matGaussian, g_matCarIn);
#else
	g_matCarIn = m_matScaledImageGray;
#endif
}

void ImageProcessing::CreateWindows()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

	ELaneDetectionApproach approach = pCache->lanedetection_approach;
	if (e_lanedetection_hls == approach)
		CreateWindowHLS();
	else if (e_lanedetection_hsv == approach)
		CreateWindowHSV();
	else if (e_lanedetection_sobel == approach)
		CreateWindowSobel();
	else if (e_lanedetection_canny == approach)
		CreateWindowCanny();
	else if (e_lanedetection_mix_hls_hsv_canny == approach)
	{
		CreateWindowHLS();
		CreateWindowHSV();
		CreateWindowMix();
		CreateWindowCanny();
	}
	else if (e_lanedetection_mix_hls_hsv_sobel_canny == approach)
	{
		CreateWindowHLS();
		CreateWindowHSV();
		CreateWindowSobel();
		CreateWindowMix();
		CreateWindowCanny();
	}
	CreateWindowCarClassifier();
}

void ImageProcessing::DestroyWindows()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
	ELaneDetectionApproach approach = pCache->lanedetection_approach;
	if (e_lanedetection_hls == approach)
		DestroyWindowHLS();
	else if (e_lanedetection_hsv == approach)
		DestroyWindowHSV();
	else if (e_lanedetection_sobel == approach)
		DestroyWindowSobel();
	else if (e_lanedetection_canny == approach)
		DestroyWindowCanny();
	else if (e_lanedetection_mix == approach)
		DestroyWindowMix();
	else if (e_lanedetection_mix_hls_hsv_canny == approach)
	{
		DestroyWindowHLS();
		DestroyWindowHSV();
		DestroyWindowMix();
		DestroyWindowCanny();
	}
	else if (e_lanedetection_mix_hls_hsv_sobel_canny == approach)
	{
        DSG(1, "SSS DestroyWindowHLS");
		DestroyWindowHLS();
		DestroyWindowHSV();
		DestroyWindowSobel();
		DestroyWindowMix();
		DestroyWindowCanny();
	}
	DestroyWindowCarClassifier();
}

void ImageProcessing::ShowTrackbarWindow(ETrackbarOnOff onoff)
{
    //DSG(1, "SSS ShowTrackbarWindow");
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    bool engineeringMode = pCache->engineeringmode_enabled;
	if (engineeringMode)
	{
		ELaneDetectionApproach approach = pCache->lanedetection_approach;
		if ((e_trackbar_force_off == onoff) && (m_isWindowExisted))
		{
			m_isWindowExisted = false;
            DSG(1, "SSS ShowTrackbarWindow 1");
			DestroyWindows();
		}
		else if ((!pCache->showtrackbar_enabled) && (m_isWindowExisted))
		{
			m_isWindowExisted = false;
            DSG(1, "SSS ShowTrackbarWindow 2");
			DestroyWindows();
		}
		else if ((pCache->showtrackbar_enabled) && (!m_isWindowExisted))
		{
			m_isWindowExisted = true;
			CreateWindows();

			if (m_isFistTimeToShow)
			{
				m_isFistTimeToShow = false;
				if (e_lanedetection_hls == approach)
				{
					moveWindow(str_win_hls, 100, 100);
					//moveWindow(str_win_hls_l_min, 120, 120);
					//moveWindow(str_win_hls_s_min, 140, 140);
				}
				else if (e_lanedetection_hsv == approach)
				{
					moveWindow(str_win_hsv, 100, 100);
				}
				else if (e_lanedetection_sobel == approach)
				{
					moveWindow(str_win_sobel, 100, 100);
				}
				else if (e_lanedetection_canny == approach)
				{
					moveWindow(str_win_canny, 100, 100);
				}
				else if (e_lanedetection_mix_hls_hsv_canny == approach)
				{
					moveWindow(str_win_hls, 20, 20);
					moveWindow(str_win_hsv, 40, 40);
					moveWindow(str_win_mix, 60, 60);
					moveWindow(str_win_canny, 80, 80);
				}
				else if (e_lanedetection_mix_hls_hsv_sobel_canny == approach)
				{
					moveWindow(str_win_hls, 20, 20);
					moveWindow(str_win_hsv, 40, 40);
					moveWindow(str_win_sobel, 60, 60);
					moveWindow(str_win_mix, 80, 80);
					moveWindow(str_win_canny, 100, 100);
				}

				if (true == pCache->showtrackbar_enabled)
				{
					moveWindow(str_win_car_classifier, 300, 300);
				}
			}
		}
	}
}

void ImageProcessing::CreateWindowHLS()
{
    namedWindow(str_win_hls, CV_WINDOW_AUTOSIZE);
	//namedWindow(str_win_hls_l_min, CV_WINDOW_AUTOSIZE);
	//namedWindow(str_win_hls_s_min, CV_WINDOW_AUTOSIZE);

#if 1
    FnThresholdCallback fn = ThresholdCallbackAll;
#else
    FnThresholdCallback fn = ThresholdCallbackHLS;
#endif

#ifdef HLS_INDIVIDUAL
    createTrackbar(k_str_hls_h, str_win_hls, &g_hlsThresholdTypeH, k_hls_h_type_max, fn );
    createTrackbar(k_str_hls_l, str_win_hls, &g_hlsThresholdTypeL, k_hls_l_type_max, fn );
    createTrackbar(k_str_hls_s, str_win_hls, &g_hlsThresholdTypeS, k_hls_s_type_max, fn );
#endif
    createTrackbar(str_win_hls_h_min, str_win_hls, &g_hlsThresholdValueH, k_hls_h_max, fn );
    createTrackbar(str_win_hls_l_min, str_win_hls, &g_hlsThresholdValueL, k_hls_l_max, fn );
    createTrackbar(str_win_hls_s_min, str_win_hls, &g_hlsThresholdValueS, k_hls_s_max, fn );
    createTrackbar(str_win_hls_h_max, str_win_hls, &g_hlsThresholdValueHMax, k_hls_h_max, fn);
    createTrackbar(str_win_hls_l_max, str_win_hls, &g_hlsThresholdValueLMax, k_hls_l_max, fn);
    createTrackbar(str_win_hls_s_max, str_win_hls, &g_hlsThresholdValueSMax, k_hls_s_max, fn);
}

void ImageProcessing::CreateWindowHSV()
{
    namedWindow(str_win_hsv, CV_WINDOW_AUTOSIZE);//CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);

#if 1
    FnThresholdCallback fn = ThresholdCallbackAll;
#else
    FnThresholdCallback fn = ThresholdCallbackHSV;
#endif

    createTrackbar(str_win_hsv_h_min, str_win_hsv, &g_hsvThresholdValueH, k_hsv_h_max, fn);
    createTrackbar(str_win_hsv_s_min, str_win_hsv, &g_hsvThresholdValueS, k_hsv_s_max, fn);
    createTrackbar(str_win_hsv_v_min, str_win_hsv, &g_hsvThresholdValueV, k_hsv_v_max, fn);
    createTrackbar(str_win_hsv_h_max, str_win_hsv, &g_hsvThresholdValueHMax, k_hsv_h_max, fn);
    createTrackbar(str_win_hsv_s_max, str_win_hsv, &g_hsvThresholdValueSMax, k_hsv_s_max, fn);
    createTrackbar(str_win_hsv_v_max, str_win_hsv, &g_hsvThresholdValueVMax, k_hsv_v_max, fn);
}

void ImageProcessing::CreateWindowSobel()
{
    namedWindow(str_win_sobel, CV_WINDOW_AUTOSIZE);// | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);

    FnThresholdCallback fn = ThresholdCallbackAll;
    void *pUserdata = static_cast<void*>(g_pImageProcessing);

    createTrackbar(k_str_sobel_dxymode, str_win_sobel, &g_sobelDxyMode, k_sobel_dxymode_max, fn, pUserdata);
    createTrackbar(k_str_sobel_ksize_x, str_win_sobel, &g_sobelKSizeX, k_sobel_ksize_x_max, fn, pUserdata);
    createTrackbar(k_str_sobel_ksize_y, str_win_sobel, &g_sobelKSizeY, k_sobel_ksize_y_max, fn, pUserdata);
    createTrackbar(k_str_sobel_threshold_x_min, str_win_sobel, &g_sobelThresholdXMin, k_sobel_threshold_x_max, fn, pUserdata);
    createTrackbar(k_str_sobel_threshold_x_max, str_win_sobel, &g_sobelThresholdXMax, k_sobel_threshold_x_max, fn, pUserdata);
    createTrackbar(k_str_sobel_threshold_y_min, str_win_sobel, &g_sobelThresholdYMin, k_sobel_threshold_y_max, fn, pUserdata);
    createTrackbar(k_str_sobel_threshold_y_max, str_win_sobel, &g_sobelThresholdYMax, k_sobel_threshold_y_max, fn, pUserdata);
    //createTrackbar(k_str_sobel_threshold_type, str_win_sobel, &g_sobelThresholdType, k_sobel_threshold_type_max, fn, pUserdata);
}

void ImageProcessing::CreateWindowMix()
{
    namedWindow(str_win_mix, CV_WINDOW_AUTOSIZE | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);
}

void ImageProcessing::CreateWindowCanny()
{
#if 1
    FnThresholdCallback fn = ThresholdCallbackAll;
#else
    FnThresholdCallback fn = ThresholdCallbackCanny;
#endif

    const string k_str_canny_threshold1("Canny threshold1");
    const string k_str_canny_threshold2("Canny threshold2");
    const string k_str_canny_aperture_size_div2("Canny aperture size");
    const string k_str_canny_gradient("Canny gradient");
    const string k_str_hough_rho("Hough rho");
    const string k_str_hough_theta("Hough theta");
    const string k_str_hough_threshold("Hough threshold");
    const string k_str_hough_min_linelength("Hough min line length");
    const string k_str_hough_max_linegap("Hough max line gap");

    const int k_canny_threshold1_max = 200;
    const int k_canny_threshold2_max = 600;
    const int k_canny_aperture_size_div2_max = 2;
    const int k_canny_is_gradient_max = 1;
    const int k_hough_rho_max = 2;
    const int k_hough_theta_max = 720;
    const int k_hough_threshold = 400;
    const int k_hough_min_linelength = 400;
    const int k_hough_max_linegap = 800;

    namedWindow(str_win_canny, CV_WINDOW_AUTOSIZE | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);
    createTrackbar(k_str_canny_threshold1, str_win_canny, &g_cannyThreshold1, k_canny_threshold1_max, fn, static_cast<void*>(g_pImageProcessing)); //Hue (0 - 179)
    createTrackbar(k_str_canny_threshold2, str_win_canny, &g_cannyThreshold2, k_canny_threshold2_max, fn, static_cast<void*>(g_pImageProcessing));
    createTrackbar(k_str_canny_aperture_size_div2, str_win_canny, &g_cannyApertureSizeDiv2, k_canny_aperture_size_div2_max, fn, static_cast<void*>(g_pImageProcessing));
    createTrackbar(k_str_canny_gradient, str_win_canny, &g_cannyIsgradient, k_canny_is_gradient_max, fn, static_cast<void*>(g_pImageProcessing));
    createTrackbar(k_str_hough_rho, str_win_canny, &g_houghRho, k_hough_rho_max, fn, static_cast<void*>(g_pImageProcessing)); //Hue (0 - 179)
    createTrackbar(k_str_hough_theta, str_win_canny, &g_houghTheta, k_hough_theta_max, fn, static_cast<void*>(g_pImageProcessing));
    createTrackbar(k_str_hough_threshold, str_win_canny, &g_houghLineThreshold, k_hough_threshold, fn, static_cast<void*>(g_pImageProcessing));
    createTrackbar(k_str_hough_min_linelength, str_win_canny, &g_houghMinLineLength, k_hough_min_linelength, fn, static_cast<void*>(g_pImageProcessing));
    createTrackbar(k_str_hough_max_linegap, str_win_canny, &g_houghMaxLineGap, k_hough_max_linegap, fn, static_cast<void*>(g_pImageProcessing));
}

void ImageProcessing::CreateWindowCarClassifier()
{
    namedWindow(str_win_car_classifier, CV_WINDOW_AUTOSIZE);// | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);

    createTrackbar("scalefctr", str_win_car_classifier, &carClassifierScaleFactor, 100, ThresholdCallbackVehicle, static_cast<void*>(g_pImageProcessing)); //Hue (0 - 179)
    createTrackbar("mineighbor", str_win_car_classifier, &carClassifierMinNeighbors, 10, ThresholdCallbackVehicle, static_cast<void*>(g_pImageProcessing));
    createTrackbar("flags", str_win_car_classifier, &carClassifierFlags, 3, ThresholdCallbackVehicle, static_cast<void*>(g_pImageProcessing));
    createTrackbar("min size", str_win_car_classifier, &carClassifierMinSize, 500, ThresholdCallbackVehicle, static_cast<void*>(g_pImageProcessing));
    createTrackbar("max size", str_win_car_classifier, &carClassifierMaxSize, 1000, ThresholdCallbackVehicle, static_cast<void*>(g_pImageProcessing));
}

void ImageProcessing::DestroyWindowHLS()
{
    DSG(1, "SSS DestroyWindowHLS str_win_hls=%s", str_win_hls);
    destroyWindow(str_win_hls);
	//destroyWindow(str_win_hls_l_min);
	//destroyWindow(str_win_hls_s_min);
}

void ImageProcessing::DestroyWindowHSV()
{
    destroyWindow(str_win_hsv);
}

void ImageProcessing::DestroyWindowSobel()
{
    destroyWindow(str_win_sobel);
}

void ImageProcessing::DestroyWindowMix()
{
    destroyWindow(str_win_mix);
}

void ImageProcessing::DestroyWindowCanny()
{

    destroyWindow(str_win_canny);
}

void ImageProcessing::DestroyWindowCarClassifier()
{
    destroyWindow(str_win_car_classifier);
}

void ImageProcessing::ShowWindowHLS()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
	if (!pCache->engineeringmode_enabled)
	{
		//qDebug("ShowWindowHLS 2");
        //DSG(1, "ShowWindowHLS maskHLSImageReady");
		emit maskHLSImageReady(g_matHLSOut);  
	}
	else
	{
		if (pCache->showtrackbar_enabled)
		{
#ifdef MASK_IS_OUTPUT
			//imshow(str_win_hls_h_min, m_matMaskHLS_H);
			//imshow(str_win_hls_l_min, m_matMaskHLS_L);
			//imshow(str_win_hls_s_min, m_matMaskHLS_S);
			imshow(str_win_hls, g_matHLSOut);
#else
			imshow(str_win_hls, g_matHLSOut);
#endif

		}
	}
}

void ImageProcessing::ShowWindowHSV()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
	if (!pCache->engineeringmode_enabled)
	{
		//qDebug("ShowWindowHSV 2");
        //DSG(1, "ShowWindowHSV maskHSVImageReady");
        //PrintMatInfo(g_matHSVOut);
		emit maskHSVImageReady(g_matHSVOut);// m_matMaskHSV);
	}
	else
	{
		if (pCache->showtrackbar_enabled)
		{
#ifdef MASK_IS_OUTPUT
			imshow(str_win_hsv, g_matHSVOut);// m_matMaskHSV);
#else
			imshow(str_win_hsv, g_matHSVOut);
#endif
		}
	}
}

void ImageProcessing::ShowWindowSobel()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
	if (!pCache->engineeringmode_enabled)
	{
        //DSG(1, "ShowWindowSobel maskSobelImageReady");
        //PrintMatInfo(g_matSobelOut);
		//qDebug("ShowWindowSobel 2");
        emit maskSobelImageReady(g_matSobelOut);// g_matSobelIn);// m_matMaskSobel);
	}
	else
	{
		if (pCache->showtrackbar_enabled)
		{
#ifdef MASK_IS_OUTPUT
			imshow(str_win_sobel, m_matMaskSobel);// g_matMaskSobelY);
#else
			imshow(str_win_sobel, g_matSobelOut);
#endif
		}
	}
}

void ImageProcessing::ShowWindowMix()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
	if (!pCache->engineeringmode_enabled)
	{
        //DSG(1, "ShowWindowMix mixOutImageReady");
        //PrintMatInfo(m_matMixOut);
		//qDebug("ShowWindowMix 2");
        //2018.1004
        emit mixOutImageReady(m_matMixOut);// m_matMixOut);
	}
	else
	{
		if (pCache->showtrackbar_enabled)
		{
			imshow(str_win_mix, m_matMixOut);
		}
	}
}

void ImageProcessing::ShowWindowCanny()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
	if (!pCache->engineeringmode_enabled)
	{
        //DSG(1, "ShowWindowCanny cannyOutImageReady");
        //PrintMatInfo(m_matCannyOut);
		//qDebug("ShowWindowCanny 2");
		emit cannyOutImageReady(m_matCannyOut);
	}
	else
	{
		if (pCache->showtrackbar_enabled)
		{
			imshow(str_win_canny, m_matCannyOut);
		}
	}
}

void ImageProcessing::ShowWindowCarClassifier()
{
#if 1
    //DSG(1, "ShowWindowCarClassifier targetImageReady");
	emit targetImageReady(m_matDisplay, m_currentFrame);
#else
	if (!pCache->engineeringmode_enabled)
	{
		//qDebug("ShowWindowCarClassifier 2");
		emit displayImageReady(m_matDisplay);
	}
	else
	{
		if (pCache->showtrackbar_enabled)
		{
			imshow(str_win_car_classifier, m_matDisplay);
		}
		else
		{

		}
	}
#endif
}

void ImageProcessing::AddCarInfo(Rect *pRect, AutoGeneratorRect *pAutogenerator)
{
//    m_carAgent.car_list.push_back(pRect);
//    m_carAgent.autogenerator_list.push_back(pAutogenerator);
}

void ImageProcessing::DeleteCarAgent()
{
//    qDebug("m_carAgent.car_list.empty()=%d", m_carAgent.car_list.empty());

//    for (int i = 0; i < m_carAgent.car_list.size(); ++i)
//    {
//        Rect *pRect = m_carAgent.car_list[i];
//        if (pRect)
//            delete pRect;
//    }
////    while (!m_carAgent.car_list.empty()) {
////        Rect *pRect = m_carAgent
////        if (pRect)
////            delete pRect;
////        m_carAgent.car_list.pop_back();
////    }
//    qDebug("m_carAgent.autogenerator_list.empty()=%d", m_carAgent.autogenerator_list.empty());
//    for (int i = 0; i < m_carAgent.autogenerator_list.size(); ++i)
//    {
//        AutoGeneratorRect *autogenerator = m_carAgent.autogenerator_list[i];
//        if (autogenerator)
//            delete autogenerator;
//    }
////    while (!m_carAgent.autogenerator_list.empty()) {
////        AutoGeneratorRect *autogenerator = m_carAgent.autogenerator_list.pop_back();
////        if (autogenerator)
////            delete autogenerator;
////    }
    m_carAgent.car_list.clear();
    m_carAgent.autogenerator_list.clear();
    //qDebug("m_carAgent.car_list.empty() 2 =%d", m_carAgent.car_list.empty());
    //qDebug("m_carAgent.car_list.empty() 2 =%d", m_carAgent.autogenerator_list.empty());
    //m_carAgent.car_list.clear();
    //m_carAgent.autogenerator_list.clear();
}

void ImageProcessing::ResetDetectedLaneImage(ImageProcessing *pImageProcessing, const cv::Mat &matRef)
{
    Mat *pLaneDetectedImage = pImageProcessing->GetLaneDetectedImage();
    pLaneDetectedImage->release();
    *pLaneDetectedImage = matRef.clone();
}

cv::Mat* ImageProcessing::ReassignDisplayImage(const cv::Mat &matRef)
{
	m_matDisplay.release();
	m_matDisplay = matRef.clone();
	return &m_matDisplay;
}

bool ImageProcessing::IsLaneDetectionEnabled()
{
    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();

	if (pCache->lanedetection_enabled) 
	{
		if (e_operation_live == pCache->operation_mode)
		{
            if (e_lanedetection_cycletime_always == pCache->lanedetecion_cycletime)
				return true;
            else if (e_lanedetection_cycletime_10s == pCache->lanedetecion_cycletime)
			{
				if (0 == m_currentFrame)
					return true;
				if (m_timerSlice.hasExpired(3000))
				{
					//qDebug("CCCCCCCCCCCCCCC");
					m_timerSlice.start();
					return true;
				}
			}
		}
		else if (e_operation_static == pCache->operation_mode)
			return true;
	}
	return false;
}

bool ImageProcessing::IsVehicleDetectionAllowed()
{
    bool ret = false;

    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();

    if (pCache->vehicledetection_enabled)
    {
        ret = true;
    }

    return ret;
}

void ImageProcessing::ReleaseLanelinesInfo(bool all)
{
	SLanelinesInfo* pLaneLinesInfo = GetDetectedLaneLinesInfo();
	//std::vector<bool> *pLaneInROI = &(pLaneLinesInfo->in_roi);
	std::vector<cv::Vec4i> *pLaneArray = &(pLaneLinesInfo->array);
	std::vector<cv::Point2f> *pLaneWinTl = &(pLaneLinesInfo->sliding_win_tl);
	std::vector<cv::Point2f> *pLaneWinBr = &(pLaneLinesInfo->sliding_win_br);
	std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList =
		&(pLaneLinesInfo->sliding_win_info_list);

	vector<vector<SSlidingWindowInfo>>::iterator itWindow = ppWinInfo2DimList->begin();
	vector<SSlidingWindowInfo> zeroInfo;
	while (itWindow != ppWinInfo2DimList->end())
	{
		(*itWindow).clear();
		(*itWindow).swap(zeroInfo);
		++itWindow;
	}

	//std::vector<bool> zeroLaneInROI;
	//pLaneInROI->clear();
	//pLaneInROI->swap(zeroLaneInROI);

	if (all)
	{
		pLaneLinesInfo->offset_x = 0.0;
		pLaneLinesInfo->offset_y = 0.0;

		std::vector<cv::Vec4i> zeroLaneArray;
		pLaneArray->clear();
		pLaneArray->swap(zeroLaneArray);
	}

	std::vector<cv::Point2f> zeroLaneWinTl;
	pLaneWinTl->clear();
	pLaneWinTl->swap(zeroLaneWinTl);
	std::vector<cv::Point2f> zeroLaneWinBr;
	pLaneWinBr->clear();
	pLaneWinBr->swap(zeroLaneWinBr);

	std::vector<std::vector<SSlidingWindowInfo>> zeroWinInfo2DimList;
	ppWinInfo2DimList->clear();
	ppWinInfo2DimList->swap(zeroWinInfo2DimList);
}

void ImageProcessing::SeeAllRect(cv::CascadeClassifier *pCascade, cv::Mat &in, cv::Mat &display, Scalar scalar)
{
	int x, y, width, height;
	Point pt1;
	Point pt2;
	{
		vector<Rect> tmpVehicleArrary1;
		cascade1.detectMultiScale(in,
			tmpVehicleArrary1, //pCarAgent->car_list, //*pCarRects,
			double(carClassifierScaleFactor + 11) / 10.0,
			carClassifierMinNeighbors,
			(1 << carClassifierFlags),// 0 | CV_HAAR_SCALE_IMAGE,
			Size(carClassifierMinSize, carClassifierMinSize),
			Size(carClassifierMaxSize, carClassifierMaxSize));
		//qDebug("aaa %d", tmpVehicleArrary1.size());
		for (size_t idx = 0; idx < tmpVehicleArrary1.size(); ++idx)
		{
			x = tmpVehicleArrary1[idx].x;
			y = tmpVehicleArrary1[idx].y;
			width = tmpVehicleArrary1[idx].width;
			height = tmpVehicleArrary1[idx].height;
			pt1.x = x;
			pt1.y = y;
			pt2.x = pt1.x + width;
			pt2.y = pt1.y + height;

			rectangle(display, pt1, pt2, scalar, 3, 8, 0);
		}
	}
}


void ImageProcessing::started()
{
	m_timerTotal.start();
	m_timerSlice.start();

    //m_CheckLaneDetectionAndCalibration = true;
	m_currentFrame = 0;
}

void ImageProcessing::stopped()
{
	qDebug("ImageProcessing::stopped() %lld", m_timerTotal.elapsed());
}

void ImageProcessing::handleEngineeringModeChanged()
{
	//qDebug("handleEngineeringModeChanged pCache->engineeringmode_enabled=%d", pCache->engineeringmode_enabled);
	//ShowTrackbarWindow(e_trackbar_force_normal);
}

void ImageProcessing::handleShowTrackbarWindow()
{
    //TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    //qDebug("handleShowTrackbarWindow pCache->showtrackbar_enabled=%d", pCache->showtrackbar_enabled);
    ShowTrackbarWindow(e_trackbar_force_normal);
}

void ImageProcessing::handleUpdateCVRectItem()
{
    ThresholdCallbackCarClassifierEngineeringMode();
}

void ImageProcessing::handleCheckAdjustedROIEffect()
{
    //Process();
}

void ImageProcessing::handleUpdateTolerance(int newTolerance)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    pCache->dwl_tolerance = newTolerance;
    //m_processingSetting.setTolerance(newTolerance);
    //qDebug("handleUpdateTolerance m_processingSetting.getTolerance()=%d", m_processingSetting.getTolerance());
    //Process();
}

void ImageProcessing::handleUpstreamStarted()
{
    qDebug("ImageProcessing::handleUpstreamStarted started()");

    m_timerTotal.restart();
    m_timerSlice.restart();
    m_currentFrame = 0;
    m_state = e_imageprocessing_state_started;
    emit videoImageProcessingStarted();
}

void ImageProcessing::handleUpstreamStopped()
{
    qDebug("ImageProcessing::handleUpstreamStopped stopped() %lld", m_timerTotal.elapsed());
    m_state = e_imageprocessing_state_stopped;
    emit videoImageProcessingStopped();
}
//QElapsedTimer m_timerCalc;
//void ImageProcessing::handleSendImage(const cv::Mat &matImage)
//{
//	//Mat scaledFrame;
//	//CVImageResize2ByWH(matImage, scaledFrame, width, height);
//	//CVImageResize2ByWH(frame, scaledFrame, frame.cols, frame.rows);
//
//	if (m_matScaledImageList.size() > 5)
//	{
//		m_matScaledImageList.dequeue();
//		m_matScaledImageList.enqueue(matImage);
//	}
//
//	m_matScaledImagePrevious.release();
//	m_matScaledImage.release();
//	m_matScaledImageF.release();
//	m_matScaledImageGray.release();
//
//	m_matScaledImagePrevious = m_matScaledImage.clone();
//	m_matScaledImage = matImage.clone();
//	m_matScaledImage.convertTo(m_matScaledImageF, CV_32FC3, 1.0 / 255, 0);
//	cv::cvtColor(m_matScaledImage, m_matScaledImageGray, COLOR_BGR2GRAY);
//
//	m_timerCalc.start();
//    m_CheckLaneDetectionAndCalibration = IsLaneDetectionEnabled(GetProcessingSetting());
//	ThresholdCallbackAll(0, 0);
//	//qDebug("handleSendImage 1frame msec=%d m_currentFrame=%d", m_timerCalc.elapsed(), m_currentFrame);
//	++m_currentFrame;
//}


const char *ImageProcessing::str_win_hls = "HLS";
const char *ImageProcessing::str_win_hls_h_min = "HMin";
const char *ImageProcessing::str_win_hls_l_min = "LMin";
const char *ImageProcessing::str_win_hls_s_min = "SMin";
const char *ImageProcessing::str_win_hls_h_max = "HMax";
const char *ImageProcessing::str_win_hls_l_max = "LMax";
const char *ImageProcessing::str_win_hls_s_max = "SMax";
const char *ImageProcessing::str_win_hsv = "HSV";
const char *ImageProcessing::str_win_hsv_h_min = "HMin";
const char *ImageProcessing::str_win_hsv_s_min = "SMin";
const char *ImageProcessing::str_win_hsv_v_min = "VMin";
const char *ImageProcessing::str_win_hsv_h_max = "HMax";
const char *ImageProcessing::str_win_hsv_s_max = "SMax";
const char *ImageProcessing::str_win_hsv_v_max = "VMax";
const char *ImageProcessing::str_win_sobel = "Sobel";
const char *ImageProcessing::str_win_canny = "Canny";
const char *ImageProcessing::str_win_mix = "Mix";
const char *ImageProcessing::str_win_car_classifier = "Car Classifier";



//void TestLaneWinRect()
//{ //test region
//  //qDebug("pLaneWinRect count=%d", pLaneWinRect->size());
//	std::vector<cv::Rect>::const_iterator itRect = pLaneWinRect->begin();
//	while (itRect != pLaneWinRect->end()) {
//
//		qDebug("test (*itRect).a() %d %d", (*itRect).x, (*itRect).y);
//		qDebug("test (*itRect).b() %d %d", (*itRect).width, (*itRect).height);
//		qDebug("test (*itRect).br() %d %d", (*itRect).br().x, (*itRect).br().y);
//		qDebug("test (*itRect).tl() %d %d", (*itRect).tl().x, (*itRect).tl().y);
//
//		++itRect;
//	}
//}
#endif