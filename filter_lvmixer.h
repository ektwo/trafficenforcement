#ifndef FILTER_LVMIXER_H
#define FILTER_LVMIXER_H

#include "filter.h"
#include "drawable_graphicsscene.h"


class LVMixerFilter : public Filter
{
    Q_OBJECT
public:
    LVMixerFilter(BaseGraph *parent, int profileIdx);
    ~LVMixerFilter();

    bool ConnectTo(Filter *pDownstreamFilter);
    void DisconnectAll();

public slots:
    void handleUpstreamStarted();
    void handleUpstreamStopped();
    void handleColorFrameCV(const cv::Mat &frame);
    void handleProcessedFrameCV(const cv::Mat &frame) {}
    //LVMixerFilter
    void handleUpdateLanelnfo(const std::vector<cv::Vec4i> &laneArray);
    //void handleUpdateLanelnfo(std::vector<cv::Vec4i> *pLanelineArray);
    void handleUpdateVehicleInfo(const std::vector<cv::Vec4i> &vehicleArray);

protected:
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
    void DrawVehicle(int idx, int x, int y, int drawLevel, 
        cv::Point leftWheel, 
        cv::Point rightWheel, 
        cv::Point pt1, cv::Point pt2,
        int width, int height);
    void ThresholdCallback(int pos, void *userdata);
    void DoImageProcessing(const cv::Mat &matImage);
    void timerEvent(QTimerEvent *event);
#else
    void ThresholdCallbackSelf();
#endif


private:
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
    DrawableGraphicsScene *m_pScene;
    cv::Mat m_matDisplay;
#endif
    cv::Mat m_matScaledImage;
    std::vector<cv::Vec4i> m_laneArray;
    std::vector<cv::Vec4i> m_vehicleArray;
    //QMutex m_mutexOp;
};

#endif // FILTER_LVMIXER_H
