#include "roi_button.h"
#include <QObject>
#include <QPushButton>
#include <QEvent>
#include <QMouseEvent>
#include <QBitmap>
#include "mainwindow.h"
#include "ui_mainwindow.h"

QPoint lastPnt;

ButtonHoverWatcher::ButtonHoverWatcher(QObject *parent)
    : QObject(parent)
    , m_mousePressed(false)
    , m_mouseMoving(false)
{
    m_strNormal = "";
    m_strHovering = "";
}

void ButtonHoverWatcher::SetIcon(const QString &strNormal, const QString &strHovering)
{
    m_strNormal = strNormal;
    m_strHovering = strHovering;
}
MainWindow* getMainWindow()
{
    foreach(QWidget *w, qApp->topLevelWidgets())
        if (MainWindow* mainWin = qobject_cast<MainWindow*>(w))
            return mainWin;
    return nullptr;
}
bool ButtonHoverWatcher::eventFilter(QObject *watched, QEvent *event)
{
    QPushButton *button = qobject_cast<QPushButton*>(watched);
    if (!button) {
        return false;
    }

    if (event->type() == QEvent::HoverEnter){//Enter) {
        // The push button is hovered by mouse
        QIcon icon(m_strHovering);
        button->setIcon(icon);
        //button->setIcon(QIcon(":/images/roi_left_ind_hov.png"));
        return true;
    }
    if (event->type() == QEvent::HoverLeave) {//Leave){
        // The push button is not hovered by mouse
        //qDebug("roi_left_ind");
        QIcon icon(m_strNormal);
        button->setIcon(icon);
        //button->setIcon(QIcon(":/images/roi_left_ind.png"));
        return true;
    }
    if (event->type() == QEvent::DragMove){
        QDragMoveEvent *e = static_cast<QDragMoveEvent*>(event);
        //qDebug("DragMove");
    }
    if (event->type() == QEvent::MouseButtonPress){
        QMouseEvent *e = static_cast<QMouseEvent*>(event);
        if (e->buttons() & Qt::LeftButton)
        {
            lastPnt = e->pos();
            m_mousePressed = true;
            //qDebug("MouseButtonPress");
        }
    }
    else if (event->type() == QEvent::MouseButtonRelease){
        m_mousePressed = false;
        if (m_mouseMoving) {
            m_mouseMoving = false;
            MainWindow *win = (MainWindow *)(this->parent());
            //MainWindow *win = (MainWindow *)getMainWindow();// qApp->activeWindow();
			//Ui::MainWindow *ui = win->getUI();
			if ((e_action_openimagefile_is_opened == win->getActionState()) ||
				(e_action_simulation_is_running == win->getActionState()))
            checkAdjustedROIEffect();
        }
    }

    else if (event->type() == QEvent::MouseMove)
    {
        if (!m_mousePressed) return false;
        m_mouseMoving = true;
        
        MainWindow *win = (MainWindow *)(this->parent());
        //MainWindow *win = (MainWindow *)getMainWindow();// qApp->activeWindow();
        Ui::MainWindow *ui = win->getUI();

        int minY = -1, maxY;
        int minX = -1, maxX;
		int minOppositeX = -1, maxOppositeX;
        QPushButton *btnLineOwner;
        QPushButton *btnOpposite;
        QPushButton *btnAdjacent;

        if (button == static_cast<QPushButton*>(win->btnROILeft1))
        {
            btnAdjacent = static_cast<QPushButton*>(win->btnROILeft2);
            minY = ui->imageView_center->geometry().y();
            maxY = btnAdjacent->geometry().y() - btnAdjacent->height();
            btnOpposite = static_cast<QPushButton*>(win->btnROIRight1);
            btnLineOwner = button;
        }
        else if (button == static_cast<QPushButton*>(win->btnROIRight1))
        {
            btnAdjacent = static_cast<QPushButton*>(win->btnROIRight2);
            minY = ui->imageView_center->geometry().y();
            maxY = btnAdjacent->geometry().y() - btnAdjacent->height();
            btnOpposite = static_cast<QPushButton*>(win->btnROILeft1);
            btnLineOwner = static_cast<QPushButton*>(win->btnROILeft1);
        }
        else if (button == static_cast<QPushButton*>(win->btnROILeft2))
        {
            btnAdjacent = static_cast<QPushButton*>(win->btnROILeft1);
            minY = btnAdjacent->geometry().y() + btnAdjacent->height();
            maxY = ui->imageView_center->geometry().y() + ui->imageView_center->height() - (button->height());
            btnOpposite = static_cast<QPushButton*>(win->btnROIRight2);
            btnLineOwner = button;
        }
        else if (button == static_cast<QPushButton*>(win->btnROIRight2))
        {
            btnAdjacent = static_cast<QPushButton*>(win->btnROIRight1);
            minY = btnAdjacent->geometry().y() + btnAdjacent->height();
            maxY = ui->imageView_center->geometry().y() + ui->imageView_center->height() - (button->height());
            btnOpposite = static_cast<QPushButton*>(win->btnROILeft2);
            btnLineOwner = static_cast<QPushButton*>(win->btnROILeft2);
        }
        else if (button == static_cast<QPushButton*>(win->btnROITop1))
        {
            btnAdjacent = static_cast<QPushButton*>(win->btnROITop2);
            minX = ui->imageView_center->geometry().x();
            maxX = btnAdjacent->geometry().x() - btnAdjacent->width();
            btnOpposite = static_cast<QPushButton*>(win->btnROIBottom1);
            btnLineOwner = button;
        }
        else if (button == static_cast<QPushButton*>(win->btnROIBottom1))
        {
            btnAdjacent = static_cast<QPushButton*>(win->btnROIBottom2);
            minX = ui->imageView_center->geometry().x();
            maxX = btnAdjacent->geometry().x() - btnAdjacent->width();
			btnOpposite = button;// static_cast<QPushButton*>(win->btnROITop1);
            btnLineOwner = static_cast<QPushButton*>(win->btnROITop1);
        }
        else if (button == static_cast<QPushButton*>(win->btnROITop2))
        {
            btnAdjacent = static_cast<QPushButton*>(win->btnROITop1);
            minX = btnAdjacent->x() + btnAdjacent->width();
            maxX = ui->imageView_center->x() + ui->imageView_center->width() - button->width();
            btnOpposite = static_cast<QPushButton*>(win->btnROIBottom2);
            btnLineOwner = button;
        }
        else if (button == static_cast<QPushButton*>(win->btnROIBottom2))
        {
            btnAdjacent = static_cast<QPushButton*>(win->btnROIBottom1);
            minX = btnAdjacent->x() + btnAdjacent->width();
            maxX = ui->imageView_center->x() + ui->imageView_center->width() - button->width();
			btnOpposite = button;// static_cast<QPushButton*>(win->btnROITop2);
            btnLineOwner = static_cast<QPushButton*>(win->btnROITop2);
        }

        QMouseEvent* e = static_cast<QMouseEvent*>(event);
        QPoint movePoint = e->pos() - lastPnt + QPoint(button->geometry().x(), button->geometry().y());

		bool moveX = ((minX > 0) && (movePoint.x() > minX) && (movePoint.x() < maxX));
        bool moveY = ((minY > 0) &&(movePoint.y() > minY) && (movePoint.y() < maxY));
		if (moveX)
		{
			button->move(movePoint.x(), button->pos().y());
			updateROILine2(btnLineOwner, btnOpposite);
		}
		if (moveY)
        {
            button->move(button->pos().x(), movePoint.y());
            btnOpposite->move(btnOpposite->pos().x(), movePoint.y());
            updateROILine(btnLineOwner, QPointF(0, e->pos().y() - lastPnt.y()));
        }
        return false;
    }

    if (event->type() == QEvent::Show) {
        // The push button is not hovered by mouse
        QIcon icon(m_strNormal);
        button->setIcon(icon);
        return true;
    }


    return false;
}
