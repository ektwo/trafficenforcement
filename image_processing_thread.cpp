#include "image_processing_thread.h"
#include <QDebug>
#include <QMutexLocker>
#include <QTimerEvent>
#include "common.h"


using namespace cv;


ImageProcessingThread::ImageProcessingThread(QObject *parent)
    : QObject(parent)
{
    qDebug("ImageProcessingThread::ImageProcessingThread parent=%p", parent);
}

ImageProcessingThread::~ImageProcessingThread()
{
    qDebug("~ImageProcessingThread");

    m_imageList.clear();
    QQueue<cv::Mat> zero;
    m_imageList.swap(zero);
}

void ImageProcessingThread::SetConfig(SCameraImageProcessingConfig *pConfig)
{
    memcpy(&m_config, pConfig, sizeof(m_config));
}

void ImageProcessingThread::handleProcessedFrameCV(const cv::Mat & frame) {

    if (m_processAll)
        process(frame);
    else
        queue(frame);
}

void ImageProcessingThread::queue(const cv::Mat & frame) {

    static int gg = 0;
    if ((gg != 0) && ((gg % 2) == 0))
    {
        if (m_imageList.size() > 10)
        {
            //DSG(1, "queue m_imageList.size() drop = %d", m_imageList.size());
            QMutexLocker locker(&m_mutex);
            Mat image = m_imageList.dequeue();
            image.release();
        }

        m_imageList.enqueue(frame);
        
        if (!m_timer.isActive())
            m_timer.start(0, this);
    }

    ++gg;
}

void ImageProcessingThread::process(cv::Mat frame) {

}

void ImageProcessingThread::stop()
{
    qDebug("ImageProcessingThread::Stop 1");
    if (!m_frame.empty())
        m_frame.release();
    qDebug("ImageProcessingThread::Stop 2");
    if (m_timer.isActive())
        m_timer.stop();
    qDebug("ImageProcessingThread::Stop 3");
}

void ImageProcessingThread::timerEvent(QTimerEvent *ev) {
    //qDebug("ImageProcessingThread::timerEvent ev->timerId()=%d %d", ev->timerId(), m_timer.timerId());
    if (ev->timerId() != m_timer.timerId()) return;

    if (m_imageList.size() > 0)
    {
        QMutexLocker locker(&m_mutex);
        Mat image = m_imageList.dequeue();
        m_frame = image.clone();
        image.release();
    }

    DoImageProcessing(m_frame);

    m_timer.stop();
}
