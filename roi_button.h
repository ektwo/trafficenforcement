#ifndef ROI_BUTTON_H
#define ROI_BUTTON_H

#include <QObject>
#include <QPushButton>

class ButtonHoverWatcher : public QObject
{
    Q_OBJECT
public:
    explicit ButtonHoverWatcher(QObject * parent = Q_NULLPTR);
    void SetIcon(const QString &strNormal, const QString &strHovering);

    virtual bool eventFilter(QObject * watched, QEvent * event) Q_DECL_OVERRIDE;

signals:
    void updateROILine(QPushButton *button, QPointF startOffset);
	void updateROILine2(QPushButton *pButton, QPushButton *pOppositeButton);
    void checkAdjustedROIEffect();

public slots:

private:
    bool m_mousePressed;
    bool m_mouseMoving;
    QString m_strNormal;
    QString m_strHovering;
};

#endif // IMAGEBUTTON_H
