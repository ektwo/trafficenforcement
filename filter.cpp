#include "filter.h"
#include <QTimerEvent>
#include "common.h"


FilterThread::FilterThread(BaseGraph *parent)
    : QObject(nullptr)
    //, m_readyToEmit(true)
    , m_nTimerID(-1)
    , m_pOwner(parent)
{
    qDebug("FilterThread");
}

FilterThread::~FilterThread()
{
    qDebug("~FilterThread");

    m_imageList.clear();
    QQueue<cv::Mat> zero;
    m_imageList.swap(zero);
}

//void FilterThread::handleProcessedFrameCV(const cv::Mat &frame)
//{
//    {
//        QMutexLocker locker(&m_mutex);
//        if (m_imageList.size() > 10)
//        {
//            DSG(1, "queue m_imageList.size() drop = %d", m_imageList.size());
//            cv::Mat image = m_imageList.dequeue();
//            image.release();
//        }
//
//        m_imageList.enqueue(frame);
//    }
//
//    //if (m_readyToEmit)
//    {
//        //if (!m_timer.isActive())
//        //    m_timer.start(0, this);
//    }
//}

void FilterThread::stop()
{
    qDebug("ImageProcessingThread::Stop 1");
//    if (!m_frame.empty())
//        m_frame.release();

    //if (m_timer.isActive())
    //    m_timer.stop();
    qDebug("ImageProcessingThread::Stop 3");
}
/*
void FilterThread::timerEvent(QTimerEvent *event) 
{
    qDebug("FilterThread::timerEvent (%s) ev->timerId()=%d %d", m_name, event->timerId(), m_timer.timerId());
    if (event->timerId() != m_timer.timerId()) return;
    //qDebug("FilterThread::timerEvent (%s) 1", m_name);
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 0)
        {
            cv::Mat image = m_imageList.dequeue();
            m_frame = image.clone();
            image.release();
        }
    }

    //qDebug("FilterThread::timerEvent (%s) m_frame=%d %d", m_name, m_frame.cols, m_frame.rows);
    DoImageProcessing(m_frame);

    m_timer.stop();

    //Sleep(1);
}
*/
Filter::Filter(BaseGraph *parent)
    : FilterThread(parent)
    , m_startedUpstreamFilters(0)
    , m_currentFrame(0)
    , m_state(e_component_state_idle)
{
    m_name = k_str_unknown_filter_name;
}

Filter::~Filter()
{
    m_pUpstreamFilterList.clear();
    m_pUpstreamFilterList.shrink_to_fit();
    m_pDownstreamFilterList.clear();
    m_pDownstreamFilterList.shrink_to_fit();
}

bool Filter::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = false;
    DSG(1, "Filter::ConnectTo=%p", pDownstreamFilter);
    if (pDownstreamFilter)
    {
        pDownstreamFilter->NotifyUpstreamPlugin(this); //my

        m_pDownstreamFilterList.push_back(pDownstreamFilter);

        DSG(1, "Filter::ConnectTo filter[%s] have %d downstreamfilters", 
            m_name, m_pDownstreamFilterList.size());

        m_state = e_component_state_initialized;

        ret = true;
    }

    return ret;
}

void Filter::DisconnectAll()
{
    if ((!m_pUpstreamFilterList.empty()) && 
        (0 == strcmp(m_pUpstreamFilterList[0]->m_class, k_str_class_source_name)))
    {
        DSG(1, "Filter::DisconnectAll UPSTREAM IS SOURCE m_name=%s", m_name);
        disconnect(m_pOwner, SIGNAL(sourceAllStarted()), 0, 0);
        disconnect(m_pOwner, SIGNAL(sourceAllStopped()), 0, 0);
    }

    m_pUpstreamFilterList.clear();
    m_pUpstreamFilterList.shrink_to_fit();
    m_pDownstreamFilterList.clear();
    m_pDownstreamFilterList.shrink_to_fit();

    m_state = e_component_state_idle;
}

void Filter::NotifyUpstreamPlugin(Filter *pUpstreamFilter)
{
    DSG(1, "Filter::NotifyUpstreamPlugin m_name=%s ", m_name);

    if (0 == strcmp(pUpstreamFilter->m_class, k_str_class_source_name))
    {
        DSG(1, "UPSTREAM IS SOURCE");
        connect(m_pOwner, SIGNAL(sourceAllStarted()), this, SLOT(handleUpstreamStarted()));
        connect(m_pOwner, SIGNAL(sourceAllStopped()), this, SLOT(handleUpstreamStopped()));
    }

    m_pUpstreamFilterList.push_back(pUpstreamFilter);
}

void Filter::handleUpstreamStarted()
{
    qDebug("Filter::handleUpstreamStarted [%s]", m_name);

//    m_currentFrame = 0;
    m_state = e_component_state_started;

    emit iamStarted();
}

void Filter::handleUpstreamStopped()
{
    qDebug("Filter::handleUpstreamStopped [%s]", m_name);

    m_state = e_component_state_stopped;

    emit iamStopped();
}

//void Filter::timerEvent(QTimerEvent *event)
//{
//    qDebug("Filter::timerEvent (%s) ev->timerId()=%d %d", m_name, event->timerId(), m_timer.timerId());
//    if (event->timerId() != m_timer.timerId()) return;
//    //qDebug("FilterThread::timerEvent (%s) 1", m_name);
//    {
//        QMutexLocker locker(&m_mutex);
//        if (m_imageList.size() > 0)
//        {
//            cv::Mat image = m_imageList.dequeue();
//            m_frame = image.clone();
//            image.release();
//        }
//    }
//
//    //qDebug("FilterThread::timerEvent (%s) m_frame=%d %d", m_name, m_frame.cols, m_frame.rows);
//    DoImageProcessing(m_frame);
//
//    m_timer.stop();
//
//    //Sleep(1);
//}