#include "parameters_table.h"


SParametersTable k_parameters_table[] = {
    { "20181005162701.mov", mask_hls_and_hsv_and_sobel,
        { 0, 39, 7, k_hls_h_max, k_hls_l_max, k_hls_s_max, nullptr },
        { 0, 0, 80, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max, nullptr },
        { 3, 3, 1, 3, 255, 5, 255, 0, nullptr },
        { 34, 268, 0, 1, nullptr },
        { 2, 160, 214, 21, 515 },
        //{ 558,20,510,710 },
        //{ 660,20,888,710 },
        { 838,20,810,710 },
        { 980,20,1208,710 },
        { 0, 1, 0, 40, 500 },
        nullptr
    },

    {
        "DSCF4644.MOV", mask_hls_and_hsv_and_sobel,
        { 0, 0, 125, k_hls_h_max, k_hls_l_max, k_hls_s_max, nullptr },
        { 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max, nullptr },
        { 3, 3, 1, 3, 255, 5, 255, 0, nullptr },
        { 74, 306, 0, 0, nullptr },
        { 0, 150, 214, 215, 515 },
        { 610,20,610,720 },
        { 720,20,1010,710 },
        { 0, 1, 0, 40, 500 },
        nullptr
    },
    nullptr

//{ "20181005162701-2-1.MOV", mask_hls_and_hsv_and_sobel,{ 0, 39, 7, k_hls_h_max, k_hls_l_max, 190 },{ 0, 0, 80, 360, 255, k_hsv_v_max },{ 3, 3, 1, 5, 255, 0, 255, 0 },{ 34, 268, 0, 1 },{ 2, 160, 214, 215, 515 },{ 558,20,510,710 },{ 660,20,888,710 },{ 0, 1, 0, 40, 500 } },
//
//{ "20181005162701-2-2.mov", mask_hls_and_hsv_and_sobel,{ 0, 39, 7, k_hls_h_max, k_hls_l_max, 190 },{ 0, 0, 80, 360, 255, k_hsv_v_max },{ 3, 3, 1, 5, 255, 0, 255, 0 },{ 34, 268, 0, 1 },{ 2, 160, 214, 215, 515 },{ 558,20,510,710 },{ 660,20,888,710 },{ 0, 1, 0, 40, 500 } },
//
//{ "20181005162701-2.mov", mask_hls_and_hsv_and_sobel,{ 0, 110, 0, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 117, 340, 80, k_hsv_v_max },{ 3, 3, 1, 13, 255, 0, 255, 0 },{ 34, 268, 0, 1 },{ 2, 160, 214, 215, 515 },{ 558,20,510,710 },{ 660,20,888,710 },{ 0, 1, 0, 40, 500 } },
//
//
//
//    //{ "20181005162701.MOV", mask_hls_and_hsv_and_sobel,{ 0, 110, 0, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 117, 340, 50, k_hsv_v_max },{ 3, 3, 1, 13, 255, 0, 255, 0 },{ 34, 268, 0, 1 },{ 2, 160, 214, 215, 515 },{558,20,510,710},{660,20,888,710}, { 0, 1, 0, 40, 500 } },
//	{ "VID_20180704_201509.mp4", mask_hls_and_hsv_or_sobel, { 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max }, { 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max }, { 3, 3, 1, 3, 255, 5, 255, 0}, { 74, 306, 0, 0}, {0, 150, 214, 215, 515},{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500} },
//	{ "DSCF4925.MOV", mask_hls_and_hsv_or_sobel, { 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max }, { 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
////	{ "DSCF4453.MOV", mask_hls_and_hsv_or_sobel, { 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max }, { 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{0,0,0,0},{0,0,0,0},{ 0, 1, 0, 40, 500 } },
////	{ "DSCF4453.MOV", mask_hls_and_hsv_or_sobel, { 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max }, { 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{0,0,0,0},{0,0,0,0},{ 0, 1, 0, 105, 500 } },
//	{ "DSCF4453.MOV", mask_hls_and_hsv_or_sobel, { 0, 0, 212, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 1, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 201, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//
//	//{ "DSCF4644.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{0,0,0,0},{0,0,0,0},{ 0, 1, 0, 40, 500 } },
//	{ "DSCF4644.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 125, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//
//	{ "DSCF4450.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 175, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 155, 190, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	{ "DSCF4447.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	{ "DSCF4449.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//
//	{ "DSCF4452.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{610,20,610,710},{ 720,20,1070,710 },{ 0, 1, 0, 40, 500 } },
//	{ "DSCF44472.avi", mask_hls_and_hsv_or_sobel,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//
//	{ "DSCF4615.MOV", mask_hls_or_hsv_or_sobel,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 125, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//
//	{ "DSCF4616.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 46, 360, 43, 255 },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 125, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	//{ "DSCF4616.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 26, 43, 46, 68, 255, 255 },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 125, 214, 215, 515 },{ 0, 1, 0, 40, 500 } },
//
//	{ "DSCF4448.MOV", mask_hls_and_hsv_or_sobel,{ 0, 0, 175, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 155, 190, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	//{ "DSCF4448.MOV", mask_gray_equalizehist_only,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 46, 360, 43, 255 },{ 3, 3, 1, 8, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 125, 214, 215, 515 },{ 0, 1, 0, 40, 500 } },
//
//	{ "DSCF4449.MOV", mask_gray_equalizehist_only,{ 0, 0, 160, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 0, 1, 0, 55, 100, 0, 0, 0 },{ 74, 306, 0, 0 },{ 0, 125, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	{ "DSCF4451.MOV", mask_gray_equalizehist_only,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	//without middleline, remove electric power line is needed.
//	{ "DSCF4663.MOV", mask_gray_equalizehist_only,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	{ "DSCF4612.MOV", mask_gray_equalizehist_only,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	//without middleline, bad error line
//	{ "DSCF4454.MOV", mask_gray_equalizehist_only,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	//without middleline, bad only one line.
//	{ "DSCF4452.MOV", mask_gray_equalizehist_only,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//	{ "DSCF4447.MOV", mask_gray_equalizehist_only,{ 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 214, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },
//
//	{ "DSCF4460.JPG", mask_hls_and_hsv_or_sobel, { 0, 0, 170, k_hls_h_max, k_hls_l_max, k_hls_s_max },{ 0, 0, 0, k_hsv_h_max, k_hsv_s_max, k_hsv_v_max },{ 3, 3, 1, 3, 255, 5, 255, 0 },{ 74, 306, 0, 0 },{ 0, 150, 153, 215, 515 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0, 1, 0, 40, 500 } },

};



