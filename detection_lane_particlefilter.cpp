#include <opencv2/opencv.hpp>
using namespace cv;


#if 0
/**
 * 对车道线进行粒子滤波
 *
 * @return vector<int>    两个坐标，是滤波后的车道线的 x 坐标
 */
template <typename T>
vector<int> lanePF(Mat& _iInput) {
    int i;

    static int stateNum = 2;
    static int measureNum = 2;
    static int sampleNum = 2000;


    static CvMat *lowerBound = cvCreateMat(stateNum, 1, CV_32F);
    static CvMat *upperBound = cvCreateMat(stateNum, 1, CV_32F);

    int peakLen = 8;
    int borderLen = 3;
    int totalLen = peakLen + borderLen * 2;
    float stdLine[totalLen] = {0};

    Mat iInput(_iInput, Rect(0, 0, _iInput.cols / 2, _iInput.rows));


    static CvConDensation *con = NULL;
    if (con == NULL) {
        /// 初始化粒子滤波器
        con = cvCreateConDensation(stateNum, measureNum, sampleNum);

        cvmSet(lowerBound, 0, 0, 0.0);
        cvmSet(lowerBound, 1, 0, 0.0);

        cvmSet(upperBound, 0, 0, iInput.cols - totalLen - 1);
        cvmSet(upperBound, 1, 0, iInput.rows - totalLen - 1);
        float A[2][2] = {
            1, 0,
            0, 1
        };
        memcpy(con->DynamMatr, A, sizeof(A));


        cvConDensInitSampleSet(con, lowerBound, upperBound);

        CvRNG rng_state = cvRNG(time(NULL));
        for(int i=0; i < sampleNum; i++){
            con->flSamples[i][0] = float(cvRandInt( &rng_state ) % iInput.cols); //width
            con->flSamples[i][1] = float(cvRandInt( &rng_state ) % iInput.rows);//height
        }

    }

    /// 初始化标准曲线
    for (int i = borderLen - 1; i < borderLen - 1 + peakLen; i++) {
        stdLine[i] = 255;
    }



    /// 计算概率（阈值车道响应拟合法）
    /*
    float maxp = 0;
    for (i = 0; i < sampleNum; i++) {
        int x, y, xp, yp, k;
        float e, p;

        x = con->flSamples[i][0];
        y = con->flSamples[i][1];


        e = 0;

        for (k = 0; k < totalLen; k++) {
            xp = x + k;
            yp = y;

            float d;
            //printf("k = %d, point=%d, stdlint=%f\n", k, iInput->ptr<T>(yp)[xp], stdLine[k]);

            d = iInput.ptr<T>(yp)[xp];
            d = abs(d - stdLine[k]);
            d /= 255.0;
            e += d;
        }
        e /= 255.0;

        p = exp(-1 * e);

        if (p > maxp) {
            maxp = p;
        }

        con->flConfidence[i] = p;
    }
    */


    /// 计算概率（垂直像素最多法）

    for (i = 0; i < sampleNum; i++) {
        int x, y;
        float e, p;

        p = 0; e = 0;

        for (y = 0; y < iInput.rows; y++) {
            x = con->flSamples[i][0] + (con->flSamples[i][1] - con->flSamples[i][0]) * (1.0 * y / iInput.rows);
            e += iInput.ptr<T>(y)[x];
        }
        e /= 255.0;
        if (e > iInput.rows) {
            e = 0;
        }

        p = exp(-0.1 * (iInput.rows - e));
        con->flConfidence[i] = p;
        //cout<<"x1="<<con->flSamples[i][0]<<", x2="<<con->flSamples[i][1]<<", P="<<p<<endl;
    }
    cout<<"更新前："<<con->State[0]<<endl;
    cvConDensUpdateByTime(con);



    /*
    for (i = 0; i < sampleNum; i++) {
        Point p1(con->flSamples[i][0], con->flSamples[i][1]);
        Point p2(p1.x + totalLen, p1.y);

        line(iInput, p1, p2, CV_RGB(128, 128, 128), 1);
    }
    */

    line(_iInput, Point(con->State[0], 0), Point(con->State[1], iInput.rows), CV_RGB(128, 128, 128), 1);
    cout<<"预测位置："<<con->State[0]<<endl;


    vector<int> ret;

    ret.push_back(con->State[0]);
    ret.push_back(con->State[1]);

    return ret;
}

#endif

void CreateParticleFilter()
{
#if 0
    vector<int> pfXs;
    pfXs = lanePF<uint8_t>(imgROIGrayIPMGaussianHistThreshold);
#endif

}

void DrawLine_ParticleFilter(Mat &matrixIPMInv)
{
#if 0
    /// 在原图上画出粒子滤波车道线
    ps.clear();
    ps.push_back(Point(pfXs[0], 0));
    ps.push_back(Point(pfXs[1], imgROIGrayIPMGaussianHistThreshold.rows));
    perspectiveTransform(ps, psOut, matrixIPMInv);

    for (size_t i = 0; i < psOut.size(); i++) {
        psOut.at(i).x += roiLane.x;
        psOut.at(i).y += roiLane.y;
    }

    /// 粒子车道线
    line(imgOrigin, psOut[0], psOut[1], CV_RGB(255, 0, 0), 2, CV_AA);
#endif

}
