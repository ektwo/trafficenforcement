#include "trvideothread.h"
#include <QTimerEvent>
#include <QDebug>
#include <QPainter>
#include "common.h"
#include "image_common.h"
#include "traffic_enforcement.h"
#include "libyuv.h"


#define MAKE_FOURCC(a,b,c,d)\
        (((uint32_t)(a)<<0)|((uint32_t)(b)<<8)|((uint32_t)(c)<<16)|((uint32_t)(d)<<24))
static const uint32_t k_out_format = MAKE_FOURCC('B', 'G', 'R', '3');

using namespace QtAV;

using namespace std;
using namespace cv;

static const int k_limited_frames = 100;


//extern LIBYUV_API
//int ConvertFromI420(const uint8_t* y,
//    int y_stride,
//    const uint8_t* u,
//    int u_stride,
//    const uint8_t* v,
//    int v_stride,
//    uint8_t* dst_sample,
//    int dst_sample_stride,
//    int width,
//    int height,
//    uint32_t fourcc);

#define USE_QTAV
TRVideoFileSource::TRVideoFileSource(BaseGraph *parent)
	: AbstractSource(parent)
    //: QObject(parent)
    //, m_state(e_component_state_idle)
	//, m_timerId(-1)
	, m_currentFrame(0)
    , m_pInBuffer(nullptr)
    , m_pOutBuffer(nullptr)
{
    m_class = k_str_class_source_name;
    m_name = k_str_video_file_filter_name;
    DSG(1, "TRVideoFileSource m_class=%s", m_class);
    DSG(1, "TRVideoFileSource m_name=%s", m_name);

	memset(&m_config, 0, sizeof(m_config));
    memset(&m_capability, 0, sizeof(m_capability));

    connect(this, &TRVideoFileSource::started, (BaseGraph*)parent, &BaseGraph::handleAllSourceStarted);
    connect(this, &TRVideoFileSource::stopped, (BaseGraph*)parent, &BaseGraph::handleAllSourceStopped);

    DSG(1, "TRVideoFileSource::TRVideoFileSource this=%p parent=%p", this, parent);

    //namedWindow("TRV", CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);
}

TRVideoFileSource::~TRVideoFileSource()
{
	qDebug("~TRVideoFileSource");

    if (m_pInBuffer)
    {
        delete m_pInBuffer;
        m_pInBuffer = nullptr;
    }
    if (m_pOutBuffer)
    {
        delete m_pOutBuffer;
        m_pOutBuffer = nullptr;
    }

    Close();
    disconnect(this, &TRVideoFileSource::stopped, 0, 0);
    disconnect(this, &TRVideoFileSource::started, 0, 0);
    qDebug("~TRVideoFileSource 2");
}

bool TRVideoFileSource::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = __super::ConnectTo(pDownstreamFilter);
    DSG(1, "TRVideoFileSource::ConnectTo=%p", pDownstreamFilter);

    connect(this, &TRVideoFileSource::cvColorReady, pDownstreamFilter, &Filter::handleColorFrameCV);

    return ret;
}

void TRVideoFileSource::DisconnectAll()
{
    __super::DisconnectAll();

    disconnect(this, &TRVideoFileSource::cvColorReady, 0, 0);
    disconnect(this, &TRVideoFileSource::qColorReady, 0, 0);
}

#ifdef USE_QTAV
bool TRVideoFileSource::SetConfig(void *pConfig)
{
    bool ret = false;

    memcpy(&m_config, pConfig, sizeof(SVideoConfig));
    DSG(1, "TRVideoFileSource::SetConfig 1");
    m_qtavReader.setMedia(QString(m_config.filename));
    DSG(1, "TRVideoFileSource::SetConfig hasVideoFrame=%d",
        m_qtavReader.hasVideoFrame());

    if (m_pOutBuffer)
    {
        delete m_pOutBuffer;
        m_pOutBuffer = nullptr;
    }
    if (!m_pOutBuffer)
        m_pOutBuffer = static_cast<uint8_t*>(malloc(m_config.want_width * m_config.want_height * 3));

    ret = true;
    //if (ret)
    {
//        Size videoSize = Size(
//            (int)m_videoCapture->get(CV_CAP_PROP_FRAME_WIDTH),
//            (int)m_videoCapture->get(CV_CAP_PROP_FRAME_HEIGHT));


        //qDebug("TRVideoFileSource::SetConfig file width=%d height=%d", videoSize.width, videoSize.height);
        //qDebug("TRVideoFileSource::SetConfig file frame_cnt=%d", m_capability.frame_cnt);

        {
            //Mat first;
            //GetFirstFrame(first);
            //testframe(first);
            //Sleep(1000);
        }
//        if (-1 == m_config.interval_ms)
//        {
//            double fps;
//            fps = m_videoCapture->get(CV_CAP_PROP_FPS);
//            qDebug("video file fps=%f", fps);
//            if (0 == fps)
//            {
//                double currentFrame;
//                double currentTime;
//                int count = 30;

//                //QTimer *m_pTimer;
//                Mat frame;

//                m_timerCalc.start();
//                qDebug("video file fps test is beginning");
//                for (;;) {
//                    *m_videoCapture >> frame;
//                    --count;
//                    if (0 == count)
//                        break;
//                }
//                qDebug("ms2=%d", m_timerCalc.elapsed());
//                fps = 100 / (m_timerCalc.elapsed() / 1000);

//            }

//            qDebug("fps2=%f", fps);
//            m_config.interval_ms = static_cast<int>(1000 / fps);// (fps / 2));
//        }
        DSG(1, "m_config.interval_ms=%lu", m_config.interval_ms);
    }

    return ret;
}
#else
bool TRVideoFileSource::SetConfig(void *pConfig)
{
    bool ret = false;

	memcpy(&m_config, pConfig, sizeof(SVideoConfig));

	if (!m_videoCapture)
		m_videoCapture.reset(new cv::VideoCapture());

	ret = m_videoCapture->open(m_config.filename);
    if (ret)
    {
	    Size videoSize = Size(
            (int)m_videoCapture->get(CV_CAP_PROP_FRAME_WIDTH), 
            (int)m_videoCapture->get(CV_CAP_PROP_FRAME_HEIGHT));

        m_capability.width = videoSize.width;
        m_capability.height = videoSize.height;
        m_capability.frame_cnt = m_videoCapture->get(CV_CAP_PROP_FRAME_COUNT);

        qDebug("TRVideoFileSource::SetConfig file width=%d height=%d", videoSize.width, videoSize.height);
	    qDebug("TRVideoFileSource::SetConfig file frame_cnt=%d", m_capability.frame_cnt);

	    {
		    //Mat first;
		    //GetFirstFrame(first);
		    //testframe(first);
		    //Sleep(1000);
	    }
	    if (-1 == m_config.interval_ms)
	    {
		    double fps;
		    fps = m_videoCapture->get(CV_CAP_PROP_FPS);
		    qDebug("video file fps=%f", fps);
		    if (0 == fps)
		    {
			    double currentFrame;
			    double currentTime;
			    int count = 30;

			    //QTimer *m_pTimer;
			    Mat frame;
			
			    m_timerCalc.start();
			    qDebug("video file fps test is beginning");
			    for (;;) {
				    *m_videoCapture >> frame;
				    --count;
				    if (0 == count)
					    break;
			    }
			    qDebug("ms2=%d", m_timerCalc.elapsed());
			    fps = 100 / (m_timerCalc.elapsed() / 1000);

		    }

		    qDebug("fps2=%f", fps);
		    m_config.interval_ms = static_cast<int>(1000 / fps);// (fps / 2));
	    }
        DSG(1, "m_config.interval_ms=%lu", m_config.interval_ms);
    }

	return ret;
}
#endif
bool TRVideoFileSource::Open()
{
    if (e_component_state_idle == m_state)
    {
#ifdef USE_QTAV
        Mat frame;
        //if (GetFirstFrame(frame))
            m_state = e_component_state_initialized;
#else
        if ((!m_videoCapture.isNull()) && (m_videoCapture->isOpened()))
        {
            m_state = e_component_state_initialized;
        }
#endif
    }

    return (e_component_state_initialized == m_state);

}

void TRVideoFileSource::Close()
{
    InternalStop();
#ifdef USE_QTAV
    while (m_qtavReader.hasVideoFrame()) {
        const VideoFrame f = m_qtavReader.getVideoFrame();
        qDebug("pts: %.3f", f.timestamp());
    }
    //m_qtavReader.deleteLater();
#else
    if (m_videoCapture->isOpened())
        m_videoCapture->release();
#endif
}

#ifdef USE_QTAV
bool TRVideoFileSource::GetFirstFrame(cv::Mat &frame)
{
    bool ret = false;

    VideoFrame f;

    do
    {
        ret = m_qtavReader.readMore();
        if (!ret) break;
                qDebug("has3=%d",m_qtavReader.hasVideoFrame());
                qDebug("has4=%d",m_qtavReader.getFrameCount());
                qDebug("has5=%d",m_qtavReader.hasEnoughVideoFrames());
        while (!m_qtavReader.hasEnoughVideoFrames())
            ;

        f = m_qtavReader.getVideoFrame();
        if (f)
        {
            m_capability.width = f.width();
            m_capability.height = f.height();
            m_capability.frame_cnt = m_qtavReader.getFrameCount();

            SendFrameFast(f);
            //SendFrame(f);        
            //DSG(1, "one frame ");
            m_qtavReader.seek(0);
            ret = true;
            break;
        }

    } while (ret);

    return ret;
}
#else
bool TRVideoFileSource::GetFirstFrame(cv::Mat &frame)
{
    bool ret = false;
	//*m_pCapture >> frame;
    if (m_videoCapture)
    {
        Mat img;
	    
	    
        m_videoCapture->read(img);
    #ifdef DO_PRESCALE
        if ((m_config.want_width != img.cols) || (m_config.want_height != img.rows))
        {
            CVImageResize2ByWH(img, frame, m_config.want_width, m_config.want_height);
        }
    #else
        frame = img;
    #endif

	    m_videoCapture->set(CV_CAP_PROP_POS_FRAMES, 0);

        ret = true;
    }

    return ret;
}
#endif
void TRVideoFileSource::start(int cam) {

    qDebug("TRVideoFileSource::start");
    if (InternalStart())
        emit started();
}

void TRVideoFileSource::stop()
{
    qDebug("TRVideoFileSource::stop");
    InternalStop();
    emit stopped();
}

bool TRVideoFileSource::InternalStart()
{
    bool ret = false;

    qDebug("TRVideoFileSource::InternalStart m_state=%d", m_state);
    if ((e_component_state_initialized == m_state) ||
        (e_component_state_stopped == m_state))
    {
    #ifdef USE_QTAV
        if (m_qtavReader.hasVideoFrame())
    #else
        if (m_videoCapture->isOpened())
    #endif
        {
            m_timerId = startTimer(m_config.interval_ms, Qt::CoarseTimer);

            m_timerCalc.start();

            m_state = e_component_state_started;
            qDebug("TRVideoFileSource::start emit started");
            ret = true;
        }
    }

    return ret;
}

void TRVideoFileSource::InternalStop()
{
    m_currentFrame = 0;
    if (e_component_state_started == m_state)
    {
        //m_timer.stop();
    #ifdef USE_QTAV
        DSG(1, "TRVideoFileSource::InternalStop()");
        m_qtavReader.seek(0);
    #endif
        if (-1 != m_timerId)
        {
            killTimer(m_timerId);
            m_timerId = -1;
        }

        m_state = e_component_state_stopped;
    }
}

void TRVideoFileSource::SendFrameFast(const QtAV::VideoFrame &videoFrame)
{
    //QtAV::VideoFormat format = videoFrame.format();
    //DSG(1, "m_qtavReader.hasEnoughVideoFrames() pixelFormat=%d", format.pixelFormat());

    //const int frameWidth = videoFrame.width();
    //const int frameHeight = videoFrame.height();
    //DSG(1, "m_qtavReader.hasEnoughVideoFrames() frameWidth=%d", frameWidth);
    //DSG(1, "m_qtavReader.hasEnoughVideoFrames() frameHeight=%d", frameHeight);
    //DSG(1, "m_qtavReader.hasEnoughVideoFrames() planeCount=%d", f.planeCount());
    //for (int i = 0; i < f.planeCount(); ++i)
    //    DSG(1, "m_qtavReader.hasEnoughVideoFrames() bytesPerLine=%d", f.bytesPerLine(i));

    {
        const uint8_t *i420_p0 = videoFrame.bits(0);
        const uint8_t *i420_p1 = videoFrame.bits(1);
        const uint8_t *i420_p2 = videoFrame.bits(2);

        libyuv::ConvertFromI420(i420_p0, videoFrame.bytesPerLine(0),
            i420_p1, videoFrame.bytesPerLine(1),
            i420_p2, videoFrame.bytesPerLine(2),
            m_pOutBuffer, 0, m_config.want_width, m_config.want_height, k_out_format);
    }
    //webrtc::ConvertFromI420(*frame, webrtc::kBGRA, 0, m_buffer.data());
    //glBindTexture(GL_TEXTURE_2D, m_texture);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_size.width(), m_size.height(), 0, GL_BGRA,
    //    GL_UNSIGNED_INT_8_8_8_8, static_cast<GLvoid*>(m_buffer.data()));
    Mat frameA = Mat(m_config.want_height, m_config.want_width, CV_8UC3, m_pOutBuffer);
    QImage img = QImage(m_pOutBuffer, m_config.want_width, m_config.want_height, QImage::Format_RGB888);
    //DSG(1, "source frameA=%d %d", frameA.cols, frameA.rows);
    emit cvColorReady(frameA);
    //emit qColorReady(img);
}

void TRVideoFileSource::SendFrame(const QtAV::VideoFrame &videoFrame)
{
    cv::Mat scaledFrame;
    QImage image(videoFrame.toImage());////QImage::Format_RGB888,
    cv::Mat frame = QImage2cvMat(image);
    CVImageResize2ByWH(frame, scaledFrame, m_config.want_width, m_config.want_height);
    emit cvColorReady(scaledFrame);
}

void TRVideoFileSource::DoImageProcessing(const cv::Mat &matImage)
{
 
}
#ifdef USE_QTAV
void TRVideoFileSource::timerEvent(QTimerEvent * event) {

    //qDebug("TRVideoFileSource::timerEvent (%s) ev->timerId()=%d %d", m_name, event->timerId(), m_timerId);
    if (event->timerId() != m_timerId) return;
    //qDebug("TRVideoFileSource::timerEvent m_state=%d", m_state);
    if (e_component_state_started != m_state) return;

    bool ret = false;

    VideoFrame f;

    do
    {
        ret = m_qtavReader.readMore();
        if (!ret) break;

        while (!m_qtavReader.hasEnoughVideoFrames())
            ;

        f = m_qtavReader.getVideoFrame();
        if (f)
        {
            //DSG(1, "11111");
            SendFrameFast(f);
            //SendFrame(f);
        }
        ret = true;
        break;

    } while (ret);
//DSG(1, "22222");
    if (!ret)
    {
        killTimer(m_timerId);
        m_timerId = -1;
    }
    else
    {
        //DSG(1, "TRVideoFileSource::timerEvent, frame.cols=%d frame.rows=%d frame.type()=%d",
        //    frame.cols, frame.rows, frame.type());
        TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
        pCache->CheckLaneDetectionAndCalibration(m_currentFrame);

        ++m_currentFrame;
        if ((m_currentFrame % 30) == 0)
            DSG(1, "TRVideoFileSource::timerEvent, m_currentFrame=%d",m_currentFrame);
    }
}
#else
void TRVideoFileSource::timerEvent(QTimerEvent * event) {

    //qDebug("TRVideoFileSource::timerEvent (%s) ev->timerId()=%d %d", m_name, event->timerId(), m_timerId);
	if (event->timerId() != m_timerId) return;

	cv::Mat frame;
	if (m_videoCapture->isOpened())
	{
        //DSG(1, "m_currentFrame=%d m_capability.frame_cnt=%d", m_currentFrame, m_capability.frame_cnt);
		if (m_currentFrame < m_capability.frame_cnt)
		{
            //DSG(1, "m_currentFrame=%d 22 m_capability.frame_cnt=%d", m_currentFrame, m_capability.frame_cnt);
            //if (m_currentFrame < 60)
            {
                Mat img;
                //DSG(1, "m_currentFrame=%d 33 m_capability.frame_cnt=%d", m_currentFrame, m_capability.frame_cnt);
                if (!m_videoCapture->read(img)) { // Blocks until a new frame is ready
                    qDebug("TRVideoFileSource::timerEvent bye");
                    m_videoCapture->set(CV_CAP_PROP_POS_FRAMES, 0);
                    m_videoCapture->read(img);
                    m_currentFrame = 0;
                    return;
                }

            #ifdef DO_PRESCALE
                if ((m_config.want_width != img.cols) || (m_config.want_height != img.rows))
                {
                    CVImageResize2ByWH(img, frame, m_config.want_width, m_config.want_height);
                }
            #else
                frame = img;
            #endif

                DSG(1, "TRVideoFileSource::timerEvent, frame.cols=%d frame.rows=%d frame.type()=%d",
                    frame.cols, frame.rows, frame.type());
			    ++m_currentFrame;
                if ((m_currentFrame % 30) == 0)
                    DSG(1, "TRVideoFileSource::timerEvent, m_currentFrame=%d",m_currentFrame);

                //if ((m_currentFrame >= k_limited_frames) && (-1 != m_timerId))	
			    if ((m_currentFrame >= m_capability.frame_cnt) && (-1 != m_timerId))
			    {
				    killTimer(m_timerId);
				    m_timerId = -1;
				    //qDebug("TRVideoCapture::timerEvent emit 2 timerId=%d", m_timerId);
			    }

                //imshow("TRV", frame);
                //if (m_currentFrame == 1)
                    //emit cvColorReady(frame);
            }
		}
		//qDebug("TRVideoCapture::timerEvent emit matReady 2");
	}
}
#endif
