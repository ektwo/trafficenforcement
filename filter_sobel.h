#ifndef FILTER_SOBEL_H
#define FILTER_SOBEL_H

#include "filter.h"
#include "escort_graphicsscene.h"

using namespace cv;
using namespace std;

class SobelFilter : public Filter
{
    Q_OBJECT
public:
    SobelFilter(BaseGraph *parent, int profileIdx);
    ~SobelFilter();

    bool ConnectTo(Filter *pDownstreamFilter);
    void DisconnectAll();

public slots:
    void handleUpstreamStarted();
    void handleUpstreamStopped();
    void handleColorFrameCV(const cv::Mat &frame);
    void handleProcessedFrameCV(const cv::Mat &frame) {}

protected:
    void PrepareImageForLaneDetection(const Mat &matScaledOrgClone);
    void ReactFromTrackbar(const Mat &in, Mat &maskX, Mat &maskY);
    void UpdateOutputMat(const Mat &src, const Mat &mask, Mat &dst);
    void ThresholdCallback(int pos, void *userdata);
    void DoImageProcessing(const Mat &matImage);
    virtual void timerEvent(QTimerEvent *event);

private:
    int m_dxyMode;
    int m_kSizeX;
    int m_kSizeY;
    int m_thresXMin;
    int m_thresXMax;
    int m_thresYMin;
    int m_thresYMax;
    int m_thresType;

    Mat m_matScaledImage;
    Mat m_matMaskSobelX;
    Mat m_matMaskSobelY;
    Mat m_matMaskSobel;
    Mat m_matSobelOut;

    EscortGraphicsScene *m_pScene;
};

#endif // FILTER_SOBEL_H
