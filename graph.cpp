#include "graph.h"
#include <QFileInfo>
#include "optionsdialog.h"
#include "parameters_table.h"
#include "mainwindow.h"
#include "common.h"


using namespace std;
using namespace cv;
extern SParametersTable k_parameters_table[2];
Q_DECLARE_METATYPE(cv::Mat)
Q_DECLARE_METATYPE(cv::Vec4i)
Q_DECLARE_METATYPE(std::vector<cv::Vec4i>)
void BaseGraph::handleUpdate(const QImage &image)
{
    MainWindow *win = (MainWindow *)parent();
    win->centralWidget()->update();
}
Graph::Graph(QObject *parent) : 
    //QObject(parent),
    BaseGraph(parent),
    m_oneMode(true),
    m_totalSourceTasks(0),
    m_totalLaneInputs(0),
    m_totalMixerInputs(0),
    m_totalSinkTasks(0),
    m_currentSourceStartedTasks(0),
    m_currentSinkStartedTasks(0),
    m_pVideoFileSource(nullptr),
    m_pVideoFileSourceThread(nullptr),
    m_pVideoCapture(nullptr),
    m_pVideoCaptureThread(nullptr),
    m_pHLSFilter(nullptr),
    m_pHSVFilter(nullptr),
    m_pSobelFilter(nullptr),
    m_pLaneFilter(nullptr),
    m_pVehicleFilter(nullptr),
    m_pLVMixerFilter(nullptr),
    m_pHLSFilterThread(nullptr),
    m_pHSVFilterThread(nullptr),
    m_pSobelFilterThread(nullptr),
    m_pLaneFilterThread(nullptr),
    m_pVehicleFilterThread(nullptr),
    m_pLVMixerFilterThread(nullptr)
{
    qRegisterMetaType<cv::Mat>();
    qRegisterMetaType<cv::Vec4i>();
    qRegisterMetaType<std::vector<cv::Vec4i>>();
}

Graph::~Graph()
{
    if (m_pLVMixerFilter)
    {
        delete m_pLVMixerFilter;
        m_pLVMixerFilter = nullptr;
    }
    if (m_pLVMixerFilterThread)
    {
        delete m_pLVMixerFilterThread;
        m_pLVMixerFilterThread = nullptr;
    }

    if (m_pVehicleFilter)
    {
        delete m_pVehicleFilter;
        m_pVehicleFilter = nullptr;
    }
    if (m_pVehicleFilterThread)
    {
        delete m_pVehicleFilterThread;
        m_pVehicleFilterThread = nullptr;
    }

    if (m_pLaneFilter)
    {
        delete m_pLaneFilter;
        m_pLaneFilter = nullptr;
    }
    if (m_pLaneFilterThread)
    {
        delete m_pLaneFilterThread;
        m_pLaneFilterThread = nullptr;
    }

    if (m_pSobelFilter)
    {
        delete m_pSobelFilter;
        m_pSobelFilter = nullptr;
    }
    if (m_pSobelFilterThread)
    {
        delete m_pSobelFilterThread;
        m_pSobelFilterThread = nullptr;
    }

    if (m_pHSVFilter)
    {
        delete m_pHSVFilter;
        m_pHSVFilter = nullptr;
    }
    if (m_pHSVFilterThread)
    {
        delete m_pHSVFilterThread;
        m_pHSVFilterThread = nullptr;
    }

    if (m_pHLSFilter)
    {
        delete m_pHLSFilter;
        m_pHLSFilter = nullptr;
    }
    if (m_pHLSFilterThread)
    {
        delete m_pHLSFilterThread;
        m_pHLSFilterThread = nullptr;
    }

    if (m_pVideoCapture)
    {
        delete m_pVideoCapture;
        m_pVideoCapture = nullptr;
    }

    if (m_pVideoCaptureThread)
    {
        delete m_pVideoCaptureThread;
        m_pVideoCaptureThread = nullptr;
    }
DSG(1, "B 3");
    if (m_pVideoFileSourceThread)
    {
        delete m_pVideoFileSourceThread;
        m_pVideoFileSourceThread = nullptr;
    }

DSG(1, "B 1");
    if (m_pVideoFileSource)
    {
        delete m_pVideoFileSource;
        m_pVideoFileSource = nullptr;
    }
DSG(1, "B 2");

}

bool Graph::OpenVideoFileProbe(SVideoConfig *pConfig)
{
    bool ret = false;

    if (!m_pVideoFileSource)
        m_pVideoFileSource = new TRVideoFileSource(this);

    if (m_pVideoFileSource)
    {
        if (m_pVideoFileSource->SetConfig((void*)pConfig))
        {
            ret = true;
        }
    }

    return ret;
}

bool Graph::OpenVideoFile(SVideoConfig *pConfig)
{
    bool ret = false;

    StopVideoFile();
    CloseVideoFile();

    do {
        if (!OpenVideoFileProbe(pConfig))
            break;

        TRVideoFileSource *pSource = m_pVideoFileSource;
        if (!pSource->Open()) break;
        if (!m_pVideoFileSourceThread)
            m_pVideoFileSourceThread = new SafeFreeThread;
        if (!m_pVideoFileSourceThread) break;
        pSource->moveToThread(m_pVideoFileSourceThread);

        ++m_totalSourceTasks;

        QFile file(QString(pConfig->filename));
        QFileInfo fileInfo(file.fileName());
        QString filename(fileInfo.fileName());
        CreateFilters(pSource, InitParameters(filename.toLatin1().constData()));

        {
            //connect(pSource, SIGNAL(started()), this, SLOT(handleAllVideoFileSourceStarted()));
            //connect(pSource, SIGNAL(stopped()), this, SLOT(handleAllVideoFileSourceStopped()));

            ConnectFilters(pSource);
        }
        ////////////////////////////////////////////////////////////////////////
        // preview image processing ////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        //{
        //    //TrafficEnforcement::GlobalCacheData *pCache = 
        //    //TrafficEnforcement::GlobalCacheData::instance();

        //    //QSettings settings(pCache->settings_file_path, QSettings::IniFormat);
        //    //settings.setIniCodec("UTF-8");

        //    //ImageProcessing config
        //    //ProcessingSetting *pSetting = pImgProc->GetProcessingSetting();

        //    //pSetting->setEnableLaneDetection(pCache->lanedetection_enabled);
        //    //pSetting->setEnableVehicleDetection(pCache->vehicledetection_enabled);
        //    //pSetting->setEnableROI(pCache->roi_enabled);
        //    //pSetting->setEnableBar(pCache->showtrackbar_enabled);
        //    //pSetting->setEnableEngineeringMode(pCache->engineeringmode_enabled);
        //    //pSetting->setTolerance(pCache->dwl_tolerance);

        //    //pSetting->setVD1PassDiffThreshold(pCache->vd_1pass_diffthreshold_idx);
        //    //pSetting->setVehicleDetectionModel(pCache->vd_algorithm_idx);

        //    ////pSetting->setOperationMode(e_operation_live);
        //    //pSetting->setLaneDetectionApproach(e_lanedetection_mix_hls_hsv_sobel_canny);

        //    //pSetting->setLaneDetectionCycleTime(k_lanedetection_cycletime[pCache->lanedetecion_cycletime_idx].mode);
        //    //pSetting->setVehicleDetectionCycleTime(k_vehicledetection_cycletime[pCache->vehicledetecion_cycletime_idx].mode);
        //}
        //{
        //    //SCameraImageProcessingConfig imgConfig;
        //    //DSG(1, "OpenVideoFile want_width=%d", pConfig->want_width);
        //    //DSG(1, "OpenVideoFile want_height=%d", pConfig->want_height);
        //    //imgConfig.want_width = pConfig->want_width;
        //    //imgConfig.want_height = pConfig->want_height;
        //    //pImgProc->SetConfig(&imgConfig);
        //    ////pImgProc->SetProcessAll(false);
        //    //pImgProc->SetApproach(e_imageprocessing_common_preview);
        //    //pImgProc->moveToThread(m_pImageProcessingColorPreviewThread);
        //}

    #if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_SOURCE
        MainWindow *pWin = (MainWindow*)(this->parent());
        DrawableGraphicsScene *pMainScene = pWin->GetMainScene();
        
        connect(pSource, &TRVideoFileSource::cvColorReady, pMainScene, &DrawableGraphicsScene::handleUpdateViewCV);
        connect(pSource, &TRVideoFileSource::qColorReady, pMainScene, &DrawableGraphicsScene::handleUpdateViewQ);
    #endif
        Mat frame;
        if (!pSource->GetFirstFrame(frame))
            break;
        //m_oneMode = true;
        //StartVideoFile();


        //pImgProc->Probe(frame, &m_imageScene);
        //pImgProc->SyncProcess();

        ret = true;

    } while (0);

    return ret;
}

bool Graph::StartVideoFile()
{
    bool ret = false;

    if (m_pVideoFileSourceThread)
        m_pVideoFileSourceThread->start();

    if (m_pVideoFileSource)
    {
        QMetaObject::invokeMethod(m_pVideoFileSource, "start", Qt::QueuedConnection, Q_ARG(int, CV_CAP_ANY));
        ret = true;
    }

    return ret;
}

void Graph::StopVideoFile()
{
    if (m_pVideoFileSource)
        QMetaObject::invokeMethod(m_pVideoFileSource, "stop", Qt::QueuedConnection);
}

void Graph::CloseVideoFile()
{
    m_totalSourceTasks = 0;
    m_totalLaneInputs = 0;
    m_totalMixerInputs = 0;
    m_totalSinkTasks = 0;

    DisconnectFilters((AbstractSource*)m_pVideoFileSource);
}

bool Graph::OpenCameraProbe(SCameraConfig *pConfig)
{
    bool ret = false;

    if (!m_pVideoCapture)
        m_pVideoCapture = new TRVideoCapture(this);

    if (m_pVideoCapture)
    {
        if (m_pVideoCapture->SetConfig((void*)pConfig))
        {
            ret = true;
        }
    }

    return ret;

}

bool Graph::OpenCamera(SCameraConfig *pConfig)
{
    bool ret = false;

    StopCamera();
    CloseCamera();

    do {
        if (!OpenCameraProbe(pConfig))
            break;

        TRVideoCapture *pSource = m_pVideoCapture;
        if (!pSource->Open()) break;
        if (!m_pVideoCaptureThread)
            m_pVideoCaptureThread = new SafeFreeThread;
        if (!m_pVideoCaptureThread) break;
        pSource->moveToThread(m_pVideoCaptureThread);

        ++m_totalSourceTasks;

        QFile file(QString(pConfig->filename));
        QFileInfo fileInfo(file.fileName());
        QString filename(fileInfo.fileName());
        CreateFilters(pSource, InitParameters(filename.toLatin1().constData()));

        {
            //connect(pSource, SIGNAL(started()), this, SLOT(handleAllVideoFileSourceStarted()));
            //connect(pSource, SIGNAL(stopped()), this, SLOT(handleAllVideoFileSourceStopped()));

            ConnectFilters(pSource);
        }
        ////////////////////////////////////////////////////////////////////////
        // preview image processing ////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        //{
        //    //TrafficEnforcement::GlobalCacheData *pCache = 
        //    //TrafficEnforcement::GlobalCacheData::instance();

        //    //QSettings settings(pCache->settings_file_path, QSettings::IniFormat);
        //    //settings.setIniCodec("UTF-8");

        //    //ImageProcessing config
        //    //ProcessingSetting *pSetting = pImgProc->GetProcessingSetting();

        //    //pSetting->setEnableLaneDetection(pCache->lanedetection_enabled);
        //    //pSetting->setEnableVehicleDetection(pCache->vehicledetection_enabled);
        //    //pSetting->setEnableROI(pCache->roi_enabled);
        //    //pSetting->setEnableBar(pCache->showtrackbar_enabled);
        //    //pSetting->setEnableEngineeringMode(pCache->engineeringmode_enabled);
        //    //pSetting->setTolerance(pCache->dwl_tolerance);

        //    //pSetting->setVD1PassDiffThreshold(pCache->vd_1pass_diffthreshold_idx);
        //    //pSetting->setVehicleDetectionModel(pCache->vd_algorithm_idx);

        //    ////pSetting->setOperationMode(e_operation_live);
        //    //pSetting->setLaneDetectionApproach(e_lanedetection_mix_hls_hsv_sobel_canny);

        //    //pSetting->setLaneDetectionCycleTime(k_lanedetection_cycletime[pCache->lanedetecion_cycletime_idx].mode);
        //    //pSetting->setVehicleDetectionCycleTime(k_vehicledetection_cycletime[pCache->vehicledetecion_cycletime_idx].mode);
        //}
        //{
        //    //SCameraImageProcessingConfig imgConfig;
        //    //DSG(1, "OpenVideoFile want_width=%d", pConfig->want_width);
        //    //DSG(1, "OpenVideoFile want_height=%d", pConfig->want_height);
        //    //imgConfig.want_width = pConfig->want_width;
        //    //imgConfig.want_height = pConfig->want_height;
        //    //pImgProc->SetConfig(&imgConfig);
        //    ////pImgProc->SetProcessAll(false);
        //    //pImgProc->SetApproach(e_imageprocessing_common_preview);
        //    //pImgProc->moveToThread(m_pImageProcessingColorPreviewThread);
        //}

#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_SOURCE
        MainWindow *pWin = (MainWindow*)(this->parent());
        DrawableGraphicsScene *pMainScene = pWin->GetMainScene();

        connect(pSource, &TRVideoCapture::cvColorReady, pMainScene, &DrawableGraphicsScene::handleUpdateViewCV);
        connect(pSource, &TRVideoCapture::qColorReady, pMainScene, &DrawableGraphicsScene::handleUpdateViewQ);
#endif
        Mat frame;
        if (!pSource->GetFirstFrame(frame))
            break;
        //m_oneMode = true;
        //StartVideoFile();


        //pImgProc->Probe(frame, &m_imageScene);
        //pImgProc->SyncProcess();

        ret = true;

    } while (0);

    return ret;
}

bool Graph::StartCamera()
{
    bool ret = false;

    m_pVideoCaptureThread->start();
    if (m_pVideoCapture)
    {
        QMetaObject::invokeMethod(m_pVideoCapture, "start", Qt::QueuedConnection, Q_ARG(int, CV_CAP_ANY));
        ret = true;
    }

    return ret;
}

void Graph::StopCamera()
{
    if (m_pVideoCapture)
        QMetaObject::invokeMethod(m_pVideoCapture, "stop", Qt::QueuedConnection);
}

void Graph::CloseCamera()
{
    m_totalSourceTasks = 0;
    m_totalLaneInputs = 0;
    m_totalMixerInputs = 0;
    m_totalSinkTasks = 0;

    DisconnectFilters((AbstractSource*)m_pVideoCapture);
}

bool Graph::Process()
{
    //if (m_pImgProcessing)
    //    m_pImgProcessing->SyncProcess();
    return true;
}

int Graph::InitParameters(const char *strFileName)
{
    int idx = -1;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    for (int i = 0; idx < DIM(k_parameters_table); ++idx)
    {
        //qDebug("11 %s", strFileName);
        //qDebug("22 %s", k_parameters_table[i].file_name);
        if (0 == strcmp(strFileName, k_parameters_table[idx].file_name))
        {
            idx = i;
            break; 
        }
    }

    if (-1 == idx)
    {
        idx = 0;
        pCache->lanedetection_maskmix_mode = mask_hls_and_hsv_and_sobel;
    }
    else
    {
        //qDebug("%s", strFileName);
        pCache->lanedetection_maskmix_mode = k_parameters_table[idx].mix_mode;
    }

    return idx;
}

bool Graph::CreateFilters(AbstractSource *pSource, int profileIdx)
{
    bool ret = false;
    
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    if ((pCache->lanedetection_maskmix_mode) & (mask_hls_and | mask_hls_or))
    {
        if (!m_pHLSFilter)
            m_pHLSFilter = new HLSFilter(this, profileIdx);
        if (m_pHLSFilter)
        {
            if (!m_pHLSFilterThread)
                m_pHLSFilterThread = new SafeFreeThread;
            if (m_pHLSFilterThread)
                m_pHLSFilter->moveToThread(m_pHLSFilterThread);
        }
    }
    if ((pCache->lanedetection_maskmix_mode) & (mask_hsv_and | mask_hsv_or))
    {
        if (!m_pHSVFilter)
            m_pHSVFilter = new HSVFilter(this, profileIdx);
        if (m_pHSVFilter)
        {
            if (!m_pHSVFilterThread)
                m_pHSVFilterThread = new SafeFreeThread;
            if (m_pHSVFilterThread)
                m_pHSVFilter->moveToThread(m_pHSVFilterThread);
        }
    }
    if ((pCache->lanedetection_maskmix_mode) & (mask_sobel_and | mask_sobel_or))
    {
        if (!m_pSobelFilter)
            m_pSobelFilter = new SobelFilter(this, profileIdx);
        if (m_pSobelFilter)
        {
            if (!m_pSobelFilterThread)
                m_pSobelFilterThread = new SafeFreeThread;
            if (m_pSobelFilterThread)
                m_pSobelFilter->moveToThread(m_pSobelFilterThread);
        }
    }

    if (!m_pLaneFilter)
        m_pLaneFilter = new LaneFilter(this, profileIdx);
    if (m_pLaneFilter)
    {
        if (!m_pLaneFilterThread)
            m_pLaneFilterThread = new SafeFreeThread;
        if (m_pLaneFilterThread)
            m_pLaneFilter->moveToThread(m_pLaneFilterThread);
    }

    if (!m_pVehicleFilter)
        m_pVehicleFilter = new VehicleFilter(this, profileIdx);
    if (m_pVehicleFilter)
    {
        if (!m_pVehicleFilterThread)
            m_pVehicleFilterThread = new SafeFreeThread;
        if (m_pVehicleFilterThread)
            m_pVehicleFilter->moveToThread(m_pVehicleFilterThread);
    }
    //if (!m_pLVMixerFilter)
    //    m_pLVMixerFilter = new LVMixerFilter(this, profileIdx);
    if (m_pLVMixerFilter)
    {
        if (!m_pLVMixerFilterThread)
            m_pLVMixerFilterThread = new SafeFreeThread;
        if (m_pLVMixerFilterThread)
            m_pLVMixerFilter->moveToThread(m_pLVMixerFilterThread);
    }

    return ret;
}

bool Graph::ConnectFilters(AbstractSource *pSource)
{
    bool ret = false;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    if ((pCache->lanedetection_maskmix_mode) & (mask_hls_and | mask_hls_or))
    {
        if (m_pHLSFilter)
        {
            pSource->ConnectTo(m_pHLSFilter);

            ++m_totalLaneInputs;
            ++m_totalSinkTasks;
        }
    }
    if ((pCache->lanedetection_maskmix_mode) & (mask_hsv_and | mask_hsv_or))
    {
        if (m_pHSVFilter)
        {
            pSource->ConnectTo(m_pHSVFilter);

            ++m_totalLaneInputs;
            ++m_totalSinkTasks;
        }
    }
    if ((pCache->lanedetection_maskmix_mode) & (mask_sobel_and | mask_sobel_or))
    {
        if (m_pSobelFilter)
        {
            pSource->ConnectTo(m_pSobelFilter);

            ++m_totalLaneInputs;
            ++m_totalSinkTasks;
        }
    }

    MainWindow *pWin = (MainWindow*)(this->parent());
    DrawableGraphicsScene *pMainScene = pWin->GetMainScene();
    if (m_pLaneFilter)
    {
        pSource->ConnectTo(m_pLaneFilter);

        if (m_pHLSFilter)
            m_pHLSFilter->ConnectTo(m_pLaneFilter);
        if (m_pHSVFilter)
            m_pHSVFilter->ConnectTo(m_pLaneFilter);
        if (m_pSobelFilter)
            m_pSobelFilter->ConnectTo(m_pLaneFilter);

        connect(m_pLaneFilter, &LaneFilter::updateLaneline, pMainScene, &DrawableGraphicsScene::handleUpdateLanelnfo);
        connect(m_pLaneFilter, &LaneFilter::updateLaneline, pWin, &MainWindow::handleUpdateLanelnfo);
        
        //connect(m_pLaneFilter, &LaneFilter::updateLaneline, m_pLVMixerFilter, &LVMixerFilter::handleUpdateLanelnfo);

        ++m_totalMixerInputs;
        ++m_totalSinkTasks;
    }

    if (m_pVehicleFilter)
    {
        pSource->ConnectTo(m_pVehicleFilter);

        //connect(m_pVehicleFilter, &VehicleFilter::updateVehicleInfo, m_pLVMixerFilter, &LVMixerFilter::handleUpdateVehicleInfo);
        connect(m_pVehicleFilter, &VehicleFilter::updateVehicleInfo, pMainScene, &DrawableGraphicsScene::handleUpdateVehicleInfo);
        connect(m_pVehicleFilter, &VehicleFilter::updateVehicleInfo, pWin, &MainWindow::handleUpdateVehicleInfo);

        ++m_totalMixerInputs;
    }

    if (m_pLVMixerFilter)
    {
        pSource->ConnectTo(m_pLVMixerFilter);

        if (m_pLaneFilter)
            m_pLaneFilter->ConnectTo(m_pLVMixerFilter);
        if (m_pVehicleFilter)
            m_pVehicleFilter->ConnectTo(m_pLVMixerFilter);

        ++m_totalSinkTasks;
    }

    connect(this, SIGNAL(filterAllRunning()), pWin, SLOT(handleAllComponentsAreRunning()));
    connect(this, SIGNAL(filterAllStopped()), pWin, SLOT(handleAllComponentsAreStopped()));
    
    //CreateConnect
    //connect(ui->trDWLToleranceSlider, &QSlider::sliderMoved, this, &handleUpdateTolerance);

    //InitROIButton
    //connect(this, SIGNAL(redirectCheckAdjustedROIEffect()), pImgProc, SLOT(handleCheckAdjustedROIEffect()));

    return ret;
}

bool Graph::DisconnectFilters(AbstractSource *pSource)
{
    bool ret = false;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    MainWindow *pWin = (MainWindow*)(this->parent());
    DrawableGraphicsScene *pMainScene = pWin->GetMainScene();

    disconnect(this, SIGNAL(filterAllRunning()), 0, 0);
    disconnect(this, SIGNAL(filterAllStopped()), 0, 0);

    if (m_pLVMixerFilter)
    {
        m_pLVMixerFilter->DisconnectAll();
    }

    if (m_pVehicleFilter)
    {
        disconnect(m_pVehicleFilter, &VehicleFilter::updateVehicleInfo, 0, 0);

        m_pVehicleFilter->DisconnectAll();
    }

    if (m_pLaneFilter)
    {
        disconnect(m_pLaneFilter, &LaneFilter::updateLaneline, 0, 0);

        m_pLaneFilter->DisconnectAll();
    }

    if ((pCache->lanedetection_maskmix_mode) & (mask_hls_and | mask_hls_or))
    {
        if (m_pHLSFilter)
        {
            m_pHLSFilter->DisconnectAll();
        }
    }
    if ((pCache->lanedetection_maskmix_mode) & (mask_hsv_and | mask_hsv_or))
    {
        if (m_pHSVFilter)
        {
            m_pHSVFilter->DisconnectAll();
        }
    }
    if ((pCache->lanedetection_maskmix_mode) & (mask_sobel_and | mask_sobel_or))
    {
        if (m_pSobelFilter)
        {
            m_pSobelFilter->DisconnectAll();
        }
    }


    //CreateConnect
    //connect(ui->trDWLToleranceSlider, &QSlider::sliderMoved, this, &handleUpdateTolerance);

    //InitROIButton
    //connect(this, SIGNAL(redirectCheckAdjustedROIEffect()), pImgProc, SLOT(handleCheckAdjustedROIEffect()));


    return ret;
}

void Graph::handleAllSourceStarted()
{
    AbstractSource *pSource = static_cast<AbstractSource*>(sender());
    DSG(1, "Graph::handleAllSourceStarted() pSource =%p ", pSource);
    if (m_pVideoFileSource == pSource)
    {
        m_currentSourceStartedTasks.ref();
        DSG(1, "Graph::handleAllSourceStarted() m_totalSourceTasks=%d m_currentSourceStartedTasks=%d ", 
            m_totalSourceTasks, m_currentSourceStartedTasks.load());
        if (m_totalSourceTasks == m_currentSourceStartedTasks.load())//m_currentSourceStartedTasks)
        {
            DSG(1, "Graph handleAllSourceStarted Emit");
            TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
            if ((pCache->lanedetection_maskmix_mode) & (mask_hls_and | mask_hls_or))
            {
                if (m_pHLSFilterThread)
                    m_pHLSFilterThread->start();
            }
            if ((pCache->lanedetection_maskmix_mode) & (mask_hsv_and | mask_hsv_or))
            {
                if (m_pHSVFilterThread)
                    m_pHSVFilterThread->start();
            }
            if ((pCache->lanedetection_maskmix_mode) & (mask_sobel_and | mask_sobel_or))
            {
                if (m_pSobelFilterThread)
                    m_pSobelFilterThread->start();
            }

            if (m_pLaneFilterThread)
            {
                if (m_pLaneFilterThread)
                    m_pLaneFilterThread->start();
            }

            if (m_pVehicleFilterThread)
            {
                if (m_pVehicleFilterThread)
                    m_pVehicleFilterThread->start();
            }

            if (m_pLVMixerFilterThread)
            {
                if (m_pLVMixerFilterThread)
                    m_pLVMixerFilterThread->start();
            }


            emit sourceAllStarted();
        }
    }
    else if (m_pVideoCapture == pSource)
    {
        m_currentSourceStartedTasks.ref();
        DSG(1, "Graph::handleAllSourceStarted() m_totalSourceTasks=%d m_currentSourceStartedTasks=%d ",
            m_totalSourceTasks, m_currentSourceStartedTasks.load());
        if (m_totalSourceTasks == m_currentSourceStartedTasks.load())//m_currentSourceStartedTasks)
        {
            DSG(1, "Graph handleAllSourceStarted Emit");
            TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
            if ((pCache->lanedetection_maskmix_mode) & (mask_hls_and | mask_hls_or))
            {
                if (m_pHLSFilterThread)
                    m_pHLSFilterThread->start();
            }
            if ((pCache->lanedetection_maskmix_mode) & (mask_hsv_and | mask_hsv_or))
            {
                if (m_pHSVFilterThread)
                    m_pHSVFilterThread->start();
            }
            if ((pCache->lanedetection_maskmix_mode) & (mask_sobel_and | mask_sobel_or))
            {
                if (m_pSobelFilterThread)
                    m_pSobelFilterThread->start();
            }

            if (m_pLaneFilterThread)
            {
                if (m_pLaneFilterThread)
                    m_pLaneFilterThread->start();
            }

            if (m_pVehicleFilterThread)
            {
                if (m_pVehicleFilterThread)
                    m_pVehicleFilterThread->start();
            }

            if (m_pLVMixerFilterThread)
            {
                if (m_pLVMixerFilterThread)
                    m_pLVMixerFilterThread->start();
            }


            emit sourceAllStarted();
        }
    }
}

void Graph::handleAllSourceStopped()
{
    AbstractSource *pSource = static_cast<AbstractSource*>(sender());
    if (m_pVideoFileSource == pSource)
    {
        qDebug("Graph handleAllSourceStopped m_currentSourceStartedTasks=%d", 
            m_currentSourceStartedTasks.load());
        if (!m_currentSourceStartedTasks.deref()) {
            // The last reference has been released
            //if (m_pImgProcessing)
            //    QMetaObject::invokeMethod(m_pImgProcessing, "stop", Qt::QueuedConnection); 
            ////will remove in next version
            ////if (m_pImageProcessingColorCapture)
            ////QMetaObject::invokeMethod(m_pImageProcessingColorCapture, "stop", Qt::QueuedConnection); 

            //qDebug("MainWindow handleAllSourceStopped Emit");
            TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
            if ((pCache->lanedetection_maskmix_mode) & (mask_hls_and | mask_hls_or))
            {
                if (m_pHLSFilter)
                    QMetaObject::invokeMethod(m_pHLSFilter, "stop", Qt::QueuedConnection);
            }
            if ((pCache->lanedetection_maskmix_mode) & (mask_hsv_and | mask_hsv_or))
            {
                if (m_pHSVFilter)
                    QMetaObject::invokeMethod(m_pHSVFilter, "stop", Qt::QueuedConnection);
            }
            if ((pCache->lanedetection_maskmix_mode) & (mask_sobel_and | mask_sobel_or))
            {
                if (m_pSobelFilter)
                    QMetaObject::invokeMethod(m_pSobelFilter, "stop", Qt::QueuedConnection);
            }

            if (m_pLaneFilter)
                QMetaObject::invokeMethod(m_pLaneFilter, "stop", Qt::QueuedConnection);

            if (m_pVehicleFilter)
                QMetaObject::invokeMethod(m_pVehicleFilter, "stop", Qt::QueuedConnection);

            if (m_pLVMixerFilter)
                QMetaObject::invokeMethod(m_pLVMixerFilter, "stop", Qt::QueuedConnection);

            emit sourceAllStopped();
        }
    }
    else if (m_pVideoCapture == pSource)
    {
        qDebug("Graph handleAllSourceStopped m_currentSourceStartedTasks=%d",
            m_currentSourceStartedTasks.load());
        if (!m_currentSourceStartedTasks.deref()) {
            // The last reference has been released
            //if (m_pImgProcessing)
            //    QMetaObject::invokeMethod(m_pImgProcessing, "stop", Qt::QueuedConnection); 
            ////will remove in next version
            ////if (m_pImageProcessingColorCapture)
            ////QMetaObject::invokeMethod(m_pImageProcessingColorCapture, "stop", Qt::QueuedConnection); 

            //qDebug("MainWindow handleAllSourceStopped Emit");
            TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
            if ((pCache->lanedetection_maskmix_mode) & (mask_hls_and | mask_hls_or))
            {
                if (m_pHLSFilter)
                    QMetaObject::invokeMethod(m_pHLSFilter, "stop", Qt::QueuedConnection);
            }
            if ((pCache->lanedetection_maskmix_mode) & (mask_hsv_and | mask_hsv_or))
            {
                if (m_pHSVFilter)
                    QMetaObject::invokeMethod(m_pHSVFilter, "stop", Qt::QueuedConnection);
            }
            if ((pCache->lanedetection_maskmix_mode) & (mask_sobel_and | mask_sobel_or))
            {
                if (m_pSobelFilter)
                    QMetaObject::invokeMethod(m_pSobelFilter, "stop", Qt::QueuedConnection);
            }

            if (m_pLaneFilter)
                QMetaObject::invokeMethod(m_pLaneFilter, "stop", Qt::QueuedConnection);

            if (m_pVehicleFilter)
                QMetaObject::invokeMethod(m_pVehicleFilter, "stop", Qt::QueuedConnection);

            if (m_pLVMixerFilter)
                QMetaObject::invokeMethod(m_pLVMixerFilter, "stop", Qt::QueuedConnection);

            emit sourceAllStopped();
        }
    }
}
//
//void Graph::handleAllVideoCaptureDeviceStarted()
//{
//    //AbstractCamera *pCamera = static_cast<AbstractCamera*>(sender());
//    //if (m_pVideoCapture == pCamera)
//    //{
//    //    //int n = m_currentSourceStartedTasks.fetchAndAddOrdered(1) + 1;
//    //    m_currentSourceStartedTasks.ref();
//    //    //++m_currentSourceStartedTasks;
//    //    qDebug("Graph handleAllVideoCaptureDeviceStarted m_currentSourceStartedTasks=%d",
//    //        m_currentSourceStartedTasks.load());// m_currentSourceStartedTasks);
//
//    //    if (m_totalSourceTasks == m_currentSourceStartedTasks.load())//m_currentSourceStartedTasks)
//    //    {
//    //        m_pImageProcessingColorPreviewThread->start();
//    //        //m_pImageProcessingColorCaptureThread->start();
//
//    //        qDebug("Graph handleAllVideoCaptureDeviceStarted Emit");
//    //        emit sourceAllStarted();
//    //    }
//    //}
//}
//
//void Graph::handleAllVideoCaptureDeviceStopped()
//{
//    //AbstractCamera *pCamera = static_cast<AbstractCamera*>(sender());
//    //if (m_pVideoCapture == pCamera)
//    //{
//    //    qDebug("Graph handleAllVideoCaptureDeviceStopped m_currentSourceStartedTasks=%d", m_currentSourceStartedTasks.load());
//    //    if (!m_currentSourceStartedTasks.deref()) {
//    //        // The last reference has been released
//    //        if (m_pImgProcessing)
//    //            QMetaObject::invokeMethod(m_pImgProcessing, "stop", Qt::QueuedConnection); //will remove in next version
//    //                                                                                       //if (m_pImageProcessingColorCapture)
//    //                                                                                       //    QMetaObject::invokeMethod(m_pImageProcessingColorCapture, "stop", Qt::QueuedConnection); //will remove in next version
//
//    //        qDebug("Graph handleAllVideoCaptureDeviceStopped Emit");
//    //        emit sourceAllStopped();
//    //    }
//    //}
//}

void Graph::handleAllComponentsAreRunning()
{
    m_currentSinkStartedTasks.ref();
    DSG(1, "Graph handleAllComponentsAreRunning m_totalSinkTasks=%d m_currentSinkStartedTasks=%d", m_totalSinkTasks, m_currentSinkStartedTasks.load());
    if (m_totalSinkTasks == m_currentSinkStartedTasks.load())
    {
        emit filterAllRunning();

        //if (m_oneMode)
        //{
        //    m_oneMode = false;
        //    StopVideoFile();
        //    //disconnect(m_pVideoFileSource, &Filter::cvColorReady, 0, 0);
        //    //while (m_currentSinkStartedTasks.load() > 0)
        //    //    m_currentSinkStartedTasks.deref();
        //}
        //ui->actionControl_Start->setEnabled(false);
        //ui->actionControl_Stop->setEnabled(true);

        //if (m_pStatusBarTimer)
        //    m_pStatusBarTimer->start(1000);
        //m_pStatusBarElapsedTimer.restart();

        ////DSG(1, "BBB 1 m_actionState=%d", m_actionState);
        //if (e_action_openimagefile_is_opened == m_actionState)
        //    m_actionState = e_action_openimagefile_is_running;
        //else if (e_action_openvideoefile_is_opened == m_actionState)
        //    m_actionState = e_action_openvideofile_is_running;
        //else if (e_action_opencamera_is_opened == m_actionState)
        //    m_actionState = e_action_opencamera_is_running;
        ////DSG(1, "BBB 2 m_actionState=%d", m_actionState);
    }
}

void Graph::handleAllComponentsAreStopped()
{
    qDebug("Graph handleAllComponentsAreStopped m_currentSinkStartedTasks=%d",
        m_currentSinkStartedTasks.load());
    if (!m_currentSinkStartedTasks.deref())
    {
        emit filterAllStopped();
        //if (m_pStatusBarTimer)
        //    m_pStatusBarTimer->stop();

        //ui->actionControl_Start->setEnabled(true);
        //ui->actionControl_Stop->setEnabled(false);

        //if (e_action_openvideofile_is_running == m_actionState)
        //    m_actionState = e_action_openvideoefile_is_opened;
        //else if (e_action_opencamera_is_running == m_actionState)
        //    m_actionState = e_action_opencamera_is_opened;
    }
}
