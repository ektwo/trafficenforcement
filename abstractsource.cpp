#include "abstractsource.h"
#include "common.h"


AbstractSource::AbstractSource(BaseGraph *parent)
    : Filter(parent)
    , m_state(e_component_state_idle)
    , m_timerId(-1)
{
}

AbstractSource::~AbstractSource()
{
    
}
