#ifndef FILTER_LANE_H
#define FILTER_LANE_H

#include <QElapsedTimer>
#include <unordered_map>
#include "filter.h"
#include "parameters_table.h"
#include "escort_graphicsscene.h"
#include "image_common.h"
//#include <queue>

using namespace std;
using namespace cv;

class LaneFilter : public Filter
{
    Q_OBJECT
public:
    LaneFilter(BaseGraph *parent, int profileIdx);
    ~LaneFilter();

    bool ConnectTo(Filter *pDownstreamFilter);
    void DisconnectAll();

protected:
    void NotifyUpstreamPlugin(Filter *pUpstreamFilter);

signals:    
    void updateLaneline(const std::vector<cv::Vec4i> &laneArray);

public slots:
    void handleUpstreamStarted();
    void handleUpstreamStopped();
    void handleColorFrameCV(const cv::Mat &frame);
    void handleProcessedFrameCV(const cv::Mat &frame);

protected:    
    bool IsLaneDetectionEnabled();
    bool CheckLaneDetectionAndCalibration();

    void ClusterLanes(std::vector<cv::Vec4i> &lines);
    void ClusterLanesSecond(std::vector<cv::Vec4i> &lines);

    void ReleaseLanelinesInfo(bool all);
    void FindLanes(const cv::Mat &in, std::vector<cv::Vec4i> *lpDetectedLaneLineArray);
    void LaneDetouching(SLanelinesInfo *pLaneLinesInfo);

    void PrepareImageForLaneDetection(const Mat &matScaledOrgClone);
    void ReactFromTrackbarLane(Mat &in, Mat &out);
    void ThresholdCallbackMix();
    void ThresholdCallbackLane(int pos, void *userdata);



    void DoImageProcessing(const Mat &matImage);
    void timerEvent(QTimerEvent *event);

private:
    //int calibration_counter;
    int m_cannyThres1;
    int m_cannyThres2;
    int m_cannyApertureSizeDiv2;
    int m_cannyIsgradient;
    int m_houghRho;
    int m_houghTheta;
    int m_houghLineThres;
    int m_houghMinLineLength;
    int m_houghMaxLineGap;

    EscortGraphicsScene *m_pScene;

    cv::Vec4i m_defaultLaneLeft;
    cv::Vec4i m_defaultLaneRight;
    //S_Parameters_Table_Tag::SLaneLineParmeter m_defaultLaneLeft;
    //S_Parameters_Table_Tag::SLaneLineParmeter m_defaultLaneRight;
    //Mat m_matScaledImage;
    //Mat m_matScaledImageF;
    Mat m_matScaledImagePrevious;
    Mat m_matScaledImageGray;
    Mat m_matMixIn;
    //Mat m_matMixOut;
    Mat m_matMaskMix;
    //Mat m_matDetectedLane;
    Mat m_matCannyIn;
    Mat m_matCannyOut;

    //SLanelinesInfo m_detectedLaneLineInfo;
    //std::vector<cv::Vec4i> m_laneArray;
    bool m_startToUseLanePool;
    std::vector<cv::Vec4i> m_workoutLanePool;
    std::vector<cv::Vec4i> m_emittedLanePool;
    
    std::unordered_map<Filter*, QQueue<cv::Mat>*> m_mapFilterImgList;
    std::unordered_map<Filter*, cv::Mat> m_mapImgPack;


    //QElapsedTimer m_timerSlice;

};

#endif // FILTER_LANE_H
