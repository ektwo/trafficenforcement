#ifndef TRCAMERATHREAD_H
#define TRCAMERATHREAD_H

#include <opencv2/opencv.hpp>
#include <QThread>
#include <QObject>
#include <QTimer>
#include <QElapsedTimer>
#include <QMetaType>
#include <QImage>
#include <QWidget>
#include "abstractcamera.h"
#include "traffic_enforcement.h"


class SafeFreeThread final : public QThread 
{ 
public: 
	~SafeFreeThread() { qDebug("SafeFreeThread"); quit(); wait(); qDebug("SafeFreeThread OK"); 
	}
};

class TRVideoCapture : public AbstractCamera
{
	Q_OBJECT
public:
    TRVideoCapture(BaseGraph *parent = {});
    ~TRVideoCapture();

    bool ConnectTo(Filter *pDownstreamFilter);
    void DisconnectAll();

    bool SetConfig(void *pConfig);
    bool Open();
    void Close();
    void OpenSettingDialog();

    bool GetFirstFrame(cv::Mat &frame);

signals:
    void started();
    void stopped();
    void cvColorReady(const cv::Mat &matImage);
    void qColorReady(const QImage &qImage);

public slots:
    void start(int cam = {});
    void stop();
    void handleColorFrameCV(const cv::Mat &frame) {}
    void handleColorFrameQ(const QImage &image) {}
    void handleProcessedFrameCV(const cv::Mat &frame) {}

protected:
    bool InternalStart();
    void InternalStop();
    void DoImageProcessing(const cv::Mat &matImage);
    void timerEvent(QTimerEvent *event);

private:

    std::vector<cv::Size> GetSupportedResolutionsProbing();

private:
    //EComponentState m_state;
    //int m_timerId;
    int m_currentFrame;
    QScopedPointer<cv::VideoCapture> m_videoCapture;
    QElapsedTimer m_timerCalc;
};


#endif // TRCAMERATHREAD_H
