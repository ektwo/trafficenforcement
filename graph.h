#ifndef GRAPH_H
#define GRAPH_H

#include <QObject>
#include "trcamerathread.h"
#include "trvideothread.h"
#include "image_processing.h"
#include "filter_hls.h"
#include "filter_hsv.h"
#include "filter_sobel.h"
#include "filter_lane.h"
#include "filter_vehicle.h"
#include "filter_lvmixer.h"


class Graph : public BaseGraph
{
    Q_OBJECT
public:
    explicit Graph(QObject *parent = nullptr);
    ~Graph();

    //bool OpenImageFileProbe(const QString &absolutefileName);
    //bool OpenImageFile(const QString &absolutefileName);

    bool OpenVideoFileProbe(SVideoConfig *pConfig);
    bool OpenVideoFile(SVideoConfig *pConfig);
    bool StartVideoFile();
    void StopVideoFile();
    void CloseVideoFile();

    bool OpenCameraProbe(SCameraConfig *pConfig);
    bool OpenCamera(SCameraConfig *pConfig);
    bool StartCamera();
    void StopCamera();
    void CloseCamera();

    bool Process();

protected:
    int  InitParameters(const char *strFileName);
    bool CreateFilters(AbstractSource *pSource, int profileIdx);
    bool ConnectFilters(AbstractSource *pSource);
    bool DisconnectFilters(AbstractSource *pSource);

signals:
    void sourceAllStarted();
    void sourceAllStopped();
    void filterAllRunning();
    void filterAllStopped();

public slots:
    void handleAllSourceStarted();
    void handleAllSourceStopped();
    //void handleAllVideoCaptureDeviceStarted();
    //void handleAllVideoCaptureDeviceStopped();
    void handleAllComponentsAreRunning();
    void handleAllComponentsAreStopped();


private:
    bool m_oneMode;
    int m_totalSourceTasks;
    int m_totalLaneInputs;
    int m_totalMixerInputs;
    int m_totalSinkTasks;
    QAtomicInt m_currentSourceStartedTasks;
    QAtomicInt m_currentSinkStartedTasks;

    TRVideoFileSource *m_pVideoFileSource;
    SafeFreeThread *m_pVideoFileSourceThread;

    TRVideoCapture *m_pVideoCapture;
    SafeFreeThread *m_pVideoCaptureThread;

    HLSFilter *m_pHLSFilter;
    HSVFilter *m_pHSVFilter;
    SobelFilter *m_pSobelFilter;
    LaneFilter *m_pLaneFilter;
    VehicleFilter *m_pVehicleFilter;
    LVMixerFilter *m_pLVMixerFilter;

    SafeFreeThread *m_pHLSFilterThread;
    SafeFreeThread *m_pHSVFilterThread;
    SafeFreeThread *m_pSobelFilterThread;
    SafeFreeThread *m_pLaneFilterThread;
    SafeFreeThread *m_pVehicleFilterThread;
    SafeFreeThread *m_pLVMixerFilterThread;

    //ImageProcessing *m_pImgProcessing;
    //SafeFreeThread  *m_pImageProcessingColorPreviewThread;
};

#endif // GRAPH_H
