#ifndef QUICK_SORT_H
#define QUICK_SORT_H

#include <opencv2/opencv.hpp>


int PartitionVec4i(std::vector<cv::Vec4i>& L, int low, int high);
void QSortVec4i(std::vector<cv::Vec4i>& L, int low, int high);
void QuickSortVec4i(std::vector<cv::Vec4i>& L);

int PartitionVec4f(std::vector<cv::Vec4f>& L, int low, int high);
void QSortVec4f(std::vector<cv::Vec4f>& L, int low, int high);
void QuickSortVec4f(std::vector<cv::Vec4f>& L);

int Partition(std::vector<int>& L, int low, int high);
void QSort(std::vector<int>& L, int low, int high);
void QuickSort(std::vector<int>& L);


#endif // QUICK_SORT_H
