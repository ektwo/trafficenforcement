#include "filter_lane.h"
#include "traffic_enforcement.h"
#include "parameters_table.h"
#include "common.h"
#include "quick_sort.h"


extern SParametersTable k_parameters_table[2];
LaneFilter::LaneFilter(BaseGraph *parent, int profileIdx)
    : Filter(parent)
    , m_startToUseLanePool(false)
    , m_cannyThres1(k_parameters_table[profileIdx].canny.threshold_1)
    , m_cannyThres2(k_parameters_table[profileIdx].canny.threshold_2)
    , m_cannyApertureSizeDiv2(k_parameters_table[profileIdx].canny.aperture_sizediv2)
    , m_cannyIsgradient(k_parameters_table[profileIdx].canny.is_gradient)
    , m_pScene(k_parameters_table[profileIdx].canny.scene)
    , m_houghRho(k_parameters_table[profileIdx].hough.rho)
    , m_houghTheta(k_parameters_table[profileIdx].hough.theta)
    , m_houghLineThres(k_parameters_table[profileIdx].hough.line_threshold)
    , m_houghMinLineLength(k_parameters_table[profileIdx].hough.minline_length)
    , m_houghMaxLineGap(k_parameters_table[profileIdx].hough.maxline_gap)
    , m_defaultLaneLeft(k_parameters_table[profileIdx].lane_left)
    , m_defaultLaneRight(k_parameters_table[profileIdx].lane_right)
{
    //m_readyToEmit = false;

    m_class = k_str_class_transform_name;
    m_name = k_str_lane_filter_name;
    DSG(1, "LaneFilter m_class=%s", m_class);
    DSG(1, "LaneFilter m_name=%s", m_name);

    if (m_pScene)
    {
        m_pScene->SetOwner(parent);
        connect(this, SIGNAL(iamStarted()), m_pScene, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), m_pScene, SLOT(handleUpstreamStopped()));
        connect(this, &Filter::cvColorReady, m_pScene, &EscortGraphicsScene::handleUpdateViewCV);
    }
    //namedWindow(k_str_hls_filter_name, WINDOW_AUTOSIZE);
    //namedWindow(k_str_hsv_filter_name, WINDOW_AUTOSIZE);
    //namedWindow(k_str_sobel_filter_name, WINDOW_AUTOSIZE);

}

LaneFilter::~LaneFilter()
{
    if (m_pScene)
    {
        m_pScene->SetOwner(nullptr);
        disconnect(this, &Filter::cvColorReady, 0, 0);
    }
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

bool LaneFilter::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = false;
    if (pDownstreamFilter)
    {
        // add downstreamfilter at the end of the vector
        // add upstreamfilter at the end of the vector of this targeted downstreamfilter
        ret = __super::ConnectTo(pDownstreamFilter); 

        connect(this, SIGNAL(iamStarted()), pDownstreamFilter, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), pDownstreamFilter, SLOT(handleUpstreamStopped()));
        //connect(this, &Filter::imgProcessedReady, pDownstreamFilter, &Filter::handleProcessedFrameCV);
    }
    return ret;
}

void LaneFilter::DisconnectAll()
{
    __super::DisconnectAll();

    //disconnect(this, &Filter::imgProcessedReady, 0, 0);
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

void LaneFilter::NotifyUpstreamPlugin(Filter *pUpstreamFilter)
{
    __super::NotifyUpstreamPlugin(pUpstreamFilter);

    //QQueue<cv::Mat> *pQueue = new QQueue<cv::Mat>;
    if (0 == strcmp(k_str_class_transform_name, pUpstreamFilter->m_class))
    {
        m_mapFilterImgList.insert(map<Filter*, QQueue<Mat>*> ::value_type(pUpstreamFilter, new QQueue<Mat>));
        m_mapImgPack.insert(map<Filter*, Mat> ::value_type(pUpstreamFilter, Mat()));
    }
    //m_mapImgPack.reserve(m_startedUpstreamFilters);
    //m_filterImageList[pUpstreamFilter] = pQueue;
}

void LaneFilter::handleUpstreamStarted()
{
    ++m_startedUpstreamFilters;
    DSG(1, "LaneFilter::handleUpstreamStarted m_startedUpstreamFilters=%d m_pUpstreamFilterList.size()=%d", 
        m_startedUpstreamFilters, m_pUpstreamFilterList.size());
    if (m_startedUpstreamFilters == m_pUpstreamFilterList.size())
    {
        __super::handleUpstreamStarted();
    }
}

void LaneFilter::handleUpstreamStopped()
{
    --m_startedUpstreamFilters;
    DSG(1, "LaneFilter::handleUpstreamStopped m_startedUpstreamFilters=%d", m_startedUpstreamFilters);
    if (m_startedUpstreamFilters == 0)
    {
        for (auto it = m_mapFilterImgList.begin(); it != m_mapFilterImgList.end(); ++it)
        {
            QQueue<cv::Mat> *pImageList = it->second;
            if (pImageList)
                delete pImageList;
        }
        //for (auto it = m_mapImgPack.begin(); it != m_mapImgPack.end(); ++it)
        //{
        //    Mat *pImageList = it->second;
        //    if (pImageList)
        //        delete pImageList;
        //}

        m_mapFilterImgList.clear();
        m_mapImgPack.clear();

        __super::handleUpstreamStopped(); // change filter state and emit signal to next filter
    }
}

void LaneFilter::handleColorFrameCV(const cv::Mat &frame)
{
    //DSG(1, "LaneFilter::handleColorFrameCV");
    //DSG(1, "HSVFilter::handleProcessedFrameCV");
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 10)
        {
            //DSG(1, "queue m_imageList.size() drop = %d", m_imageList.size());
            cv::Mat image = m_imageList.dequeue();
            image.release();
        }

        m_imageList.enqueue(frame);
    }

    //if (m_readyToEmit)
    {
        if (!m_timer.isActive())
            m_timer.start(0, this);
    }
    //DSG(1, "LaneFilter::handleColorFrameCV 2");
}

void LaneFilter::handleProcessedFrameCV(const cv::Mat &frame)
{
    //DSG(1, "LaneFilter::handleProcessedFrameCV");
    int idx = 0;
    int readyChannelCnt = 0;
    Filter *pFilter = static_cast<Filter*>(sender());
    //if (0 == strcmp(k_str_hls_filter_name, pFilter->m_name))
    //{
    //    imshow("maskHLS 2", frame);
    //    DSG(1, "handleProcessedFrameCV maskHLS 2");
    //    PrintMatInfo(frame);
    //}
    //update the frame corresponding to its list
    {
        QMutexLocker locker(&m_mutex);
        for (auto it = m_mapFilterImgList.begin(); it != m_mapFilterImgList.end(); ++it)
        {
            QQueue<cv::Mat> *pImageList = it->second;
            if (pFilter == it->first)
            {
                if (pImageList->size() > 10)
                {
                    cv::Mat image = pImageList->dequeue();
                    image.release();
                }
                pImageList->enqueue(frame);
            }
            if (pImageList->size() > 0)
            {
                ++readyChannelCnt;
            //    m_mapImgPack[it->first] = &(pImageList->head().clone());
            }
            //
        }
    }
    //DSG(1, "LaneFilter::handleProcessedFrameCV %d", readyChannelCnt);
    if (readyChannelCnt == m_pUpstreamFilterList.size())
    {
        //m_readyToEmit = false;
        if (!m_timer.isActive())
            m_timer.start(0, this);
    }
    //DSG(1, "LaneFilter::handleProcessedFrameCV 2");
}

bool LaneFilter::IsLaneDetectionEnabled()
{
    bool ret = false;
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if ((pCache->lanedetection_enabled) && (!pCache->lanedetection_paused))
        //if ((!pCache->lanedetection_manual_enabled) && (pCache->lanedetection_enabled) && (!pCache->lanedetection_paused))
    {
        ret = true;
        //if (e_operation_live == pCache->operation_mode)
        //{
        //    
        //}
        //else if (e_operation_static == pCache->operation_mode)
        //    ret = true;
    }

    return ret;
}

void LaneFilter::ClusterLanes(std::vector<cv::Vec4i> &lines)
{
    std::vector<cv::Vec4i>::iterator it2 = lines.begin();
    int countIdx = 0;
    int x1, y1, x2, y2;
    int oldX = INT_MAX, newX;
    int totalX1 = 0, totalY1 = 0, totalX2 = 0, totalY2 = 0;
    int avgX1 = 0, avgY1 = 0, avgX2 = 0, avgY2 = 0;
    int numsOfLines;
    Vec4i avg;
    vector<Vec4i> realLines;
    vector<Vec4i> currLines;
    while (it2 != lines.end()) {
        // end points
        x1 = (*it2)[0];
        y1 = (*it2)[1];
        x2 = (*it2)[2];
        y2 = (*it2)[3];
        //qDebug("GHH2 [%d] %d %d %d %d", countIdx, x1, y1, x2, y2);
        if (y1 > y2)
            newX = x1;
        else
            newX = x2;
        countIdx++;
        //qDebug("(newX - oldX)=%d", (newX - oldX));
        if ((newX - oldX) > k_cluster_distance)
        {
            totalX1 = totalY1 = totalX2 = totalY2 = 0;
            numsOfLines = currLines.size();
            //qDebug("currLines.size()1=%d", currLines.size());
            for (int i = 0; i < numsOfLines; ++i)
            {
                totalX1 += currLines[i][0];
                totalY1 += currLines[i][1];
                totalX2 += currLines[i][2];
                totalY2 += currLines[i][3];
            }

            avg[0] = totalX1 / numsOfLines;
            avg[1] = totalY1 / numsOfLines;
            avg[2] = totalX2 / numsOfLines;
            avg[3] = totalY2 / numsOfLines;
            //qDebug("111 avg %d %d %d %d", avg[0], avg[1], avg[2], avg[3]);
            currLines.clear();

            if (it2 == (lines.end() - 1)) // coincidence
            {
                realLines.push_back(*it2);
            }
            else
                currLines.push_back(*it2);
            realLines.push_back(avg);
        }
        else if (it2 == (lines.end() - 1))
        {
            totalX1 = totalY1 = totalX2 = totalY2 = 0;

            currLines.push_back(*it2);

            numsOfLines = currLines.size();
            //qDebug("currLines.size()2=%d", currLines.size());
            for (int i = 0; i < numsOfLines; ++i)
            {
                totalX1 += currLines[i][0];
                totalY1 += currLines[i][1];
                totalX2 += currLines[i][2];
                totalY2 += currLines[i][3];
            }

            avg[0] = totalX1 / numsOfLines;
            avg[1] = totalY1 / numsOfLines;
            avg[2] = totalX2 / numsOfLines;
            avg[3] = totalY2 / numsOfLines;
            //qDebug("222 %d %d %d", avg[0], avg[1], avg[2]);
            realLines.push_back(avg);
            break;
        }
        else
        {
            currLines.push_back(*it2);
            //qDebug("333 [%d] %d %d %d %d", countIdx, x1, y1, x2, y2);
        }
        oldX = newX;

        ++it2;
    }

    //qDebug("realLines.size()=%d", realLines.size());
    if (realLines.size() > 0)
    {
        lines.clear();
        lines.shrink_to_fit();
        lines.assign(realLines.begin(), realLines.end());
    }
    //qDebug("FindLanes 32 size=%d", (*pLineArray).size());
}
void LaneFilter::ClusterLanesSecond(std::vector<cv::Vec4i> &lines)
{
    std::vector<cv::Vec4i>::iterator it2 = lines.begin();
    int countIdx = 0;
    int x1, y1, x2, y2;
    int oldX = INT_MAX, newX;
    int totalX1 = 0, totalY1 = 0, totalX2 = 0, totalY2 = 0;
    int avgX1 = 0, avgY1 = 0, avgX2 = 0, avgY2 = 0;
    int numsOfLines;
    Vec4i avg;
    vector<Vec4i> realLines;
    vector<Vec4i> currLines;
    while (it2 != lines.end()) {
        // end points
        x1 = (*it2)[0];
        y1 = (*it2)[1];
        x2 = (*it2)[2];
        y2 = (*it2)[3];
        //qDebug("GHH2 [%d] %d %d %d %d", countIdx, x1, y1, x2, y2);
        if (y1 > y2)
            newX = x1;
        else
            newX = x2;
        countIdx++;
        //qDebug("(newX - oldX)=%d", (newX - oldX));
        if ((newX - oldX) > k_cluster_distance)
        {
            totalX1 = totalY1 = totalX2 = totalY2 = 0;
            numsOfLines = currLines.size();
            //qDebug("currLines.size()1=%d", currLines.size());
            for (int i = 0; i < numsOfLines; ++i)
            {
                totalX1 += currLines[i][0];
                totalY1 += currLines[i][1];
                totalX2 += currLines[i][2];
                totalY2 += currLines[i][3];
            }

            avg[0] = totalX1 / numsOfLines;
            avg[1] = totalY1 / numsOfLines;
            avg[2] = totalX2 / numsOfLines;
            avg[3] = totalY2 / numsOfLines;
            //qDebug("111 avg %d %d %d %d", avg[0], avg[1], avg[2], avg[3]);
            currLines.clear();

            if (it2 == (lines.end() - 1)) // coincidence
            {
                realLines.push_back(*it2);
            }
            else
                currLines.push_back(*it2);
            realLines.push_back(avg);
}
        else if (it2 == (lines.end() - 1))
        {
            totalX1 = totalY1 = totalX2 = totalY2 = 0;

            currLines.push_back(*it2);

            numsOfLines = currLines.size();
            //qDebug("currLines.size()2=%d", currLines.size());
            for (int i = 0; i < numsOfLines; ++i)
            {
                totalX1 += currLines[i][0];
                totalY1 += currLines[i][1];
                totalX2 += currLines[i][2];
                totalY2 += currLines[i][3];
            }

            avg[0] = totalX1 / numsOfLines;
            avg[1] = totalY1 / numsOfLines;
            avg[2] = totalX2 / numsOfLines;
            avg[3] = totalY2 / numsOfLines;
            //qDebug("222 %d %d %d", avg[0], avg[1], avg[2]);
            realLines.push_back(avg);
            break;
        }
        else
        {
            currLines.push_back(*it2);
            //qDebug("333 [%d] %d %d %d %d", countIdx, x1, y1, x2, y2);
        }
        oldX = newX;

        ++it2;
    }

    //qDebug("realLines.size()=%d", realLines.size());
    if (realLines.size() > 0)
    {
        lines.clear();
        lines.shrink_to_fit();
        lines.assign(realLines.begin(), realLines.end());
    }
    //qDebug("FindLanes 32 size=%d", (*pLineArray).size());
}
void LaneFilter::ReleaseLanelinesInfo(bool all)
{
#if 0
    SLanelinesInfo* pLaneLinesInfo = &m_detectedLaneLineInfo;
    //std::vector<bool> *pLaneInROI = &(pLaneLinesInfo->in_roi);
    std::vector<cv::Vec4i> *pLaneArray = &(pLaneLinesInfo->array);
    std::vector<cv::Point2f> *pLaneWinTl = &(pLaneLinesInfo->sliding_win_tl);
    std::vector<cv::Point2f> *pLaneWinBr = &(pLaneLinesInfo->sliding_win_br);
    std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList =
        &(pLaneLinesInfo->sliding_win_info_list);

    vector<vector<SSlidingWindowInfo>>::iterator itWindow = ppWinInfo2DimList->begin();
    vector<SSlidingWindowInfo> zeroInfo;
    while (itWindow != ppWinInfo2DimList->end())
    {
        (*itWindow).clear();
        (*itWindow).swap(zeroInfo);
        ++itWindow;
    }

    //std::vector<bool> zeroLaneInROI;
    //pLaneInROI->clear();
    //pLaneInROI->swap(zeroLaneInROI);

    if (all)
    {
        pLaneLinesInfo->offset_x = 0.0;
        pLaneLinesInfo->offset_y = 0.0;

        std::vector<cv::Vec4i> zeroLaneArray;
        pLaneArray->clear();
        pLaneArray->swap(zeroLaneArray);
    }

    std::vector<cv::Point2f> zeroLaneWinTl;
    pLaneWinTl->clear();
    pLaneWinTl->swap(zeroLaneWinTl);
    std::vector<cv::Point2f> zeroLaneWinBr;
    pLaneWinBr->clear();
    pLaneWinBr->swap(zeroLaneWinBr);

    std::vector<std::vector<SSlidingWindowInfo>> zeroWinInfo2DimList;
    ppWinInfo2DimList->clear();
    ppWinInfo2DimList->swap(zeroWinInfo2DimList);
#endif
}

void LaneFilter::FindLanes(const cv::Mat &in, std::vector<cv::Vec4i> *lpDetectedLaneLineArray)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    std::vector<cv::Vec4i> *pLineArray = lpDetectedLaneLineArray;

    bool div2Needed = false;

#ifdef DOWNSCALE_WHEN_LANDETECTION
    Mat out;
    CVImageResize2ByWH(in, out, in.cols / 2, in.rows / 2);
    HoughLinesP(out, 
                *pLineArray, 1 + m_houghRho, 
                CV_PI / double(m_houghTheta), 
                m_houghLineThres, 
                m_houghMinLineLength, 
                m_houghMaxLineGap);
    {
        std::vector<cv::Vec4i>::iterator it = (*pLineArray).begin();
        while (it != (*pLineArray).end()) {
            //qDebug("GHH1 [%d] %d %d %d %d", countIdx, (*it)[0], (*it)[1], (*it)[2], (*it)[3]);
            (*it) *= 2;
            //qDebug("GHH2 [%d] %d %d %d %d", countIdx, (*it)[0], (*it)[1], (*it)[2], (*it)[3]);
            ++it;
        }
    }
#else
    HoughLinesP(in,
                *pLineArray, 1 + m_houghRho,
                CV_PI / double(m_houghTheta),
                m_houghLineThres,
                m_houghMinLineLength,
                m_houghMaxLineGap);
#endif
    //qDebug("FindLanes 1 size=%d", (*pLineArray).size());
    CVRemoveLinesOfInconsistentOrientations(*pLineArray, in.cols, in.rows);
    qDebug("FindLanes 2 size=%d", (*pLineArray).size());

    QuickSortVec4i(*pLineArray);
    //
    ClusterLanes(*pLineArray);
    qDebug("FindLanes 3 size=%d", (*pLineArray).size());
}

void LaneFilter::LaneDetouching(SLanelinesInfo *pLaneLinesInfo)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    //std::vector<bool> *pLaneInROI = &(pLaneLinesInfo->in_roi);
    std::vector<cv::Vec4i> *pLaneArray = &(pLaneLinesInfo->array);
    std::vector<cv::Point2f> *pLaneWinTl = &(pLaneLinesInfo->sliding_win_tl);
    std::vector<cv::Point2f> *pLaneWinBr = &(pLaneLinesInfo->sliding_win_br);
    //std::vector<cv::Rect> *pLaneWinRect;
    std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList =
        &(pLaneLinesInfo->sliding_win_info_list);

    //int idx = 0;
    int roiLeft, roiRight, roiTop, roiBottom;
    //vector<Point2f> roiContours;
    if (pCache->roi_enabled)
    {
        //int roiLeft = roiRight = roiTop = roiBottom = 0;
        //QVector<DrawableGraphicsItem<QGraphicsLineItem, QPushButton>*> *pROILines;
        //pROILines = GetScene()->getROILines();
        //for (size_t i = 0; i < pROILines->size(); ++i)
        //{
        //    DrawableGraphicsItem<QGraphicsLineItem, QPushButton> *pROILine = pROILines->at(i);
        //    QGraphicsLineItem *pLine = static_cast<QGraphicsLineItem*>(pROILine->getItem());
        //    if (0 == i) roiTop = pLine->pos().y();
        //    else if (1 == i) roiBottom = pLine->pos().y();
        //    else if (2 == i) roiLeft = pLine->pos().x();
        //    else if (3 == i) roiRight = pLine->pos().x();
        //}
        ////	roiContours.push_back(Point2f(roiLeft, roiTop));
        ////	roiContours.push_back(Point2f(roiRight, roiTop));
        ////	roiContours.push_back(Point2f(roiRight, roiBottom));
        ////	roiContours.push_back(Point2f(roiLeft, roiBottom));
        ////	//roiLeft -= 10; roiRight += 10; roiTop -= 10; roiBottom += 10;
        //qDebug("roi %d %d %d %d", roiLeft, roiRight, roiTop, roiBottom);
    }

    //if (bBackupSWInfoFile)
    {
        int previousX, previousY;
        int minX, maxX;
        vector<Vec4i>::iterator it = pLaneArray->begin();

        //    while (it != pLaneArray->end())
        //    {
        //        qDebug("line[%d] %d %d %d %d", idx, (*it)[0], (*it)[1], (*it)[2], (*it)[3]);
        //        ++it;
        //        idx++;
        //    }
        //	it = pLaneArray->begin();
    #ifdef BACKUP_SWINFOFILE
        OpenAndWriteSlidingWindowsFile(k_swinfo_filename);
        SaveSlidingWindowsNumsOfLines(pLaneArray->size());
    #endif
        int p1x, p1y, p2x, p2y;
        int lineleft, lineright;
        int rcMinX, rcMaxX, rcMinY, rcMaxY;
        while (it != pLaneArray->end())
        {
            // get the info about the line is valid or not. //////////////
            Point ptLeft;
            Point ptRight;
            Vec4i line = (*it);//[0];

            if (line[0] > line[2])
            {
                ptLeft.x = line[2];
                ptLeft.y = line[3];
                ptRight.x = line[0];
                ptRight.y = line[1];
            }
            else
            {
                ptLeft.x = line[0];
                ptLeft.y = line[1];
                ptRight.x = line[2];
                ptRight.y = line[3];
            }

            //			if (pCache->roi_enabled)
            //			{
            //				lineleft = line[0];
            //				lineright = line[2];
            //				if (lineleft > lineright)
            //				    std::swap(lineleft, lineright);
            //				//qDebug("roi checking %d %d %d %d", line[0], line[1], line[2], line[3]);
            //				//qDebug("roi checking roiLeft %d %d", roiLeft, roiRight);
            //				if ((ptLeft.x <= roiLeft) || (ptRight.x >= roiRight))
            //				{
            //					pLaneInROI->push_back(false);
            //				}
            //				else
            //				{
            //#ifdef DRAW_LANELINE_ON_MAT_TO_TEST
            //					cv::line(src, ptLeft, ptRight, Scalar(k_laneline_clr_b, k_laneline_clr_g, k_laneline_clr_r), k_laneline_width, 8);
            //#endif		
            //					pLaneInROI->push_back(true);
            //				}
            //			}
            //			else
            //			{
            //				pLaneInROI->push_back(false);
            //			}

            //normal rect roi
            //get rect of line //////////////
            {
                cv::Rect rect;
                p1x = (*it)[0];
                p1y = (*it)[1];
                p2x = (*it)[2];
                p2y = (*it)[3];
                //qDebug("%d %d %d %d %d %d %d", p1x, p1y, p2x, p2y, k_sliding_windows_radius, src.cols, src.rows);

                CVFindRect(p1x, p1y, p2x, p2y, k_sliding_windows_radius, m_matCannyIn.cols, m_matCannyIn.rows, rect);
                //qDebug("%d %d %d %d", rect.x, rect.y, rect.width, rect.height);
                pLaneWinTl->push_back(Point2f(rect.br()));
                pLaneWinBr->push_back(Point2f(rect.tl()));
                //pLaneWinRect->push_back(rect);
            }

        #ifdef CONNECT_SOURCE_LANEFILTER
            //get all point of line //////////////
            LineIterator itLine(src, Point(p1x, p1y), Point(p2x, p2y), 8);
            // do ~ while 
            //qDebug("itLine count=%d", itLine.count);
            previousX = itLine.pos().x;
            previousY = itLine.pos().y;
            minX = itLine.pos().x - k_sliding_windows_radius;
            maxX = itLine.pos().x + k_sliding_windows_radius;
            //qDebug("itLine pt[%d]=(%d,%d)", 0, itLine.pos().x, itLine.pos().y);
            ++itLine;
            
        #ifdef BACKUP_SWINFOFILE
            vector<SSlidingWindowInfo> currentWindowInfoList;
            SaveSlidingWindowsNumsOfLines(itLine.count);
        #endif
            for (int i = 1; i < itLine.count; ++i, ++itLine)
            {
                //qDebug("itLine pt[%d]=(%d,%d)", i, itLine.pos().x, itLine.pos().y);
                if (previousY == itLine.pos().y) // point are moving on the same horizontal axis.
                {
                    //qDebug("itLine same pt[%d]=(%d,%d)", i, itLine.pos().x, itLine.pos().y);
                    if (minX > itLine.pos().x - k_sliding_windows_radius)
                        minX = itLine.pos().x - k_sliding_windows_radius;
                    if (maxX < itLine.pos().x + k_sliding_windows_radius)
                        maxX = itLine.pos().x + k_sliding_windows_radius;

                    if ((i + 1) == itLine.count)
                    {
                    #ifdef UPDATE_SLIDINGWINDOW_CONTENT
                        AddSlidingWindowsContent(src, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY);
                    #endif				
                        //                    info.min_x = minX;
                        //                    info.max_x = maxX;
                        //                    info.centroid = itLine.pos().x;
                        //                    info.y = previousY;
                        //                    if (k_sliding_windows_be_erased)
                        //                    {
                        //                        for (int j = minX; j < maxX; ++j)
                        //                        {
                        //                            src.at<Vec3b>(previousY, j)[0] = 255;
                        //                            src.at<Vec3b>(previousY, j)[1] = 255;
                        //                            src.at<Vec3b>(previousY, j)[2] = 255;
                        //                        }
                        //                    }
                        //currentWindowInfoList.push_back(info);
                    }
                }
                else
                {
                    //                info.min_x = minX;
                    //                info.max_x = maxX;
                    //                info.centroid = itLine.pos().x;
                    //                info.y = previousY;
                    //                if (k_sliding_windows_be_erased)
                    //                {
                    //                    for (int j = minX; j < maxX; ++j)
                    //                    {
                    //                        src.at<Vec3b>(previousY, j)[0] = 255;
                    //                        src.at<Vec3b>(previousY, j)[1] = 255;
                    //                        src.at<Vec3b>(previousY, j)[2] = 255;
                    //                    }
                    //                }
                #ifdef UPDATE_SLIDINGWINDOW_CONTENT
                    AddSlidingWindowsContent(src, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY);
                #endif			
                    //currentWindowInfoList.push_back(info);

                    minX = itLine.pos().x - k_sliding_windows_radius;
                    maxX = itLine.pos().x + k_sliding_windows_radius;

                    //preivousX = itLine.pos().x;
                    previousY = itLine.pos().y;

                    if ((i + 1) == itLine.count)
                    {
                        //                    info.min_x = minX;
                        //                    info.max_x = maxX;
                        //                    info.centroid = itLine.pos().x;
                        //                    info.y = previousY;
                        //                    if (k_sliding_windows_be_erased)
                        //                    {
                        //                        for (int j = minX; j < maxX; ++j)
                        //                        {
                        //                            src.at<Vec3b>(previousY, j)[0] = 255;
                        //                            src.at<Vec3b>(previousY, j)[1] = 255;
                        //                            src.at<Vec3b>(previousY, j)[2] = 255;
                        //                        }
                        //                    }
                    #ifdef UPDATE_SLIDINGWINDOW_CONTENT
                        AddSlidingWindowsContent(src, currentWindowInfoList, minX, maxX, itLine.pos().x, previousY);
                    #endif		
                        //currentWindowInfoList.push_back(info);
                    }
                }
            }
        #ifdef UPDATE_SLIDINGWINDOW_CONTENT
            ppWinInfo2DimList->push_back(currentWindowInfoList);
        #endif		
        #endif            
            ++it;
        }

        #ifdef BACKUP_SWINFOFILE
            CloseAndWriteSlidingWindowsFile();
        #endif  
    }

#ifdef CONNECT_SOURCE_LANEFILTER
#ifdef SAVE_MODIFIED_PIC
    imwrite("../withWhite.jpg", src);
#endif
#endif

    int idx = 0;
#if 0
    vector<vector<SSlidingWindowInfo>>::iterator itWindow = ppWinInfo2DimList->begin();
    //qDebug("ppWinInfo2DimList size=%d", ppWinInfo2DimList->size());
    while (itWindow != ppWinInfo2DimList->end())
    {
        vector<SSlidingWindowInfo> *pCurrentWindowInfoList = &(*itWindow);
        vector<SSlidingWindowInfo>::iterator itChild = pCurrentWindowInfoList->begin();
        //qDebug("(*itWindow)[%d] size=%d", idx, pCurrentWindowInfoList->size());
        ++idx;
        int idxChild = 0;
        while (itChild != pCurrentWindowInfoList->end())
        {
            SSlidingWindowInfo *pInfo = &(*itChild);
            if (pInfo)
            {
                for (int i = pInfo->min_x; i < pInfo->max_x + 1; ++i)
                {
                    src.at<Vec3b>(pInfo->y, i)[0] = 255;
                    src.at<Vec3b>(pInfo->y, i)[1] = 0;
                    src.at<Vec3b>(pInfo->y, i)[2] = 0;
                }

                //qDebug("(*itChild)[%d] minX=%d maxX=%d centroid=%d y=%d",
                //       idxChild, pInfo->min_x, pInfo->max_x, pInfo->centroid, pInfo->y);
            }
            ++idxChild;
            ++itChild;
        }

        ++itWindow;
    }
#endif
}

void LaneFilter::PrepareImageForLaneDetection(const Mat &matScaledOrgClone)
{
    //m_matMixIn.release();
    //m_matMixOut.release();
    //m_matMaskMix.release();
    m_matMaskMix = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    //m_matMixOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);
    //DSG(1, "PrepareImageForLaneDetection m_matMaskMix=");
    //PrintMatInfo(m_matMaskMix);

    m_matCannyIn.release();
    m_matCannyOut.release();
    m_matCannyOut = Mat::zeros(matScaledOrgClone.size(), CV_8U);

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if (pCache->lanedetection_manual_enabled)
    {
        m_emittedLanePool.push_back(pCache->manual_leftlane);
        m_emittedLanePool.push_back(pCache->manual_rightlane);
    }
}

void LaneFilter::ReactFromTrackbarLane(Mat &in, Mat &out)
{
    Canny(in, out, m_cannyThres1, m_cannyThres2, 3 + (m_cannyApertureSizeDiv2 << 1), (bool)m_cannyIsgradient);
}

void LaneFilter::ThresholdCallbackMix()
{
    //const Mat *pSrc = pImageProcessing->GetScaledImage();
    //const Mat *pSrcF = pImageProcessing->GetScaledImageF();
    //const Mat *pHLSMask = pImageProcessing->GetHLSMask();
    //const Mat *pHSVMask = pImageProcessing->GetHSVMask();
    //const Mat *pSobelMask = pImageProcessing->GetSobelMask();
    //const Mat *pMixMask;
    //const Mat *pMixOut;
    //const Mat *pMixMask = &m_matMaskMix;// pImageProcessing->GetMixMask();
    //const Mat *pMixOut = &m_matMixOut;// pImageProcessing->GetMixOutImage();


    //Mat src = m_matScaledImage;

    bool bFound;
    //pImageProcessing->UpdateOutputMatMix_HLSAndHSVAndSobel(*pSrc, *pHLSMask, *pHSVMask, *pSobelMask,
    //    *(const_cast<Mat*>(pMixMask)), *(const_cast<Mat*>(pMixOut)));
    Mat maskHLS;
    Mat maskHSV;
    Mat maskSobel;

    int idx = 0;
    for (auto it = m_mapImgPack.begin(); it != m_mapImgPack.end(); ++it)
    {
        if (0 == strcmp(k_str_hls_filter_name, it->first->m_name))
        {
            maskHLS = it->second.clone();
            //imshow("maskHLS 2", maskHLS);
            //PrintMatInfo(maskHLS);
        }
        else if (0 == strcmp(k_str_hsv_filter_name, it->first->m_name))
        {
            maskHSV = it->second.clone();
            //imshow("maskHSV 2", maskHSV);
            //PrintMatInfo(maskHSV);
        }
        else if (0 == strcmp(k_str_sobel_filter_name, it->first->m_name))
        {
            maskSobel = it->second.clone();
            //imshow("maskSobel 2", maskSobel);
           // PrintMatInfo(maskSobel);
        }
        //if (0 == idx) {
        //    maskHLS = it->second.clone();
        //    {
        //        DSG(1, "eeeeeeee %s", it->first->m_name);
        //        imshow("maskHLS 2", maskHLS);


        //    }
        //    it->second.release();


        //    //imshow(k_str_hls_filter_name, maskHLS);
        //   // DSG(1, "ThresholdCallbackMix maskHLS=%d %d %d", maskHLS.type(), maskHLS.cols, maskHLS.rows);
        //}
        //else if (1 == idx) {
        //    maskHSV = it->second.clone();
        //    it->second.release();
        //    //imshow(k_str_hsv_filter_name, maskHSV);
        //    //DSG(1, "ThresholdCallbackMix maskHSV=%d %d %d", maskHSV.type(), maskHSV.cols, maskHSV.rows);
        //}
        //else if (2 == idx) {
        //    maskSobel = it->second.clone();
        //    it->second.release();
        //    //imshow(k_str_sobel_filter_name, maskSobel);
        //   // DSG(1, "ThresholdCallbackMix maskSobel=%d %d %d", maskSobel.type(), maskSobel.cols, maskSobel.rows);
        //}
        //else if (1 == idx) maskHSV = it->second;
        //else if (2 == idx) maskSobel = it->second;
        ++idx;
    }
    //DSG(1, "ThresholdCallbackMix 22 %d", m_mapImgPack.size());
    
    //
    //imshow(k_str_sobel_filter_name, maskSobel);
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    for (int rowIdx = 0; rowIdx < maskHLS.rows; ++rowIdx)
    {
        for (int colIdx = 0; colIdx < maskHLS.cols; ++colIdx)
        {
            bFound = false;
            if (mask_hls_and_hsv_and_sobel == pCache->lanedetection_maskmix_mode)
            {
                if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) &&
                     (255 == maskHSV.at<uchar>(rowIdx, colIdx))) &&
                     (255 == maskSobel.at<uchar>(rowIdx, colIdx)))
                    bFound = true;
            }
            else if (mask_hls_and_hsv_or_sobel == pCache->lanedetection_maskmix_mode)
            {
                if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) &&
                     (255 == maskHSV.at<uchar>(rowIdx, colIdx))) ||
                     (255 == maskSobel.at<uchar>(rowIdx, colIdx)))
                    bFound = true;
            }
            else if (mask_hls_or_hsv_or_sobel == pCache->lanedetection_maskmix_mode)
            {
                if (((255 == maskHLS.at<uchar>(rowIdx, colIdx)) ||
                     (255 == maskHSV.at<uchar>(rowIdx, colIdx))) ||
                     (255 == maskSobel.at<uchar>(rowIdx, colIdx)))
                    bFound = true;
            }

            if (bFound)
            {
                m_matMaskMix.at<uchar>(rowIdx, colIdx) = 255;
                //m_matMixOut.at<Vec3b>(rowIdx, colIdx) = src.at<Vec3b>(rowIdx, colIdx);
            }
            else
            {
                m_matMaskMix.at<uchar>(rowIdx, colIdx) = 0;
                //m_matMixOut.at<Vec3b>(rowIdx, colIdx) = 0;
            }
        }
    }
    //imshow("tmp", m_matMaskMix);
}

void LaneFilter::ThresholdCallbackLane(int pos, void *userdata)
{
    //DSG(1, "ThresholdCallbackLane CheckLaneDetectionAndCalibration()=%d", CheckLaneDetectionAndCalibration());
    //if (CheckLaneDetectionAndCalibration() || (calibration_counter > 0)) // or in calibration procedure
    {
    //DSG(1, "ThresholdCallbackLane");
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    //const Mat *pSrc = &m_matScaledImage;

    //const Mat *pSrcGray = &m_matScaledImageGray;

    //pImageProcessing->ResetDetectedLaneImage(pImageProcessing, *pSrc);
    //Mat *pLaneDetectedImage = &m_matDetectedLane;
    //pLaneDetectedImage->release();
    //*pLaneDetectedImage = (*pSrc).clone();

    int numsOfLines;
    int roiLeft, roiRight, roiTop, roiBottom;
    int lineLeft, lineRight, lineTop, lineBottom;

    //const Mat *pMixMask = &m_matMaskMix;// pImageProcessing->GetMixMask();
    //const Mat *pMixOut = m_matMixOut;// pImageProcessing->GetMixOutImage();
    //const Mat *pCannyOut = &m_matCannyOut;// pImageProcessing->GetCannyOutImage();

    //if (pCache->video_stability_enabled)
    //    PredictLanes();
    
    if (pCache->lanedetection_manual_enabled)
    {

    }
    else
    {
        m_matCannyIn = m_matMaskMix;
        //m_matCannyOut.release();
        ReactFromTrackbarLane(m_matCannyIn, m_matCannyOut);
        //imshow("m_matCannyIn", m_matCannyIn);
        //imshow("m_matCannyOut", m_matCannyOut);
        if (pCache->roi_enabled)
        {
            //roiLeft = roiRight = roiTop = roiBottom = 0;
            //QVector<DrawableGraphicsItem<QGraphicsLineItem, QPushButton>*> *pROILines;
            //pROILines = pImageProcessing->GetScene()->getROILines();
            //for (size_t i = 0; i < pROILines->size(); ++i)
            //{
            //    DrawableGraphicsItem<QGraphicsLineItem, QPushButton> *pROILine = pROILines->at(i);
            //    QGraphicsLineItem *pLine = static_cast<QGraphicsLineItem*>(pROILine->getItem());

            //    if (0 == i)
            //        roiTop = pLine->pos().y();
            //    else if (1 == i)
            //        roiBottom = pLine->pos().y();
            //    else if (2 == i)
            //        roiLeft = pLine->pos().x();
            //    else if (3 == i)
            //        roiRight = pLine->pos().x();
            //}
            ////// To Do ...
            ////cv::Rect rcROI;
            ////CVFindRect(roiLeft, roiTop, roiRight, roiBottom, 10, (*pSrc).cols, (*pSrc).rows, rcROI);

            ////Mat matROISrc(*pSrc, rcROI);
        }

        //std::vector<bool> *pLaneInROI = &(pLaneLinesInfo->in_roi);
        //SLanelinesInfo* pLaneLinesInfo = &m_detectedLaneLineInfo;
        //std::vector<cv::Vec4i> *pLaneArray = &(pLaneLinesInfo->array);
        std::vector<cv::Vec4i> tmpLaneArray;
        //std::vector<cv::Point2f> *pLaneWinTl = &(pLaneLinesInfo->sliding_win_tl);
        //std::vector<cv::Point2f> *pLaneWinBr = &(pLaneLinesInfo->sliding_win_br);
        //std::vector<std::vector<SSlidingWindowInfo>> *ppWinInfo2DimList =
        //    &(pLaneLinesInfo->sliding_win_info_list);

        //ReleaseLanelinesInfo(true);

        FindLanes(m_matCannyOut, &tmpLaneArray);

        //LaneDetouching(pLaneLinesInfo);


        //DSG(1, "XXX calibration_counter=%d, max_fps = %d", calibration_counter, max_fps);
        //DSG(1, "XXX m_workoutLanePool 1 .size()=%d", m_workoutLanePool.size());

        if (pCache->calibration_counter < pCache->max_fps)
        {
            if (0 == pCache->calibration_counter)
            {
                //DSG(1, "XXXXX start to calibration_counter=%d", pCache->calibration_counter);
                m_workoutLanePool.clear();
                m_workoutLanePool.shrink_to_fit();
            }
            ++pCache->calibration_counter;
            //DSG(1, "XXXXX working calibration_counter=%d", pCache->calibration_counter);

            m_workoutLanePool.insert(std::end(m_workoutLanePool), std::begin(tmpLaneArray), std::end(tmpLaneArray));
        }
        //DSG(1, "XXX m_workoutLanePool 2 .size()=%d", m_workoutLanePool.size());
        if (pCache->calibration_counter == pCache->max_fps)
        {
            pCache->calibration_counter = 0;
            pCache->lanedetection_paused = true;
            //DSG(1, "XXXXX finish calibration_counter=%d", pCache->calibration_counter);


            //ClusterLanesSecond(m_workoutLanePool);
            //calibration_counter = 0;
            //m_startToUseLanePool = true;
            if (m_workoutLanePool.empty())
            {
                //DSG(1, "pImageProcessing->m_lanelineLeft=%d %d %d %d"
                //    , pImageProcessing->m_lanelineLeft[0]
                //    , pImageProcessing->m_lanelineLeft[1]
                //    , pImageProcessing->m_lanelineLeft[2]
                //    , pImageProcessing->m_lanelineLeft[3]);
                m_workoutLanePool.push_back(m_defaultLaneLeft);
                m_workoutLanePool.push_back(m_defaultLaneRight);
            }
            //DSG(1, "XXX m_workoutLanePool 3 empty=%d .size()=%d", m_workoutLanePool.empty(), m_workoutLanePool.size());
            m_emittedLanePool.clear();
            m_emittedLanePool.shrink_to_fit();
            m_emittedLanePool.assign(m_workoutLanePool.begin(), m_workoutLanePool.end());
        }
    }

    }
}

void LaneFilter::DoImageProcessing(const Mat &matImage)
{
    //DSG(1, "LaneFilter::DoImageProcessing");
    //if (CheckLaneDetectionAndCalibration() || (calibration_counter > 0)) // or in calibration procedure
    if (IsLaneDetectionEnabled())
    {
        if (0 == m_currentFrame)
            PrepareImageForLaneDetection(matImage);

#ifdef TEST_TIME
        m_calcTimer.ResetTimer();
#endif
        ThresholdCallbackMix(); //5ms
#ifdef TEST_TIME
        m_calcTimer.Print(0, "LaneFilter::DoImageProcessing ThresholdCallbackMix");//4
        m_calcTimer.ResetTimer();
#endif
        ThresholdCallbackLane(0, 0); 
#ifdef TEST_TIME
        m_calcTimer.Print(1, "LaneFilter::DoImageProcessing ThresholdCallbackLane");//25~27
#endif
        DSG(1, "LaneFilter::DoImageProcessing emit updateLaneline(m_emittedLanePool)");
        emit updateLaneline(m_emittedLanePool);
        emit cvColorReady(m_matCannyOut);

        //emit cvColorReady(matImage);
    }
    else
        emit cvColorReady(matImage);
}

void LaneFilter::timerEvent(QTimerEvent *event) {
    //DSG(1, "LaneFilter::timerEvent ev->timerId()=%d %d", event->timerId(), m_timer.timerId());
    //if (event->timerId() != m_nTimerID) return;
    if (event->timerId() != m_timer.timerId())  return; // { QObject::timerEvent(event); return; }

    if (e_component_state_started != m_state)
        return;

    m_timer.stop();
    {
        QMutexLocker locker(&m_mutex);
        
        //DSG(1, "LaneFilter::timerEvent::IsLaneDetectionEnabled()=%d", IsLaneDetectionEnabled());
        if (IsLaneDetectionEnabled())
        {
            int numOfChReady = 0;
            //DSG(1, "LaneFilter::timerEvent m_mapFilterImgList.size()=%d", m_mapFilterImgList.size());
            for (auto it = m_mapFilterImgList.begin(); it != m_mapFilterImgList.end(); ++it)
            {
                //DSG(1, "LaneFilter::timerEvent it->second->length()=%d", it->second->length());
                if (it->second->length() > 0)
                {
                    cv::Mat image = it->second->dequeue();
                    //DSG(1, "LaneFilter::timerEvent event from %s", it->first->m_name);

                    m_mapImgPack[it->first] = image.clone();
                    image.release();
                    ++numOfChReady;
                }
            }
            if (numOfChReady == m_mapFilterImgList.size())
            {
                DoImageProcessing((*m_mapImgPack.begin()).second);

                ++m_currentFrame;
                if ((m_currentFrame % 30) == 0)
                    DSG(1, "LaneFilter::DoImageProcessing m_currentFrame=%d", m_currentFrame);
            }
        }
        else
        { 
            //DSG(1, "LaneFilter::timerEvent m_imageList.size()=%d", m_imageList.size());
            if (m_imageList.size() > 0)
            {
                cv::Mat image = m_imageList.dequeue();
                m_frame = image.clone();
                image.release();

                emit cvColorReady(m_frame);
            }
        }
    }
    
    //for (auto it = m_mapImgPack.begin(); it != m_mapImgPack.end(); ++it)
    //{
    //    //it->second.release();
    //    //cv::Mat image = it->second;
    //    //DSG(1, "LaneFilter::timerEvent event from %s", it->first->m_name);

    //    //m_mapImgPack[it->first] = image.clone();
    //    //image.release();
    //}
    //DoImageProcessing2(m_mapImgPack[m_mapImgPack.begin()->first]);
}
