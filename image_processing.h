#ifndef IMAGE_PROCESSING_H
#define IMAGE_PROCESSING_H
#if 0
#include <QObject>
#include <QElapsedTimer>
#include <QQueue>
#include <vector>
#include <opencv2/opencv.hpp>
#include "autogenerator_line.h"
#include "autogenerator_rect.h"
#include "drawable_graphicsscene.h"
#include "common.h"
#include "image_common.h"
#include "image_processing_thread.h"
#include "optionsdialog.h"
#include "trvideothread.h"
#include "pythonwrapper.h"


#define USE_CV_32FC3

using namespace cv;

typedef enum E_Trackbar_OnOff_Tag {
	e_trackbar_force_normal = 0,
	e_trackbar_force_off,
	e_trackbar_force_on
} ETrackbarOnOff;

typedef struct S_Car_Agent_Tag {
    std::vector<cv::Rect> car_list;
    std::vector<AutoGeneratorRect> autogenerator_list;

} SCarAgent;

/*
class ImageProcessing;
class ProcessingSetting : public QObject
{
    Q_OBJECT

public:
    ProcessingSetting(ImageProcessing *pParent = nullptr)
        : m_pParent(pParent)
		, m_laneDetectionEnabled(true)
        , m_vehicleDetectionEnabled(true)
        , m_roiEnabled(false)
        , m_videoStabilityEnabled(false)
        , m_trackBarEnabled(false)
		, m_engineeringmodeEnabled(false)
        , m_tolerance(25)
        , m_vd1PassDiffThreshold(30)
        , m_lane_detection_approach(e_lanedetection_mix_hls_hsv_canny)
        , rc_roi(cv::Rect(0, 0, 0, 0))
    {
		m_roi_points.resize(4);
    }
    void SetParent(ImageProcessing *pParent) { m_pParent = pParent; }
	void setEnableLaneDetection(bool on) { m_laneDetectionEnabled = on; }
	void setEnableVehicleDetection(bool on) { m_vehicleDetectionEnabled = on; }
    void setEnableROI(bool on) { m_roiEnabled = on; }
	void setEnableVideoStability(bool on) { m_videoStabilityEnabled = on; }
    void setEnableBar(bool on) { m_trackBarEnabled = on; }
	void setEnableEngineeringMode(bool on) { m_engineeringmodeEnabled = on; }
    void setTolerance(int tolerance) { m_tolerance = tolerance; }
	void setVD1PassDiffThreshold(int vd1PassDiffThreshold) { m_vd1PassDiffThreshold = vd1PassDiffThreshold; }
	void setVehicleDetectionModel(int model) { m_vehicleDetectionModel = model; }

    void setOperationMode(EOperationMode operationMode) { m_operationMode = operationMode; }
    void setLaneDetectionApproach(ELaneDetectionApproach approach) { m_lane_detection_approach = approach; }
	void setLaneDetectionCycleTime(ELaneDetectionCycleTime cycletime) { m_lane_detection_cycletime = cycletime; }
	void setVehicleDetectionCycleTime(EVehicleDetectionCycleTime cycletime) { m_vehicle_detection_cycletime = cycletime; }
	void setROIPoints(std::vector<cv::Point> roiPoints) { m_roi_points.clear();  m_roi_points.resize(4); m_roi_points = roiPoints; }

	bool getEnableLaneDetection() { return m_laneDetectionEnabled; }
	bool getEnableVehicleDetection() { return m_vehicleDetectionEnabled; }
    bool getEnableROI() { return m_roiEnabled; }
	bool getEnableVideoStability() { return m_videoStabilityEnabled; }
    bool getEnableBar() { return m_trackBarEnabled; }
	bool getEnableEngineeringMode() { return m_engineeringmodeEnabled; }
	std::vector<cv::Point>* getROIPoints() { return &m_roi_points; }

    int getTolerance() { return m_tolerance; }
	int getVD1PassDiffThreshold() { return m_vd1PassDiffThreshold; }
	int getVehicleDetectionModel() { return m_vehicleDetectionModel; }

    EOperationMode getOperationMode() { return m_operationMode; }
    ELaneDetectionApproach getLaneDetectionApproach() { return m_lane_detection_approach; }
	ELaneDetectionCycleTime getLaneDetectionCycleTime() { return m_lane_detection_cycletime; }
	EVehicleDetectionCycleTime getVehicleDetectionCycleTime() { return m_vehicle_detection_cycletime; }

private:
    ImageProcessing *m_pParent;
	bool m_laneDetectionEnabled;
	bool m_vehicleDetectionEnabled;
    bool m_roiEnabled;
	bool m_videoStabilityEnabled;
    bool m_trackBarEnabled;
	bool m_engineeringmodeEnabled;
    int m_tolerance;
	int m_vd1PassDiffThreshold;
	int m_vehicleDetectionModel;
    EOperationMode m_operationMode;
    ELaneDetectionApproach m_lane_detection_approach;
	ELaneDetectionCycleTime m_lane_detection_cycletime;
	EVehicleDetectionCycleTime m_vehicle_detection_cycletime;

    cv::Rect rc_roi;
    std::vector<cv::Point> m_roi_points;
};
*/

class ImageProcessing
        //: public QObject {
        : public ImageProcessingThread {
    Q_OBJECT


public:
    explicit ImageProcessing(QObject *parent = 0);
    ~ImageProcessing();
    
    void SetRatInMazeMode(bool enable) { m_isRatInMaze = enable; }
    //bool CheckLaneDetectionAndCalibration() { return m_CheckLaneDetectionAndCalibration; }
    bool IsReadyToDetectVehicle() { return m_isReadyToDetectVehicle; }

    int getcurrentFrameIdx() { return m_currentFrame; }
    DrawableGraphicsScene* GetScene() { return m_pScene; }
    CPythonWrapperModel* GetModel() { return m_pPythonWrapperModel; }
    SCarAgent* GetCarAgent() { return &m_carAgent; }
    //ProcessingSetting* GetProcessingSetting() { return &m_processingSetting; }
	SVideoConfig* GetVideoConfig() { return &m_videoConfigFromSource; }
    std::vector<cv::Vec4i>* GetDetectedLaneLines() { return &g_detectedLaneLines; }
    //std::vector<cv::Vec4i>* GetRoiAllowedLaneLines() { return &g_roiAllowedLaneLines; }
    SLanelinesInfo* GetDetectedLaneLinesInfo() { return &m_detectedLaneLineInfo; }
	//QQueue<cv::Mat>* GetScaledImageList() { return &m_matScaledImageList; }
	const cv::Mat* GetScaledImage() { return &m_matScaledImage; }
	const cv::Mat* GetScaledImageF() { return &m_matScaledImageF; }
	const cv::Mat* GetScaledImageP() { return &m_matScaledImagePrevious; }
	const cv::Mat* GetScaledImageGray() { return &m_matScaledImageGray; }

    void SetApproach(EImageProcessingApproach approach) { m_approach = approach;  }
//#ifdef USE_CV_32FC3
//        return &m_matScaledImageF;
//#else
//        return &m_matScaledImage;
//#endif
//    }
	const std::vector<cv::Mat>* GetHLSChannel() { return &m_matHLSChannel; }
	const cv::Mat* GetHLSMaskH() { return &m_matMaskHLS_H; }
	const cv::Mat* GetHLSMaskL() { return &m_matMaskHLS_L; }
	const cv::Mat* GetHLSMaskS() { return &m_matMaskHLS_S; }
	const cv::Mat* GetHLSMask() { return &m_matMaskHLS; }
	const cv::Mat* GetHSVMaskH() { return &m_matMaskHSV_H; }
	const cv::Mat* GetHSVMaskS() { return &m_matMaskHSV_S; }
	const cv::Mat* GetHSVMaskV() { return &m_matMaskHSV_V; }
	const cv::Mat* GetHSVMask() { return &m_matMaskHSV; }
	const cv::Mat* GetSobelMask() { return &m_matMaskSobel; }
	const cv::Mat* GetMixMask() { return &m_matMaskMix; }
	const cv::Mat* GetMixOutImage() { return &m_matMixOut; }
    const cv::Mat* GetCannyOutImage() { return &m_matCannyOut; }
    cv::Mat* GetLaneDetectedImage() { return &m_matDetectedLane; }
    

	//void InitParameters(const char *strFileName);
	//void SetVideoConfig(SVideoConfig *pConfig) { memcpy(&m_videoConfigFromSource, pConfig, sizeof(SVideoConfig)); }
    int  Probe(cv::Mat &matScaledOrgClone, DrawableGraphicsScene *pScene);
    void SyncProcess();
    void AsyncProcess(const cv::Mat &matScaledOrgClone);
    void Release();

	void EmitMaskHLSImageReady(const cv::Mat &matImage);
	void EmitMaskHSVImageReady(const cv::Mat &matImage);
	void EmitMaskSobelImageReady(const cv::Mat &matImage);
	void EmitMixOutImageReady(const cv::Mat &matImage);
	void EmitCannyOutImageReady(const cv::Mat &matImage);
    void EmitDisplayImageReady(const cv::Mat &matImage);

    void EmitUpdateLaneline(std::vector<cv::Vec4i> *pLanelineArray);
	void EmitUpdateVehicleInfo(std::vector<cv::Rect> *pVehicleArray);

    void ReactFromTrackbarHLS(const cv::Mat &in, cv::Mat *lpMaskH, cv::Mat *lpMaskL, cv::Mat *lpMaskS);
	void ReactFromTrackbarHSV(const cv::Mat &in, cv::Mat *lpMaskH, cv::Mat *lpMaskS, cv::Mat *lpMaskV);
    //void ReactFromTrackbarHSV(const cv::Mat &in, cv::Mat &mask);
    void ReactFromTrackbarSobel(const cv::Mat &in, cv::Mat &maskX, cv::Mat &maskY);
    void ReactFromTrackbarCanny(cv::Mat &in, cv::Mat &out);

    //void UpdateOutputMatHLS(const cv::Mat &orgImage, const cv::Mat &maskGray, cv::Mat &dst);
	void UpdateOutputMatHLS(const Mat &srcF, const Mat &maskH, const Mat &maskL, const Mat &maskS, cv::Mat &mask, cv::Mat &dst);
	void UpdateOutputMatHSV(const Mat &srcF, const Mat &maskH, const Mat &maskS, const Mat &maskV, cv::Mat &mask, cv::Mat &dst);
	
    //void UpdateOutputMatHSV(const cv::Mat &orgImage, const cv::Mat &mask, cv::Mat &dst);
    //void UpdateOutputMatSobel(cv::Mat &orgImage, cv::Mat &maskX, cv::Mat &maskY, cv::Mat &dst);
	void UpdateOutputMatSobel(const cv::Mat &src, const cv::Mat &mask, cv::Mat &dst);

    void FindLanes(const cv::Mat &in, std::vector<cv::Vec4i> *lpDetectedLaneLineArray);
    bool FindDiffBetweenMatrix(cv::Mat &matBg, cv::Mat &matScaled, SLanelinesInfo *pLaneLinesInfo, float delta);
    void LaneDetouching(cv::Mat &src, SLanelinesInfo *pLaneLinesInfo, bool bGenSWInfoFile, QString swInfoFileName);
	void CollectSlidingWindowInStaticMode();
    void LaneDetouching(ImageProcessing *pImageProcessing, SLanelinesInfo *pLaneLinesInfo);// , cv::Mat &src);
    void PredictLanes();// cv::Mat &src);
	bool CheckPointInROI();

    static void UpdateOutputMatMix_HLSAndHSV(
			const cv::Mat &src, const cv::Mat &maskHLS, const cv::Mat &maskHSV, cv::Mat &mixOut);
    static void UpdateOutputMatMix_HLSAndHSVAndSobel(
			const cv::Mat &src, const cv::Mat &maskHLS, const cv::Mat &maskHSV, const cv::Mat &maskSobel, 
			cv::Mat &mixMask, cv::Mat &mixOut);

protected:
	void DoImageProcessing(const cv::Mat &matImage);
    void DoImageProcessingForPreview(const cv::Mat &matImage);
    void DoImageProcessingForGrabberPreview(const cv::Mat &matImage);
    void DoImageProcessingForGrabberCapture(const cv::Mat &matImage);
    void DoImageProcessingForEtronPreview(const cv::Mat &matImage);
    void DoImageProcessingForEtronCapture(const cv::Mat &matImage);

    static void ThresholdCallbackAll(int pos, void *userdata);
    static void ThresholdCallbackAllAsync(int pos, void *userdata);
    static void ThresholdCallbackHLS(int pos, void *userdata);
    static void ThresholdCallbackHSV(int pos, void *userdata);
    static void ThresholdCallbackSobel(int pos, void *userdata, bool willUpdateOutputMat);
    static void ThresholdCallbackMix(ImageProcessing *pImageProcessing);
    static void ThresholdCallbackCanny( int, void* );
    static void ThresholdCallbackCanny2(int, void*);

    static void ThresholdCallbackVehicle( int, void* );
    static void ThresholdCallbackCarClassifierEngineeringMode();

    void PrepareImageForLaneDetectionHLS(cv::Mat &matImage);
    void PrepareImageForLaneDetectionHSV(cv::Mat &matImage);
    void PrepareImageForLaneDetectionMix(cv::Mat &matImage);
    void PrepareImageForLaneDetectionSobel(cv::Mat &matImage);
    void PrepareImageForLaneDetectionCanny(cv::Mat &matImage);
    void PrepareImageForCarDetection(cv::Mat &matImage);

    void CreateWindowHLS();
    void CreateWindowHSV();
    void CreateWindowSobel();
    void CreateWindowMix();
    void CreateWindowCanny();
    void CreateWindowCarClassifier();

    void DestroyWindowHLS();
    void DestroyWindowHSV();
    void DestroyWindowSobel();
    void DestroyWindowMix();
    void DestroyWindowCanny();
    void DestroyWindowCarClassifier();

    void ShowWindowHLS();
    void ShowWindowHSV();
    void ShowWindowSobel();
    void ShowWindowMix();
    void ShowWindowCanny();
    void ShowWindowCarClassifier();

	void CreateWindows();
	void DestroyWindows();
    void ShowTrackbarWindow(ETrackbarOnOff onoff);

    void AddCarInfo(cv::Rect *pRect, AutoGeneratorRect *pAutogenerator);
    void DeleteCarAgent();


    void ResetDetectedLaneImage(ImageProcessing *pImageProcessing, const cv::Mat &matRef);
	cv::Mat* ReassignDisplayImage(const cv::Mat &matRef);
    bool IsLaneDetectionEnabled();
    bool IsVehicleDetectionAllowed();
	void ReleaseLanelinesInfo(bool all);

	void SeeAllRect(cv::CascadeClassifier *pCascade, cv::Mat &in, cv::Mat &display, Scalar scalar);

signals:
    void videoImageProcessingStarted();
    void videoImageProcessingStopped();
    void targetImageReady(const cv::Mat &matResult, qint64 currentFrameIdx);
    //void displayImageReady(const cv::Mat &matImage);
    void maskHLSImageReady(const cv::Mat &matImage);
    void maskHSVImageReady(const cv::Mat &matImage);
    void maskSobelImageReady(const cv::Mat &matImage);
    void mixOutImageReady(const cv::Mat &matImage);
    void cannyOutImageReady(const cv::Mat &matImage);
    void diffImageReady(const cv::Mat &matImage);
    void demoImageReady(const cv::Mat &matImage);

    void updateLaneline(std::vector<cv::Vec4i> *pLanelineArray);
    void updateVehicleInfo(std::vector<cv::Rect> *pVehicleArray);


public slots:
	void started();
	void stopped();
	void handleEngineeringModeChanged();
    void handleShowTrackbarWindow();
    void handleUpdateCVRectItem();
    void handleCheckAdjustedROIEffect();
    void handleUpdateTolerance(int newTolerance);
    void handleUpstreamStarted();
    void handleUpstreamStopped();	
	//void handleSendImage(const cv::Mat &matImage);

signals:


private:
    //static const char *str_win_image;
    static const char *str_win_hls;
    static const char *str_win_hls_h_min;
    static const char *str_win_hls_l_min;
    static const char *str_win_hls_s_min;
    static const char *str_win_hls_h_max;
    static const char *str_win_hls_l_max;
    static const char *str_win_hls_s_max;
    static const char *str_win_hsv;
    static const char *str_win_hsv_h_min;
    static const char *str_win_hsv_s_min;
    static const char *str_win_hsv_v_min;
    static const char *str_win_hsv_h_max;
    static const char *str_win_hsv_s_max;
    static const char *str_win_hsv_v_max;
    static const char *str_win_sobel;
    static const char *str_win_mix;
    static const char *str_win_canny;
    static const char *str_win_car_classifier;


private:
    bool m_isRatInMaze;
    bool m_isWindowExisted;
    bool m_isFistTimeToShow;
	bool m_isEngineeringMode;
    //bool m_CheckLaneDetectionAndCalibration;
    bool m_isReadyToDetectVehicle;
    int32_t m_currentFrame;

	EImageProcessingState m_state;
    EImageProcessingApproach m_approach;

    CPythonWrapper *m_pPythonWrapper;
    CPythonWrapperModel *m_pPythonWrapperModel;
    DrawableGraphicsScene *m_pScene;

	QElapsedTimer m_timerTotal;
	QElapsedTimer m_timerSlice;

    //ProcessingSetting m_processingSetting;
	SVideoConfig m_videoConfigFromSource;
    SCarAgent m_carAgent;

    std::vector<cv::Vec4i> g_detectedLaneLines;
    //std::vector<cv::Vec4i> g_roiAllowedLaneLines;
    SLanelinesInfo m_detectedLaneLineInfo;

	//QQueue<cv::Mat> m_matScaledImageList;
	cv::Mat m_matScaledImagePrevious;
    cv::Mat m_matScaledImage;
    cv::Mat m_matScaledImageF;
	cv::Mat m_matScaledImageGray;
    cv::Mat m_matDetectedLane;

    std::vector<cv::Mat> m_matHLSChannel;
	cv::Mat m_matMaskHLS_H;
	cv::Mat m_matMaskHLS_L;
	cv::Mat m_matMaskHLS_S;
	cv::Mat m_matMaskHLS;
    //std::vector<cv::Mat> m_matHSVChannel;
	cv::Mat m_matMaskHSV_H;
	cv::Mat m_matMaskHSV_S;
	cv::Mat m_matMaskHSV_V;
	cv::Mat m_matMaskHSV;
    
	cv::Mat m_matMaskSobel;
	cv::Mat m_matMaskMix;
	cv::Mat m_matMixOut;
	cv::Mat m_matCannyOut;
	cv::Mat m_matDisplay;

public:
    cv::Vec4i m_lanelineLeft;
    cv::Vec4i m_lanelineRight;
};
#endif
#endif // IMAGE_MANAGER_H
