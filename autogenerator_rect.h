#ifndef AUTOGENERATOR_RECTANGLE_H
#define AUTOGENERATOR_RECTANGLE_H

#include <QRectF>
#include <QtGlobal>
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
#   include <QRandomGenerator>
#else
#include <QTime>
#endif

#include <opencv2/opencv.hpp>

class AutoGeneratorRect
{
public:
    enum { e_weight_more_lower, e_weight_more_higher };

private:
    int m_weightStrategyOfHorizontal;
    int m_weightStrategyOfVertical;
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    QRandomGenerator *m_generator;
#endif
    QRectF m_rect;

public:
    AutoGeneratorRect();

    void SetBoundary(QRectF rect);
    QRectF GenQtRect();
    QRectF TuneQtRect(QRectF rect);
    bool TuneCVRect(cv::Rect *pRect, cv::Rect *lpRect);
};

#endif // AUTOGENERATOR_RECTANGLE_H
