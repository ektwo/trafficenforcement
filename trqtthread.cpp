#include "trqtthread.h"
#if 0
#include "autogenerator_line.h"
#include "autogenerator_rect.h"

TRQtThread::TRQtThread(QObject *parent)
    : QThread(parent)
    , m_isStopped(false)
{

}

void TRQtThread::AddQTTask(SMovingQTObject *object)
{
	m_movingQTObjectList.push_back(*object);
}

void TRQtThread::Stop()
{
	qDebug("TRQtThread::Stop() m_isStopped=%d", m_isStopped);
	m_isStopped = true;
}

void TRQtThread::run()
{
    while (!m_isStopped) {

        SMovingQTObject *pObject = nullptr;
        QGraphicsItem *pItem;
        //qDebug("thread_run begin=%d", m_movingQTObjectList.count());
        for (int i = 0; i < m_movingQTObjectList.count(); ++i)
        {
            pObject = &m_movingQTObjectList[i];
            //qDebug("params %d %d ", pObject->interval_ms, pObject->approach);
            pItem = pObject->item;
            if (QGraphicsLineItem::Type == pItem->type())
            {
                QGraphicsLineItem *pLineItem = qgraphicsitem_cast<QGraphicsLineItem*>(pItem);
                AutoGeneratorLine *pAutoGeneratorLine = static_cast<AutoGeneratorLine*>(pObject->autogenerator);
                QLineF newLine = pAutoGeneratorLine->tuneLine(QLineF(pLineItem->pos().x(), pLineItem->pos().y(),
                                                                     pLineItem->pos().x() + pLineItem->line().x2(), pLineItem->pos().y() + pLineItem->line().y2()));
                emit updateAutoGeneratedLineItem(pLineItem, QPointF(newLine.x1(), newLine.y1()), QPointF(newLine.x2(), newLine.y2()));
                QThread::msleep(pObject->interval_ms);
            }
            else if (QGraphicsRectItem::Type == pItem->type())
            {
                QGraphicsRectItem *pRectItem = qgraphicsitem_cast<QGraphicsRectItem*>(pItem);
                AutoGeneratorRect *pAutoGeneratorRect = static_cast<AutoGeneratorRect*>(pObject->autogenerator);
                QRectF newRect = pAutoGeneratorRect->TuneQtRect(pRectItem->rect());
                emit updateRectItem(pRectItem, newRect);
                QThread::msleep(pObject->interval_ms);
            }
            //QThread::msleep(pObject->interval_ms);
        }
    }

    qDebug("TRQtThread::Run() done");
    emit done();
}
#endif