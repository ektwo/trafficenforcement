#include "filter_sobel.h"
#include "parameters_table.h"
#include "common.h"


extern SParametersTable k_parameters_table[2];
SobelFilter::SobelFilter(BaseGraph *parent, int profileIdx)
    : Filter(parent)
    , m_dxyMode(k_parameters_table[profileIdx].sobel.dxymode)
    , m_kSizeX(k_parameters_table[profileIdx].sobel.ksize_x)
    , m_kSizeY(k_parameters_table[profileIdx].sobel.ksize_y)
    , m_thresXMin(k_parameters_table[profileIdx].sobel.threshold_x_min)
    , m_thresXMax(k_parameters_table[profileIdx].sobel.threshold_x_max)
    , m_thresYMin(k_parameters_table[profileIdx].sobel.threshold_y_min)
    , m_thresYMax(k_parameters_table[profileIdx].sobel.threshold_y_max)
    , m_thresType(k_parameters_table[profileIdx].sobel.threshold_type)
    , m_pScene(k_parameters_table[profileIdx].sobel.scene)
{
    m_class = k_str_class_transform_name;
    m_name = k_str_sobel_filter_name;
    DSG(1, "SobelFilter m_class=%s", m_class);
    DSG(1, "SobelFilter m_name=%s", m_name);

    if (m_pScene)
    {
        m_pScene->SetOwner(parent);
        connect(this, SIGNAL(iamStarted()), m_pScene, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), m_pScene, SLOT(handleUpstreamStopped()));
        connect(this, &Filter::cvColorReady, m_pScene, &EscortGraphicsScene::handleUpdateViewCV);
    }
}

SobelFilter::~SobelFilter()
{
    if (m_pScene)
    {
        m_pScene->SetOwner(nullptr);
        disconnect(this, &Filter::cvColorReady, 0, 0);
    }
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

bool SobelFilter::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = false;
    if (pDownstreamFilter)
    {
        ret = __super::ConnectTo(pDownstreamFilter);

        connect(this, SIGNAL(iamStarted()), pDownstreamFilter, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), pDownstreamFilter, SLOT(handleUpstreamStopped()));
        connect(this, &Filter::imgProcessedReady, pDownstreamFilter, &Filter::handleProcessedFrameCV);
    }
    return ret;
}

void SobelFilter::DisconnectAll()
{
    __super::DisconnectAll();

    disconnect(this, &Filter::imgProcessedReady, 0, 0);
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

void SobelFilter::handleUpstreamStarted()
{
    ++m_startedUpstreamFilters;
    DSG(1, "SobelFilter::handleUpstreamStarted m_startedUpstreamFilters=%d m_pUpstreamFilterList.size()=%d",
        m_startedUpstreamFilters, m_pUpstreamFilterList.size());
    if (m_startedUpstreamFilters == m_pUpstreamFilterList.size())
    {
        __super::handleUpstreamStarted();


    }
}

void SobelFilter::handleUpstreamStopped()
{
    --m_startedUpstreamFilters;
    DSG(1, "SobelFilter::handleUpstreamStopped m_startedUpstreamFilters=%d", m_startedUpstreamFilters);
    if (m_startedUpstreamFilters == 0)
    {
        m_imageList.clear();

        __super::handleUpstreamStopped(); // change filter state and emit signal to next filter
    }
}

void SobelFilter::handleColorFrameCV(const cv::Mat &frame)
{
    //DSG(1, "SobelFilter::handleProcessedFrameCV");
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 10)
        {
            DSG(1, "queue m_imageList.size() drop = %d", m_imageList.size());
            cv::Mat image = m_imageList.dequeue();
            image.release();
        }

        m_imageList.enqueue(frame);
    }

    //if (m_readyToEmit)
    {
        if (!m_timer.isActive())
            m_timer.start(0, this);
    }
}

void SobelFilter::PrepareImageForLaneDetection(const Mat &matScaledOrgClone)
{
    m_matMaskSobelX = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matMaskSobelY = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matMaskSobel = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matSobelOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);
}

void SobelFilter::ReactFromTrackbar(const Mat &in, Mat &maskX, Mat &maskY)
{
    Mat dstX, dstY;
    Mat absDstX, absDstY;

    int threadTypeX = m_thresType;
    int threadTypeY = m_thresType;

    if (0 == m_dxyMode) //Gradient X
    {
        Sobel(in, dstX, CV_16S, 1, 0, (m_kSizeX * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstX, absDstX); 
        inRange(absDstX, Scalar(m_thresXMin), Scalar(m_thresXMax), maskX);
    }
    else if (1 == m_dxyMode) // Gradient Y
    {
        Sobel(in, dstY, CV_16S, 0, 1, (m_kSizeY * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstY, absDstY); 
        inRange(absDstY, Scalar(m_thresYMin), Scalar(m_thresYMax), maskY);
    }
    else if (2 == m_dxyMode)
    {
        Sobel(in, dstX, CV_64F, 1, 0, (m_kSizeX * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstX, absDstX); 

        inRange(absDstX, Scalar(m_thresXMin), Scalar(m_thresXMax), maskX);

        Sobel(in, dstY, CV_16S, 0, 1, (m_kSizeY * 2) + 1, 1, 0, BORDER_DEFAULT);
        convertScaleAbs(dstY, absDstY); 

        inRange(absDstY, Scalar(m_thresYMin), Scalar(m_thresYMax), maskY);


    }
    else if (3 == m_dxyMode)
    {
        Sobel(in, dstX, CV_64F, 1, 0, (m_kSizeX * 2) + 1, 1, 0, BORDER_DEFAULT);

        Mat sobelpow2X, sobelpow2Y;

        cv::pow(dstX, 2, sobelpow2X);

        Mat sobel_abs = cv::abs(sobelpow2X);

        double minv = 0.0, maxv = 0.0;
        minMaxIdx(sobel_abs, &minv, &maxv);

        Mat sobel_abs_norm = sobel_abs.mul(255.0 / maxv);

        cv::inRange(sobel_abs_norm, Scalar(m_thresXMin), Scalar(m_thresXMax), maskX);
    }
}

void SobelFilter::UpdateOutputMat(const Mat &src, const Mat &mask, Mat &dst)
{
    for (int rowIdx = 0; rowIdx < src.rows; ++rowIdx)
    {
        for (int colIdx = 0; colIdx < src.cols; ++colIdx)
        {
            if (255 == mask.at<uchar>(rowIdx, colIdx))

            {
                dst.at<Vec3b>(rowIdx, colIdx) = src.at<Vec3b>(rowIdx, colIdx);
            }
            else
                dst.at<Vec3b>(rowIdx, colIdx) = 0;

        }
    }
}

void SobelFilter::ThresholdCallback(int pos, void *userdata)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    {
        m_calcTimer.ResetTimer();

        Mat matSobelIn;
        cvtColor(m_matScaledImage, matSobelIn, COLOR_BGR2GRAY);

        ReactFromTrackbar(matSobelIn, m_matMaskSobelX, m_matMaskSobelY);

        bitwise_or(m_matMaskSobelX, m_matMaskSobelY, m_matMaskSobel);

        UpdateOutputMat(m_matScaledImage, m_matMaskSobel, m_matSobelOut);
#ifdef TEST_TIME
        m_calcTimer.Print(0, "SobelFilter::ThresholdCallback ");
#endif
    }
}

void SobelFilter::DoImageProcessing(const Mat &matImage)
{
    if (0 == m_currentFrame)
        PrepareImageForLaneDetection(matImage);

    m_matScaledImage = matImage;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    if ((pCache->lanedetection_enabled) && (!pCache->lanedetection_paused))
        //if ((!pCache->lanedetection_manual_enabled) && (pCache->lanedetection_enabled) && (!pCache->lanedetection_paused))
    {
        ThresholdCallback(0, 0);
        //imshow("m_matMaskSobel", m_matMaskSobel);
        //PrintMatInfo(m_matMaskSobel);
        emit imgProcessedReady(m_matMaskSobel);
        emit cvColorReady(m_matSobelOut);

    }
    else
        emit cvColorReady(matImage);
}

void SobelFilter::timerEvent(QTimerEvent *event)
{
    //qDebug("SobelFilter::timerEvent (%s) ev->timerId()=%d m_nTimerID=%d", m_name, event->timerId(), m_timer.timerId());
    //if (event->timerId() != m_nTimerID) return;
    if (event->timerId() != m_timer.timerId())  return; // { QObject::timerEvent(event); return; }
    //qDebug("FilterThread::timerEvent (%s) 1", m_name);
    if (e_component_state_started != m_state)
        return;

    m_timer.stop();
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 0)
        {
            cv::Mat image = m_imageList.dequeue();
            m_frame = image.clone();
            image.release();
        }
    }

    //qDebug("FilterThread::timerEvent (%s) m_frame=%d %d", m_name, m_frame.cols, m_frame.rows);
    DoImageProcessing(m_frame);

    ++m_currentFrame;
    if ((m_currentFrame % 30) == 0)
        DSG(1, "SobelFilter::DoImageProcessing m_currentFrame=%d", m_currentFrame);

    //Sleep(1);
}