#ifndef COMMON_H
#define COMMON_H


#include <windows.h>
#include <iostream>
#include <string>
#include <vector>

#ifdef QT_VERSION
#define DSG(mode, __format__, ...) { if (1 == mode) qDebug(__format__, ##__VA_ARGS__); }
#else
#ifdef __linux__
#define DSG(mode, __format__, ...) { if (1 == mode) printf(__format__, ##__VA_ARGS__); }
#elif _WIN32
#include <windows.h>
#define MAX_DBG_MSG_LEN (1024)
static void DSG(int mode, const char * format, ...)
{
    if (1 == mode) {
        char buf[MAX_DBG_MSG_LEN];
        va_list ap;
        va_start(ap, format);
        _vsnprintf(buf, sizeof(buf), format, ap);
        va_end(ap);
        OutputDebugStringA(buf);
    }
}
#include <tchar.h>
static void DSGW(int mode, const TCHAR *format, ...)
{
    if (1 == mode) {
        TCHAR buf[MAX_DBG_MSG_LEN];
        va_list ap;
        va_start(ap, format);
        _vsntprintf_s(buf, sizeof(buf), format, ap);
        va_end(ap);
        OutputDebugString(buf);
    }
}
#endif
#endif
//
//static void StdQueueClear(std::queue<int> &q)
//{
//    std::queue<int> empty;
//    std::swap(q, empty);
//}

#include <QElapsedTimer>

class CQElapsedTimer
{
    struct SElapseTimer
    {
        int64_t curr_idx;
        int64_t total;
    };
public:
    QElapsedTimer timer;
    CQElapsedTimer() { timer.start(); }
    
    void ResetTimer() { timer.start(); }
    void Print(int idx, const char * format, ...) {
        char buf[1024];
        va_list ap;
        va_start(ap, format);
        _vsnprintf_s(buf, sizeof(buf), _TRUNCATE, format, ap);

        if (idx >= m_timeInfoArray.size())
        {
            SElapseTimer t;
            t.curr_idx = 0; t.total = 0;
            m_timeInfoArray.push_back(t);
        }

        int64_t currentTime = timer.elapsed();
        m_timeInfoArray[idx].curr_idx = m_timeInfoArray[idx].curr_idx + 1;
        m_timeInfoArray[idx].total += currentTime;
        _snprintf_s(buf, sizeof(buf), _TRUNCATE, "%s [%d] curT=%lld avgT=%lld", 
            buf, idx, currentTime, m_timeInfoArray[idx].total / m_timeInfoArray[idx].curr_idx);

        va_end(ap);
        if ((m_timeInfoArray[idx].curr_idx % 20) == 0)
            qDebug(buf);
    }
    int64_t GetElapsed() { return timer.elapsed(); }
    ~CQElapsedTimer() { }

private:
    std::vector<SElapseTimer> m_timeInfoArray;
};

class CCritSec {

    // make copy constructor and assignment operator inaccessible

    CCritSec(const CCritSec &refCritSec);
    CCritSec &operator=(const CCritSec &refCritSec);

    CRITICAL_SECTION m_CritSec;

#ifdef DEBUG
public:
    DWORD   m_currentOwner;
    DWORD   m_lockCount;
    BOOL    m_fTrace;        // Trace this one
public:
    CCritSec();
    ~CCritSec();
    void Lock();
    void Unlock();
#else

public:
    CCritSec() {
        InitializeCriticalSection(&m_CritSec);
    };

    ~CCritSec() {
        DeleteCriticalSection(&m_CritSec);
    };

    void Lock() {
        EnterCriticalSection(&m_CritSec);
    };

    void Unlock() {
        LeaveCriticalSection(&m_CritSec);
    };
#endif
};

class CAutoLock {

    // make copy constructor and assignment operator inaccessible

    CAutoLock(const CAutoLock &refAutoLock);
    CAutoLock &operator=(const CAutoLock &refAutoLock);

protected:
    CCritSec * m_pLock;

public:
    CAutoLock(CCritSec * plock)
    {
        m_pLock = plock;
        m_pLock->Lock();
    };

    ~CAutoLock() {
        m_pLock->Unlock();
    };
};

std::string GetCurrentModuleFolder();

template<class T> class Interpolator
{

public:

    inline static T Linear(float target, T *v)
    {
        return (T)(target*(v[1]) + (T(1.0f) - target)*(v[0]));
    }

    inline static T Bilinear(float *target, T *v)
    {
        T v_prime[2] = {
            Linear(target[1], &(v[0])),
            Linear(target[1], &(v[2]))
        };

        return Linear(target[0], v_prime);

    }

    inline static T Trilinear(float *target, T *v)
    {
        T v_prime[2] = {
            Bilinear(&(target[0]), &(v[0])),
            Bilinear(&(target[1]), &(v[4]))
        };

        return Linear(target[2], v_prime);
    }
};

#endif // COMMON_H
