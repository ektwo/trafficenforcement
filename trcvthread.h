#ifndef TRCVTHREAD_H
#define TRCVTHREAD_H
#if 0
#include <QThread>
#include <QObject>
#include <QList>
#include <QGraphicsLineItem>
#include "image_processing.h"
#include "traffic_enforcement.h"

typedef struct S_MovingCVObject_Tag {
    qint32 interval_ms;
    EMovingApproach approach;
    SCarAgent *car_agent;

} SMovingCVObject;

class TRCVThread : public QThread
{
    Q_OBJECT
public:
    TRCVThread(QObject *parent = 0);
    void Stop();
    void AddCVTask(SMovingCVObject *object);

protected:
    void run();

private:
    bool m_isStopped;
    QList<SMovingCVObject> m_movingCVObjectList;
	
signals:
	void updateCVRectItem();
    void done();

public slots:
    void update();
};

#endif
#endif // TRTHREAD_H
