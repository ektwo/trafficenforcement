#include "cqtopencvviewergl.h"
#include "image_common.h"
#include "common.h"
#include <QOpenGLTexture>
#include <QPainter>
CQtOpenCVViewerGl::CQtOpenCVViewerGl(QWidget *parent)
    : QOpenGLWidget(parent)
    , m_pGraph(nullptr)
{
    mBgColor = QColor::fromRgb(100,10,10);//150, 150, 150);
}

void CQtOpenCVViewerGl::SetOwner(BaseGraph *pGraph)
{
    if (pGraph)
    {
        m_pGraph = pGraph;
        connect(this, SIGNAL(videoRenderStarted()), pGraph, SLOT(handleAllComponentsAreRunning()));
        connect(this, SIGNAL(videoRenderStopped()), pGraph, SLOT(handleAllComponentsAreStopped()));
    }
    else
    {
        m_pGraph = nullptr;
        disconnect(this, SIGNAL(videoRenderStarted()), 0, 0);
        disconnect(this, SIGNAL(videoRenderStopped()), 0, 0);
    }
}

//void CQtOpenCVViewerGl::setImage(const QImage & image) {
//    Q_ASSERT(image.format() == QImage::Format_RGB888);
//    m_image = image;
//    update();
//}

void CQtOpenCVViewerGl::handleUpstreamStarted()
{
    qDebug("CQtOpenCVViewerGl::handleUpstreamStarted");
    emit videoRenderStarted();
}

void CQtOpenCVViewerGl::handleUpstreamStopped()
{
    qDebug("CQtOpenCVViewerGl::handleUpstreamStopped");
    emit videoRenderStopped();
}

void CQtOpenCVViewerGl::handleUpdateViewCV(const cv::Mat &matImage)
{
    DSG(1, "handddd");
    //drawMutex.lock();
#if 1
    m_matOrigImage = matImage;
    m_imgOrig = cvMat2QImage(matImage);
    //m_imgOrig = m_matOrigImage.size().scaled(this->size(), Qt::KeepAspectRatio);
#else
    if ((matImage.cols != (int)(this->width())) || (matImage.rows != (int)(this->height())))
    {
        CVImageResize2ByWH(matImage, m_matOrigImage, (int)(this->width()), (int)(this->height()));
    }
    else
        m_matOrigImage = matImage;

    m_imgOrig = cvMat2QImage(m_matOrigImage);
#endif
    imshow("OrigImage", m_matOrigImage);

    //recalculatePosition();

    updateScene();

    //drawMutex.unlock();
}
void CQtOpenCVViewerGl::loadGLTextures()
{
//    QImage tex,buf;
//    QString picname=":/images/DSCF4460.JPG";
//    bool isok=buf.load(picname);

//    if(!isok)//加载图片失败
//    {
//        QImage dummy(128,128,QImage::Format_ARGB32);//size and color format
//        dummy.fill(Qt::green);
//        buf=dummy;   //赋值
//    }

//    tex=QOpenGLTexture:(buf);//特别用来转换图片的函数，

//   // 上面的函数得到合适的texture来源

//    glGenTextures(1,&texture[0]);
//    glBindTexture(GL_TEXTURE_2D,texture[0]);


//    glTexImage2D( GL_TEXTURE_2D, 0, 3, tex.width(), tex.height(), 0,
//    GL_RGBA, GL_UNSIGNED_BYTE, tex.bits() );

//    //以上三个函数绑定来源


//    //平滑效果处理
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
//    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
}
void CQtOpenCVViewerGl::initializeGL()
{
    //makeCurrent();
    initializeOpenGLFunctions();
    //return;
    float r = ((float)mBgColor.darker().red())/255.0f;
    float g = ((float)mBgColor.darker().green())/255.0f;
    float b = ((float)mBgColor.darker().blue())/255.0f;
    glClearColor(r,g,b,1.0f);

    return;

    loadGLTextures();
    //这个一定是要启用的，不启用纹理映射，生成一个纯白色的立方体
    glEnable( GL_TEXTURE_2D );
    // In this example the widget's corresponding top-level window can change
    // several times during the widget's lifetime. Whenever this happens, the
    // QOpenGLWidget's associated context is destroyed and a new one is created.
    // Therefore we have to be prepared to clean up the resources on the
    // aboutToBeDestroyed() signal, instead of the destructor. The emission of
    // the signal will be followed by an invocation of initializeGL() where we
    // can recreate all resources.
//    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &GLWidget::cleanup);

//    initializeOpenGLFunctions();
//    glClearColor(0, 0, 0, m_transparent ? 0 : 1);

//    m_program = new QOpenGLShaderProgram;
//    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, m_core ? vertexShaderSourceCore : vertexShaderSource);
//    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment, m_core ? fragmentShaderSourceCore : fragmentShaderSource);
//    m_program->bindAttributeLocation("vertex", 0);
//    m_program->bindAttributeLocation("normal", 1);
//    m_program->link();

//    m_program->bind();
//    m_projMatrixLoc = m_program->uniformLocation("projMatrix");
//    m_mvMatrixLoc = m_program->uniformLocation("mvMatrix");
//    m_normalMatrixLoc = m_program->uniformLocation("normalMatrix");
//    m_lightPosLoc = m_program->uniformLocation("lightPos");

//    // Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
//    // implementations this is optional and support may not be present
//    // at all. Nonetheless the below code works in all cases and makes
//    // sure there is a VAO when one is needed.
//    m_vao.create();
//    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);

//    // Setup our vertex buffer object.
//    m_logoVbo.create();
//    m_logoVbo.bind();
//    m_logoVbo.allocate(m_logo.constData(), m_logo.count() * sizeof(GLfloat));

//    // Store the vertex attribute bindings for the program.
//    setupVertexAttribs();

//    // Our camera never changes in this example.
//    m_camera.setToIdentity();
//    m_camera.translate(0, 0, -1);

//    // Light position is fixed.
//    m_program->setUniformValue(m_lightPosLoc, QVector3D(0, 0, 70));

//    m_program->release();
}

void CQtOpenCVViewerGl::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    update();
    return;
    //makeCurrent();
    glViewport(0, 0, (GLint)width, (GLint)height);
DSG(1, "resizeGL %d %d", width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, -height, 0, 0, 1);

    glMatrixMode(GL_MODELVIEW);

    //recalculatePosition();
    emit imageSizeChanged(width, height);
    //emit imageSizeChanged(mRenderWidth, mRenderHeight);

    updateScene();
}

void CQtOpenCVViewerGl::updateScene()
{
    //if (this->isVisible())
    {
        update();
    }
}
void ck() {
   for(GLenum err; (err = glGetError()) != GL_NO_ERROR;) qDebug() << "gl error" << err;
}
int ww = 0;
void    CQtOpenCVViewerGl::paintEvent(QPaintEvent *e)
{
    DSG(1, "ww=%d", ww);
    makeCurrent();
    paintGL();
    QPainter pter(this);

    //QImage pic;
    //pic.load(":/images/DSCF4460.JPG");

    pter.setPen(Qt::blue);

    QString str;
    str.sprintf("This is a Text! %d", ww++);
    pter.drawText(20, 50, str);// "This is a Text!");


    pter.drawEllipse(80, 100, 40, 50);
    //pter.drawImage(200, 40, pic);

    pter.end();
    update();

}
#if 0
int gg = 0;
void CQtOpenCVViewerGl::paintGL()
{
#if 1
#if 1
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBegin(GL_TRIANGLES);
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(-0.5, -0.5, 0);
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0.5, -0.5, 0);
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.5, 0);
    glEnd();

    QPainter p(this);
    p.setPen(Qt::red);
    p.drawLine(rect().topLeft(), rect().bottomRight());
#else
    if ((m_image.width() <= 0) || (m_image.width() <= 0) ||
        (m_image.format() != QImage::Format::Format_RGB888))
        return;
    //drawMutex.lock();
    DSG(1, "paintGL %d", gg++);
    //auto scaled = m_image.size().scaled(this->size(), Qt::KeepAspectRatio);
    GLuint texID;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.2f, 0.3f, 0.4f, 1.0f);
    glGenTextures(1, &texID);
    glEnable(GL_TEXTURE_RECTANGLE);
    glBindTexture(GL_TEXTURE_RECTANGLE, texID);
    glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB, m_image.width(), m_image.height(), 0,
        GL_RGB, GL_UNSIGNED_BYTE, m_image.constBits());

    cv::Mat mat = QImage2cvMat(m_image);
    imshow("mat", mat);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex2f(0, 0);
    glTexCoord2f(m_image.width(), 0);
    glVertex2f(width(), 0);
    glTexCoord2f(m_image.width(), m_image.height());
    glVertex2f(width(), height());
    glTexCoord2f(0, m_image.height());
    glVertex2f(0, height());
    glEnd();
    glDisable(GL_TEXTURE_RECTANGLE);
    glDeleteTextures(1, &texID);
    //ck();
    //drawMutex.unlock();
#endif
#else

//        //return;
//    //DSG(1, "paintGL");
//    makeCurrent();

//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//    renderImage();
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);

    //m_matOrigImage = cvMat2QImage(matImage);
    //m_imgOrig = m_matOrigImage.size().scaled(this->size(), Qt::KeepAspectRatio);
    //auto scaled = m_imgOrig.size().scaled(this->size(), Qt::KeepAspectRatio);
    if ((m_imgOrig.width() <= 0) || (m_imgOrig.width() <= 0) ||
        (m_imgOrig.format() != QImage::Format::Format_RGB888))
            return;
    DSG(1, "m_imgOrig.width()=%d m_imgOrig.height()=%d %d",
        m_imgOrig.width(), m_imgOrig.height(), m_imgOrig.format());
    DSG(1, "width()=%d width()=%d",
        width(), height());
    cv::Mat mat = QImage2cvMat(m_imgOrig);
    imshow("mat", mat);
    GLuint texID;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glGenTextures(1, &texID);
    glEnable(GL_TEXTURE_RECTANGLE);
    glBindTexture(GL_TEXTURE_RECTANGLE, texID);
    glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB,
                 m_imgOrig.width(), m_imgOrig.height(), 0,
                 GL_RGB, GL_UNSIGNED_BYTE, m_imgOrig.constBits());

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex2f(0, 0);
    glTexCoord2f(m_imgOrig.width(), 0);
    glVertex2f(width(), 0);
    glTexCoord2f(m_imgOrig.width(), m_imgOrig.height());
    glVertex2f(width(), height());
    glTexCoord2f(0, m_imgOrig.height());
    glVertex2f(0, height());
    glEnd();
    glDisable(GL_TEXTURE_RECTANGLE);
    glDeleteTextures(1, &texID);
    ck();



//    m_world.setToIdentity();
//    m_world.rotate(180.0f - (m_xRot / 16.0f), 1, 0, 0);
//    m_world.rotate(m_yRot / 16.0f, 0, 1, 0);
//    m_world.rotate(m_zRot / 16.0f, 0, 0, 1);

//    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
//    m_program->bind();
//    m_program->setUniformValue(m_projMatrixLoc, m_proj);
//    m_program->setUniformValue(m_mvMatrixLoc, m_camera * m_world);
//    QMatrix3x3 normalMatrix = m_world.normalMatrix();
//    m_program->setUniformValue(m_normalMatrixLoc, normalMatrix);

//    glDrawArrays(GL_TRIANGLES, 0, m_logo.vertexCount());

//    m_program->release();
}

void CQtOpenCVViewerGl::renderImage()
{
    //drawMutex.lock();

//    makeCurrent();

//    glClear(GL_COLOR_BUFFER_BIT);
////DSG(1, "renderImage m_imgOrig.isNull()=%d", m_imgOrig.isNull());
//    if (!m_imgOrig.isNull())
//    {
//        glLoadIdentity();

//        glPushMatrix();
//        {
//            //DSG(1, "mRenderWidth=%d m_imgOrig.width()=%d", mRenderWidth, m_imgOrig.width());
//            //DSG(1, "mRenderHeight=%d m_imgOrig.height()=%d", mRenderHeight, m_imgOrig.height());
//            if (m_imgResized.width() <= 0)
//            {
//                if (mRenderWidth == m_imgOrig.width() && mRenderHeight == m_imgOrig.height())
//                    m_imgResized = m_imgOrig;
//                else
//                    m_imgResized = m_imgOrig.scaled(QSize(mRenderWidth, mRenderHeight),
//                                                      Qt::IgnoreAspectRatio,
//                                                      Qt::SmoothTransformation);
//            }

//            // ---> Centering image in draw area

//            glRasterPos2i(mRenderPosX, mRenderPosY);

//            glPixelZoom(1, -1);
			
//            glDrawPixels(m_imgResized.width(), m_imgResized.height(), GL_RGB, GL_UNSIGNED_BYTE, m_imgResized.bits());
//            //glDrawPixels(m_imgResized.width(), m_imgResized.height(), GL_RGBA, GL_UNSIGNED_BYTE, m_imgResized.bits());
			
//        }
//        glPopMatrix();

//        // end
//        glFlush();
//    }

    //drawMutex.unlock();
#endif
}
#endif

void CQtOpenCVViewerGl::recalculatePosition()
{
    mImgRatio = (float)m_matOrigImage.cols/(float)m_matOrigImage.rows;

    mRenderWidth = this->size().width();
    mRenderHeight = floor(mRenderWidth / mImgRatio);

    if (mRenderHeight > this->size().height())
    {
        mRenderHeight = this->size().height();
        mRenderWidth = floor(mRenderHeight * mImgRatio);
    }

    mRenderPosX = floor((this->size().width() - mRenderWidth) / 2);
    mRenderPosY = -floor((this->size().height() - mRenderHeight) / 2);

    DSG(1, "mRenderPosX=%d mRenderPosX=%d mRenderPosX=%d mRenderPosX=%d",
        mRenderPosX, mRenderPosY, mRenderWidth, mRenderHeight);
    m_imgResized = QImage();
}
