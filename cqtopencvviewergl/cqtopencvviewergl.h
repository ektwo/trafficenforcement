#ifndef CQTOPENCVVIEWERGL_H
#define CQTOPENCVVIEWERGL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions_2_0>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <mutex>
#include "base_graph.h"
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QMatrix4x4>

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram)

class CQtOpenCVViewerGl : public QOpenGLWidget, protected QOpenGLFunctions_2_0
{
    Q_OBJECT
        QImage m_image;
public:
    explicit CQtOpenCVViewerGl(QWidget *parent = 0);
    void SetOwner(BaseGraph *pGraph);

signals:
    void imageSizeChanged( int outW, int outH ); /// Used to resize the image outside the widget
    void videoRenderStarted();
    void videoRenderStopped();

public slots:
    //bool showImage(const cv::Mat& matImage); /// Used to set the image to be viewed
    void handleUpstreamStarted();
    void handleUpstreamStopped();
    void handleUpdateViewCV(const cv::Mat &matImage);
    void setImage(const QImage & image) {
        Q_ASSERT(image.format() == QImage::Format_RGB888);
        //drawMutex.lock();
        m_image = image;
        update();
        //drawMutex.unlock();
    }
protected:
    void 	initializeGL(); /// OpenGL initialization
    //void 	paintGL(); /// OpenGL Rendering
    void    paintEvent(QPaintEvent *e) ;
    void 	resizeGL(int width, int height);        /// Widget Resize Event

    void updateScene();
    void renderImage();



void loadGLTextures();
private:
    BaseGraph *m_pGraph;
    QImage      m_imgOrig;           /// Qt image to be rendered
    QImage      m_imgResized;
    cv::Mat     m_matOrigImage;             /// original OpenCV image to be shown

    QColor      mBgColor;		/// Background color

    float       mImgRatio;             /// height/width ratio

    int mRenderWidth;
    int mRenderHeight;
    int mRenderPosX;
    int mRenderPosY;

    void recalculatePosition();

	std::mutex drawMutex;

    bool m_core;
    int m_xRot;
    int m_yRot;
    int m_zRot;
    QPoint m_lastPos;
    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_logoVbo;
    QOpenGLShaderProgram *m_program;
    int m_projMatrixLoc;
    int m_mvMatrixLoc;
    int m_normalMatrixLoc;
    int m_lightPosLoc;
    QMatrix4x4 m_proj;
    QMatrix4x4 m_camera;
    QMatrix4x4 m_world;
    static bool m_transparent;
};

#endif // CQTOPENCVVIEWERGL_H
