#ifndef IMAGE_PROCESSING_THREAD_H
#define IMAGE_PROCESSING_THREAD_H


#include <QObject>
#include <QTimer>
#include <QQueue>
#include <QMutex>
#include "traffic_enforcement.h"
#include "opencv2/opencv.hpp"


class ImageProcessingThread : public QObject
{
    Q_OBJECT
    
    void queue(const cv::Mat & frame);
    void process(cv::Mat frame);
    void timerEvent(QTimerEvent * ev);

public:
    explicit ImageProcessingThread(QObject * parent = nullptr);
    ~ImageProcessingThread();

    SCameraImageProcessingConfig* GetConfig() { return &m_config; }
    void SetConfig(SCameraImageProcessingConfig *pConfig);
    void SetProcessAll(bool all) { m_processAll = all; }

protected:
    virtual void DoImageProcessing(const cv::Mat &matImage) = 0;

signals: 
    //void cvColorReady(const QImage &);

public slots:
    void handleProcessedFrameCV(const cv::Mat & frame);
    void stop();

    static void matDeleter(void* mat) { delete static_cast<cv::Mat*>(mat); }

private:
    bool m_processAll;
    SCameraImageProcessingConfig m_config;
    QBasicTimer m_timer;
    QQueue<cv::Mat> m_imageList;
    cv::Mat m_frame;
    QMutex m_mutex;
};

#endif // IMAGE_PROCESSING_THREAD_H
