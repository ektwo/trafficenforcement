#include "filter_hls.h"
#include "parameters_table.h"
#include "graph.h"
#include "common.h"


const char *str_win_hls = "HLS";
extern SParametersTable k_parameters_table[2];
HLSFilter::HLSFilter(BaseGraph *parent, int profileIdx)
    : Filter(parent)
    , m_thresH(k_parameters_table[profileIdx].hls.threshold_value_h)
    , m_thresL(k_parameters_table[profileIdx].hls.threshold_value_l)
    , m_thresS(k_parameters_table[profileIdx].hls.threshold_value_s)
    , m_thresHMax(k_parameters_table[profileIdx].hls.threshold_value_h_max)
    , m_thresLMax(k_parameters_table[profileIdx].hls.threshold_value_l_max)
    , m_thresSMax(k_parameters_table[profileIdx].hls.threshold_value_s_max)
    , m_pScene(k_parameters_table[profileIdx].hls.scene)
{
    m_class = k_str_class_transform_name;
    m_name = k_str_hls_filter_name;
    DSG(1, "HLSFilter m_class=%s", m_class);
    DSG(1, "HLSFilter m_name=%s", m_name);

    if (m_pScene)
    {
#if 1
        m_pScene->SetOwner(parent);
        connect(this, SIGNAL(iamStarted()), m_pScene, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), m_pScene, SLOT(handleUpstreamStopped()));
        connect(this, &Filter::cvColorReady, m_pScene, &EscortGraphicsScene::handleUpdateViewCV);
#else
        CQtOpenCVViewerGl *pScene = (CQtOpenCVViewerGl*)m_pScene;
        pScene->SetOwner(parent);
        //m_pScene->SetOwner(parent);
        connect(this, SIGNAL(iamStarted()), m_pScene, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), m_pScene, SLOT(handleUpstreamStopped()));
        //connect(this, &Filter::cvColorReady, m_pScene, &EscortGraphicsScene::handleUpdateViewCV);
        //connect(this, &Filter::cvColorReady, m_pScene, &CQtOpenCVViewerGl::handleUpdateViewCV);
        //connect(this, &HLSFilter::newFrame, m_pScene, &CQtOpenCVViewerGl::setImage);

        connect(this, &HLSFilter::newFrame, pScene, &CQtOpenCVViewerGl::setImage);
        connect(this, &HLSFilter::update, m_pOwner, &BaseGraph::handleUpdate);
        
#endif

    }
}

HLSFilter::~HLSFilter()
{
    if (m_pScene)
    {
        //CQtOpenCVViewerGl *pScene = (CQtOpenCVViewerGl*)m_pScene;
        //pScene->SetOwner(nullptr);
        m_pScene->SetOwner(nullptr);
        disconnect(this, &Filter::cvColorReady, 0, 0);
    }
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);    
}

bool HLSFilter::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = false;
    if (pDownstreamFilter)
    {
        ret = __super::ConnectTo(pDownstreamFilter);

        connect(this, SIGNAL(iamStarted()), pDownstreamFilter, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), pDownstreamFilter, SLOT(handleUpstreamStopped()));
        connect(this, &Filter::imgProcessedReady, pDownstreamFilter, &Filter::handleProcessedFrameCV);
    }
    return ret;
}

void HLSFilter::DisconnectAll()
{
    __super::DisconnectAll();

    disconnect(this, &Filter::imgProcessedReady, 0, 0);
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

void HLSFilter::handleUpstreamStarted()
{
    ++m_startedUpstreamFilters;
    DSG(1, "HLSFilter::handleUpstreamStarted m_startedUpstreamFilters=%d m_pUpstreamFilterList.size()=%d",
        m_startedUpstreamFilters, m_pUpstreamFilterList.size());
    if (m_startedUpstreamFilters == m_pUpstreamFilterList.size())
    {
        __super::handleUpstreamStarted();

        //m_nTimerID = startTimer(33, Qt::CoarseTimer);
    }
}

void HLSFilter::handleUpstreamStopped()
{
    --m_startedUpstreamFilters;
    DSG(1, "HLSFilter::handleUpstreamStopped m_startedUpstreamFilters=%d", m_startedUpstreamFilters);
    if (m_startedUpstreamFilters == 0)
    {
        m_imageList.clear();
        //if (-1 != m_nTimerID)
        //{
        //    killTimer(m_nTimerID);
        //    m_nTimerID = -1;
        //}

        __super::handleUpstreamStopped(); // change filter state and emit signal to next filter
    }
}

void HLSFilter::handleColorFrameCV(const cv::Mat &frame)
{
    //DSG(1, "HLSFilter::handleProcessedFrameCV");
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 10)
        {
            DSG(1, "queue m_imageList.size() drop = %d", m_imageList.size());
            cv::Mat image = m_imageList.dequeue();
            image.release();
        }

        m_imageList.enqueue(frame);
    }

    //if (m_readyToEmit)
    {
        if (!m_timer.isActive())
            m_timer.start(0, this);
    }
}

void HLSFilter::PrepareImageForLaneDetection(const Mat &matScaledOrgClone)
{
    m_matScaledImageF = Mat::zeros(matScaledOrgClone.size(), CV_32FC3);

    m_matMaskHLS_H = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matMaskHLS_L = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matMaskHLS_S = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matMaskHLS = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matHLSOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);

}

void HLSFilter::ReactFromTrackbar(const Mat &in, Mat *lpMaskH, Mat *lpMaskL, Mat *lpMaskS)
{
    //DSG(1, "ReactFromTrackbar 1");
    m_matHLSChannel.clear();
    m_matHLSChannel.shrink_to_fit();

    split(in, m_matHLSChannel);

    inRange(m_matHLSChannel[0], Scalar(m_thresH), Scalar(m_thresHMax), *lpMaskH);
    inRange(m_matHLSChannel[1], Scalar(m_thresL / float(k_hls_l_max)), Scalar(m_thresLMax / float(k_hls_l_max)), *lpMaskL);
    inRange(m_matHLSChannel[2], Scalar(m_thresS / float(k_hls_s_max)), Scalar(m_thresSMax / float(k_hls_s_max)), *lpMaskS);
    //DSG(1, "ReactFromTrackbar m_thresH=%d", m_thresH);
}

void HLSFilter::ReactFromTrackbar()
{
#if 0
    DSG(1, "ReactFromTrackbar 1");
    m_matHLSChannel.clear();
    m_matHLSChannel.shrink_to_fit();

    split(in, m_matHLSChannel);

    inRange(m_matHLSChannel[0], Scalar(m_thresH), Scalar(m_thresHMax), *lpMaskH);
    inRange(m_matHLSChannel[1], Scalar(m_thresL / float(k_hls_l_max)), Scalar(m_thresLMax / float(k_hls_l_max)), *lpMaskL);
    inRange(m_matHLSChannel[2], Scalar(m_thresS / float(k_hls_s_max)), Scalar(m_thresSMax / float(k_hls_s_max)), *lpMaskS);
    DSG(1, "ReactFromTrackbar m_thresH=%d", m_thresH);
#endif
}

void 
HLSFilter::UpdateOutputMat(const Mat &srcF, const Mat &maskH, const Mat &maskL, const Mat &maskS, Mat &mask, Mat &dst)
{
    //DSG(1, "UpdateOutputMat 1");
    Mat matTmpDst = Mat::zeros(srcF.size(), CV_32FC3);
    PrintMatInfo(matTmpDst);
    for (int rowIdx = 0; rowIdx < srcF.rows; ++rowIdx)
    {
        for (int colIdx = 0; colIdx < srcF.cols; ++colIdx)
        {
            if ((255 == maskH.at<uchar>(rowIdx, colIdx)) &&
                (255 == maskL.at<uchar>(rowIdx, colIdx)) &&
                (255 == maskS.at<uchar>(rowIdx, colIdx))) // the pixel pass the mask detection
            {
                mask.at<uchar>(rowIdx, colIdx) = 255;
                matTmpDst.at<Vec3f>(rowIdx, colIdx) = srcF.at<Vec3f>(rowIdx, colIdx);
            }
            else
            {
                mask.at<uchar>(rowIdx, colIdx) = 0;
                matTmpDst.at<Vec3f>(rowIdx, colIdx) = 0;
            }
        }
    }
    DSG(1, "UpdateOutputMat 2");
    matTmpDst.convertTo(dst, CV_8UC3, 255, 0);
}

void HLSFilter::ShowWindow()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if (pCache->engineeringmode_enabled)
    {
        if (pCache->showtrackbar_enabled)
        {
            imshow(str_win_hls, m_matHLSOut);
        }
    }
}

void HLSFilter::ThresholdCallback(int pos, void *userdata)
{
    //HLSFilter *pFilter;

    //TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    {
        m_calcTimer.ResetTimer();
        Mat hlsInF;
        cv::cvtColor(m_matScaledImageF, hlsInF, COLOR_BGR2HLS);

        vector<Mat> matHLSChannel;
        split(hlsInF, matHLSChannel);
        //DSG(1, "m_thresH=%d m_thresHMax=%d", m_thresH, m_thresHMax);
        //DSG(1, "m_thresL=%d m_thresLMax=%d", m_thresL, m_thresLMax);
       // DSG(1, "m_thresS=%d m_thresSMax=%d", m_thresS, m_thresSMax);

        inRange(matHLSChannel[0], Scalar(m_thresH), Scalar(m_thresHMax), m_matMaskHLS_H);
        inRange(matHLSChannel[1], Scalar(m_thresL / float(k_hls_l_max)), Scalar(m_thresLMax / float(k_hls_l_max)), m_matMaskHLS_L);
        inRange(matHLSChannel[2], Scalar(m_thresS / float(k_hls_s_max)), Scalar(m_thresSMax / float(k_hls_s_max)), m_matMaskHLS_S);
#ifdef TEST_TIME
        m_calcTimer.Print(0, "HLSFilter::ThresholdCallback");

        m_calcTimer.ResetTimer();
#endif
        Mat matTmpDst = m_matScaledImageF.clone();
        for (int rowIdx = 0; rowIdx < matTmpDst.rows; ++rowIdx)
        {
            for (int colIdx = 0; colIdx < matTmpDst.cols; ++colIdx)
            {
                if ((255 == m_matMaskHLS_H.at<uchar>(rowIdx, colIdx)) &&
                    (255 == m_matMaskHLS_L.at<uchar>(rowIdx, colIdx)) &&
                    (255 == m_matMaskHLS_S.at<uchar>(rowIdx, colIdx))) 
                {
                    m_matMaskHLS.at<uchar>(rowIdx, colIdx) = 255;
                    matTmpDst.at<Vec3f>(rowIdx, colIdx) = m_matScaledImageF.at<Vec3f>(rowIdx, colIdx);
                }
                else
                {
                    m_matMaskHLS.at<uchar>(rowIdx, colIdx) = 0;
                    matTmpDst.at<Vec3f>(rowIdx, colIdx) = 0;
                }
            }
        }

        matTmpDst.convertTo(m_matHLSOut, CV_8UC3, 255, 0);

        //imshow("matTmpDst", matTmpDst);
#ifdef TEST_TIME
        m_calcTimer.Print(0, "HLSFilter::ThresholdCallback");
#endif
    }
}

void HLSFilter::DoImageProcessing(const cv::Mat &matImage)
{
    if (0 == m_currentFrame)
        PrepareImageForLaneDetection(matImage);

    m_matScaledImageF.release();
    matImage.convertTo(m_matScaledImageF, CV_32FC3, 1.0 / 255, 0);

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if ((pCache->lanedetection_enabled) && (!pCache->lanedetection_paused))
    //if ((!pCache->lanedetection_manual_enabled) && (pCache->lanedetection_enabled) && (!pCache->lanedetection_paused))
    {
        ThresholdCallback(0, 0);
        //imshow("m_matMaskHLS", m_matMaskHLS);
        //PrintMatInfo(m_matMaskHLS);
        emit imgProcessedReady(m_matMaskHLS);
        emit cvColorReady(m_matHLSOut);
    }
    else
        emit cvColorReady(matImage);

    //opengl
    //QImage image = cvMat2QImage(matImage);
    //m_pScene->showImage(matImage);
    //emit newFrame(image);
}
//void HLSFilter::timerEvent(QTimerEvent * ev)
//{
//    if (ev->timerId() != m_timer.timerId()) return;

//    m_timer.stop();
//}

void HLSFilter::timerEvent(QTimerEvent *event)
{
    //qDebug("HLSFilter::timerEvent (%s) ev->timerId()=%d m_nTimerID=%d", m_name, event->timerId(), m_timer.timerId());
    //if (event->timerId() != m_nTimerID) return;
    if (event->timerId() != m_timer.timerId())  return; // { QObject::timerEvent(event); return; }
    //qDebug("FilterThread::timerEvent (%s) 1", m_name);

    if (e_component_state_started != m_state)
        return;

    m_timer.stop();
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 0)
        {
            cv::Mat image = m_imageList.dequeue();
            m_frame = image.clone();
            image.release();
        }
    }

    //qDebug("FilterThread::timerEvent (%s) m_frame=%d %d", m_name, m_frame.cols, m_frame.rows);
    DoImageProcessing(m_frame);

    ++m_currentFrame;
    if ((m_currentFrame % 30) == 0)
        DSG(1, "HLSFilter::DoImageProcessing m_currentFrame=%d", m_currentFrame);
}

//void HLSFilter::CreateWindowHLS()
//{
//    namedWindow(str_win_hls, CV_WINDOW_AUTOSIZE);
//    //namedWindow(str_win_hls_l_min, CV_WINDOW_AUTOSIZE);
//    //namedWindow(str_win_hls_s_min, CV_WINDOW_AUTOSIZE);
//
//#if 1
//    FnThresholdCallback fn = ThresholdCallback;
//#else
//    FnThresholdCallback fn = ThresholdCallbackHLS;
//#endif
//
//#ifdef HLS_INDIVIDUAL
//    createTrackbar(k_str_hls_h, str_win_hls, &g_hlsThresholdTypeH, k_hls_h_type_max, fn);
//    createTrackbar(k_str_hls_l, str_win_hls, &g_hlsThresholdTypeL, k_hls_l_type_max, fn);
//    createTrackbar(k_str_hls_s, str_win_hls, &g_hlsThresholdTypeS, k_hls_s_type_max, fn);
//#endif
//    createTrackbar(str_win_hls_h_min, str_win_hls, &g_hlsThresholdValueH, k_hls_h_max, fn);
//    createTrackbar(str_win_hls_l_min, str_win_hls, &g_hlsThresholdValueL, k_hls_l_max, fn);
//    createTrackbar(str_win_hls_s_min, str_win_hls, &g_hlsThresholdValueS, k_hls_s_max, fn);
//    createTrackbar(str_win_hls_h_max, str_win_hls, &g_hlsThresholdValueHMax, k_hls_h_max, fn);
//    createTrackbar(str_win_hls_l_max, str_win_hls, &g_hlsThresholdValueLMax, k_hls_l_max, fn);
//    createTrackbar(str_win_hls_s_max, str_win_hls, &g_hlsThresholdValueSMax, k_hls_s_max, fn);
//}