#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "traffic_enforcement.h"
#include <QtWidgets>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QFileDialog>
#include <QLocalSocket>
#include <opencv2/opencv.hpp>
#include "common.h"
#include "image_cvtapi.h"
#include "image_common.h"
#include "image_undistortion.h"
#include "image_thresholdfilter.h"
#include "image_laneboundary.h"
#include "emoptionsdialog.h"
#include "optionsdialog.h"
#include "view_videoassetmanagement.h"

#include "test_hsv.h"
#include "test_rgb.h"
#include "image_affine.h"
#include "framelesswindow.h"
#include <QGLWidget>

using namespace cv;

extern FramelessWindow *g_f;
extern MainWindow *g_m;

extern SParametersTable k_parameters_table[2];

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_quitting(false),
    m_bVehicleRectSwitch(false),
    m_bFullScreen(false),
    //m_currentSourceStartedTasks(0),
    //m_currentSinkStartedTasks(0),
    //m_totalSourceTasks(0),
    //m_totalSinkTasks(0),
    m_actionState(e_action_idle),
    //m_pTrQtThread(nullptr),
    //m_pTrCVThread(nullptr),
    m_pGraph(nullptr),
    m_pStatusBarTimer(nullptr),
    m_pQuitTimer(nullptr),
	m_pProcessTFA(nullptr),
	m_pServer(nullptr),
	m_pOptionDlg(nullptr),
	m_pVAMDlg(nullptr),
    m_pBtnROILeft1Watcher(nullptr),
    m_pBtnROIRight1Watcher(nullptr),
    m_pBtnROILeft2Watcher(nullptr),
    m_pBtnROIRight2Watcher(nullptr),
    m_pBtnROITop1Watcher(nullptr),
    m_pBtnROIBottom1Watcher(nullptr),
    m_pBtnROITop2Watcher(nullptr),
    m_pBtnROIBottom2Watcher(nullptr),
    m_dwlPosition(new DWLPosition()),
	m_dwlPositionVehicle(new DWLPosition()),
    ui(new Ui::MainWindow)
{
    
    g_m = this;
    ui->setupUi(this);

	//m_settingsFileName = QApplication::applicationDirPath() + "/TrafficEnforcement.ini";

    btnROILeft1 = new QPushButton(ui->imageView_center);
    btnROILeft2 = new QPushButton(ui->imageView_center);
    btnROIRight1 = new QPushButton(ui->imageView_center);
    btnROIRight2 = new QPushButton(ui->imageView_center);
    btnROITop1 = new QPushButton(ui->imageView_center);
    btnROITop2 = new QPushButton(ui->imageView_center);
    btnROIBottom1 = new QPushButton(ui->imageView_center);
    btnROIBottom2 = new QPushButton(ui->imageView_center);

    m_imageScene.setParent(this);
	m_imageSceneHLeft.setParent(this);
	m_imageSceneHMiddle.setParent(this);
	m_imageSceneHRight.setParent(this);
    m_imageSceneV1st.setParent(this);
	m_imageSceneV2nd.setParent(this);
	m_imageSceneV3rd.setParent(this);
	m_imageSceneCorner.setParent(this);

	m_imageScene.setBackgroundBrush(Qt::black);
	m_imageSceneHLeft.setBackgroundBrush(Qt::black);
	m_imageSceneHMiddle.setBackgroundBrush(Qt::black);
	m_imageSceneHRight.setBackgroundBrush(Qt::black);
    m_imageSceneV1st.setBackgroundBrush(Qt::black);
	m_imageSceneV2nd.setBackgroundBrush(Qt::black);
	m_imageSceneV3rd.setBackgroundBrush(Qt::black);
	m_imageSceneCorner.setBackgroundBrush(Qt::black);

    for (int i = 0; i < DIM(k_parameters_table); ++i)
    {
        k_parameters_table[i].hls.scene = &m_imageSceneV1st;
        //k_parameters_table[i].hls.scene = ui->imageView_V1st;
        k_parameters_table[i].hsv.scene = &m_imageSceneV2nd;
        k_parameters_table[i].sobel.scene = &m_imageSceneV3rd;
        k_parameters_table[i].canny.scene = &m_imageSceneHLeft;
        k_parameters_table[i].display_scene = &m_imageScene;
    }

    LoadSettings();

    CreateAction();
    CreateConnect();
    InitMenubar();
	InitViews();
    InitButton();

    InitMainWidget();

	ui->trVehicleImageWidget->setSpacing(10);
	ui->trVehicleImageWidget->setViewMode(QListView::IconMode);
	ui->trVehicleImageWidget->setIconSize(QSize(256/2, 192/2));

    setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );

    m_pStatusBarTimer = new QTimer(this);
    if (m_pStatusBarTimer)
    {
        connect(m_pStatusBarTimer, SIGNAL(timeout()), this, SLOT(handleStatusBarTimer()));
    }

    on_actionView_VideoAssetManagement_triggered();
    on_actionView_TrafficFlowAnalysis_triggered();
}

MainWindow::~MainWindow()
{
    SaveSettings();
    qDebug("~MainWindow");

	if (m_pServer)
	{
		m_pServer->close();
		delete m_pServer;
		m_pServer = nullptr;
	}

	if (m_pProcessTFA)
	{
		delete m_pProcessTFA;
		m_pProcessTFA = nullptr;
	}

	if (m_pOptionDlg)
	{
		delete m_pOptionDlg;
		m_pOptionDlg = nullptr;
	}

	if (m_pVAMDlg)
	{
		delete m_pVAMDlg;
		m_pVAMDlg = nullptr;
	}

    //if (m_pTrQtThread)
    //{
    //    m_pTrQtThread->Stop();
    //    delete m_pTrQtThread;
    //    m_pTrQtThread = nullptr;
    //}
    //if (m_pTrCVThread)
    //{
    //    m_pTrCVThread->Stop();
    //    delete m_pTrCVThread;
    //    m_pTrCVThread = nullptr;
    //}

    if (m_pGraph)
    {
        delete m_pGraph;
        m_pGraph = nullptr;
    }


    if (m_dwlPosition)
        delete m_dwlPosition;
	if (m_dwlPositionVehicle)
		delete m_dwlPositionVehicle;

    delete btnROILeft1;
    delete btnROILeft2;
    delete btnROIRight1;
    delete btnROIRight2;
    delete btnROITop1;
    delete btnROITop2;
    delete btnROIBottom1;
    delete btnROIBottom2;

    delete ui;


}

void MainWindow::addLineIntoDWLWidget(QPointF startPoint, QPointF endPoint)
{
    Ui::MainWindow *ui = this->ui;
    DWLPosition *dwlPol = m_dwlPosition;
	//qDebug("addLineIntoDWLWidget 1");

    if (dwlPol)
    {
        int currentIdx = dwlPol->m_total - 1;
        QString str;

        ui->trDWLPositionOfLinesWidget->setRowCount(ui->trDWLPositionOfLinesWidget->rowCount() + 1);

        str.sprintf("(%.2f, %.2f)", startPoint.x(), startPoint.y());
        ui->trDWLPositionOfLinesWidget->setItem(currentIdx, 0, new QTableWidgetItem(str));

        str.sprintf("(%.2f, %.2f)", endPoint.x(), endPoint.y());
        ui->trDWLPositionOfLinesWidget->setItem(currentIdx, 1, new QTableWidgetItem(str));

        {
            QTableWidgetItem *item;
            for (int i = 0; i < ui->trDWLPositionOfLinesWidget->columnCount(); ++i) {
                item = ui->trDWLPositionOfLinesWidget->itemAt(currentIdx, i);
                item->setFlags(item->flags() | Qt::ItemIsEditable);
            }
        }

        dwlPol->m_total = ui->trDWLPositionOfLinesWidget->rowCount();
        ui->trDWLPositionOfLinesWidget->resizeColumnsToContents();
    }
	//qDebug("addLineIntoDWLWidget 2");
}

void MainWindow::AddVehicleInfoIntoWidget(cv::Vec4i &ltrb)
{
	Ui::MainWindow *ui = this->ui;
	DWLPosition *dwlPol = m_dwlPositionVehicle;
	//qDebug("AddVehicleInfoIntoWidget 1");

	if (dwlPol)
	{
		int currentIdx = dwlPol->m_total - 1;
		QString str;

		ui->trVehicleInfoWidget->setRowCount(ui->trVehicleInfoWidget->rowCount() + 1);

		str.sprintf("(%d, %d)", ltrb[0], ltrb[1]);
		ui->trVehicleInfoWidget->setItem(currentIdx, 0, new QTableWidgetItem(str));

		str.sprintf("(%d, %d)", ltrb[2], ltrb[3]);
		ui->trVehicleInfoWidget->setItem(currentIdx, 1, new QTableWidgetItem(str));

		str.sprintf("YES");
		ui->trVehicleInfoWidget->setItem(currentIdx, 2, new QTableWidgetItem(str));

		{
			QTableWidgetItem *item;
			for (int i = 0; i < ui->trVehicleInfoWidget->columnCount(); ++i) {
				item = ui->trVehicleInfoWidget->itemAt(currentIdx, i);
				//item->setFlags(item->flags() | Qt::ItemIsEditable);
			}
		}

		dwlPol->m_total = ui->trVehicleInfoWidget->rowCount();
		ui->trVehicleInfoWidget->resizeColumnsToContents();
	}
	//qDebug("AddVehicleInfoIntoWidget 2");
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Traffic Enforcement",
        tr("Are you sure?\n"),
        QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
        QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    }
    else {
        if (m_pQuitTimer)
        {
            delete m_pQuitTimer;
            m_pQuitTimer = nullptr;
        }
        {
            if ((e_action_openimagefile_is_running == m_actionState) ||
                (e_action_openvideofile_is_running == m_actionState) ||
                (e_action_opencamera_is_running == m_actionState))
            {
                m_quitting = true;
                m_pQuitTimer = new QTimer(this);
                if (m_pQuitTimer)
                {
                    connect(m_pQuitTimer, SIGNAL(timeout()), this, SLOT(handleQuitTime()));
                    m_pQuitTimer->start(30);
                }

                on_actionControl_Stop_triggered();
            }
        }

        cv::destroyAllWindows();

        g_f->deleteLater();

        event->accept();
    }
}

void MainWindow::LoadSettings()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    QSettings settings(pCache->settings_file_path, QSettings::IniFormat);
    settings.setIniCodec("UTF-8");

    int value;

    ui->actionTools_EnableROI->setChecked(settings.value(QString(k_str_tools_roi_enabled), false).toBool());
	ui->actionTools_VideoStability->setChecked(settings.value(QString(k_str_tools_video_stability_enabled), false).toBool());

	ui->actionView_ROILine->setChecked(settings.value(QString(k_str_view_roiline), false).toBool());
    ui->actionView_VideoAssetManagement->setChecked(settings.value(QString(k_str_view_videoassetmanagement), false).toBool());
    ui->actionView_TrafficFlowAnalysis->setChecked(settings.value(QString(k_str_view_gpsstation), false).toBool());

	ui->actionEngineering_Enable->setChecked(settings.value(QString(k_str_engineeringmode_enabled), false).toBool());
    ui->actionEngineering_DWL_Randomly_Generate_Lines->setChecked(settings.value(QString(k_str_em_dwl_randomly_generate_lanelines), false).toBool());
    ui->actionEngineering_ShowTrackbar->setChecked(settings.value(QString(k_str_showtrackbar_enabled), false).toBool());
    ui->actionEngineering_ShowTrackbar->setChecked(settings.value(QString(k_str_showtrackbar_enabled), false).toBool());
    ui->actionEngineering_StaticMode_Always_backup_info_of_sliding_windows->setChecked(settings.value(QString(k_str_em_sm_always_backup_info_of_slidingwindows), false).toBool());
    ui->actionEngineering_StaticMode_Use_sliding_windows_info_file->setChecked(settings.value(QString(k_str_em_sm_use_slidingwindows_info_file), false).toBool());

    {
        value = settings.value(QString(k_str_dwl_tolerance), 25).toInt();
        ui->trDWLToleranceSlider->setRange(0, k_tolerance_max);
        ui->trDWLToleranceSlider->setValue(value);
        QString str = QString("%1").arg(value);
        ui->trDWLToleranceValue->setText(str);
    }

    ui->cbLaneDetection->setChecked(settings.value(QString(k_str_lanedetection_enabled), true).toBool());
    ui->cbVehicleDetection->setChecked(settings.value(QString(k_str_vehicledetection_enabled), true).toBool());
}

void MainWindow::SaveSettings()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    QSettings settings(pCache->settings_file_path, QSettings::IniFormat);
    settings.setIniCodec("UTF-8");

    settings.setValue(QString(k_str_tools_roi_enabled), ui->actionTools_EnableROI->isChecked());
	settings.setValue(QString(k_str_tools_video_stability_enabled), ui->actionTools_VideoStability->isChecked());

	settings.setValue(QString(k_str_view_roiline), ui->actionView_ROILine->isChecked());
    settings.setValue(QString(k_str_view_videoassetmanagement), ui->actionView_VideoAssetManagement->isChecked());
    settings.setValue(QString(k_str_view_gpsstation), ui->actionView_TrafficFlowAnalysis->isChecked());

	settings.setValue(QString(k_str_engineeringmode_enabled),
					  ui->actionEngineering_Enable->isChecked());
    settings.setValue(QString(k_str_em_dwl_randomly_generate_lanelines),
                      ui->actionEngineering_DWL_Randomly_Generate_Lines->isChecked());
    settings.setValue(QString(k_str_showtrackbar_enabled),
                      ui->actionEngineering_ShowTrackbar->isChecked());
    settings.setValue(QString(k_str_em_sm_always_backup_info_of_slidingwindows),
                      ui->actionEngineering_StaticMode_Always_backup_info_of_sliding_windows->isChecked());
    settings.setValue(QString(k_str_em_sm_use_slidingwindows_info_file),
                      ui->actionEngineering_StaticMode_Use_sliding_windows_info_file->isChecked());

    settings.setValue(QString(k_str_dwl_tolerance), ui->trDWLToleranceSlider->value());
    settings.setValue(QString(k_str_lanedetection_enabled), ui->cbLaneDetection->isChecked());
    settings.setValue(QString(k_str_vehicledetection_enabled), ui->cbVehicleDetection->isChecked());

    settings.setValue(QString(k_str_lanedetection_manual_enabled), pCache->lanedetection_manual_enabled);
    settings.setValue(QString(k_str_lanedetection_manual_line1_pt1_x), pCache->manual_leftlane[0]);
    settings.setValue(QString(k_str_lanedetection_manual_line1_pt1_y), pCache->manual_leftlane[1]);
    settings.setValue(QString(k_str_lanedetection_manual_line1_pt2_x), pCache->manual_leftlane[2]);
    settings.setValue(QString(k_str_lanedetection_manual_line1_pt2_y), pCache->manual_leftlane[3]);
    settings.setValue(QString(k_str_lanedetection_manual_line2_pt1_x), pCache->manual_rightlane[0]);
    settings.setValue(QString(k_str_lanedetection_manual_line2_pt1_y), pCache->manual_rightlane[1]);
    settings.setValue(QString(k_str_lanedetection_manual_line2_pt2_x), pCache->manual_rightlane[2]);
    settings.setValue(QString(k_str_lanedetection_manual_line2_pt2_y), pCache->manual_rightlane[3]);

    settings.setValue(QString(k_str_lanedetection_max_fps), pCache->max_fps);
    settings.setValue(QString(k_str_videofile_fps), pCache->fps);
    settings.setValue(QString(k_str_workspace_path), pCache->workspace_path);


    int value;
    

    QRect rcBtnROILeft1 = this->btnROILeft1->geometry();
    QRect rcBtnROILeft2 = this->btnROILeft2->geometry();
    QRect rcBtnROITop1 = this->btnROITop1->geometry();
    QRect rcBtnROITop2 = this->btnROITop2->geometry();
	QRect rcBtnROIBottom1 = this->btnROIBottom1->geometry();
	QRect rcBtnROIBottom2 = this->btnROIBottom2->geometry();

    value = rcBtnROILeft1.top() + k_offset_button;
    if (value < 0) value = 0;
    settings.setValue(QString(k_str_roi_horizontal_pos1), value);

    value = rcBtnROILeft2.top() + k_offset_button;
    if (value < 0) value = 0;
    settings.setValue(QString(k_str_roi_horizontal_pos2), value);

    value = rcBtnROITop1.left() + k_offset_button;
    if (value < 0) value = 0;
    settings.setValue(QString(k_str_roi_location_top1), value);

    value = rcBtnROITop2.left() + k_offset_button;
    if (value < 0) value = 0;
    settings.setValue(QString(k_str_roi_location_top2), value);

	value = rcBtnROIBottom1.left() + k_offset_button;
	if (value < 0) value = 0;
	settings.setValue(QString(k_str_roi_location_bottom1), value);

	value = rcBtnROIBottom2.left() + k_offset_button;
	if (value < 0) value = 0;
	settings.setValue(QString(k_str_roi_location_bottom2), value);
}

void MainWindow::CreateAction()
{
//    QAction *actOpenImageFile = new QAction(tr("&Open"), this);
//    actOpenImageFile->setShortcuts(QKeySequence::Open);
//    actOpenImageFile->setStatusTip(tr("Open an existing image file"));

//    QAction *actOpenImageFile = new QAction(tr("&Open"), this);
//    actOpenImageFile->setShortcuts(QKeySequence::Open);
//    actOpenImageFile->setStatusTip(tr("Open an existing image file"));

//    QAction *actPreferences = new QAction(tr("&Preferences"), this);
//    actOpenImageFile->setShortcuts(QKeySequence::Preferences);

//    connect(actOpenImageFile, &QAction::triggered, this, &MainWindow::on_actionFile_Open_triggered);
//    connect(actPreferences, &QAction::triggered, this, &MainWindow::on_actionFile_Preferences_triggered);
}

void MainWindow::CreateConnect()
{
    connect(ui->trDWLToleranceSlider, &QSlider::valueChanged, this, &MainWindow::on_trDWLToleranceSlider_valueChanged);
    connect(ui->trDWLToleranceSlider, &QSlider::sliderMoved, this, &MainWindow::on_trDWLToleranceSlider_sliderMoved);
}

void MainWindow::InitMenubar()
{
    ui->actionControl_Start->setEnabled(false);
    ui->actionControl_Stop->setEnabled(false);
    ui->actionEngineering_DWL_Car_Rect_RatInMaze_Start->setEnabled(false);
    ui->actionEngineering_DWL_Car_Rect_RatInMaze_Stop->setEnabled(false);
}

void MainWindow::InitViews()
{
	//all of imageViews
	//ui->imageView_center->geometry().x(), ui->imageView_center->geometry().y()

	//m_imageScene.setSceneRect(
	//0,0,
	//ui->imageView_center->geometry().width(), ui->imageView_center->geometry().height());

	//m_imageSceneHLeft.setSceneRect(
	//	ui->imageView_HLeft->geometry().x(), ui->imageView_HLeft->geometry().y(),
	//	ui->imageView_HLeft->geometry().width(), ui->imageView_HLeft->geometry().height());


	ui->imageView_center->setScene(&m_imageScene);
	ui->imageView_center->setRenderHints(QPainter::Antialiasing);
	m_imageScene.SetMode(DrawableGraphicsScene::DrawLine);
	m_imageScene.setSceneRect(0, 0, ui->imageView_center->geometry().width(), ui->imageView_center->geometry().height());

	ui->imageView_HLeft->setScene(&m_imageSceneHLeft);
	ui->imageView_HLeft->setRenderHints(QPainter::Antialiasing);
	m_imageSceneHLeft.SetMode(EscortGraphicsScene::DrawLine);
	m_imageSceneHLeft.setSceneRect(0, 0, ui->imageView_HLeft->geometry().width(), ui->imageView_HLeft->geometry().height());

	ui->imageView_HMiddle->setScene(&m_imageSceneHMiddle);
	ui->imageView_HMiddle->setRenderHints(QPainter::Antialiasing);
	m_imageSceneHMiddle.SetMode(EscortGraphicsScene::DrawLine);
	m_imageSceneHMiddle.setSceneRect(0, 0, ui->imageView_HMiddle->geometry().width(), ui->imageView_HMiddle->geometry().height());

    ui->imageView_HRight->setScene(&m_imageSceneHRight);
    ui->imageView_HRight->setRenderHints(QPainter::Antialiasing);
    m_imageSceneHRight.SetMode(EscortGraphicsScene::DrawLine);
    m_imageSceneHRight.setSceneRect(0, 0, ui->imageView_HRight->geometry().width(), ui->imageView_HRight->geometry().height());

 //   QGLWidget* pWidget = new QGLWidget(QGLFormat(QGL::SampleBuffers), this);
 //   //CQtOpenCVViewerGl* pWidget = new CQtOpenCVViewerGl(QGLFormat(QGL::SampleBuffers), this);
 //   pWidget->makeCurrent();

    //ui->imageView_V1st->setViewport(pWidget);
    //ui->imageView_V1st->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    //ui->imageView_V1st->setScene(&m_imageSceneV1st);
    ////connect(pScene, SIGNAL(SwitchShader(const QString&)),
    ////    pView, SLOT(SwitchShader(const QString&)));
    ////connect(pScene, SIGNAL(SetLightPos(const QVector3D&)),
    ////    pView, SLOT(SetLightPos(const QVector3D&)));

    ui->imageView_V1st->setScene(&m_imageSceneV1st);
    ui->imageView_V1st->setRenderHints(QPainter::Antialiasing);
    m_imageSceneV1st.SetMode(EscortGraphicsScene::DrawLine);
    m_imageSceneV1st.setSceneRect(0, 0, ui->imageView_V1st->geometry().width(), ui->imageView_V1st->geometry().height());

	ui->imageView_V2nd->setScene(&m_imageSceneV2nd);
	ui->imageView_V2nd->setRenderHints(QPainter::Antialiasing);
	m_imageSceneV2nd.SetMode(EscortGraphicsScene::DrawLine);
	m_imageSceneV2nd.setSceneRect(0, 0, ui->imageView_V2nd->geometry().width(), ui->imageView_V2nd->geometry().height());

	ui->imageView_V3rd->setScene(&m_imageSceneV3rd);
	ui->imageView_V3rd->setRenderHints(QPainter::Antialiasing);
	m_imageSceneV3rd.SetMode(EscortGraphicsScene::DrawLine);
	m_imageSceneV3rd.setSceneRect(0, 0, ui->imageView_V3rd->geometry().width(), ui->imageView_V3rd->geometry().height());

	ui->imageView_Corner->setScene(&m_imageSceneCorner);
	ui->imageView_Corner->setRenderHints(QPainter::Antialiasing);
	m_imageSceneCorner.SetMode(EscortGraphicsScene::DrawLine);
	m_imageSceneCorner.setSceneRect(0, 0, ui->imageView_Corner->geometry().width(), ui->imageView_Corner->geometry().height());
}

void MainWindow::InitButton()
{
    InitROIButton();
}

void MainWindow::InitMainWidget()
{
    ui->tabTrafficRegulationWidget->setTabText(0, "double white lines");
    ui->tabTrafficRegulationWidget->setTabText(1, "double yellow lines");

    {
        ui->trDWLPositionOfLinesWidget->setRowCount(1);
        ui->trDWLPositionOfLinesWidget->setColumnCount(2);

        QStringList header;
        header << tr("Start Point") << tr("End Point");
        ui->trDWLPositionOfLinesWidget->setHorizontalHeaderLabels(header);
        ui->trDWLPositionOfLinesWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        m_dwlPosition->m_total = 
        ui->trDWLPositionOfLinesWidget->rowCount();
    }

    ui->trDWLToleranceSlider->setEnabled(false);
    ui->trDWLPositionOfLinesAddButton->setEnabled(false);
    ui->trDWLPositionOfLinesDelButton->setEnabled(false);
    ui->cbVehicleDetection->setEnabled(false);
    ui->cbLaneDetection->setEnabled(false);

    {
        ui->trVehicleInfoWidget->setRowCount(1);
        ui->trVehicleInfoWidget->setColumnCount(3);

        QStringList headerVehicleInfo;
        headerVehicleInfo << tr("Left/Top") << tr("Right/Bottom") << tr("violation");
        ui->trVehicleInfoWidget->setHorizontalHeaderLabels(headerVehicleInfo);

        m_dwlPositionVehicle->m_total = 
        ui->trVehicleInfoWidget->rowCount();
    }
}

void MainWindow::InitROIButton()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    QSettings settings(pCache->settings_file_path, QSettings::IniFormat);

    QRect rcImageView = ui->imageView_center->geometry();
    //qDebug("geo %d %d %d %d", rcImageView.left(), rcImageView.top(), rcImageView.width(), rcImageView.height());

    pCache->InitROILineCoordinate(rcImageView.width(), rcImageView.height());

    int value;
    value = settings.value(QString(k_str_roi_horizontal_pos1), (rcImageView.height() >> 2)).toInt();

    //qDebug("rcImageView %d %d %d %d",
    //       rcImageView.left(), rcImageView.top(), rcImageView.right(), rcImageView.bottom());
    //qDebug("value=%d", value);
    int x, y;
    ButtonHoverWatcher *pBtnROILeft1Watcher = new ButtonHoverWatcher(this);
    if (pBtnROILeft1Watcher)
    {
        m_pBtnROILeft1Watcher = pBtnROILeft1Watcher;
        x = 0;
        y = value - k_offset_button;

        pBtnROILeft1Watcher->SetIcon(
        QString(":/images/roi_left_ind.png"),
        QString(":/images/roi_left_ind_hov.png"));

        this->btnROILeft1->installEventFilter(pBtnROILeft1Watcher);

            //g_m = pBtnROILeft1Watcher->parent
            //this->btnROILeft1->parent = this->btnROILeft1->parentWidget
        //DSG(1, "WWW framelessWindow=%p", g_f);
        //DSG(1, "WWW mainWindow=%p", g_m);
        //DSG(1, "WWW pBtnROILeft1Watcher=%p", pBtnROILeft1Watcher);
        //DSG(1, "WWW pBtnROILeft1Watcher->parent=%p", pBtnROILeft1Watcher->parent());
        //DSG(1, "WWW pBtnROILeft1Watcher->parent->parent=%p", pBtnROILeft1Watcher->parent()->parent());
        //DSG(1, "WWW this->btnROILeft1=%p", this->btnROILeft1);
        //DSG(1, "WWW this->btnROILeft1->parent=%p", this->btnROILeft1->parent());
        //DSG(1, "WWW this->btnROILeft1->parentWidget=%p", this->btnROILeft1->parentWidget());
        //DSG(1, "WWW this->btnROILeft1->parentWidget=%p", this->btnROILeft1->parent()->parent());
        //DSG(1, "WWW this->btnROILeft1->parentWidget=%p", this->btnROILeft1->parentWidget()->parent());

        this->btnROILeft1->setGeometry(x, y, k_roi_button_size, k_roi_button_size);
        this->btnROILeft1->hide();
        //QRect rcBtnROILeft1 = this->btnROILeft1->geometry();
        //qDebug("QQ %d %d %d %d", rcBtnROILeft1.left(), rcBtnROILeft1.top(), rcBtnROILeft1.width(), rcBtnROILeft1.height());
        connect(pBtnROILeft1Watcher, &ButtonHoverWatcher::updateROILine, &m_imageScene, &DrawableGraphicsScene::handleUpdateROILine);
        //connect(btnROILeft1Watcher, SIGNAL(checkAdjustedROIEffect()), m_pImgProcessing, SLOT(handleCheckAdjustedROIEffect()));
        connect(pBtnROILeft1Watcher, SIGNAL(checkAdjustedROIEffect()), this, SLOT(handleCheckAdjustedROIEffect()));
    }
    ButtonHoverWatcher *pBtnROIRight1Watcher = new ButtonHoverWatcher(this);
    if (pBtnROIRight1Watcher)
    {
        m_pBtnROIRight1Watcher = pBtnROIRight1Watcher;
        x = rcImageView.width() - k_roi_button_size;
        y = value - k_offset_button;

        pBtnROIRight1Watcher->SetIcon(
        QString(":/images/roi_right_ind.png"),
        QString(":/images/roi_right_ind_hov.png"));
        this->btnROIRight1->installEventFilter(pBtnROIRight1Watcher);
        this->btnROIRight1->setGeometry(x, y, k_roi_button_size, k_roi_button_size);
        this->btnROIRight1->hide();

        connect(pBtnROIRight1Watcher, &ButtonHoverWatcher::updateROILine, &m_imageScene, &DrawableGraphicsScene::handleUpdateROILine);
        //connect(btnRORight1Watcher, SIGNAL(checkAdjustedROIEffect()), m_pImgProcessing, SLOT(handleCheckAdjustedROIEffect()));
        connect(pBtnROIRight1Watcher, SIGNAL(checkAdjustedROIEffect()), this, SLOT(handleCheckAdjustedROIEffect()));
    }

    value = settings.value(QString(k_str_roi_horizontal_pos2), (rcImageView.height() * 0.75)).toInt();

    ButtonHoverWatcher *pBtnROILeft2Watcher = new ButtonHoverWatcher(this);
    if (pBtnROILeft2Watcher)
    {
        m_pBtnROILeft2Watcher = pBtnROILeft2Watcher;
        x = 0;
        y = value - k_offset_button;

        pBtnROILeft2Watcher->SetIcon(
        QString(":/images/roi_left_ind.png"),
        QString(":/images/roi_left_ind_hov.png"));
        this->btnROILeft2->installEventFilter(pBtnROILeft2Watcher);
        this->btnROILeft2->setGeometry(x, y, k_roi_button_size, k_roi_button_size);
        this->btnROILeft2->hide();

        connect(pBtnROILeft2Watcher, &ButtonHoverWatcher::updateROILine, &m_imageScene, &DrawableGraphicsScene::handleUpdateROILine);
        //connect(btnROILeft2Watcher, SIGNAL(checkAdjustedROIEffect()), m_pImgProcessing, SLOT(handleCheckAdjustedROIEffect()));
        connect(pBtnROILeft2Watcher, SIGNAL(checkAdjustedROIEffect()), this, SLOT(handleCheckAdjustedROIEffect()));
    }
    ButtonHoverWatcher *pBtnROIRight2Watcher = new ButtonHoverWatcher(this);
    if (pBtnROIRight2Watcher)
    {
        m_pBtnROIRight2Watcher = pBtnROIRight2Watcher;
        x = rcImageView.width() - k_roi_button_size;
        y = value - k_offset_button;

        pBtnROIRight2Watcher->SetIcon(
        QString(":/images/roi_right_ind.png"),
        QString(":/images/roi_right_ind_hov.png"));
        this->btnROIRight2->installEventFilter(pBtnROIRight2Watcher);
        this->btnROIRight2->setGeometry(x, y, k_roi_button_size, k_roi_button_size);
        this->btnROIRight2->hide();

        connect(pBtnROIRight2Watcher, &ButtonHoverWatcher::updateROILine, &m_imageScene, &DrawableGraphicsScene::handleUpdateROILine);
        //connect(btnRORight2Watcher, SIGNAL(checkAdjustedROIEffect()), m_pImgProcessing, SLOT(handleCheckAdjustedROIEffect()));
        connect(pBtnROIRight2Watcher, SIGNAL(checkAdjustedROIEffect()), this, SLOT(handleCheckAdjustedROIEffect()));
    }

    value = settings.value(QString(k_str_roi_location_top1), (rcImageView.width() >> 2)).toInt();
    ButtonHoverWatcher *pBtnROITop1Watcher = new ButtonHoverWatcher(this);
    if (pBtnROITop1Watcher)
    {
        m_pBtnROITop1Watcher = pBtnROITop1Watcher;
        x = value - k_offset_button;
        y = 0;

        pBtnROITop1Watcher->SetIcon(
        QString(":/images/roi_top_ind.png"),
        QString(":/images/roi_top_ind_hov.png"));
        this->btnROITop1->installEventFilter(pBtnROITop1Watcher);
        this->btnROITop1->setGeometry(x, y, k_roi_button_size, k_roi_button_size);
        this->btnROITop1->hide();

        connect(pBtnROITop1Watcher, &ButtonHoverWatcher::updateROILine2, &m_imageScene, &DrawableGraphicsScene::handleUpdateROILine2);
        //connect(btnROITop1Watcher, SIGNAL(checkAdjustedROIEffect()), m_pImgProcessing, SLOT(handleCheckAdjustedROIEffect()));
        connect(pBtnROITop1Watcher, SIGNAL(checkAdjustedROIEffect()), this, SLOT(handleCheckAdjustedROIEffect()));
    }

	value = settings.value(QString(k_str_roi_location_bottom1), (rcImageView.width() >> 2)).toInt();
    ButtonHoverWatcher *pBtnROIBottom1Watcher = new ButtonHoverWatcher(this);
    if (pBtnROIBottom1Watcher)
    {
        m_pBtnROIBottom1Watcher = pBtnROIBottom1Watcher;
        x = value - k_offset_button;
        y = rcImageView.height() - k_roi_button_size;

        pBtnROIBottom1Watcher->SetIcon(
        QString(":/images/roi_bottom_ind.png"),
        QString(":/images/roi_bottom_ind_hov.png"));
        this->btnROIBottom1->installEventFilter(pBtnROIBottom1Watcher);
        this->btnROIBottom1->setGeometry(x, y, k_roi_button_size, k_roi_button_size);
        this->btnROIBottom1->hide();

        connect(pBtnROIBottom1Watcher, &ButtonHoverWatcher::updateROILine2, &m_imageScene, &DrawableGraphicsScene::handleUpdateROILine2);
        //connect(btnROIBottom1Watcher, SIGNAL(checkAdjustedROIEffect()), m_pImgProcessing, SLOT(handleCheckAdjustedROIEffect()));
        connect(pBtnROIBottom1Watcher, SIGNAL(checkAdjustedROIEffect()), this, SLOT(handleCheckAdjustedROIEffect()));
    }

    value = settings.value(QString(k_str_roi_location_top2), (rcImageView.width() * 0.75)).toInt();
    ButtonHoverWatcher *pBtnROITop2Watcher = new ButtonHoverWatcher(this);
    if (pBtnROITop2Watcher)
    {
        m_pBtnROITop2Watcher = pBtnROITop2Watcher;
        x = value - k_offset_button;
        y = 0;

        pBtnROITop2Watcher->SetIcon(
        QString(":/images/roi_top_ind.png"),
        QString(":/images/roi_top_ind_hov.png"));
        this->btnROITop2->installEventFilter(pBtnROITop2Watcher);
        this->btnROITop2->setGeometry(x, y, k_roi_button_size, k_roi_button_size);
        this->btnROITop2->hide();

        connect(pBtnROITop2Watcher, &ButtonHoverWatcher::updateROILine2, &m_imageScene, &DrawableGraphicsScene::handleUpdateROILine2);
        //connect(btnROITop2Watcher, SIGNAL(checkAdjustedROIEffect()), m_pImgProcessing, SLOT(handleCheckAdjustedROIEffect()));
        connect(pBtnROITop2Watcher, SIGNAL(checkAdjustedROIEffect()), this, SLOT(handleCheckAdjustedROIEffect()));
    }

	value = settings.value(QString(k_str_roi_location_bottom2), (rcImageView.width() * 0.75)).toInt();
    ButtonHoverWatcher *pBtnROIBottom2Watcher = new ButtonHoverWatcher(this);
    if (pBtnROIBottom2Watcher)
    {
        m_pBtnROIBottom2Watcher = pBtnROIBottom2Watcher;
        x = value - k_offset_button;
        y = rcImageView.height() - k_roi_button_size;

        pBtnROIBottom2Watcher->SetIcon(
        QString(":/images/roi_bottom_ind.png"),
        QString(":/images/roi_bottom_ind.png"));
        this->btnROIBottom2->installEventFilter(pBtnROIBottom2Watcher);
        this->btnROIBottom2->setGeometry(x, y, k_roi_button_size, k_roi_button_size);
        this->btnROIBottom2->hide();

        connect(pBtnROIBottom2Watcher, &ButtonHoverWatcher::updateROILine2, &m_imageScene, &DrawableGraphicsScene::handleUpdateROILine2);
        //connect(btnROIBottom2Watcher, SIGNAL(checkAdjustedROIEffect()), m_pImgProcessing, SLOT(handleCheckAdjustedROIEffect()));
        connect(pBtnROIBottom2Watcher, SIGNAL(checkAdjustedROIEffect()), this, SLOT(handleCheckAdjustedROIEffect()));
    }

	connect(&m_imageScene, &DrawableGraphicsScene::roiLineChanged, this, &MainWindow::handleROILineChanged);
}

void MainWindow::SwitchROIButton()
{
    if (true == ui->actionView_ROILine->isChecked())
    {
        this->btnROILeft1->show();
        this->btnROILeft2->show();
        this->btnROIRight1->show();
        this->btnROIRight2->show();
        this->btnROITop1->show();
        this->btnROITop2->show();
        this->btnROIBottom1->show();
        this->btnROIBottom2->show();

        m_imageScene.SwitchROILine(true);
        //m_imageProcessing.GetProcessingSetting()->setEnableROI(true);
    }
    else
    {
        this->btnROILeft1->hide();
        this->btnROILeft2->hide();
        this->btnROIRight1->hide();
        this->btnROIRight2->hide();
        this->btnROITop1->hide();
        this->btnROITop2->hide();
        this->btnROIBottom1->hide();
        this->btnROIBottom2->hide();

        m_imageScene.SwitchROILine(false);
        //m_imageProcessing.GetProcessingSetting()->setEnableROI(true);
    }
}

void MainWindow::ReflectMainWidget(bool bInit)
{   
    if (bInit)
    {
        ui->actionControl_Start->setEnabled(true);
        ui->actionControl_Stop->setEnabled(false);
        ui->actionEngineering_DWL_Car_Rect_RatInMaze_Start->setEnabled(true);
        ui->actionEngineering_DWL_Car_Rect_RatInMaze_Stop->setEnabled(false);

        ui->trDWLPositionOfLinesAddButton->setEnabled(true);
        ui->trDWLPositionOfLinesDelButton->setEnabled(true);
        ui->cbLaneDetection->setEnabled(true);
        ui->cbVehicleDetection->setEnabled(true);
        ui->trDWLToleranceSlider->setEnabled(true);

        // after LoadSettings
        if (true == ui->actionEngineering_DWL_Randomly_Generate_Lines->isChecked())
        {
            InitRandomLine();
            InitRandomRect();
        }
        else
        {
            // m_imageScene
            this->InitImageSceneROILine();
            m_imageScene.SwitchROILine(ui->actionView_ROILine->isChecked());

            // QPushButton
            this->SwitchROIButton();
        }

        ShowImageList();
        ShowTreeWidget();
    }
}

bool MainWindow::OpenImageFileProbe(const QString &absolutefileName)
{
    bool ret = false;

    ret = true;

    return ret;
}

//bool MainWindow::OpenImageFile(const QString &absolutefileName)
//{
//    bool ret = false;
//
//    QFile file(absolutefileName);
//    QFileInfo fileInfo(file.fileName());
//    QString filename(fileInfo.fileName());
//    QString str("TrafficEnforcement");
//    QWidget::setWindowTitle(str + tr("-") + filename);
//#if 0
//    this->m_imageProcessing.InitParameters(filename.toLatin1().constData());
//
//    SVideoConfig videoConfig;
//    videoConfig.interval_ms = -1;
//    strcpy(videoConfig.filename, filename.toLatin1().constData());
//    m_imageProcessing.SetVideoConfig(&videoConfig);
//
//    ui->imageView_center->setRenderHints(QPainter::Antialiasing);
//    ui->imageView_HLeft->setRenderHints(QPainter::Antialiasing);
//    ui->imageView_HMiddle->setRenderHints(QPainter::Antialiasing);
//    ui->imageView_HRight->setRenderHints(QPainter::Antialiasing);
//    ui->imageView_V1st->setRenderHints(QPainter::Antialiasing);
//    ui->imageView_center->setRenderHints(QPainter::Antialiasing);
//    ui->imageView_V2nd->setRenderHints(QPainter::Antialiasing);
//    ui->imageView_V3rd->setRenderHints(QPainter::Antialiasing);
//    ui->imageView_Corner->setRenderHints(QPainter::Antialiasing);
//
//    m_imageScene.SetMode(DrawableGraphicsScene::DrawLine);
//    m_imageSceneHLeft.SetMode(EscortGraphicsScene::DrawLine);
//    m_imageSceneHMiddle.SetMode(EscortGraphicsScene::DrawLine);
//    m_imageSceneHRight.SetMode(EscortGraphicsScene::DrawLine);
//    m_imageSceneV1st.SetMode(EscortGraphicsScene::DrawLine);
//    m_imageSceneV2nd.SetMode(EscortGraphicsScene::DrawLine);
//    m_imageSceneV3rd.SetMode(EscortGraphicsScene::DrawLine);
//    m_imageSceneCorner.SetMode(EscortGraphicsScene::DrawLine);
//
//    //InitMenubar
//    ui->actionEngineering_DWL_Car_Rect_RatInMaze_Start->setEnabled(true);
//    ui->actionEngineering_DWL_Car_Rect_RatInMaze_Stop->setEnabled(false);
//    //InitButton
//
//
//    //initWidget
//    ui->actionControl_Start->setEnabled(true);
//    ui->actionControl_Stop->setEnabled(false);
//    ui->trDWLPositionOfLinesAddButton->setEnabled(true);
//    ui->trDWLPositionOfLinesDelButton->setEnabled(true);
//    //ui->cbLaneDetection->setEnabled(true);
//    //ui->cbVehicleDetection->setEnabled(true);
//    ui->trDWLToleranceSlider->setEnabled(true);
//
//    ui->trDWLPositionOfLinesWidget->setRowCount(1);
//    ui->trDWLPositionOfLinesWidget->setColumnCount(2);
//    QStringList header;
//    header << tr("Start Point") << tr("End Point");
//    ui->trDWLPositionOfLinesWidget->setHorizontalHeaderLabels(header);
//    ui->trDWLPositionOfLinesWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//    //ui->trDWLPositionOfLinesWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
//    //ui->trDWLPositionOfLinesWidget->setSelectionMode(QAbstractItemView::SingleSelection);
//    //ui->trDWLPositionOfLinesWidget->resize(250, 200);
//    m_dwlPosition->m_total = ui->trDWLPositionOfLinesWidget->rowCount();
//
//    ui->trVehicleInfoWidget->setRowCount(1);
//    ui->trVehicleInfoWidget->setColumnCount(3);
//    QStringList headerVehicleInfo;
//    headerVehicleInfo << tr("Left/Top") << tr("Right/Bottom") << tr("violation");
//    ui->trVehicleInfoWidget->setHorizontalHeaderLabels(headerVehicleInfo);
//    //ui->trVehicleInfoWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//    //ui->trVehicleInfoWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
//    //ui->trVehicleInfoWidget->setSelectionMode(QAbstractItemView::SingleSelection);
//    //ui->trVehicleInfoWidget->resize(250, 200);
//
//    m_dwlPositionVehicle->m_total = ui->trVehicleInfoWidget->rowCount();
//
//    //on_cbLaneDetection_stateChanged(ui->cbLaneDetection->checkState());
//    //on_cbVehicleDetection_stateChanged(ui->cbVehicleDetection->checkState());
//
//    if (true == ui->actionEngineering_DWL_Randomly_Generate_Lines->isChecked())
//    {
//        InitRandomLine();
//        InitRandomRect();
//    }
//    else
//    {
//        // m_imageScene
//        this->InitImageSceneROILine();
//        m_imageScene.SwitchROILine(ui->actionView_ROILine->isChecked());
//
//        // QPushButton
//        this->SwitchROIButton();
//
//        // m_imageProcessing
//        connect(&m_imageProcessing, SIGNAL(displayImageReady(const cv::Mat &)),
//            &m_imageScene, SLOT(handleUpdateMainImageView(const cv::Mat &)));
//
//        QSettings settings(m_settingsFileName, QSettings::IniFormat);
//        settings.setIniCodec("UTF-8");
//        ProcessingSetting *pSetting = m_imageProcessing.GetProcessingSetting();
//        pSetting->setEnableLaneDetection(ui->cbLaneDetection->isChecked());
//        pSetting->setEnableVehicleDetection(ui->cbVehicleDetection->isChecked());
//        pSetting->setEnableROI(ui->actionTools_EnableROI->isChecked());
//        pSetting->setEnableBar(ui->actionEngineering_ShowTrackbar->isChecked());
//        pSetting->setEnableEngineeringMode(ui->actionEngineering_Enable->isChecked());
//        pSetting->setTolerance(ui->trDWLToleranceSlider->value());
//
//        pSetting->setVD1PassDiffThreshold(settings.value(QString(k_str_tools_opt_vd_1pass_diffthreshold_idx), 30).toInt());
//        pSetting->setVehicleDetectionModel(settings.value(QString(k_str_tools_opt_vd_model_idx), 0).toInt());
//
//        pSetting->setOperationMode(e_operation_static);
//        //pSetting->setLaneDetectionApproach(e_lanedetection_canny);//e_lanedetection_mix_hls_hsv_canny);//e_lanedetection_hsv);//e_lanedetection_hls//e_lanedetection_mix;//e_lanedetection_hsv;// e_lanedetection_canny;
//        pSetting->setLaneDetectionApproach(e_lanedetection_mix_hls_hsv_sobel_canny);//e_lanedetection_mix_hls_hsv_canny);
//        pSetting->setLaneDetectionCycleTime(
//            k_lanedetection_cycletime[settings.value(QString(k_str_tools_opt_ld_cycletime_idx), 0).toInt()].mode);
//        pSetting->setVehicleDetectionCycleTime(
//            k_vehicledetection_cycletime[settings.value(QString(k_str_tools_opt_vd_cycletime_idx), 0).toInt()].mode);
//
//        Mat frame = CVImageOpen(absolutefileName.toLatin1().constData());
//        Mat scaledFrame;
//        DSG(1, "ui->imageView_center->geometry().width()=%d", ui->imageView_center->geometry().width());
//        DSG(1, "ui->imageView_center->geometry().height()=%d", ui->imageView_center->geometry().height());
//        PrintMatInfo(frame);
//        CVImageResize2ByWH(frame, scaledFrame,
//            ui->imageView_center->geometry().width(),
//            ui->imageView_center->geometry().height());
//
//
//        PrintMatInfo(scaledFrame);
//
//        m_imageProcessing.Probe(scaledFrame, &m_imageScene);
//        m_imageProcessing.Process();
//    }
//    m_actionState = e_action_openimagefile_is_opened;
//#endif
//
//    return ret;
//}

bool MainWindow::OpenVideoFileProbe(const QString &absolutefileName)
{
    bool ret = false;

    //if (!m_pVideoFileSource)
    //    m_pVideoFileSource = new TRVideoFileSource(nullptr);

    //if (m_pVideoFileSource)
    //{
    //    SVideoConfig config;
    //    config.interval_ms = 33;// -1;// 33;
    //    config.want_width = ui->imageView_center->geometry().width();
    //    config.want_height = ui->imageView_center->geometry().height();
    //    strcpy_s(config.filename, absolutefileName.toLatin1().constData());

    //    if (m_pVideoFileSource->SetConfig(&config))
    //    {
    //        ret = true;
    //    }
    //}

    return ret;
}

bool MainWindow::OpenVideoFile(const QString &absolutefileName)
{
//    bool ret = false;
//
//    do {
//        SVideoConfig config;
//        config.interval_ms = 33;// -1;// 33;
//        config.want_width = ui->imageView_center->geometry().width();
//        config.want_height = ui->imageView_center->geometry().height();
//        strcpy_s(config.filename, absolutefileName.toLatin1().constData());
//
//        if (!OpenVideoFileProbe(absolutefileName))
//            break;
//
//        if (!m_pVideoFileSourceThread)
//            m_pVideoFileSourceThread = new SafeFreeThread;
//
//        if (!m_pImgProcessing)
//            m_pImgProcessing = new ImageProcessing(nullptr);
//        if (!m_pImageProcessingColorPreviewThread)
//            m_pImageProcessingColorPreviewThread = new SafeFreeThread;
//
//        if ((!m_pVideoFileSourceThread) ||
//            (!m_pImgProcessing) ||
//            (!m_pImageProcessingColorPreviewThread))
//            break;
//
//        ImageProcessing *pImgProc = m_pImgProcessing;
//        TRVideoFileSource *pSource = m_pVideoFileSource;
//        if (!pSource->Open())
//            break;
//
//        QFile file(absolutefileName);
//        QFileInfo fileInfo(file.fileName());
//        QString filename(fileInfo.fileName());
//        pImgProc->InitParameters(filename.toLatin1().constData());
//
//        ++m_totalSourceTasks;
//
//        pSource->moveToThread(m_pVideoFileSourceThread);
//
//        ////////////////////////////////////////////////////////////////////////
//        // preview image processing ////////////////////////////////////////////
//        ////////////////////////////////////////////////////////////////////////
//        {
//            //QSettings settings(m_settingsFileName, QSettings::IniFormat);
//            //settings.setIniCodec("UTF-8");
//
//            ////ImageProcessing config
//            //ProcessingSetting *pSetting = pImgProc->GetProcessingSetting();
//
//            //pSetting->setEnableLaneDetection(ui->cbLaneDetection->isChecked());
//            //pSetting->setEnableVehicleDetection(ui->cbVehicleDetection->isChecked());
//            //pSetting->setEnableROI(ui->actionTools_EnableROI->isChecked());
//            //pSetting->setEnableBar(ui->actionEngineering_ShowTrackbar->isChecked());
//            //pSetting->setEnableEngineeringMode(ui->actionEngineering_Enable->isChecked());
//            //pSetting->setTolerance(ui->trDWLToleranceSlider->value());
//
//            //pSetting->setVD1PassDiffThreshold(settings.value(QString(k_str_tools_opt_vd_1pass_diffthreshold_idx), 30).toInt());
//            //pSetting->setVehicleDetectionModel(settings.value(QString(k_str_tools_opt_vd_model_idx), 0).toInt());
//
//            //pSetting->setOperationMode(e_operation_live);
//            //pSetting->setLaneDetectionApproach(e_lanedetection_mix_hls_hsv_sobel_canny);//e_lanedetection_mix_hls_hsv_canny);
//            //pSetting->setLaneDetectionCycleTime(k_lanedetection_cycletime[settings.value(QString(k_str_tools_opt_ld_cycletime_idx), 0).toInt()].mode);
//            //pSetting->setVehicleDetectionCycleTime(k_vehicledetection_cycletime[settings.value(QString(k_str_tools_opt_vd_cycletime_idx), 0).toInt()].mode);
//        }
//        {
//            SCameraImageProcessingConfig config;
//            DSG(1, "OpenCamera ui->imageView_center->geometry().width()=%d", ui->imageView_center->geometry().width());
//            DSG(1, "OpenCamera ui->imageView_center->geometry().height()=%d", ui->imageView_center->geometry().height());
//            config.want_width = ui->imageView_center->geometry().width();
//            config.want_height = ui->imageView_center->geometry().height();
//            pImgProc->SetConfig(&config);
//            pImgProc->SetProcessAll(false);
//            pImgProc->SetApproach(e_imageprocessing_common_preview);
//            pImgProc->moveToThread(m_pImageProcessingColorPreviewThread);
//        }
//
//        qRegisterMetaType<cv::Mat>();
//        connect(pSource, &TRVideoFileSource::started, this, &MainWindow::handleAllVideoFileSourceStarted);
//        connect(pSource, &TRVideoFileSource::stopped, this, &MainWindow::handleAllVideoFileSourceStopped);
//        connect(pSource, &TRVideoFileSource::cvColorReady, pImgProc, &ImageProcessing::handleProcessedFrameCV);
//        connect(this, &MainWindow::sourceAllStarted, pImgProc, &ImageProcessing::handleUpstreamStarted);
//        connect(this, &MainWindow::sourceAllStopped, pImgProc, &ImageProcessing::handleUpstreamStopped);
//
//        connect(pImgProc, SIGNAL(videoImageProcessingStarted()), &m_imageScene, SLOT(handleUpstreamStarted()));
//        connect(pImgProc, SIGNAL(videoImageProcessingStopped()), &m_imageScene, SLOT(handleUpstreamStopped()));
//        connect(&m_imageScene, &DrawableGraphicsScene::videoRenderStarted, this, &MainWindow::handleAllComponentsAreRunning);
//        connect(&m_imageScene, &DrawableGraphicsScene::videoRenderStopped, this, &MainWindow::handleAllComponentsAreStopped);
//        connect(pImgProc, &ImageProcessing::targetImageReady, &m_imageScene, &DrawableGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::maskHLSImageReady, &m_imageSceneHLeft, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::maskHSVImageReady, &m_imageSceneHMiddle, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::maskSobelImageReady, &m_imageSceneHRight, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::mixOutImageReady, &m_imageSceneV1st, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::cannyOutImageReady, &m_imageSceneV2nd, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::diffImageReady, &m_imageSceneV3rd, &EscortGraphicsScene::handleUpdateViewCV);
//
//        //CreateConnect
//        connect(ui->trDWLToleranceSlider, &QSlider::sliderMoved, pImgProc, &ImageProcessing::handleUpdateTolerance);
//        connect(pImgProc, &ImageProcessing::updateLaneline, this, &MainWindow::handleUpdateLanelnfo);
//        connect(pImgProc, &ImageProcessing::updateLaneline, &m_imageScene, &DrawableGraphicsScene::handleUpdateLanelnfo);
//        connect(pImgProc, &ImageProcessing::updateVehicleInfo, this, &MainWindow::handleUpdateVehicleInfo);
//
//        //InitROIButton
//        connect(this, SIGNAL(redirectCheckAdjustedROIEffect()), pImgProc, SLOT(handleCheckAdjustedROIEffect()));
//
//        ++m_totalSinkTasks;
//
//        Mat frame;
//        if (!pSource->GetFirstFrame(frame))
//            break;
////
////#ifdef DO_PRESCALE
////        Mat scaledFrame;
////        //PrintMatInfo(frame);
////        //DSG(1, "OpenVideoFile ui->imageView_center->geometry().width()=%d", ui->imageView_center->geometry().width());
////        //DSG(1, "OpenVideoFile ui->imageView_center->geometry().height()=%d", ui->imageView_center->geometry().height());
////
////        CQElapsedTimer timer;
////        CVImageResize2ByWH(frame, scaledFrame, ui->imageView_center->geometry().width(), ui->imageView_center->geometry().height());
////        timer.Print("width=%d height=%d", ui->imageView_center->geometry().width(), ui->imageView_center->geometry().height());
////        //PrintMatInfo(scaledFrame);
////
////        pImgProc->Probe(scaledFrame, &m_imageScene);
////#else
////        pImgProc->Probe(frame, &m_imageScene);
////#endif
//        pImgProc->Probe(frame, &m_imageScene);
//        pImgProc->SyncProcess();
//        //imwrite("first_frameinwindow.bmp", frame);
//
//        ret = true;
//
//    } while (0);
//
//    return ret;



    return true;
}

void MainWindow::CloseVideoFile()
{
    //m_totalSourceTasks = 0;
    //m_totalSinkTasks = 0;

    //disconnect(this, SIGNAL(sourceAllStarted()), 0, 0);
    //disconnect(this, SIGNAL(sourceAllStopped()), 0, 0);

    //ImageProcessing *pImageProcessing = m_pImgProcessing;
    //if (pImageProcessing)
    //{
    //    //InitROIButton
    //    disconnect(this, SIGNAL(redirectCheckAdjustedROIEffect()), 0, 0);
    //    //CreateConnect
    //    disconnect(ui->trDWLToleranceSlider, &QSlider::sliderMoved, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::updateLaneline, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::updateVehicleInfo, 0, 0);

    //    disconnect(pImageProcessing, &ImageProcessing::videoImageProcessingStarted, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::videoImageProcessingStopped, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::targetImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::maskHLSImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::maskHSVImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::maskSobelImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::mixOutImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::cannyOutImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::diffImageReady, 0, 0);

    //    delete m_pImgProcessing;
    //    m_pImgProcessing = nullptr;
    //}

    //if (m_pImageProcessingColorPreviewThread)
    //{
    //    delete m_pImageProcessingColorPreviewThread;
    //    m_pImageProcessingColorPreviewThread = nullptr;
    //}

    //if (m_pVideoFileSource)
    //{
    //    disconnect(m_pVideoFileSource, &TRVideoFileSource::cvColorReady, 0, 0);

    //    disconnect(m_pVideoFileSource, &TRVideoFileSource::stopped, 0, 0);
    //    disconnect(m_pVideoFileSource, &TRVideoFileSource::started, 0, 0);

    //    delete m_pVideoFileSource;
    //    m_pVideoFileSource = nullptr;
    //}

    //if (m_pVideoFileSourceThread)
    //{
    //    delete m_pVideoFileSourceThread;
    //    m_pVideoFileSourceThread = nullptr;
    //}

    //disconnect(&m_imageScene, &DrawableGraphicsScene::videoRenderStarted, 0, 0);
    //disconnect(&m_imageScene, &DrawableGraphicsScene::videoRenderStopped, 0, 0);
}

bool MainWindow::OpenCameraProbe(const QString &absolutefileName)
{
    bool ret = false;

    //if (!m_pVideoCapture)
    //    m_pVideoCapture = new TRVideoCapture(nullptr);

    //if (m_pVideoCapture)
    //{
    //    SCameraConfig config;
    //    if (absolutefileName.isEmpty())
    //        config.device_idx = 0; //pGlobalCacheData->devices_capture_idx;
    //    else
    //    {
    //        config.device_idx = -1;
    //        strcpy_s(config.filename, absolutefileName.toLatin1().constData());
    //    }

    //    config.want_width = ui->imageView_center->geometry().width();
    //    config.want_height = ui->imageView_center->geometry().height(); 
    //    // pDeviceFilterInfoList->capability[pGlobalCacheData->devices_color_resolution_idx].width;
    //    // pDeviceFilterInfoList->capability[pGlobalCacheData->devices_color_resolution_idx].height;
    //    config.interval_ms = 33;// -1;// 33;
    //    qDebug("MainWindow::OpenCameraProbe config.device_idx=%d", config.device_idx);

    //    if (m_pVideoCapture->SetConfig(&config))
    //    {
    //        ret = true;
    //    }
    //}

    return ret;

}

bool MainWindow::OpenCamera(const QString &absolutefileName)
{
    bool ret = false;

//    do {
//
//        if (!OpenCameraProbe(absolutefileName))
//            break;
//
//        if (!m_pVideoCaptureThread)
//            m_pVideoCaptureThread = new SafeFreeThread;
//
//        if (!m_pImgProcessing)
//            m_pImgProcessing = new ImageProcessing(nullptr);
//        if (!m_pImageProcessingColorPreviewThread)
//            m_pImageProcessingColorPreviewThread = new SafeFreeThread;
//
//        if ((!m_pVideoCaptureThread) ||
//            (!m_pImgProcessing) ||
//            (!m_pImageProcessingColorPreviewThread))
//            break;
//
//        ImageProcessing *pImgProc = m_pImgProcessing;
//        TRVideoCapture *pSource = m_pVideoCapture;
//        if (!pSource->Open())
//            break;
//
//        ++m_totalSourceTasks;
//
//        pSource->moveToThread(m_pVideoCaptureThread);
//
//        ////////////////////////////////////////////////////////////////////////
//        // preview image processing ////////////////////////////////////////////
//        ////////////////////////////////////////////////////////////////////////
//        {
//            //QSettings settings(m_settingsFileName, QSettings::IniFormat);
//            //settings.setIniCodec("UTF-8");
//
//            ////ImageProcessing config
//            //ProcessingSetting *pSetting = pImgProc->GetProcessingSetting();
//
//            //pSetting->setEnableLaneDetection(ui->cbLaneDetection->isChecked());
//            //pSetting->setEnableVehicleDetection(ui->cbVehicleDetection->isChecked());
//            //pSetting->setEnableROI(ui->actionTools_EnableROI->isChecked());
//            //pSetting->setEnableBar(ui->actionEngineering_ShowTrackbar->isChecked());
//            //pSetting->setEnableEngineeringMode(ui->actionEngineering_Enable->isChecked());
//            //pSetting->setTolerance(ui->trDWLToleranceSlider->value());
//
//            //pSetting->setVD1PassDiffThreshold(settings.value(QString(k_str_tools_opt_vd_1pass_diffthreshold_idx), 30).toInt());
//            //pSetting->setVehicleDetectionModel(settings.value(QString(k_str_tools_opt_vd_model_idx), 0).toInt());
//
//            //pSetting->setOperationMode(e_operation_live);
//            //pSetting->setLaneDetectionApproach(e_lanedetection_mix_hls_hsv_sobel_canny);//e_lanedetection_mix_hls_hsv_canny);
//            //pSetting->setLaneDetectionCycleTime(k_lanedetection_cycletime[settings.value(QString(k_str_tools_opt_ld_cycletime_idx), 0).toInt()].mode);
//            //pSetting->setVehicleDetectionCycleTime(k_vehicledetection_cycletime[settings.value(QString(k_str_tools_opt_vd_cycletime_idx), 0).toInt()].mode);
//        }
//        {
//            SCameraImageProcessingConfig config;
//            DSG(1, "OpenCamera ui->imageView_center->geometry().width()=%d", ui->imageView_center->geometry().width());
//            DSG(1, "OpenCamera ui->imageView_center->geometry().height()=%d", ui->imageView_center->geometry().height());
//            config.want_width = ui->imageView_center->geometry().width();
//            config.want_height = ui->imageView_center->geometry().height();
//            pImgProc->SetConfig(&config);
//            pImgProc->SetProcessAll(false);
//            pImgProc->SetApproach(e_imageprocessing_common_preview);
//            pImgProc->moveToThread(m_pImageProcessingColorPreviewThread);
//        }
//
//        qRegisterMetaType<cv::Mat>();
//        connect(pSource, &TRVideoCapture::started, this, &MainWindow::handleAllVideoCaptureDeviceStarted);
//        connect(pSource, &TRVideoCapture::stopped, this, &MainWindow::handleAllVideoCaptureDeviceStopped);
//        connect(pSource, &TRVideoCapture::cvColorReady, pImgProc, &ImageProcessing::handleProcessedFrameCV);
//        connect(this, &MainWindow::sourceAllStarted, pImgProc, &ImageProcessing::handleUpstreamStarted);
//        connect(this, &MainWindow::sourceAllStopped, pImgProc, &ImageProcessing::handleUpstreamStopped);
//        connect(pImgProc, SIGNAL(videoImageProcessingStarted()), &m_imageScene, SLOT(handleUpstreamStarted()));
//        connect(pImgProc, SIGNAL(videoImageProcessingStopped()), &m_imageScene, SLOT(handleUpstreamStopped()));
//        connect(&m_imageScene, &DrawableGraphicsScene::videoRenderStarted, this, &MainWindow::handleAllComponentsAreRunning);
//        connect(&m_imageScene, &DrawableGraphicsScene::videoRenderStopped, this, &MainWindow::handleAllComponentsAreStopped);
//        connect(pImgProc, &ImageProcessing::targetImageReady, &m_imageScene, &DrawableGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::maskHLSImageReady, &m_imageSceneHLeft, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::maskHSVImageReady, &m_imageSceneHMiddle, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::maskSobelImageReady, &m_imageSceneHRight, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::mixOutImageReady, &m_imageSceneV1st, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::cannyOutImageReady, &m_imageSceneV2nd, &EscortGraphicsScene::handleUpdateViewCV);
//        connect(pImgProc, &ImageProcessing::diffImageReady, &m_imageSceneV3rd, &EscortGraphicsScene::handleUpdateViewCV);
//
//        //CreateConnect
//        connect(ui->trDWLToleranceSlider, &QSlider::sliderMoved, pImgProc, &ImageProcessing::handleUpdateTolerance);
//        connect(pImgProc, &ImageProcessing::updateLaneline, this, &MainWindow::handleUpdateLanelnfo);
//        connect(pImgProc, &ImageProcessing::updateLaneline, &m_imageScene, &DrawableGraphicsScene::handleUpdateLanelnfo);
//        connect(pImgProc, &ImageProcessing::updateVehicleInfo, this, &MainWindow::handleUpdateVehicleInfo);
//
//        //InitROIButton
//        connect(this, SIGNAL(redirectCheckAdjustedROIEffect()), pImgProc, SLOT(handleCheckAdjustedROIEffect()));
//
//        ++m_totalSinkTasks;
//
//        Mat frame;
//        if (!pSource->GetFirstFrame(frame))
//            break;
////
////#ifdef DO_PRESCALE
////        Mat scaledFrame;
////        //PrintMatInfo(frame);
////        //DSG(1, "OpenVideoFile ui->imageView_center->geometry().width()=%d", ui->imageView_center->geometry().width());
////        //DSG(1, "OpenVideoFile ui->imageView_center->geometry().height()=%d", ui->imageView_center->geometry().height());
////
////        CQElapsedTimer timer;
////        CVImageResize2ByWH(frame, scaledFrame, 
////            ui->imageView_center->geometry().width(), ui->imageView_center->geometry().height());
////        //namedWindow("frame", CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);
////        //imshow("frame", frame);
////        //cv::waitKey(0);
////
////        timer.Print("width=%d height=%d", 
////            ui->imageView_center->geometry().width(), 
////            ui->imageView_center->geometry().height());
////        //PrintMatInfo(scaledFrame);
////        
////        pImgProc->Probe(scaledFrame, &m_imageScene);
////#else
////        pImgProc->Probe(frame, &m_imageScene);
////#endif
////
//        pImgProc->Probe(frame, &m_imageScene);
//        pImgProc->SyncProcess();
//        //imwrite("first_frameinwindow.bmp", frame);
//
//        ret = true;
//
//    } while (0);

    return ret;
}

void MainWindow::CloseCamera()
{
    //cv::destroyAllWindows();
    //m_totalSourceTasks = 0;
    //m_totalSinkTasks = 0;

    //disconnect(this, SIGNAL(sourceAllStarted()), 0, 0);
    //disconnect(this, SIGNAL(sourceAllStopped()), 0, 0);

    //ImageProcessing *pImageProcessing = m_pImgProcessing;
    //if (pImageProcessing)
    //{
    //    //InitROIButton
    //    disconnect(this, SIGNAL(redirectCheckAdjustedROIEffect()), 0, 0);
    //    //CreateConnect
    //    disconnect(ui->trDWLToleranceSlider, &QSlider::sliderMoved, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::updateLaneline, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::updateVehicleInfo, 0, 0);

    //    disconnect(pImageProcessing, &ImageProcessing::videoImageProcessingStarted, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::videoImageProcessingStopped, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::targetImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::maskHLSImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::maskHSVImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::maskSobelImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::mixOutImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::cannyOutImageReady, 0, 0);
    //    disconnect(pImageProcessing, &ImageProcessing::diffImageReady, 0, 0);

    //    delete m_pImgProcessing;
    //    m_pImgProcessing = nullptr;
    //}

    //if (m_pImageProcessingColorPreviewThread)
    //{
    //    delete m_pImageProcessingColorPreviewThread;
    //    m_pImageProcessingColorPreviewThread = nullptr;
    //}

    //if (m_pVideoCapture)
    //{
    //    disconnect(m_pVideoCapture, &TRVideoCapture::cvColorReady, 0, 0);

    //    disconnect(m_pVideoCapture, &TRVideoCapture::stopped, 0, 0);
    //    disconnect(m_pVideoCapture, &TRVideoCapture::started, 0, 0);

    //    delete m_pVideoCapture;
    //    m_pVideoCapture = nullptr;
    //}

    //if (m_pVideoCaptureThread)
    //{
    //    delete m_pVideoCaptureThread;
    //    m_pVideoCaptureThread = nullptr;
    //}

    //disconnect(&m_imageScene, &DrawableGraphicsScene::videoRenderStarted, 0, 0);
    //disconnect(&m_imageScene, &DrawableGraphicsScene::videoRenderStopped, 0, 0);
}

bool MainWindow::OpenVideoFileProbeAll(int idx, const QString &absolutefileName)
{
    bool ret = false;

    return ret;
}

bool MainWindow::OpenVideoFileAll(int idx, const QString &absolutefileName)
{
    bool ret = false;

    return ret;
}

void MainWindow::CloseVideoFileAll(int idx)
{

}

#if 0
bool MainWindow::OpenDemo()
{
    bool ret = false;

	if (!m_pVideoFileDemoThread)
		m_pVideoFileDemoThread = new SafeFreeThread;

#ifdef DEMO_CAMERA
	if (!m_pVideoDemo)
		m_pVideoDemo = new TRVideoCapture(nullptr);
	SCameraConfig config;
	config.device_idx = 0;
	config.interval_ms = -1;// 33;
#else
	if (!m_pVideoDemo)
		m_pVideoDemo = new TRVideoFileSource(nullptr);

	const QString absolutefileNameDemo = "D:\\Work\\projects\\TrafficEnforcementData\\video\\complex\\DSCF4449.MOV";
	SVideoConfig config;
	config.interval_ms = -1;// 33;
	strcpy(config.filename, absolutefileNameDemo.toLatin1().constData());
#endif

	qRegisterMetaType<cv::Mat>();
	if (m_pVideoDemo->SetConfig((void*)(&config)))
	{
		m_pVideoDemo->moveToThread(m_pVideoFileDemoThread);
	#ifdef DEMO_CAMERA
		connect(m_pVideoDemo, &TRVideoCapture::matReady, &m_imageSceneCorner, &EscortGraphicsScene::handleUpdateViewCV);
	#else
		connect(m_pVideoDemo, &TRVideoFileSource::matReady, &m_imageSceneCorner, &EscortGraphicsScene::handleUpdateViewCV);
	#endif
		m_pVideoFileDemoThread->start();

		QMetaObject::invokeMethod(m_pVideoDemo, "start", Qt::QueuedConnection, Q_ARG(int, CV_CAP_ANY));
		//QMetaObject::invokeMethod(m_pVideoDemo, "stop", Qt::QueuedConnection);
	}

    ret = true;

    return ret;
}
#endif
void MainWindow::InitRandomLine()
{
    //if (ui->imageView_center->scene())
    //{
    //    for(int i = 0; i < k_engineering_default_nums_of_lines; ++i)
    //    {
    //        QLineF line = m_imageScene.CreateAutoGeneratedLineThenDrawIt();
    //        addLineIntoDWLWidget(QPointF(line.x1(), line.y1()), QPointF(line.x2(), line.y2()));
    //    }
    //}
}

void MainWindow::InitRandomRect()
{
    //if (ui->imageView_center->scene())
    //{
    //    if (m_imageScene.getRectCount() <= 0)
    //    {
    //        for(int i = 0; i < k_engineering_default_nums_of_rects; ++i)
    //        {
    //            m_imageScene.CreateAutoGeneratedRect();//rect);
    //        }
    //    }
    //}
}

void MainWindow::InitImageSceneROILine()
{
    if (ui->imageView_center->scene())
    {
        int x, y;

		QRect rcImageView = ui->imageView_center->geometry();

        QRect rcROILeft1 = this->btnROILeft1->geometry();
        y = rcROILeft1.top() + k_offset_button;
        QLineF line1 = m_imageScene.CreateROILine(this->btnROILeft1, QPoint(0, y), QPoint(rcImageView.width(), y), k_border_size);

        QRect rcROILeft2 = this->btnROILeft2->geometry();
        y = rcROILeft2.top() + k_offset_button;
        QLineF line2 = m_imageScene.CreateROILine(this->btnROILeft2, QPoint(0, y), QPoint(rcImageView.width(), y), k_border_size);

		QLineF line3 = m_imageScene.CreateROILine2(this->btnROITop1, this->btnROIBottom1);
		QLineF line4 = m_imageScene.CreateROILine2(this->btnROITop2, this->btnROIBottom2);

		handleROILineChanged();
    }
}

void MainWindow::SendMsgToClient()
{
	quint32 cmdType = 1;
	quint32 serverIsReady = 1;

	QByteArray block;
	QDataStream out(&block, QIODevice::ReadWrite);
	out.setVersion(QDataStream::Qt_4_0);
	out << (quint32)'#';
	out << cmdType;
	out << serverIsReady;
	//out << m_pServer.at(qrand() % m_pServer->size());
	out.device()->seek(0);
	out << (quint32)block.size();// (block.size() - sizeof(quint32));
	qDebug("server try");

	QLocalSocket *clientConnection = m_pServer->nextPendingConnection();
	connect(clientConnection, SIGNAL(disconnected()),
		clientConnection, SLOT(deleteLater()));

	clientConnection->write(block);
	clientConnection->flush();
	clientConnection->disconnectFromServer();
	qDebug("server finish");
}
//
//void MainWindow::handleAllVideoFileSourceStarted()
//{
//    TRVideoFileSource *pSource = static_cast<TRVideoFileSource*>(sender());
//    if (m_pVideoFileSource == pSource)
//    {
//        //int n = m_currentSourceStartedTasks.fetchAndAddOrdered(1) + 1;
//        m_currentSourceStartedTasks.ref();
//        //++m_currentSourceStartedTasks;
//        //qDebug("MainWindow handleAllVideoFileSourceStarted m_currentSourceStartedTasks=%d",
//        //    m_currentSourceStartedTasks.load());// m_currentSourceStartedTasks);
//
//        if (m_totalSourceTasks == m_currentSourceStartedTasks.load())//m_currentSourceStartedTasks)
//        {
//            m_pImageProcessingColorPreviewThread->start();
//            //m_pImageProcessingColorCaptureThread->start();
//
//            qDebug("MainWindow handleAllVideoFileSourceStarted Emit");
//            emit sourceAllStarted();
//        }
//    }
//}
//
//void MainWindow::handleAllVideoFileSourceStopped()
//{
//    TRVideoFileSource *pSource = static_cast<TRVideoFileSource*>(sender());
//    if (m_pVideoFileSource == pSource)
//    {
//        qDebug("MainWindow handleAllVideoFileSourceStopped m_currentSourceStartedTasks=%d", m_currentSourceStartedTasks.load());
//        if (!m_currentSourceStartedTasks.deref()) {
//            // The last reference has been released
//            if (m_pImgProcessing)
//                QMetaObject::invokeMethod(m_pImgProcessing, "stop", Qt::QueuedConnection); //will remove in next version
//                //if (m_pImageProcessingColorCapture)
//                //    QMetaObject::invokeMethod(m_pImageProcessingColorCapture, "stop", Qt::QueuedConnection); //will remove in next version
//
//            qDebug("MainWindow handleAllVideoFileSourceStopped Emit");
//            emit sourceAllStopped();
//        }
//    }
//}
//
//void MainWindow::handleAllVideoCaptureDeviceStarted()
//{
//    AbstractCamera *pCamera = static_cast<AbstractCamera*>(sender());
//    if (m_pVideoCapture == pCamera)
//    {
//        //int n = m_currentSourceStartedTasks.fetchAndAddOrdered(1) + 1;
//        m_currentSourceStartedTasks.ref();
//        //++m_currentSourceStartedTasks;
//        qDebug("MainWindow handleAllVideoCaptureDeviceStarted m_currentSourceStartedTasks=%d",
//            m_currentSourceStartedTasks.load());// m_currentSourceStartedTasks);
//
//        if (m_totalSourceTasks == m_currentSourceStartedTasks.load())//m_currentSourceStartedTasks)
//        {
//            m_pImageProcessingColorPreviewThread->start();
//            //m_pImageProcessingColorCaptureThread->start();
//
//            qDebug("MainWindow handleAllVideoCaptureDeviceStarted Emit");
//            emit sourceAllStarted();
//        }
//    }
//}
//
//void MainWindow::handleAllVideoCaptureDeviceStopped()
//{
//    AbstractCamera *pCamera = static_cast<AbstractCamera*>(sender());
//    if (m_pVideoCapture == pCamera)
//    {
//        qDebug("MainWindow handleAllVideoCaptureDeviceStopped m_currentSourceStartedTasks=%d", m_currentSourceStartedTasks.load());
//        if (!m_currentSourceStartedTasks.deref()) {
//            // The last reference has been released
//            if (m_pImgProcessing)
//                QMetaObject::invokeMethod(m_pImgProcessing, "stop", Qt::QueuedConnection); //will remove in next version
//            //if (m_pImageProcessingColorCapture)
//            //    QMetaObject::invokeMethod(m_pImageProcessingColorCapture, "stop", Qt::QueuedConnection); //will remove in next version
//
//            qDebug("MainWindow handleAllVideoCaptureDeviceStopped Emit");
//            emit sourceAllStopped();
//        }
//    }
//}
//
void MainWindow::handleAllComponentsAreRunning()
{
    DSG(1, "MainWindow::handleAllComponentsAreRunning()");
    //m_currentSinkStartedTasks.ref();
    ////DSG(1, "MainWindow handleAllComponentsAreRunning m_currentSinkStartedTasks=%d", m_currentSinkStartedTasks.load());
    //if (m_totalSinkTasks == m_currentSinkStartedTasks.load())
    //{
        ui->actionControl_Start->setEnabled(false);
        ui->actionControl_Stop->setEnabled(true);

        if (m_pStatusBarTimer)
            m_pStatusBarTimer->start(1000);
        m_pStatusBarElapsedTimer.restart();

        //DSG(1, "BBB 1 m_actionState=%d", m_actionState);
        if (e_action_openimagefile_is_opened == m_actionState)
            m_actionState = e_action_openimagefile_is_running;
        else if (e_action_openvideoefile_is_opened == m_actionState)
            m_actionState = e_action_openvideofile_is_running;
        else if (e_action_opencamera_is_opened == m_actionState)
            m_actionState = e_action_opencamera_is_running;
        //DSG(1, "BBB 2 m_actionState=%d", m_actionState);
    //}
}	

void MainWindow::handleAllComponentsAreStopped()
{
    //qDebug("MainWindow handleAllComponentsAreStopped m_currentSinkStartedTasks=%d", 
    //    m_currentSinkStartedTasks.load());
    //if (!m_currentSinkStartedTasks.deref())
    //{
        if (m_pStatusBarTimer)
            m_pStatusBarTimer->stop();

        ui->actionControl_Start->setEnabled(true);
        ui->actionControl_Stop->setEnabled(false);

        if (e_action_openvideofile_is_running == m_actionState)
            m_actionState = e_action_openvideoefile_is_opened;
        else if (e_action_opencamera_is_running == m_actionState)
            m_actionState = e_action_opencamera_is_opened;
    //}
}

void MainWindow::handleStatusBarTimer()
{
    QString str;
    str.sprintf("%s:%llu", "elapsed time", (m_pStatusBarElapsedTimer.elapsed() / 1000));
    //this->ui->statusBar->showMessage(str);
}

void MainWindow::handleTRQtThreadFinish()
{
    //QMessageBox::information(NULL,tr("Message"),("Qt thread is completed"));
    qDebug("handleTRQtThreadFinish");
}

void MainWindow::handleTRVideoThreadFinish()
{
    qDebug("handleTRVideoThreadFinish");
}

void MainWindow::handleTRCameraThreadFinish()
{
    qDebug("handleTRCameraThreadFinish");
}

//void MainWindow::handleUpdateAutoGeneratedLineItem(QGraphicsLineItem *pItem, QPointF startOffset, QPointF endOffset)
//{
//    //m_imageScene.UpdateAutoGeneratedLine(pItem, startOffset, endOffset);
//}
//
//void MainWindow::handleUpdateAutoGeneratedRectItem(QGraphicsRectItem *pItem, QRectF rect)
//{
//    //m_imageScene.UpdateAutoGeneratedRect(pItem, rect);
//}

void MainWindow::handleCheckAdjustedROIEffect()
{
    emit redirectCheckAdjustedROIEffect();
}

//void MainWindow::handleUpdateLanelnfo(std::vector<cv::Vec4i> *pLanelineArray)
void MainWindow::handleUpdateLanelnfo(const std::vector<cv::Vec4i> &laneArray)
{
	//qDebug("MainWindow::handleUpdateLanelnfo");
	ui->trDWLPositionOfLinesWidget->clearContents();
	ui->trDWLPositionOfLinesWidget->setRowCount(0);
	ui->trDWLPositionOfLinesWidget->setRowCount(1);
	//qDebug("MainWindow::handleUpdateLanelnfo ui->trDWLPositionOfLinesWidget->rowCount()=%d", 
	//	ui->trDWLPositionOfLinesWidget->rowCount());
	m_dwlPosition->m_total = ui->trDWLPositionOfLinesWidget->rowCount();
	//m_dwlPosition->m_total = 1;
	m_dwlPosition->m_currentIdx = 0;

	std::vector<cv::Vec4i>::const_iterator itDetectedLine = laneArray.begin();
	//qDebug("itDetectedLine count=%d", pLanelineArray->size());
	int lineIdx = 0;
	while (itDetectedLine != laneArray.end()) {
		cv::Vec4i line = (*itDetectedLine);
		//qDebug("itDetectedLine %d %d %d %d", line[0], line[1], line[2], line[3]);
		addLineIntoDWLWidget(QPointF(line[0], line[1]), QPointF(line[2], line[3]));
		++itDetectedLine;
	}
	//qDebug("MainWindow::handleUpdateLanelnfo over");
}

void MainWindow::handleUpdateVehicleInfo(const std::vector<cv::Vec4i> &vehicleArray)
{
	//qDebug("MainWindow::handleUpdateLanelnfo");
	ui->trVehicleInfoWidget->clearContents();
	ui->trVehicleInfoWidget->setRowCount(0);
	ui->trVehicleInfoWidget->setRowCount(1);
	//qDebug("MainWindow::handleUpdateLanelnfo ui->trDWLPositionOfLinesWidget->rowCount()=%d", 
	//	ui->trDWLPositionOfLinesWidget->rowCount());
	m_dwlPositionVehicle->m_total = ui->trVehicleInfoWidget->rowCount();
	//m_dwlPosition->m_total = 1;
	m_dwlPositionVehicle->m_currentIdx = 0;

	std::vector<cv::Vec4i>::const_iterator it = vehicleArray.begin();
	//qDebug("handleUpdateVehicleInfo count=%d", pVehicleArray->size());
	int lineIdx = 0;
	while (it != vehicleArray.end()) {
		cv::Vec4i ltrb = (*it);
		//qDebug("car1:%d", rect.x);
		//qDebug("car2:%d", (*it).x);
		//qDebug("itDetectedLine %d %d %d %d", line[0], line[1], line[2], line[3]);
		AddVehicleInfoIntoWidget(ltrb);
		++it;
	}
	//qDebug("MainWindow::handleUpdateVehicleInfo over");
}

void MainWindow::handleROILineChanged()
{
    //if (!m_pImgProcessing) return;
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

	QRect rcImageView = ui->imageView_center->geometry();
	int boundWidth = rcImageView.width();
	int boundHeight = rcImageView.height();

    cv::Vec4i line1;
    cv::Vec4i line2;
    cv::Vec4i line3;
    cv::Vec4i line4;
	//cv::Point line1pt1, line1pt2;
	//cv::Point line2pt1, line2pt2;
	//cv::Point line3pt1, line3pt2;
	//cv::Point line4pt1, line4pt2;
	//cv::Point pt1, pt2, pt3, pt4, pt5, pt6;
	//QLineF &line1, QLineF &line2, QLineF &line3, QLineF &line4,
	QVector<DrawableGraphicsItem<QGraphicsLineItem, QPushButton>*> *pROILines;
	pROILines = m_imageScene.getROILines();
	for (size_t i = 0; i < pROILines->size(); ++i)
	{
		DrawableGraphicsItem<QGraphicsLineItem, QPushButton> *pROILine = pROILines->at(i);
		QGraphicsLineItem *pLine = static_cast<QGraphicsLineItem*>(pROILine->getItem());
		if (0 == i) {
            line1[0] = static_cast<int>(pLine->pos().x());
            line1[1] = static_cast<int>(pLine->pos().y());
            line1[2] = static_cast<int>(pLine->pos().x() + pLine->line().p2().x());
            line1[3] = static_cast<int>(pLine->pos().y() + pLine->line().p2().y());
			//line1pt1.x = static_cast<int>(pLine->pos().x());
			//line1pt1.y = static_cast<int>(pLine->pos().y());
			//line1pt2.x = static_cast<int>(pLine->pos().x() + pLine->line().p2().x());
			//line1pt2.y = static_cast<int>(pLine->pos().y() + pLine->line().p2().y());
		}
		else if (1 == i)
		{
            line2[0] = static_cast<int>(pLine->pos().x());
            line2[1] = static_cast<int>(pLine->pos().y());
            line2[2] = static_cast<int>(pLine->pos().x() + pLine->line().p2().x());
            line2[3] = static_cast<int>(pLine->pos().y() + pLine->line().p2().y());
			//line2pt1.x = static_cast<int>(pLine->pos().x());
			//line2pt1.y = static_cast<int>(pLine->pos().y());
			//line2pt2.x = static_cast<int>(pLine->pos().x() + pLine->line().p2().x());
			//line2pt2.y = static_cast<int>(pLine->pos().y() + pLine->line().p2().y());
		}
		else if (2 == i)
		{
            line3[0] = static_cast<int>(pLine->pos().x());
            line3[1] = static_cast<int>(pLine->pos().y());
            line3[2] = static_cast<int>(pLine->pos().x() + pLine->line().p2().x());
            line3[3] = static_cast<int>(pLine->pos().y() + pLine->line().p2().y());
			//line3pt1.x = static_cast<int>(pLine->pos().x());
			//line3pt1.y = static_cast<int>(pLine->pos().y());
			//line3pt2.x = static_cast<int>(pLine->pos().x() + pLine->line().p2().x());
			//line3pt2.y = static_cast<int>(pLine->pos().y() + pLine->line().p2().y());
		}
		else if (3 == i)
		{
            line4[0] = static_cast<int>(pLine->pos().x());
            line4[1] = static_cast<int>(pLine->pos().y());
            line4[2] = static_cast<int>(pLine->pos().x() + pLine->line().p2().x());
            line4[3] = static_cast<int>(pLine->pos().y() + pLine->line().p2().y());
			//line4pt1.x = static_cast<int>(pLine->pos().x());
			//line4pt1.y = static_cast<int>(pLine->pos().y());
			//line4pt2.x = static_cast<int>(pLine->pos().x() + pLine->line().p2().x());
			//line4pt2.y = static_cast<int>(pLine->pos().y() + pLine->line().p2().y());
		}
	}
    pCache->UpdateROILineCoordinate(line1, line2, line3, line4);

	//qDebug("line1pt1 %d %d", line1pt1.x, line1pt1.y);
	//qDebug("line1pt2 %d %d", line1pt2.x, line1pt2.y);
	//qDebug("line2pt1 %d %d", line2pt1.x, line2pt1.y);
	//qDebug("line2pt2 %d %d", line2pt2.x, line2pt2.y);
	//qDebug("line3pt1 %d %d", line3pt1.x, line3pt1.y);
	//qDebug("line3pt2 %d %d", line3pt2.x, line3pt2.y);
	//qDebug("line1pt1 %d %d", line4pt1.x, line4pt1.y);
	//qDebug("line4pt2 %d %d", line4pt2.x, line4pt2.y);

	//GetIntersectionPoint(line1pt1, line1pt2, line2pt1, line2pt2, pt1);
	//GetIntersectionPoint(line1pt1, line1pt2, line3pt1, line3pt2, pt2);
	//GetIntersectionPoint(line1pt1, line1pt2, line4pt1, line4pt2, pt3);
	//GetIntersectionPoint(line2pt1, line2pt2, line4pt1, line4pt2, pt4);
	//GetIntersectionPoint(line2pt1, line2pt2, line3pt1, line3pt2, pt5);
	//GetIntersectionPoint(line3pt1, line3pt2, line4pt1, line4pt2, pt6);

    //2018.1007
 //   std::vector<cv::Point> *pPoints = m_pImgProcessing->GetProcessingSetting()->getROIPoints();
	//std::vector<cv::Point> zeroPoints;
	//pPoints->clear();
	//pPoints->swap(zeroPoints);

	////qDebug("pt1 %d %d", pt1.x, pt1.y);
	////qDebug("pt2 %d %d", pt2.x, pt2.y);
	////qDebug("pt3 %d %d", pt3.x, pt3.y);
	////qDebug("pt4 %d %d", pt4.x, pt4.y);
	////qDebug("pt5 %d %d", pt5.x, pt5.y);
	////qDebug("pt6 %d %d", pt6.x, pt6.y);

	////if ((0 < pt1.x) && (0 < pt1.y) && (pt1.x <= boundWidth) && (pt1.y <= boundHeight))
	////	pPoints->push_back(pt1);
	//if ((0 < pt2.x) && (0 < pt2.y) && (pt2.x <= boundWidth) && (pt2.y <= boundHeight))
	//	pPoints->push_back(pt2);
	//if ((0 < pt3.x) && (0 < pt3.y) && (pt3.x <= boundWidth) && (pt3.y <= boundHeight))
	//	pPoints->push_back(pt3);
	//if ((0 < pt4.x) && (0 < pt4.y) && (pt4.x <= boundWidth) && (pt4.y <= boundHeight))
	//	pPoints->push_back(pt4);
	//if ((0 < pt5.x) && (0 < pt5.y) && (pt5.x <= boundWidth) && (pt5.y <= boundHeight))
	//	pPoints->push_back(pt5);
	////if ((0 < pt6.x) && (0 < pt6.y) && (pt6.x <= boundWidth) && (pt6.y <= boundHeight))
	////	pPoints->push_back(pt6);

	////qDebug("size=%d", pPoints->size());
}

void MainWindow::serverNewConnectionHandler()
{
	qDebug() << "Server serverNewConnectionHandler";
	QLocalSocket* pSocket = m_pServer->nextPendingConnection();
	QObject::connect(pSocket, SIGNAL(readyRead()), this, SLOT(socketReadyReadHandler()));
	QObject::connect(pSocket, SIGNAL(disconnected()), this, SLOT(deleteLater()));
	QObject::connect(pSocket, SIGNAL(disconnected()), pSocket, SLOT(deleteLater()));
}

void MainWindow::socketReadyReadHandler()
{
	QLocalSocket *pSocket = static_cast<QLocalSocket*>(sender());
	if (pSocket)
	{
		QDataStream in(pSocket);
		//QByteArray stream(pSocket);
		//QTextStream stream(pSocket);
		//qDebug() << "Read Data From Client:" << stream.readAll();
		while (pSocket->bytesAvailable() < 4) {
			pSocket->waitForReadyRead();
		}
		quint8 headmark;
		in >> headmark;
		quint8 cmdType;
		in >> cmdType;
		quint8 cmdValue;
		in >> cmdValue;
		quint8 cmdLength;
		in >> cmdLength;

		qDebug("server socketReadyReadHandler headmark=%u cmdType=%u cmdValue=%u cmdLength=%u",
			headmark, cmdType, cmdValue, cmdLength);

		QString response = "Hello Client, ";
		pSocket->write(response.toUtf8());
		pSocket->flush();

        if (cmdValue == 117)
        {
            //on_actionSources_OpenCamera_triggered();
            //OpenCamera(QString("rtsp://root:jolyone@61.216.153.174/axis-media/media.amp"));
            //on_actionControl_Start_triggered();
            if (e_action_idle == m_actionState)
            {
                QString absFileName = QFileDialog::getOpenFileName(this, "open video file",
                    ".", "Video files (*.mpg *.mpeg *.mp2 *.mp4 *.avi *.MOV;;All files (*.*)");
                if (!absFileName.isEmpty())
                {
                    qDebug("on_actionSources_OpenVideoFile_triggered 2");
                
                    if (OpenVideoFileAll(7, "D:\\Work\\projects\\TrafficEnforcementUbuntu\\bin\\video\\DSCF4449.MOV"))
                    {
                        QFile file(absFileName);
                        QFileInfo fileInfo(file.fileName());
                        QString filename(fileInfo.fileName());

                        QString str("TrafficEnforcement");
                        QWidget::setWindowTitle(str + tr("-") + filename);
                        qDebug("on_actionSources_OpenVideoFile_triggered 3");

                        ReflectMainWidget(true);

                        qDebug("on_actionSources_OpenVideoFile_triggered e_action_openvideoefile_is_opened");

                        m_actionState = e_action_openvideoefile_is_opened;
                    }
                }
            }
        }
	}
}

void MainWindow::clientDisconnectionHandler()
{
    on_actionControl_Stop_triggered();
    CloseCamera();
}

void MainWindow::handleUpdateImageList()
{
    ui->trVehicleImageWidget->clear();

    QDir dir("D:/storage");
    dir.setSorting(QDir::Name);
    QStringList list = dir.entryList(QDir::AllEntries | QDir::Dirs);

    for (auto tmp : list)
    {
        QListWidgetItem *imageItem = new QListWidgetItem;

        QString str = dir.absoluteFilePath(tmp);
        QFileInfo file(str);
        if (file.isFile() == false)
            continue;

        qDebug("str=%s", str.toLatin1().constData());

        imageItem->setIcon(QIcon(str));

        imageItem->setSizeHint(QSize(66, 20 + 66));

        ui->trVehicleImageWidget->addItem(imageItem);
    }

    //ui->trVehicleImageWidget->show();
}

void MainWindow::handleQuitTime()
{
    if (m_quitting) 
    {
        DSG(1, "handleQuitTime");
        if ((e_action_openimagefile_is_running != m_actionState) &&
            (e_action_openvideofile_is_running != m_actionState) &&
            (e_action_opencamera_is_running != m_actionState))
        {
            DSG(1, "handleQuitTime 2");
            m_quitting = false;
        }
    }
}

void MainWindow::on_actionSources_OpenImageFile_triggered()
{
    //QString absFileName = QFileDialog::getOpenFileName(
    //    this, "open image file",
    //    ".",
    //    "Image files (*.bmp *.jpg *.pbm *.pgm *.png *.ppm *.xbm *.xpm);;All files (*.*)");
    //if (!absFileName.isEmpty())
    //{
    //    if (OpenImageFile(absFileName))
    //    {
    //        QFile file(absFileName);
    //        QFileInfo fileInfo(file.fileName());
    //        QString filename(fileInfo.fileName());

    //        QString str("TrafficEnforcement");
    //        QWidget::setWindowTitle(str + tr("-") + filename);

    //        ReflectMainWidget(true);

    //        qDebug("on_actionSources_OpenImageFile_triggered e_action_openimagefile_is_opened");

    //        m_actionState = e_action_openimagefile_is_opened;
    //    }
    //}
}

void MainWindow::on_actionSources_OpenVideoFile_triggered()
{
    if (e_action_idle == m_actionState)
    {
        TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

        QString absFileName = QFileDialog::getOpenFileName(this, "open video file",
        ".", "Video files (*.mpg *.mpeg *.mp2 *.mp4 *.avi *.MOV;;All files (*.*)");
        if (!absFileName.isEmpty())
        {
            QFile file(absFileName);
            QFileInfo fileInfo(file.fileName());
            QString filename(fileInfo.fileName());

            SVideoConfig config;
            config.interval_ms = pCache->fps;// 200;// 33;// -1;// 33;
            config.want_width = ui->imageView_center->geometry().width();
            config.want_height = ui->imageView_center->geometry().height();
            strcpy_s(config.filename, absFileName.toLatin1().constData());

            if (m_pGraph)
            {
                delete m_pGraph;
                m_pGraph = nullptr;
            }
            m_pGraph = new Graph(this);
            if (!m_pGraph)
                return;
            
            if (m_pGraph->OpenVideoFile(&config))
            {
                QString str("TrafficEnforcement");
                QWidget::setWindowTitle(str + tr("-") + filename);

                ReflectMainWidget(true);

                qDebug("on_actionSources_OpenVideoFile_triggered e_action_openvideoefile_is_opened");

                m_actionState = e_action_openvideoefile_is_opened;

            }
        }
    }
}

void MainWindow::on_actionSources_OpenCamera_triggered()
{
    qDebug("on_actionSources_OpenCamera_triggered");
    if (e_action_idle == m_actionState)
    {
        SCameraConfig config;

        config.device_idx = -1;
        config.want_width = ui->imageView_center->geometry().width();
        config.want_height = ui->imageView_center->geometry().height();
        config.interval_ms = 33;// -1;// 33;
        strcpy_s(config.filename, "rtsp://root:jolyone@61.216.153.174/axis-media/media.amp");

        //qDebug("MainWindow::OpenCameraProbe config.device_idx=%d", config.device_idx);

        if (m_pGraph)
        {
            delete m_pGraph;
            m_pGraph = nullptr;
        }
        m_pGraph = new Graph(this);
        if (!m_pGraph)
            return;

        if (m_pGraph->OpenCamera(&config))
        {
            QString str("TrafficEnforcement");
            QWidget::setWindowTitle(str + tr("-") + tr("camera"));

            ReflectMainWidget(true);

            m_actionState = e_action_opencamera_is_opened;
        }
    }
}

void MainWindow::on_actionSources_Exit_triggered()
{
    close();
}

void MainWindow::on_actionControl_Start_triggered()
{
	qDebug("on_actionControl_Start_triggered m_actionState=%d", m_actionState);
    if (e_action_openvideoefile_is_opened == m_actionState)
    {
        m_actionState = e_action_openvideofile_is_running;

        ui->actionControl_Start->setEnabled(false);
        ui->actionControl_Stop->setEnabled(true);

        if (m_pGraph)
            m_pGraph->StartVideoFile();
        //m_pVideoFileSourceThread->start();
        //if (m_pVideoFileSource)
        //    QMetaObject::invokeMethod(m_pVideoFileSource, "start", Qt::QueuedConnection, Q_ARG(int, CV_CAP_ANY));
    }
    else if (e_action_opencamera_is_opened == m_actionState)
    {
        m_actionState = e_action_opencamera_is_running;

        ui->actionControl_Start->setEnabled(false);
        ui->actionControl_Stop->setEnabled(true);

        if (m_pGraph)
            m_pGraph->StartCamera();

        //m_pVideoCaptureThread->start();
        //if (m_pVideoCapture)
        //    QMetaObject::invokeMethod(m_pVideoCapture, "start", Qt::QueuedConnection, Q_ARG(int, CV_CAP_ANY));
    }
}

void MainWindow::on_actionControl_Stop_triggered()
{
    DSG(1, "on_actionControl_Stop_triggered::m_actionState 1 =%d", m_actionState);
	if (e_action_openvideofile_is_running == m_actionState)
	{
        if (m_pGraph)
            m_pGraph->StopVideoFile();
        //if (m_pVideoFileSource)
        //    QMetaObject::invokeMethod(m_pVideoFileSource, "stop", Qt::QueuedConnection);
        //Sleep(2000);
	}
    else if (e_action_opencamera_is_running == m_actionState)
    {
        if (m_pGraph)
            m_pGraph->StopCamera();
        //if (m_pVideoCapture)
        //    QMetaObject::invokeMethod(m_pVideoCapture, "stop", Qt::QueuedConnection);
        //Sleep(2000);
    }
    DSG(1, "on_actionControl_Stop_triggered::m_actionState 2 =%d", m_actionState);
    int count = 5;
    do
    {
        if (e_action_openvideofile_is_running == m_actionState)
        {
            QElapsedTimer t;
            t.start();
            while (t.elapsed() < 200)
                QCoreApplication::processEvents();
        }
        else if (e_action_opencamera_is_running == m_actionState)
        {
            QElapsedTimer t;
            t.start();
            while (t.elapsed() < 200)
                QCoreApplication::processEvents();
        }
        --count;

    } while (count > 0);
    DSG(1, "on_actionControl_Stop_triggered::m_actionState 3 =%d", m_actionState);
    if (e_action_openvideoefile_is_opened == m_actionState)
    {
        if (m_pGraph)
            m_pGraph->CloseVideoFile();

        m_actionState = e_action_idle;
    }
    else if (e_action_opencamera_is_opened == m_actionState)
    {
        if (m_pGraph)
            m_pGraph->CloseCamera();

        m_actionState = e_action_idle;
    }

    ui->actionControl_Start->setEnabled(true);
    ui->actionControl_Stop->setEnabled(false);
}

void MainWindow::on_actionControl_Snapshot_triggered()
{

}

void MainWindow::on_actionTools_EnableROI_triggered()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    pCache->roi_enabled = ui->actionTools_EnableROI->isChecked();

    if ((e_action_openimagefile_is_opened == m_actionState) ||
        (e_action_opencamera_is_opened == m_actionState))
    {
        if (m_pGraph)
            m_pGraph->Process();
    }
}

void MainWindow::on_actionTools_VideoStability_triggered()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    pCache->video_stability_enabled = ui->actionTools_VideoStability->isChecked();

	if ((e_action_openimagefile_is_opened == m_actionState) ||
		(e_action_opencamera_is_opened == m_actionState))
	{
        if (m_pGraph)
            m_pGraph->Process();
	}
}

void MainWindow::on_actionTools_Options_triggered()
{
	//OptionsDialog *dlg = new OptionsDialog(this);
	//dlg->exec();
	if (!m_pOptionDlg)
	{
		m_pOptionDlg = new OptionsDialog;
		m_pOptionDlg->setModal(false);
		m_pOptionDlg->show();
	}
}

void MainWindow::on_actionView_ROILine_triggered()
{
    if ((e_action_openimagefile_is_opened == m_actionState) ||
		(e_action_openvideoefile_is_opened == m_actionState) ||
        (e_action_opencamera_is_opened == m_actionState))
    {
        this->SwitchROIButton();
    }
}

void MainWindow::on_actionView_VideoAssetManagement_triggered()
{
	if (true == this->ui->actionView_VideoAssetManagement->isChecked())
	{
		if (!m_pVAMDlg)
		{
			m_pVAMDlg = new VideoAssetManagement;
			m_pVAMDlg->setModal(false);
			m_pVAMDlg->show();
		}
	}
	else
	{
		if (m_pVAMDlg)
		{
			delete m_pVAMDlg;
			m_pVAMDlg = nullptr;
		}
	}
}

void MainWindow::on_actionView_TrafficFlowAnalysis_triggered()
{
    if (true == this->ui->actionView_TrafficFlowAnalysis->isChecked())
    {
	    QString strExe("TrafficFlowAnalysis.exe");
	    if (!m_pProcessTFA)
	    {
		    m_pProcessTFA = new QProcess(this);
		    m_pProcessTFA->start(strExe);
	    }

	    if (!m_pServer)
	    {
		    m_pServer = new QLocalServer(this);
		    if (m_pServer)
		    { 
			    QObject::connect(m_pServer, SIGNAL(newConnection()), this, SLOT(serverNewConnectionHandler()));
			    m_pServer->removeServer("TrafficEnforcement");
			    if (!m_pServer->listen("TrafficEnforcement")) {
				    QMessageBox::critical(this, tr("TrafficEnforcement Server"),
					    tr("Unable to start the server: %1.")
					    .arg(m_pServer->errorString()));
				    delete m_pServer;
				    m_pServer = nullptr;
				    return;
			    }
			    else
				    qDebug("server listen successful");

			    //QObject::connect(m_pServer, SIGNAL(newConnection()), this, SLOT(sendMsgToClient()));
		    }
	    }
    }
    else
    {
        if (!m_pServer)
        {
            delete m_pServer;
            m_pServer = nullptr;
        }
        if (!m_pProcessTFA)
        {
            delete m_pProcessTFA;
            m_pProcessTFA = nullptr;
        }
    }
	//QProcess tasklist;
	//tasklist.start("tasklist",
	//	QStringList() << "/NH"
	//	<< "/FO" << "CSV"
	//	<< "/FI" << QString("IMAGENAME eq %1").arg(strExe));
	//tasklist.waitForFinished();
	//QString strOutput = tasklist.readAllStandardOutput();
	//if (!strOutput.startsWith(QString("\"%1").arg(strExe)))
	//{
	//	QJsonObject json;

	//	json.insert("UserName", QStringLiteral(""));
	//	json.insert("Password", "123456");

	//	QJsonDocument document;
	//	document.setObject(json);
	//	QByteArray byteArray = document.toJson(QJsonDocument::Compact);

	//	QStringList arguments;
	//	arguments << byteArray;
	//	m_pProcessTFA->startDetached(strExe, arguments);
	//}
}
#include "framelesswindow.h"
FramelessWindow* getMainWindow()
{
    foreach (QWidget *w, qApp->topLevelWidgets())
        if (FramelessWindow* mainWin = qobject_cast<FramelessWindow*>(w))
            return mainWin;
    return nullptr;
}

void MainWindow::on_actionView_FullScreen_triggered()
{
    if(m_bFullScreen)//windowState() == Qt::WindowFullScreen)
    {

        DSG(1, "org full");
        FramelessWindow *fw = getMainWindow();
        fw->Wrap_on_restoreButton_clicked();
        //setWindowState(Qt::WindowFullScreen | Qt::WindowMaximized);
    }
    else
    {
        DSG(1, "non full");
        //setWindowState(Qt::WindowMaximized);
        FramelessWindow *fw = getMainWindow();
        fw->Wrap_on_maximizeButton_clicked();
    }
    m_bFullScreen = !m_bFullScreen;
//    FramelessWindow *fw = (FramelessWindow*)(this->parent);
//    //bool isFullScreen = this->parent->windowState().testFlag(Qt::WindowFullScreen);
//    fw->showFullScreen();
//    //this->showFullScreen();
//    //QMainWindow::showFullScreen();
}

void MainWindow::on_actionEngineering_Enable_triggered()
{
	if ((e_action_openimagefile_is_opened == m_actionState) || (e_action_opencamera_is_opened == m_actionState))
	{
		//m_imageProcessing.GetProcessingSetting()->setEnableEngineeringMode(ui->actionEngineering_Enable->isChecked());
		//m_imageProcessing.handleShowTrackbarWindow();
	}
}

void MainWindow::on_actionEngineering_DWL_Randomly_Generate_Lines_triggered()
{
    if (false == ui->actionEngineering_DWL_Randomly_Generate_Lines->isChecked())
    {
        //ui->trDWLPositionOfLinesAddButton->setEnabled(true);
        //ui->trDWLPositionOfLinesDelButton->setEnabled(true);

        //if (Qt::Unchecked == ui->cbLaneDetection->checkState())
        //    ui->trDWLPositionOfLinesWidget->setEnabled(true);
    }
    else
    {
        //ui->trDWLPositionOfLinesWidget->setEnabled(false);
        //ui->trDWLPositionOfLinesAddButton->setEnabled(false);
        //ui->trDWLPositionOfLinesDelButton->setEnabled(false);

        //ui->trDWLPositionOfLinesWidget->clear();
        //ui->trDWLPositionOfLinesWidget->setRowCount(1);
        //DWLPosition *dwlPol = m_dwlPosition;
        //if (dwlPol)
        //{
       //     dwlPol->m_total = ui->trDWLPositionOfLinesWidget->rowCount();
        //}


    }
}

void MainWindow::on_actionEngineering_DWL_Car_Rect_RatInMaze_Start_triggered()
{
    //if ((e_action_openimagefile_is_opened == m_actionState) ||
    //    (e_action_opencamera_is_opened == m_actionState))
    //{
    //    if (m_pImgProcessing)
    //    {
    //        m_actionState = e_action_simulation_is_running;
    //        ui->actionEngineering_DWL_Car_Rect_RatInMaze_Start->setEnabled(false);
    //        ui->actionEngineering_DWL_Car_Rect_RatInMaze_Stop->setEnabled(true);

    //        if (nullptr == m_pTrCVThread)
    //        {
    //            m_pTrCVThread = new TRCVThread(this);

    //            connect(m_pTrCVThread, SIGNAL(updateCVRectItem()), m_pImgProcessing, SLOT(handleUpdateCVRectItem()));
    //            connect(m_pTrCVThread, SIGNAL(done()), this, SLOT(handleTRQtThreadFinish()));
    //        }

    //        if ((m_pTrCVThread) && (false == m_pTrCVThread->isRunning()))
    //        {
    //            SMovingCVObject movingObject;
    //            movingObject.interval_ms = 25;// 25;
    //            movingObject.car_agent = m_pImgProcessing->GetCarAgent();

    //            m_pTrCVThread->AddCVTask(&movingObject);
    //            m_pTrCVThread->start();
    //        }
    //    }
    //}
}

void MainWindow::on_actionEngineering_DWL_Car_Rect_RatInMaze_Stop_triggered()
{
    //if (e_action_simulation_is_running == m_actionState)
    //{
    //    m_actionState = e_action_idle;
    //    ui->actionEngineering_DWL_Car_Rect_RatInMaze_Start->setEnabled(true);
    //    ui->actionEngineering_DWL_Car_Rect_RatInMaze_Stop->setEnabled(false);

    //    qDebug("on_actionEngineering_DWL_Car_Rect_RatInMaze_Stop_triggered is stopped 1");
    //    if ((m_pTrCVThread) && (true == m_pTrCVThread->isRunning()))
    //    {
    //        qDebug("on_actionEngineering_DWL_Car_Rect_RatInMaze_Stop_triggered is stopped 2");
    //        m_pTrCVThread->Stop();
    //    }
    //}
}

void MainWindow::on_actionEngineering_ShowTrackbar_triggered()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    pCache->showtrackbar_enabled = ui->actionEngineering_ShowTrackbar->isChecked();

    if ((e_action_openimagefile_is_opened == m_actionState) || (e_action_opencamera_is_opened == m_actionState))
    {
        if (m_pGraph)
            m_pGraph->Process();
            //m_pImgProcessing->handleShowTrackbarWindow();
    }
}

void MainWindow::on_actionEngineering_Test_RGB_triggered()
{
    TestRGB test;
    test.Run();
}

void MainWindow::on_actionEngineering_Test_HSV_triggered()
{
    TestHSV test;
    test.Run();
}

void MainWindow::on_actionEngineering_StaticMode_Always_backup_info_of_sliding_windows_triggered()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    QSettings settings(pCache->settings_file_path, QSettings::IniFormat);
    settings.setIniCodec("UTF-8");

    settings.setValue(QString(k_str_em_sm_always_backup_info_of_slidingwindows),
        ui->actionEngineering_StaticMode_Always_backup_info_of_sliding_windows->isChecked());
}

void MainWindow::on_actionEngineering_StaticMode_Use_sliding_windows_info_file_triggered()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    QSettings settings(pCache->settings_file_path, QSettings::IniFormat);
    settings.setIniCodec("UTF-8");

    settings.setValue(QString(k_str_em_sm_use_slidingwindows_info_file),
                      ui->actionEngineering_StaticMode_Use_sliding_windows_info_file->isChecked());
}

void MainWindow::on_actionEngineering_Options_triggered()
{
    EMOptionsDialog *dlg = new EMOptionsDialog(this);
    dlg->exec();
}

void MainWindow::on_actionHelp_About_triggered()
{

}

void MainWindow::on_trApplyButton_released()
{
    qDebug("on_trApplyButton_released");
}

void MainWindow::on_trCancelButton_released()
{
    qDebug("on_trCancelButton_released");
}

void MainWindow::on_trDefaultButton_released()
{
    qDebug("on_trDefaultButton_released");
}

void MainWindow::on_trDWLPositionOfLinesAddButton_released()
{
    qDebug("on_trDWLPositionAddButton_released");
    //m_imageScene.UpdateAutoGeneratedLine(nullptr, QPointF(0,0), QPointF(0,0));
}

void MainWindow::on_trDWLPositionOfLinesDelButton_released()
{
    QModelIndexList selection = ui->trDWLPositionOfLinesWidget->selectionModel()->selectedRows();

    qDebug("selection.count = %d", selection.count());
    // Multiple rows can be selected
    for(int i = 0; i < selection.count(); i++)
    {
        QModelIndex index = selection.at(i);
        QTableWidgetItem *widgetItem1 = ui->trDWLPositionOfLinesWidget->item(index.row(), 0);
        QTableWidgetItem *widgetItem2 = ui->trDWLPositionOfLinesWidget->item(index.row(), 1);
		//widgetItem1 = nullptr;
		//widgetItem2 = nullptr;
        //qDebug("on_trDWLPositionDelButton_released widgetItem1=%s", widgetItem1->text().toLatin1().constData());
        //qDebug("on_trDWLPositionDelButton_released widgetItem2=%s", widgetItem2->text().toLatin1().constData());
        ui->trDWLPositionOfLinesWidget->removeRow(index.row());
        if (m_dwlPosition->m_total <= ui->trDWLPositionOfLinesWidget->rowCount())
            ui->trDWLPositionOfLinesWidget->setRowCount(ui->trDWLPositionOfLinesWidget->rowCount() + 1);
        m_dwlPosition->m_total = ui->trDWLPositionOfLinesWidget->rowCount();

        //m_imageScene.DeleteAutoGeneratedLine(index.row());
    }
}

void MainWindow::on_trDWLToleranceSlider_sliderMoved(int position)
{
    Q_UNUSED(position);
    //qDebug("on_trDWLToleranceSlider_sliderMoved position=%d", position);
    //ui->trDWLToleranceValue->setText(QString::number(ui->trDWLToleranceSlider->value()));
}

void MainWindow::on_trDWLToleranceSlider_valueChanged(int value)
{
    //Q_UNUSED(value);
    QString str = QString("%1").arg(value);
    ui->trDWLToleranceValue->setText(str);
}

void MainWindow::on_cbLaneDetection_stateChanged(int arg1)
{
    TrafficEnforcement::GlobalCacheData *pCache =
    TrafficEnforcement::GlobalCacheData::instance();

    pCache->lanedetection_enabled = ui->cbLaneDetection->isChecked();

    //if (m_pImgProcessing)
    //    m_pImgProcessing->GetProcessingSetting()->setEnableLaneDetection(ui->cbLaneDetection->isChecked());

    //if (Qt::Unchecked == arg1)
    //{
    //    //2018.1004
    //    ui->trDWLPositionOfLinesAddButton->setEnabled(true);
    //    ui->trDWLPositionOfLinesDelButton->setEnabled(true);
    //    ui->trDWLPositionOfLinesWidget->setEnabled(true);

    //    m_imageScene.SwitchAutoGeneratedLine(false);
    //}
    //else if (Qt::Checked == arg1)
    //{
    //    //2018.1004
    //    ui->trDWLPositionOfLinesAddButton->setEnabled(false);
    //    ui->trDWLPositionOfLinesDelButton->setEnabled(false);
    //    ui->trDWLPositionOfLinesWidget->setEnabled(false);

    //    m_imageScene.SwitchAutoGeneratedLine(true);
    //}
}

void MainWindow::on_cbVehicleDetection_stateChanged(int arg1)
{
    TrafficEnforcement::GlobalCacheData *pCache =
        TrafficEnforcement::GlobalCacheData::instance();

    pCache->vehicledetection_enabled = ui->cbVehicleDetection->isChecked();

    //if (m_pImgProcessing)
    //    m_pImgProcessing->GetProcessingSetting()->setEnableVehicleDetection(ui->cbVehicleDetection->isChecked());

    //if (Qt::Unchecked == arg1)
    //{
    //    //2018.1004
    //    ui->trDWLPositionOfLinesAddButton->setEnabled(true);
    //    ui->trDWLPositionOfLinesDelButton->setEnabled(true);
    //    ui->trDWLPositionOfLinesWidget->setEnabled(true);

    //    m_imageScene.SwitchAutoGeneratedLine(false);
    //}
    //else if (Qt::Checked == arg1)
    //{
    //    //2018.1004
    //    ui->trDWLPositionOfLinesAddButton->setEnabled(false);
    //    ui->trDWLPositionOfLinesDelButton->setEnabled(false);
    //    ui->trDWLPositionOfLinesWidget->setEnabled(false);

    //    m_imageScene.SwitchAutoGeneratedLine(true);
    //}
    //if (Qt::Unchecked == arg1)
    //{
    //    m_imageScene.SwitchAutoGeneratedRect(false);
    //}
    //else if (Qt::Checked == arg1)
    //{
    //    m_imageScene.SwitchAutoGeneratedRect(true);
    //}
}

void MainWindow::on_trDWLPositionOfLinesWidget_itemChanged(QTableWidgetItem *item)
{
    //if ((e_action_openimagefile_is_opened == m_actionState) || (e_action_opencamera_is_opened == m_actionState))
    //    m_imageScene.ChangeAutoGeneratedLine(item);
}

void MainWindow::on_trDWLPositionOfLinesWidget_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
    //qDebug("on_trDWLPositionOfLinesWidget_currentCellChanged");
}

void MainWindow::on_trDWLPositionOfLinesWidget_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous)
{
    //qDebug("on_trDWLPositionOfLinesWidget_currentItemChanged");


}

void MainWindow::ShowImageList()
{

    ////QPixmap pix1(QString(":/list/image/1.jpg"));
    ////QPixmap pix2(":/list/image/2.jpg");
    //QPixmap pix1(QString("D:/storage/1/car_0_pass.jpg"));
    //QPixmap pix2(QString("D:/storage/1/car_1_pass.jpg"));
    //QPixmap pix3(QString("D:/storage/1/car_2_pass.jpg"));
    //QPixmap pix4(QString("D:/storage/1/car_3_pass.jpg"));

    //QListWidgetItem *item1 = new QListWidgetItem(QIcon(pix1.scaled(QSize(256 / 2, 192 / 2))), "car 0 pass");
    //QListWidgetItem *item2 = new QListWidgetItem(QIcon(pix2.scaled(QSize(256 / 2, 192 / 2))), "car 1 pass");
    //QListWidgetItem *item3 = new QListWidgetItem(QIcon(pix1.scaled(QSize(256 / 2, 192 / 2))), "car 2 pass");
    //QListWidgetItem *item4 = new QListWidgetItem(QIcon(pix2.scaled(QSize(256 / 2, 192 / 2))), "car 3 pass");

    //ui->trVehicleImageWidget->addItem(item1);

    //item1->setSizeHint(QSize(256 / 2, 10 + 192 / 2));
    //item2->setSizeHint(QSize(256 / 2, 10 + 192 / 2));
    //item3->setSizeHint(QSize(256 / 2, 10 + 192 / 2));
    //item4->setSizeHint(QSize(256 / 2, 10 + 192 / 2));

    ////ui->trVehicleImageWidget->clear();
    //ui->trVehicleImageWidget->insertItem(1, item2);
    //ui->trVehicleImageWidget->insertItem(2, item3);
    //ui->trVehicleImageWidget->insertItem(3, item4);
    ////item2->setSizeHint(QSize(256 / 2, 10 + 192 / 2));
    //ui->trVehicleImageWidget->show();


    ui->trVehicleImageWidget->setViewMode(QListView::IconMode);

    ui->trVehicleImageWidget->setIconSize(QSize(66, 66));

    ui->trVehicleImageWidget->setSpacing(10);

    ui->trVehicleImageWidget->setResizeMode(QListWidget::Adjust);

    ui->trVehicleImageWidget->setMovement(QListWidget::Static);

    QDir dir("D:/storage/1");
    dir.setSorting(QDir::Name);
    QStringList list = dir.entryList(QDir::AllEntries | QDir::Dirs);

    for (auto tmp : list)
    {
        QListWidgetItem *imageItem = new QListWidgetItem;

        QString str = dir.absoluteFilePath(tmp);
        QFileInfo file(str);
        if (file.isFile() == false)
            continue;

        qDebug("str=%s", str.toLatin1().constData());

        imageItem->setIcon(QIcon(str));

        imageItem->setSizeHint(QSize(66, 20 + 66));

        ui->trVehicleImageWidget->addItem(imageItem);
    }

    ui->trVehicleImageWidget->show();
}


int g_numCars = 0;
int g_numLines = 0;

void MainWindow::ShowTreeWidget()
{
    //myTreeWidget = new QTreeWidget(this);
    QStringList headers;
    headers << "Title" << "Description";
    ui->treeWidget->setHeaderLabels(headers);
    ui->treeWidget->setWindowTitle(tr("QTreeWidget"));


    QTreeWidgetItem *root1 = new QTreeWidgetItem(ui->treeWidget, QStringList() << QString("Line") << "Line");
    QTreeWidgetItem *root2 = new QTreeWidgetItem(ui->treeWidget, QStringList() << QString("Vehicle") << "Vehicle");
    QTreeWidgetItem *leaf1 = new QTreeWidgetItem(root1, QStringList() << QString("count:") << QString::number(g_numLines, 10));
    QTreeWidgetItem *leaf2 = new QTreeWidgetItem(root2, QStringList() << QString("count:") << QString::number(g_numCars, 10));
    //leaf2->setCheckState(0, Qt::Checked);  

    QList<QTreeWidgetItem *> rootList;
    rootList << root1 << root2;
    ui->treeWidget->insertTopLevelItems(0, rootList);
    ui->treeWidget->resize(250, 200);
}
