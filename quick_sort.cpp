#include "quick_sort.h"
#include <QDebug>
#include <opencv2/opencv.hpp>


int PartitionVec4i(std::vector<cv::Vec4i>& L, int low, int high) {
    int lowX1;
    int lowY1;
    int lowX2;
    int lowY2;
    int highX1;
    int highY1;
    int highX2;
    int highY2;
    int pivotKeyX1;
    int pivotKeyX2;
    int pivotKeyY1;
    int pivotKeyY2;
    int pivotkey;
    int LHigh, LLow;

    pivotKeyX1 = L[low][0];
    pivotKeyY1 = L[low][1];
    pivotKeyX2 = L[low][2];
    pivotKeyY2 = L[low][3];
    if (157 == pivotKeyX1)
    {
        qDebug("157 low=%d high=%d L=%d", low, high, L.size());
    }
    if (pivotKeyY1 > pivotKeyY2)
        pivotkey = pivotKeyX1;
    else
        pivotkey = pivotKeyX2;

    //qDebug("pivotKey %d %d %d %d %d", pivotKeyX1, pivotKeyY1, pivotKeyX2, pivotKeyY2, pivotkey);
//qDebug("low=%d", low);
//qDebug("high=%d", high);
    while (low < high)
    {
        while (low < high) {
            highX1 = L[high][0];
            highY1 = L[high][1];
            highX2 = L[high][2];
            highY2 = L[high][3];

            if (highY1 > highY2)
                LHigh = highX1;
            else
                LHigh = highX2;
//qDebug("highX1 %d %d %d %d %d", highX1, highY1, highX2, highY2, LHigh);
            if (LHigh >= pivotkey)
                --high;
            else
                break;
        }

        L[low][0] = L[high][0];
        L[low][1] = L[high][1];
        L[low][2] = L[high][2];
        L[low][3] = L[high][3];

        while (low < high)
        {
            lowX1 = L[low][0];
            lowY1 = L[low][1];
            lowX2 = L[low][2];
            lowY2 = L[low][3];
            if (lowY1 > lowY2)
                LLow = lowX1;
            else
                LLow = lowX2;
//qDebug("highX1 %d %d %d %d %d", lowX1, lowY1, lowX2, lowY2, LLow);
            if (LLow <= pivotkey)
                ++low;
            else
                break;
        }

        L[high][0] = L[low][0];
        L[high][1] = L[low][1];
        L[high][2] = L[low][2];
        L[high][3] = L[low][3];
    }

    L[low][0] = pivotKeyX1;
    L[low][1] = pivotKeyY1;
    L[low][2] = pivotKeyX2;
    L[low][3] = pivotKeyY2;

    //qDebug("low=%d", low);
    return low;
}

void QSortVec4i(std::vector<cv::Vec4i>& L, int low, int high) {
    if (low < high) {
        int pivotloc = PartitionVec4i(L, low, high);
        QSortVec4i(L, low, pivotloc-1);
        QSortVec4i(L, pivotloc+1, high);
    }
}

void QuickSortVec4i(std::vector<cv::Vec4i>& L){

    QSortVec4i(L, 0, L.size() - 1);
}

int PartitionVec4f(std::vector<cv::Vec4f>& L, int low, int high) {
	float lowX1;
	float lowY1;
	float lowX2;
	float lowY2;
	float highX1;
	float highY1;
	float highX2;
	float highY2;
	float pivotKeyX1;
	float pivotKeyX2;
	float pivotKeyY1;
	float pivotKeyY2;
	float pivotkey;
	float LHigh, LLow;

	pivotKeyX1 = L[low][0];
	pivotKeyY1 = L[low][1];
	pivotKeyX2 = L[low][2];
	pivotKeyY2 = L[low][3];
	if (157 == pivotKeyX1)
	{
		qDebug("157 low=%d high=%d L=%d", low, high, L.size());
	}
	if (pivotKeyY1 > pivotKeyY2)
		pivotkey = pivotKeyX1;
	else
		pivotkey = pivotKeyX2;

	//qDebug("pivotKey %d %d %d %d %d", pivotKeyX1, pivotKeyY1, pivotKeyX2, pivotKeyY2, pivotkey);
	//qDebug("low=%d", low);
	//qDebug("high=%d", high);
	while (low < high)
	{
		while (low < high) {
			highX1 = L[high][0];
			highY1 = L[high][1];
			highX2 = L[high][2];
			highY2 = L[high][3];

			if (highY1 > highY2)
				LHigh = highX1;
			else
				LHigh = highX2;
			//qDebug("highX1 %d %d %d %d %d", highX1, highY1, highX2, highY2, LHigh);
			if (LHigh >= pivotkey)
				--high;
			else
				break;
		}

		L[low][0] = L[high][0];
		L[low][1] = L[high][1];
		L[low][2] = L[high][2];
		L[low][3] = L[high][3];

		while (low < high)
		{
			lowX1 = L[low][0];
			lowY1 = L[low][1];
			lowX2 = L[low][2];
			lowY2 = L[low][3];
			if (lowY1 > lowY2)
				LLow = lowX1;
			else
				LLow = lowX2;
			//qDebug("highX1 %d %d %d %d %d", lowX1, lowY1, lowX2, lowY2, LLow);
			if (LLow <= pivotkey)
				++low;
			else
				break;
		}

		L[high][0] = L[low][0];
		L[high][1] = L[low][1];
		L[high][2] = L[low][2];
		L[high][3] = L[low][3];
	}

	L[low][0] = pivotKeyX1;
	L[low][1] = pivotKeyY1;
	L[low][2] = pivotKeyX2;
	L[low][3] = pivotKeyY2;

	//qDebug("low=%d", low);
	return low;
}

void QSortVec4f(std::vector<cv::Vec4f>& L, int low, int high) {
	if (low < high) {
		int pivotloc = PartitionVec4f(L, low, high);
		QSortVec4f(L, low, pivotloc - 1);
		QSortVec4f(L, pivotloc + 1, high);
	}
}

void QuickSortVec4f(std::vector<cv::Vec4f>& L) {

	QSortVec4f(L, 0, L.size() - 1);
}
int Partition(std::vector<int>& L, int low, int high) {
    int pivotkey = L[low];
    while (low < high) {
        while (low<high && L[high]>=pivotkey)
            --high;
        L[low] = L[high];
        while (low<high && L[low]<=pivotkey)
            ++low;
        L[high] = L[low];
    }
    L[low] = pivotkey;

    return low;
}

void QSort(std::vector<int>& L, int low, int high) {
    if (low < high) {
        int pivotloc = Partition(L, low, high);
        QSort(L, low, pivotloc-1);
        QSort(L, pivotloc+1, high);
    }
}

void QuickSort(std::vector<int>& L){
    QSort(L, 0, L.size());
}

