#ifndef ABSTRACTSOURCE_H
#define ABSTRACTSOURCE_H

#include <QObject>
#include <opencv2/opencv.hpp>
#include "traffic_enforcement.h"
#include "filter.h"


class AbstractSource : public Filter//public QObject
{
    Q_OBJECT
public:
    explicit AbstractSource(BaseGraph *parent = {});
    ~AbstractSource();

    virtual bool SetConfig(void *pConfig) = 0;
    virtual bool Open() = 0;
    virtual void Close() = 0;

signals:
    void started();
    void stopped();
    void cvColorReady(const cv::Mat &matImage);
    void qColorReady(const QImage &qImage);

public slots:
    virtual void start(int cam = {}) = 0;
    virtual void stop() = 0;
    virtual void handleProcessedFrameCV(const cv::Mat &frame) = 0;
//protected:
//    virtual bool Start() = 0;
//    virtual void Stop() = 0;

protected:
    EComponentState m_state;
    int m_timerId;
};

#endif // ABSTRACTSOURCE_H
