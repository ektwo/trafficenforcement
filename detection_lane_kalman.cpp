#include <opencv2/opencv.hpp>

using namespace cv;

void CreateLaneKalmanFilter(Mat &imgROIIPM)
{
#ifdef LANE_USE_KALMAN
    if (lkf == NULL) {
        lkf = new LaneKalmanFilter(imgROIIPM.size());
        onKFChange(0, NULL);
        lkf->setStateLaneL(KFStateL);
        lkf->setStateLaneR(KFStateR);
    }

    lkf->next(); //new iteration
#endif
}

void detection_lane_kalman(Mat &imgROIIPM, Mat &matrixIPMInv)
{
#ifdef LANE_USE_KALMAN
    /// kalman lane line detection
    lkf->predict();

    /// filtered curving line on IPM 32
    vector<Point> lLane, rLane, lSLane, rSLane;

    lLane = lkf->getPredictL();
    rLane = lkf->getPredictR();
    line(imgROIIPM, lLane[0], lLane[1], CV_RGB(128, 64, 255));
    line(imgROIIPM, rLane[0], rLane[1], CV_RGB(128, 64, 255));

    lSLane = lkf->getStateL();
    rSLane = lkf->getStateR();
    line(imgROIIPM, lSLane[0], lSLane[1], CV_RGB(0, 0, 255));
    line(imgROIIPM, rSLane[0], rSLane[1], CV_RGB(0, 0, 255));


    /// filered lane line and status line on imgOrigin
    vector<Point2f> ps;
    vector<Point2f> psOut;

    ps.push_back(lLane[0]);
    ps.push_back(lLane[1]);
    ps.push_back(rLane[0]);
    ps.push_back(rLane[1]);
    ps.push_back(lSLane[0]);
    ps.push_back(lSLane[1]);
    ps.push_back(rSLane[0]);
    ps.push_back(rSLane[1]);

    perspectiveTransform(ps, psOut, matrixIPMInv);

    for (size_t i = 0; i < psOut.size(); i++) {
        psOut.at(i).x += roiLane.x;
        psOut.at(i).y += roiLane.y;
    }

    /// filtered lane line
    line(imgOrigin, psOut[0], psOut[1], CV_RGB(128, 128, 255), 4, CV_AA);
    line(imgOrigin, psOut[2], psOut[3], CV_RGB(128, 128, 255), 4, CV_AA);
    /// target car line
    line(imgOrigin, psOut[4], psOut[5], CV_RGB(0, 0, 255), 1, CV_AA);
    line(imgOrigin, psOut[6], psOut[7], CV_RGB(0, 0, 255), 1, CV_AA);
#endif
}
