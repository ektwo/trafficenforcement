#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "detection_header.h"

//parameters for vehicle detection
#if 0
using namespace cv;

int mmppx = 100;    /// lengh of pixel in IPM
int carOriginX = 380;    /// front car's coordination X in imgROIIPM
int carOriginY = 200;    /// front car's coordination y in imgROIIPM
int carROIX = 0;
int carROIY = 350;
int carROIWidth = 1300;
int carROIHeight = 400;
Rect roiCarDetect;

void createTrackbar_Car(TrackbarCallback callback)
{
    createTrackbar("mm/px", winConfig, &mmppx, 1000, callback);
    createTrackbar("car X", winConfig, &carOriginX, 1000, callback);
    createTrackbar("car Y", winConfig, &carOriginY, 1000, callback);
    createTrackbar("car ROI X", winConfig, &carROIX, 1000, callback);
    createTrackbar("car ROI Y", winConfig, &carROIY, 1000, callback);
    createTrackbar("car ROI Width", winConfig, &carROIWidth, 2000, callback);
    createTrackbar("car ROI Height", winConfig, &carROIHeight, 2000, callback);
}

void onROIChange_Car()
{
    /// ROI of Car detection
    roiCarDetect.x = carROIX;
    roiCarDetect.y = carROIY;
    roiCarDetect.width = carROIWidth;
    roiCarDetect.height = carROIHeight;
}

#if 0


/**
 * arguments : Gray level of origin pciture and ROI
 */
void detectCar(Mat *imgInput, Rect _roi) {
    vector<Rect> cars;
    vector<Point2f> pIn, pOut;
    char cTxt[1024] = {0};
    static CascadeClassifier _cascade;
    static CascadeClassifier* cascade = NULL;
    int ret;

    if (cascade == NULL) {
        cascade = &_cascade;
        ret = cascade->load(CAR_CASCADE);
        if (!ret) {
            fprintf(stderr, "Could not load cascade file: `%s'\n", CAR_CASCADE);
            exit(-1);
        }
    }

    if (_roi.x < 0 || _roi.x >= imgInput->cols) {
        _roi.x = 0;
    }
    if (_roi.y < 0 || _roi.y >= imgInput->rows) {
        _roi.y = 0;
    }
    if (_roi.x + _roi.width >= imgInput->cols) {
        _roi.width = imgInput->cols - 1 - _roi.x;
    }
    if (_roi.y + _roi.height >= imgInput->rows) {
        _roi.height = imgInput->rows - 1 - _roi.y;
    }

    if (carOriginX > imgOrigin.cols) {
        carOriginX = imgOrigin.cols - 1;
    }
    if (carOriginY > imgOrigin.rows) {
        carOriginY = imgOrigin.rows - 1;
    }

    Mat iROI(*imgInput, _roi);
    Mat iGray;

    cvtColor(iROI, iGray, COLOR_BGR2GRAY);


    //Draw vehicle detection area
    rectangle(imgOrigin, Point(_roi.x, _roi.y), Point(_roi.x + _roi.width, _roi.y + _roi.height), CV_RGB(0, 255, 255), 1);

    //Draw imgROIIPM location. (use red point on imgROIIPM)
    circle(imgROIIPM, Point(carOriginX, carOriginY), 6, CV_RGB(255, 0, 0), 3, CV_AA);

    //to detect vehicle
    cascade->detectMultiScale(iGray, cars, 1.1, 1, 0 | CASCADE_SCALE_IMAGE, Size(30, 30) );


    for( size_t i = 0; i < cars.size(); i++ )
    {
        //draw vehicle indicating rect
        Point2f p1, p2, pc, pcT, pcR, pTxt;
        p1.x = cars[i].x + _roi.x;
        p1.y = cars[i].y + _roi.y;
        p2.x = p1.x + cars[i].width;
        p2.y = p1.y + cars[i].height;
        rectangle(imgOrigin, p1, p2, CV_RGB(255, 200, 255), 2);

        pTxt.x = (p1.x + p2.x) / 2;
        pTxt.y = (p1.y + p2.y) / 2;

        //calculate the distance between cars. convert it to IPM32 coordination then calculate it.
        //pc retreieve from middle point of bottom line

        pc.x = (p1.x + p2.x) / 2;
        pc.y = MAX(p1.y, p2.y);

        pcR.x = pc.x - roiLane.x;
        pcR.y = pc.y - roiLane.y;

        pIn.clear();
        pIn.push_back(pcR);
        perspectiveTransform(pIn, pOut, matrixIPM);
        pcT = pOut[0];



        float dispx = sqrt((pcT.x - carOriginX) * (pcT.x - carOriginX) + (pcT.y - carOriginY) * (pcT.y - carOriginY));
        float dis = dispx * mmppx / 1000;


        fprintf(stderr, "Car #%lu, O(%.1f, %.1f), R(%.1f, %.1f), I(%.1f, %.1f), (I)dis: %0.2fpx, %0.2fM\n", i, pc.x, pc.y, pcR.x, pcR.y, pcT.x, pcT.y, dispx, dis);

        circle(imgROIIPM, pcT, 4, CV_RGB(0, 0, 255), 2);
        circle(imgOrigin, pc, 4, CV_RGB(0, 0, 255), 2);

        /// If exceed the ROI region. display is not allowed.
        if (!(pc.x >= roiLane.x && pc.x <= roiLane.x + roiLane.width)) {
            continue;
        }
        if (!(pc.y >= roiLane.y && pc.y <= roiLane.y + roiLane.height)) {
            continue;
        }


        snprintf(cTxt, sizeof(cTxt) - 1, "%0.1fM", dis);
        putText(imgOrigin, String(cTxt), pTxt, FONT_HERSHEY_SIMPLEX, 0.7, CV_RGB(0, 0, 255));
        snprintf(cTxt, sizeof(cTxt) - 1, "%0.1f", cars[i].width * 1.0 / tan(cars[i].y));
        putText(imgOrigin, String(cTxt), p1, FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(255, 128, 0));


    }
}


#endif
#endif
