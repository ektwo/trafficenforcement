#ifndef PARAMETERS_TABLE_H
#define PARAMETERS_TABLE_H


#include <opencv2/opencv.hpp>
#include "traffic_enforcement.h"
#include "escort_graphicsscene.h"
#include "drawable_graphicsscene.h"
#include "cqtopencvviewergl/cqtopencvviewergl.h"

//H (hue) for the line colors
//L (lightness) channel threshold eliminates edges generated from shadows in the frame.
//S (saturation) channel threshold enhances white & yellow lanes.

//0: Binary
//1: Binary Inverted
//2: Threshold Truncated
//3: Threshold to Zero
//4: Threshold to Zero Inverted
#define USE_CV_32FC3
#ifdef USE_CV_32FC3
static const int k_hls_h_max = 360;
static const int k_hls_l_max = 255;
static const int k_hls_s_max = 255;
#else
static const int k_hls_h_max = 180;
static const int k_hls_l_max = 255;
static const int k_hls_s_max = 255;
#endif

#ifdef USE_CV_32FC3
static const int k_hsv_h_max = 360;
static const int k_hsv_s_max = 255;
static const int k_hsv_v_max = 255;
#else
static const int k_hsv_h_max = 360 / 2;
static const int k_hsv_s_max = 255;
static const int k_hsv_v_max = 255;
#endif


static const int k_sobel_dxymode_max = 3;
static const int k_sobel_ksize_x_max = 3;
static const int k_sobel_ksize_y_max = 3;
static const int k_sobel_threshold_x_max = 255;
static const int k_sobel_threshold_y_max = 255;
static const int k_sobel_threshold_type_max = 3;

static const cv::String k_str_hls_h("H");
static const cv::String k_str_hls_l("L");
static const cv::String k_str_hls_s("S");

static const cv::String k_str_hsv_h("H");
static const cv::String k_str_hsv_s("S");
static const cv::String k_str_hsv_v("V");


static const cv::String k_str_sobel_dxymode("dxyMode");
static const cv::String k_str_sobel_ksize_x("ksize X");
static const cv::String k_str_sobel_ksize_y("ksize Y");
static const cv::String k_str_sobel_threshold_x_min("X min");
static const cv::String k_str_sobel_threshold_x_max("X max");
static const cv::String k_str_sobel_threshold_y_min("Y min");
static const cv::String k_str_sobel_threshold_y_max("Y max");
static const cv::String k_str_sobel_threshold_type("type");

typedef struct S_Parameters_Table_Tag {
	const char *file_name;
	ELaneDetectionMaskMixMode mix_mode;
	struct HLSParameters {
		int threshold_value_h;
		int threshold_value_l;
		int threshold_value_s;
		int threshold_value_h_max;
		int threshold_value_l_max;
		int threshold_value_s_max;
        EscortGraphicsScene *scene;
        //CQtOpenCVViewerGl *scene;
        //QOpenGLWidget *scene2;
	};
	HLSParameters hls;

	struct HSVParameter {
		int threshold_value_h;
		int threshold_value_s;
		int threshold_value_v;
		int threshold_value_h_max;
		int threshold_value_s_max;
		int threshold_value_v_max;
        EscortGraphicsScene *scene;
	};
	HSVParameter hsv;

	struct SSobelParameter {
		int dxymode;
		int ksize_x;
		int ksize_y;
		int threshold_x_min;
		int threshold_x_max;
		int threshold_y_min;
		int threshold_y_max;
		int threshold_type;
        EscortGraphicsScene *scene;
	};
	SSobelParameter sobel;

	struct CannyParameter {
		int threshold_1;
		int threshold_2;
		int aperture_sizediv2;
		int is_gradient;
        EscortGraphicsScene *scene;
	};
	CannyParameter canny;

	struct HoughParameter {
		int rho;
		int theta;
		int line_threshold;
		int minline_length;
		int maxline_gap;
	};
	HoughParameter hough;

    struct SLaneLineParmeter
    {
        int x1;
        int y1;
        int x2;
        int y2;
    };

    cv::Vec4i lane_left;
    cv::Vec4i lane_right;
    //SLaneLineParmeter lane_left;
    //SLaneLineParmeter lane_right;

	struct SCarClassifier {
		int scale_factor;
		int min_heighbors;
		int flags;
		int min_size;
		int max_size;
	};
	SCarClassifier car_classifier;

    DrawableGraphicsScene *display_scene;

} SParametersTable;

typedef enum E_Parameters_Table_Tag {
	e_parameters_table_
} EParametersTable;



#endif // PARAMETERS_TABLE_H
