#include "emoptionsdialog.h"
#include <QSettings>
#include <QFileDialog>
#include "ui_emoptionsdialog.h"
#include "traffic_enforcement.h"


EMOptionsDialog::EMOptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EMOptionsDialog)
{
    ui->setupUi(this);

    m_settingsFileName = QApplication::applicationDirPath() + "/TrafficEnforcement.ini";

    LoadSettings();
}

EMOptionsDialog::~EMOptionsDialog()
{
    delete ui;
}

void EMOptionsDialog::closeEvent(QCloseEvent *bar)
{
    //qDebug("EMOptionsDialog::closeEvnet");


}

void EMOptionsDialog::LoadSettings()
{
    QSettings settings(m_settingsFileName, QSettings::IniFormat);

    settings.setIniCodec("UTF-8");

    settings.beginGroup(QString(k_str_engineeringmode));

    ui->actionEMOptions_Check_UseBaseImageFile->setChecked(
        settings.value(QString(k_str_emdialog_sm_baseimagefile), false).toBool());

    ui->actionEMOptions_Check_UseSWInfoFile->setChecked(
        settings.value(QString(k_str_emdialog_sm_slidingwindowsfile), false).toBool());

    m_baseImageFileName = settings.value(QString(k_str_emdialog_sm_baseimagefilepath), "C:\\").toString();
    ui->actionEMOptions_Edit_BaseImageFile->setText(m_baseImageFileName);

    m_SWInfoFileName = settings.value(QString(k_str_emdialog_sm_slidingwindowsfilepath), "C:\\").toString();
    ui->actionEMOptions_Edit_SWInfoFile->setText(m_SWInfoFileName);

	ui->actionEMOptions_CB_LiveLaneDetectionMode->insertItem(0, QString("manual"));
	ui->actionEMOptions_CB_LiveLaneDetectionMode->insertItem(1, QString("auto:mask_hls_and_hsv_or_sobel"));
	ui->actionEMOptions_CB_LiveLaneDetectionMode->insertItem(2, QString("auto:mask_hls_or_hsv_or_sobel"));
	ui->actionEMOptions_CB_LiveLaneDetectionMode->insertItem(3, QString("auto:mask_gray_equalizehist_only"));

	//ui->actionEMOptions_CB_LiveLaneDetectionMode->addItem(QString("manual"));
	//ui->actionEMOptions_CB_LiveLaneDetectionMode->addItem(QString("auto:mask_hls_and_hsv_or_sobel"));
	//ui->actionEMOptions_CB_LiveLaneDetectionMode->addItem(QString("auto:mask_hls_or_hsv_or_sobel"));
	//ui->actionEMOptions_CB_LiveLaneDetectionMode->addItem(QString("auto:mask_gray_equalizehist_only"));

	ui->actionEMOptions_CB_LiveLaneDetectionMode->setCurrentIndex(
		settings.value(QString(k_str_emdialog_lm_lld_mode), 0).toInt());

    settings.endGroup();
}

void EMOptionsDialog::SaveSettings()
{
    QSettings settings(m_settingsFileName, QSettings::IniFormat);

    settings.setIniCodec("UTF-8");

    settings.beginGroup(QString(k_str_engineeringmode));

    settings.setValue(QString(k_str_emdialog_sm_baseimagefile),
                      ui->actionEMOptions_Check_UseBaseImageFile->isChecked());
    settings.setValue(QString(k_str_emdialog_sm_slidingwindowsfile),
                      ui->actionEMOptions_Check_UseSWInfoFile->isChecked());

    settings.setValue(QString(k_str_emdialog_sm_baseimagefilepath),
                      ui->actionEMOptions_Edit_BaseImageFile->text());
    settings.setValue(QString(k_str_emdialog_sm_slidingwindowsfilepath),
                      ui->actionEMOptions_Edit_SWInfoFile->text());

	settings.setValue(QString(k_str_emdialog_lm_lld_mode),
		ui->actionEMOptions_CB_LiveLaneDetectionMode->currentIndex());

    settings.endGroup();
}

void EMOptionsDialog::on_actionEMOptions_Btn_BaseImageFileBrowser_clicked()
{
    qDebug("on_actionEMOptions_Btn_BaseFileBrowser_clicked");

    m_baseImageFileName = QFileDialog::getOpenFileName(
        this, "open image file",
        ".",
        "Image files (*.bmp *.jpg *.pbm *.pgm *.png *.ppm *.xbm *.xpm);;All files (*.*)");

    ui->actionEMOptions_Edit_BaseImageFile->setText(m_baseImageFileName);
}

void EMOptionsDialog::on_actionEMOptions_Btn_SWInfoFileBrowser_clicked()
{
    qDebug("on_actionEMOptions_Btn_SWInfoFileBrowser_clicked");

    m_SWInfoFileName = QFileDialog::getOpenFileName(
        this, "open binary file",
        ".",
        "Binary files (*.bin);;All files (*.*)");

    ui->actionEMOptions_Edit_SWInfoFile->setText(m_SWInfoFileName);
}

void EMOptionsDialog::on_actionEMOptions_Check_UseBaseImageFile_clicked()
{

}

void EMOptionsDialog::on_actionEMOptions_Check_UseSWInfoFile_clicked()
{

}

void EMOptionsDialog::on_actionEMOptions_CB_LiveLaneDetectionMode_currentIndexChanged(int index)
{

}

void EMOptionsDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    if ((QPushButton *)button == ui->buttonBox->button(QDialogButtonBox::Ok))
    {
        SaveSettings();
        this->close();
    }
    else if ((QPushButton *)button == ui->buttonBox->button((QDialogButtonBox::Cancel)))
    {
        this->close();
    }
}
