#ifndef EMOPTIONSDIALOG_H
#define EMOPTIONSDIALOG_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class EMOptionsDialog;
}

class EMOptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EMOptionsDialog(QWidget *parent = 0);
    ~EMOptionsDialog();

private:
    void closeEvent(QCloseEvent *bar);

    void LoadSettings();
    void SaveSettings();


private slots:
    void on_actionEMOptions_Btn_BaseImageFileBrowser_clicked();
    void on_actionEMOptions_Btn_SWInfoFileBrowser_clicked();
    void on_actionEMOptions_Check_UseBaseImageFile_clicked();
    void on_actionEMOptions_Check_UseSWInfoFile_clicked();
	void on_actionEMOptions_CB_LiveLaneDetectionMode_currentIndexChanged(int index);
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    QString m_settingsFileName;
    QString m_baseImageFileName;
    QString m_SWInfoFileName;
    Ui::EMOptionsDialog *ui;
};

#endif // EMOPTIONSDIALOG_H
