#include "autogenerator_rect.h"
//#include <windows.h>
//#include <Synchapi.h>
#include "traffic_enforcement.h"

static const int k_move_step = 2;


AutoGeneratorRect::AutoGeneratorRect()
    : m_rect(QRectF(-1, -1, -1, -1))
    , m_weightStrategyOfHorizontal(e_weight_more_lower)
    , m_weightStrategyOfVertical(e_weight_more_lower)
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    , m_generator(QRandomGenerator::global())
#endif
{
}

void AutoGeneratorRect::SetBoundary(QRectF rect)
{
    m_rect = rect;
}

static
void AdjustBoundary(int &strategy, qreal &left, qreal &right, qreal border, qreal minSize, qreal maxDistance)
{
    if (left < right)
    {
        if (right > (maxDistance - k_border_size))
        {
            left -= (right - (maxDistance - k_border_size));//maxDistance - minSize;
            right = (maxDistance - k_border_size);
            strategy = AutoGeneratorRect::e_weight_more_lower;
        }
        else if (right < (border + minSize))
        {
            left = border;
            right = border + minSize;
            strategy = AutoGeneratorRect::e_weight_more_higher;
        }
        else
        {
            if (left < border)
            {
                right += border - left;
                left = border;
                strategy = AutoGeneratorRect::e_weight_more_higher;
            }
            if ((right - left) < minSize)
                right = left + minSize;
        }
    }
    else if (right < left)
    {
        if (left > (maxDistance - k_border_size))
        {
            right -= (left - (maxDistance - k_border_size));//maxDistance - minSize;
            left = (maxDistance - k_border_size);
            strategy = AutoGeneratorRect::e_weight_more_lower;
        }
        else if (left < (border + minSize))
        {
            right = border;
            left = border + minSize;
            strategy = AutoGeneratorRect::e_weight_more_higher;
        }
        else
        {
            if (right < border)
            {
                left += border - right;
                right = border;
                strategy = AutoGeneratorRect::e_weight_more_higher;
            }
            if ((left - right) < minSize)
                left = right + minSize;
        }
    }
    else
    {
        if (right > (maxDistance - minSize))
        {
            left -= (right - (maxDistance - minSize));
            //left = right - minSize;
            right = (maxDistance - minSize);
            strategy = AutoGeneratorRect::e_weight_more_lower;
        }
        else if (left < border)
        {
            left = border;
            right += (border - left);//border + minSize;
            strategy = AutoGeneratorRect::e_weight_more_higher;
        }
        else
        {

        }
    }
}

QRectF AutoGeneratorRect::GenQtRect()
{
    if ((m_rect.width() <= 0) || (m_rect.height() <= 0))
        return m_rect;

#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    int offsetLeft = m_generator->bounded(static_cast<int>(m_rect.left() + k_border_size), static_cast<int>(m_rect.right() - k_border_size + 1));
    int offsetRight = m_generator->bounded(static_cast<int>(m_rect.left() + k_border_size), static_cast<int>(m_rect.right() - k_border_size + 1));
    int offsetTop = m_generator->bounded(static_cast<int>(m_rect.top()+ k_border_size), static_cast<int>(m_rect.bottom() - k_border_size + 1));
    int offsetBottom = m_generator->bounded(static_cast<int>(m_rect.top()+ k_border_size), static_cast<int>(m_rect.bottom() - k_border_size + 1));
#else    
    int offsetLeft = GetBounded(static_cast<int>(m_rect.left() + k_border_size), static_cast<int>(m_rect.right() - k_border_size));
    int offsetRight = GetBounded(static_cast<int>(m_rect.left() + k_border_size), static_cast<int>(m_rect.right() - k_border_size ));
    int offsetTop = GetBounded(static_cast<int>(m_rect.top()+ k_border_size), static_cast<int>(m_rect.bottom() - k_border_size));
    int offsetBottom = GetBounded(static_cast<int>(m_rect.top()+ k_border_size), static_cast<int>(m_rect.bottom() - k_border_size));
#endif

    quint32 maxWidth = static_cast<quint32>(m_rect.right() - m_rect.left()) - (k_border_size << 1);
    quint32 maxHeight = static_cast<quint32>(m_rect.bottom() - m_rect.top()) - (k_border_size << 1);

    qreal left = static_cast<qreal>(offsetLeft);
    qreal top = static_cast<qreal>(offsetTop);
    qreal right = static_cast<qreal>(offsetRight);
    qreal bottom = static_cast<qreal>(offsetBottom);

    qreal realLeft, realRight, realTop, realBottom;
    if (left > right)
    {
        realRight = left;
        realLeft = right;
    }
    else
    {
        realLeft = left;
        realRight = right;
    }

    if (top > bottom)
    {
        realBottom = top;
        realTop = bottom;
    }
    else
    {
        realTop = top;
        realBottom = bottom;
    }

    AdjustBoundary(m_weightStrategyOfHorizontal,realLeft, realRight, k_border_size, k_min_width_of_vehicle, maxWidth);
    AdjustBoundary(m_weightStrategyOfVertical, realTop, realBottom, k_border_size, k_min_height_of_vehicle, maxHeight);

    return QRectF(realLeft, realTop, realRight - realLeft, realBottom - realTop);
}

QRectF AutoGeneratorRect::TuneQtRect(QRectF rect)
{
    //qDebug("tuneRect1 (%.2f,%.2f, %.2f,%.2f, %.2f,%.2f)", rect.left(), rect.top(), rect.right(), rect.bottom(), rect.width(), rect.height());
    if ((m_rect.width() <= 0) || (m_rect.height() <= 0))
        return QRectF(0, 0, 0, 0);

    int horizontalLowBound, horizontalHighBound, verticalLowBound, verticalHighBound;
    int offsetLeft, offsetRight, offsetTop, offsetBottom;
    if (e_weight_more_lower == m_weightStrategyOfHorizontal)
    {
        horizontalLowBound = - (k_move_step << 1);
        horizontalHighBound = k_move_step;
    }
    else if (e_weight_more_higher == m_weightStrategyOfHorizontal)
    {
        horizontalLowBound = - k_move_step;
        horizontalHighBound = (k_move_step << 1);
    }

    if (e_weight_more_lower == m_weightStrategyOfVertical)
    {
        verticalLowBound = - (k_move_step << 1);
        verticalHighBound = k_move_step;
    }
    else if (e_weight_more_higher == m_weightStrategyOfVertical)
    {
        verticalLowBound = - k_move_step;
        verticalHighBound = (k_move_step << 1);
    }
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    offsetLeft = m_generator->bounded(static_cast<int>(horizontalLowBound), static_cast<int>(horizontalHighBound) + 1);
    offsetRight = m_generator->bounded(static_cast<int>(horizontalLowBound), static_cast<int>(horizontalHighBound) + 1);
    offsetTop = m_generator->bounded(static_cast<int>(verticalLowBound), static_cast<int>(verticalHighBound) + 1);
    offsetBottom = m_generator->bounded(static_cast<int>(verticalLowBound), static_cast<int>(verticalHighBound) + 1);
#else
    offsetLeft = GetBounded(horizontalLowBound, horizontalHighBound);
    offsetRight = GetBounded(horizontalLowBound, horizontalHighBound);
    offsetTop = GetBounded(verticalLowBound, verticalHighBound);
    offsetBottom = GetBounded(verticalLowBound, verticalHighBound);
#endif

//qDebug("adj %d %d %d %d", offsetLeft, offsetRight, offsetTop, offsetBottom);
    qreal maxWidth = static_cast<qreal>(m_rect.right() - m_rect.left() - (k_border_size << 1)) ;
    qreal maxHeight = static_cast<qreal>(m_rect.bottom() - m_rect.top() - (k_border_size << 1));

    qreal left = static_cast<qreal>(rect.left() + offsetLeft);
    qreal top = static_cast<qreal>(rect.top() + offsetTop);
    qreal right = static_cast<qreal>(rect.right() + offsetRight);
    qreal bottom = static_cast<qreal>(rect.bottom() + offsetBottom);

    AdjustBoundary(m_weightStrategyOfHorizontal, left, right, k_border_size, k_min_width_of_vehicle, maxWidth);
    AdjustBoundary(m_weightStrategyOfVertical, top, bottom, k_border_size, k_min_height_of_vehicle, maxHeight);

    //qDebug("tuneRect2 (%.2f,%.2f, %.2f,%.2f) [%.2f,%.2f]", left, top, right, bottom, right - left, bottom - top);
    return QRectF(left, top, right - left, bottom - top);
}

bool AutoGeneratorRect::TuneCVRect(cv::Rect *pRect, cv::Rect *lpRect)
{
    bool ret = false;

    //qDebug("TuneCVRect 11 x=%d y=%d width=%d height=%d", pRect->x, pRect->y, pRect->width, pRect->height);
    if ((m_rect.width() <= 0) || (m_rect.height() <= 0))
        return ret;

    int horizontalLowBound, horizontalHighBound, verticalLowBound, verticalHighBound;
    int offsetLeft, offsetRight, offsetTop, offsetBottom;
    if (e_weight_more_lower == m_weightStrategyOfHorizontal)
    {
        horizontalLowBound = - (k_move_step << 1);
        horizontalHighBound = k_move_step;
    }
    else if (e_weight_more_higher == m_weightStrategyOfHorizontal)
    {
        horizontalLowBound = - k_move_step;
        horizontalHighBound = (k_move_step << 1);
    }

    if (e_weight_more_lower == m_weightStrategyOfVertical)
    {
        verticalLowBound = - (k_move_step << 1);
        verticalHighBound = k_move_step;
    }
    else if (e_weight_more_higher == m_weightStrategyOfVertical)
    {
        verticalLowBound = - k_move_step;
        verticalHighBound = (k_move_step << 1);
    }
//qDebug("TuneCVRect bound %d %d %d %d", horizontalLowBound, horizontalHighBound, verticalLowBound, verticalHighBound);
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    offsetLeft = m_generator->bounded(static_cast<int>(horizontalLowBound), static_cast<int>(horizontalHighBound) + 1);
    offsetRight = m_generator->bounded(static_cast<int>(horizontalLowBound), static_cast<int>(horizontalHighBound) + 1);
    offsetTop = m_generator->bounded(static_cast<int>(verticalLowBound), static_cast<int>(verticalHighBound) + 1);
    offsetBottom = m_generator->bounded(static_cast<int>(verticalLowBound), static_cast<int>(verticalHighBound) + 1);
#else
    offsetLeft = GetBounded(horizontalLowBound, horizontalHighBound);
    offsetRight = GetBounded(horizontalLowBound, horizontalHighBound);
    offsetTop = GetBounded(verticalLowBound, verticalHighBound);
    offsetBottom = GetBounded(verticalLowBound, verticalHighBound);
#endif

//qDebug("TuneCVRect offset %d %d %d %d", offsetLeft, offsetRight, offsetTop, offsetBottom);
    qreal maxWidth = static_cast<qreal>(m_rect.right() - m_rect.left() - (k_border_size << 1)) ;
    qreal maxHeight = static_cast<qreal>(m_rect.bottom() - m_rect.top() - (k_border_size << 1));

    qreal left = static_cast<qreal>(pRect->tl().x + offsetLeft);
    qreal top = static_cast<qreal>(pRect->tl().y + offsetTop);
    qreal right = static_cast<qreal>(pRect->br().x + offsetRight);
    qreal bottom = static_cast<qreal>(pRect->br().y + offsetBottom);

    AdjustBoundary(m_weightStrategyOfHorizontal, left, right, k_border_size, k_min_width_of_vehicle, maxWidth);
    AdjustBoundary(m_weightStrategyOfVertical, top, bottom, k_border_size, k_min_height_of_vehicle, maxHeight);

//    qDebug("TuneCVRect (%.2f,%.2f, %.2f,%.2f) [%.2f,%.2f]", left, top, right, bottom, right - left, bottom - top);
    lpRect->x = left;
    lpRect->y = top;
    lpRect->width = right - left;
    lpRect->height = bottom - top;
    //qDebug("TuneCVRect 22 x=%d y=%d width=%d height=%d",lpRect->x, lpRect->y, lpRect->width, lpRect->height);

    ret = true;

    return ret;
}
