#ifndef FILTER_H
#define FILTER_H

#include <QObject>
#include <QTimer>
#include <QQueue>
#include <QMutex>
#include <opencv2/opencv.hpp>
#include "traffic_enforcement.h"
#include "base_graph.h"
#include "common.h"


class FilterThread : public QObject
{
    Q_OBJECT
public:
    explicit FilterThread(BaseGraph *parent = nullptr);
    ~FilterThread();

protected:
    virtual void DoImageProcessing(const cv::Mat &matImage) = 0;

public slots:
    void stop();
    virtual void handleProcessedFrameCV(const cv::Mat &frame) = 0;

    static void matDeleter(void* mat) { delete static_cast<cv::Mat*>(mat); }

protected:
    virtual void timerEvent(QTimerEvent *event) = 0;

protected:
    //bool m_readyToEmit;
    QBasicTimer m_timer;
    int m_nTimerID;
    cv::Mat m_frame;
    QMutex m_mutex;
    QQueue<cv::Mat> m_imageList;
    BaseGraph *m_pOwner;
public:
    const char *m_class;
    const char *m_name;

};

class Filter : public FilterThread
{
    Q_OBJECT
public:
    explicit Filter(BaseGraph *parent = nullptr);
    ~Filter();

public:
    virtual bool ConnectTo(Filter *pDownstreamFilter) = 0;
    virtual void DisconnectAll() = 0;

protected:
    virtual void NotifyUpstreamPlugin(Filter *pUpstreamFilter);

signals:
    void started();
    void stopped();
    void iamStarted();
    void iamStopped();
    void cvColorReady(const cv::Mat &matImage);
    void qColorReady(const QImage &qImage);
    void imgProcessedReady(const cv::Mat &matImage);

public slots:
    void handleUpstreamStarted();
    void handleUpstreamStopped();
    virtual void handleColorFrameCV(const cv::Mat &frame) = 0;
    virtual void handleProcessedFrameCV(const cv::Mat &frame) = 0;
protected:
    //virtual void timerEvent(QTimerEvent *event) = 0;

protected:
    int32_t m_startedUpstreamFilters;
    int32_t m_currentFrame;
    EComponentState m_state;

    std::vector<Filter*> m_pUpstreamFilterList;
    std::vector<Filter*> m_pDownstreamFilterList;
    CQElapsedTimer m_calcTimer;
public:

};

#endif // FILTER_H
