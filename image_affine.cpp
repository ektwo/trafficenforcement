#include "image_affine.h"
#include <opencv2/opencv.hpp>
#include <vector>

using namespace std;

#define PI 3.141592
#define MAX_COUNT 30

cv::Mat preTransGray, curTransGray;
cv::Mat preRotaGray , curRotaGray;

std::vector<cv::Point2f> transPoints[2];
std::vector<cv::Point2f> rotaPoints[2];
std::vector<cv::Point2f> knowPoint[2];
std::vector<cv::Mat> status;
std::vector<cv::Mat> err;

void testOpticalFlowEstimation()
{
    std::cout << "optical flow methon for estimating the transform form reference scan "
              << "to reference scan ..." << std::endl;


    double refPose_x = 250, refPose_y = 250;                             // Init rectangle center
    cv::Mat trans_src_image = cv::imread("D:\\Work\\projects\\TrafficEnforcementData\\Images\\DSCF4472.JPG");   // resolution: 500*500  and the rectangle center is (250,250)
    cv::Mat trans_dst_image = cv::imread("D:\\Work\\projects\\TrafficEnforcementData\\Images\\DSCF4473.JPG");   // resolution: 500*500  and the rectangle center is (260,240)
    //cv::Mat rota_src_image = cv::imread("./testImage/rotateRectRef.png");
    //cv::Mat rota_dst_image = cv::imread("./testImage/rotateRectCur.png");
    //cv::Mat contra_image   = cv::imread("./testImage/contrastRef_Rotate.png");

    cvtColor(trans_src_image,preTransGray,  cv::COLOR_BGR2GRAY);
    cvtColor(trans_dst_image,curTransGray,  cv::COLOR_BGR2GRAY);
    //cvtColor(rota_src_image,preRotaGray,  cv::COLOR_BGR2GRAY);
    //cvtColor(rota_dst_image,curRotaGray,  cv::COLOR_BGR2GRAY);


    cv::TermCriteria termcrit(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, 20 ,0.01);

    cv::Size subPixWinSize(10,10), winSize(31,31);
    goodFeaturesToTrack(preTransGray,transPoints[0],MAX_COUNT,0.01,10);         // find corners
    goodFeaturesToTrack(preRotaGray,rotaPoints[0],MAX_COUNT,0.01,10);           // find corners


    /*subpiexl detection*/
    //cornerSubPix(curGray,points[0],subPixWinSize,cv::Size(-1,-1),termcrit);
    cornerSubPix(preTransGray,transPoints[0],subPixWinSize,cv::Size(-1,-1),termcrit);
    cornerSubPix(preRotaGray,rotaPoints[0],subPixWinSize,cv::Size(-1,-1),termcrit);
    /*PryLK method calculate optical flow*/
    //calcOpticalFlowPyrLK(preGray,curGray,points[0],points[1],status,err,winSize,5,termcrit,0,0.01);
    calcOpticalFlowPyrLK(preTransGray,curTransGray,transPoints[0],transPoints[1],status,err,winSize,3,termcrit,0,0.01);
    calcOpticalFlowPyrLK(preRotaGray,curRotaGray,rotaPoints[0],rotaPoints[1],status,err,winSize,3,termcrit,0,0.01);

    // Notice the last para in estimateRigidTransform func
    // if you choose 0 : partAffine , indicate you will perform  a rigid transform
    //        choose 1 : fullAffine , indicate you will perform  a non-rigid transfrom
    cv::Mat transEstimate = estimateRigidTransform(transPoints[0],transPoints[1],0);
    cv::Mat rotaEstimate  = estimateRigidTransform(rotaPoints[0],rotaPoints[1],0);


#if 0
        This program silce tests some points rigid transform
        Note :that we already know the Points position before & after transform
#endif
    cv::Point2f a(230,220),b(270,220),c(270,280),d(230,280);
    cv::Point2f Ta(220,270),Tb(220,230),Tc(280,230),Td(280,270);
	knowPoint[0].push_back(a);
	knowPoint[1].push_back(Ta);
    knowPoint[0].push_back(b), knowPoint[1].push_back(Tb);
    knowPoint[0].push_back(c), knowPoint[1].push_back(Tc);
    knowPoint[0].push_back(d), knowPoint[1].push_back(Td);
    cv::Mat pointEstimate   = estimateRigidTransform(knowPoint[0],knowPoint[1],0);
    std::cout << "pointEstimate : \n"  << pointEstimate << std::endl;
    std::cout << "transEstimate : \n"  << transEstimate << std::endl;
    std::cout << "rotaEstimate  : \n"  << rotaEstimate  << std::endl;


    //std::cout << "Test: before transform the center is " << refPose_x <<"  "<< refPose_y << std::endl;
    //refPose_x = transEstimate.at(0)* refPose_x + transEstimate.at(1)* refPose_y + transEstimate.at(2);
    //refPose_y = transEstimate.at(3)* refPose_x + transEstimate.at(4)* refPose_y + transEstimate.at(5);

    //bool displayKeypoints = false;
    //if(displayKeypoints)
    //{
    //    for(auto c:transPoints[0])
    //    {
    //        std::cout << c << std::endl;
    //        circle(preTransGray,c,2,cv::Scalar(0,15,255),-1);

    //    }
    //    std::cout << "========================== " << std::endl;
    //    for(auto c:transPoints[1])
    //    {
    //        std::cout << c << std::endl;
    //        circle(curTransGray,c,2,cv::Scalar(0,97,25),-1);
    //    }

    //}
    //cv::imshow("preGray",preTransGray);
    //cv::waitKey(0);
    //cv::imshow("vertGray",curTransGray);
    //cv::waitKey(0);
    //std::cout << "Test after transform the center is  " << refPose_x << "  "<< refPose_y << std::endl;

}
