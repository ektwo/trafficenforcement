#ifndef ESCORT_GRAPHICSSCENE_H
#define ESCORT_GRAPHICSSCENE_H

#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <opencv2/opencv.hpp>
#include "base_graph.h"

class EscortGraphicsScene : public QGraphicsScene
{
	Q_OBJECT
public:
	enum Mode { NoMode, SelectObject, DrawLine };
    EscortGraphicsScene(QObject* parent = 0);
	~EscortGraphicsScene();

    void SetOwner(BaseGraph *pGraph);
	void SetMode(Mode mode);


signals:
    void videoRenderStarted();
    void videoRenderStopped();

public slots:
    void handleUpstreamStarted();
    void handleUpstreamStopped();
	void handleUpdateViewCV(const cv::Mat &matImage);

protected:
	void MakeItemsControllable(bool areControllable);

private:
	Mode m_sceneMode;
    BaseGraph *m_pGraph;
	QGraphicsPixmapItem *m_imagePixmap;	
};

#endif // ESCORT_GRAPHICSSCENE_H
