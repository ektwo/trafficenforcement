#include "image_common.h"
#include"traffic_enforcement.h"
#include <QFile>
#include <QDataStream>
#include "common.h"

using namespace cv;
using namespace std;
cv::Mat CVImageOpen(const QString &fileName)
{
    return cv::imread(fileName.toLatin1().constData());
}
//INTER_NEAREST : nearest neighbor interpolation
//INTER_LINEAR 	: bilinear interpolation
//INTER_CUBIC 	: bicubic interpolation
//INTER_AREA    : resampling using pixel area relation. It may be a preferred method for image decimation, as it gives moire'-free results. But when the image is zoomed, it is similar to the INTER_NEAREST method.
//INTER_LANCZOS4: Lanczos interpolation over 8x8 neighborhood
void CVImageResize(cv::Mat& src, cv::Mat &dst, double fx, double fy)
{
    //rows = height
    //cols = width
    //Size dsize = Size(src.cols * scale, src.rows * scale);

    //qDebug("CVImageResize src %d %d", src.cols, src.rows);

    cv::Size dsize = cv::Size(src.cols * fx, src.rows * fy);
    cv::Mat matTmp = cv::Mat(dsize, CV_8UC3);
    //qDebug("CVImageResize matTmp %d %d", matTmp.cols, matTmp.rows);

    //cv::Mat matTmp2 = cv::Mat(src.rows * fy, src.cols * fx, CV_8UC3);
    //qDebug("CVImageResize matTmp2 %d %d", matTmp2.cols, matTmp2.rows);
    //fast, good for scaling down (CV_INTER_CUBIC, good for scaling up)
    cv::resize(src, matTmp, cvSize(0, 0), fx, fy, cv::INTER_AREA);

    dst = matTmp;
    //qDebug("CVImageResize dst %d %d", matTmp.cols, matTmp.rows);
    //for rotation, scaling, mapping
    //cv::remap()

}

void CVImageResize2ByWH(const cv::Mat& src, cv::Mat &dst, int width, int height)
{
    cv::resize(src, dst, cvSize(width, height), 0, 0, cv::INTER_AREA);
}

//void CVGetWindowRect(const char* name, int &x, int &y, int &width, int &height)
//{
//    CV_FUNCNAME( "cvGetWindowRect" );

//    CvWindow* window;
//    RECT rect;

//    if( !name )
//        CV_ERROR( CV_StsNullPtr, "NULL name" );

//    window = icvFindWindowByName(name);
//    if(!window)
//        EXIT;

//    GetWindowRect( window->frame, &rect );
//    x = rect.left;
//    y = rect.top;
//    width = rect.right - rect.left;
//    height = rect.bottom - rect.top;
//}

#if 0
// Draw the detected lines on an image
void drawDetectedLines(cv::Mat &image, cv::Scalar color=cv::Scalar(255,255,255)) {

    // Draw the lines
    std::vector<cv::Vec4i>::const_iterator it2= lines.begin();

    while (it2!=lines.end()) {

        cv::Point pt1((*it2)[0],(*it2)[1]);
        cv::Point pt2((*it2)[2],(*it2)[3]);

        cv::line( image, pt1, pt2, color);

        ++it2;
    }
}
#endif

void CVRemoveLinesOfInconsistentOrientations(std::vector<cv::Vec4i> &lines, int width, int height)
{
	std::vector<cv::Vec4i>::iterator it = lines.begin();
	int countt = 0;
	int countIdx = 0;
	// check all lines
	while (it != lines.end()) {
		// end points
		int x1 = (*it)[0];
		int y1 = (*it)[1];
		int x2 = (*it)[2];
		int y2 = (*it)[3];
		float line_angle = atan2(static_cast<double>(y1 - y2), static_cast<double>(x1 - x2));
		//if (line_angle < 0)
		//	line_angle += (180 / CV_PI);

		countIdx++;

        //double a = atan2(1, 1);
        //double b = atan2(1, -1);
        //double c = atan2(-1, -1);
        //double d = atan2(-1, 1);
        //double e = atan(1);
        //double f = atan(-1);
        //DSG(1, "AAA = %f", a * 180 / CV_PI);//45
        //DSG(1, "BBB = %f", b * 180 / CV_PI);//135
        //DSG(1, "CCC = %f", c * 180 / CV_PI );//-135
        //DSG(1, "CCC2 = %f", (c + 1) * 180 / CV_PI);//-135
        //DSG(1, "DDD = %f", d * 180 / CV_PI );///-45
        //DSG(1, "EEE = %f", e * 180 / CV_PI);//45
        //DSG(1, "FFF = %f", f * 180 / CV_PI);//-45
        int degree = abs((int)((line_angle * 180) / CV_PI));
        //DSG(1, "degree %d %d %d %d %f %d", x1, y1, x2, y2, (line_angle * 180) / CV_PI, degree);



        if ((degree > 45) && (degree < 135))
		//if ((line_angle > k_angel60) && (line_angle < k_angel120))
		{
            //if (8 != countIdx)
            //{
            //    it = lines.erase(it);
            //    ++countt;
            //    continue;
            //}
            //float theta_deg = (line_angle / CV_PI * 180) + (line_angle > 0 ? 0 : 360);
            //DSG(1, "theta_deg= (%d %d %d %d ) %f", x1, y1, x2, y2, theta_deg);
            // && 
            //(abs(x2 - x1) < (width / 6)) && (abs(y2 - y1) > (height >> 1))
		}
		else
		{
			it = lines.erase(it);
			++countt;
			continue;
		}

		++it;
	}
}

void CVRemoveLinesOfInconsistentOrientations(
    std::vector<cv::Vec4f> &lines,
    const cv::Mat &orientations, double percentage, double delta)
{
    //return;
    std::vector<cv::Vec4f>::iterator it= lines.begin();
    int countt = 0;
    int countIdx = 0;
    // check all lines
    while (it!=lines.end()) {
        // end points
        int x1= (*it)[0];
        int y1= (*it)[1];
        int x2= (*it)[2];
        int y2= (*it)[3];
        float line_angle = atan2(static_cast<double>(y1 - y2),static_cast<double>(x1 - x2));
        if (line_angle < 0)
            line_angle += CV_PI;

//qDebug("GGG %d %d %d %d %d", countIdx, x1, y1, x2, y2);
        countIdx++;
        if ((line_angle > k_angel60) && (line_angle < k_angel120))
        {
//            // for all points on the line
//            cv::LineIterator lit(orientations,cv::Point(x1,y1),cv::Point(x2,y2));
//            int i,count=0;
//            for(i = 0, count=0; i < lit.count; i++, ++lit) {
//                float ori = *(reinterpret_cast<float *>(*lit));
//                if (ori < 0)
//                    ori += CV_PI;

//                // is line orientation similar to gradient orientation ?
//                if (fabs(line_angle - ori) < delta)
//                    count++;

//            }

//            double consistency= count/static_cast<double>(i);
//            // set to zero lines of inconsistent orientation
//            if (consistency < percentage) {
//                (*it)[0]=(*it)[1]=(*it)[2]=(*it)[3]=0;
//                it = lines.erase(it);
//                ++countt;
//            }
        }
        else
        {
            it = lines.erase(it);
            //qDebug("bye");
            ++countt;
            continue;
        }

        ++it;
    }
    //qDebug("lines.size()=%d", lines.size());

//qDebug("FindLanes countt=%d", countt);

}


void CVFindDiffBetweenMatrix(Mat &matScaled)
{
    QString str("../withWhite.jpg");
    //QString str("../DSCF4460.JPG");

    Mat matBg;
#ifdef USE_SCALED_TO_COMPARE
    Mat matOrg = CVImageOpen(str.toLatin1().constData());
    Mat matBgScaled;
    if ((matOrg.cols * matOrg.rows) >  (4096 * 2160))
        CVImageResize(matOrg, matBgScaled, 0.125, 0.125);
    else if ((matOrg.cols * matOrg.rows) > (2560 * 1440))
        CVImageResize(matOrg, matBgScaled, 0.25, 0.25);
    else if ((matOrg.cols * matOrg.rows) >  (1920 * 1080))
        CVImageResize(matOrg, matBgScaled, 0.5, 0.5);
    else
        CVImageResize(matOrg, matBgScaled, 1, 1);
    matBg = matBgScaled;
#else
    matBg = CVImageOpen(str.toLatin1().constData());
#endif

    Mat matDiff;
#ifdef USE_HSV_TO_COMPARE
    Mat matBgHSV;
    Mat matScaledHSV;
    cv::cvtColor(matBg, matBgHSV, CV_BGR2HSV);

    cv::cvtColor(matScaled, matScaledHSV, CV_BGR2HSV);
    cv::absdiff(matBgHSV, matScaledHSV, matDiff);
#else
    cv::absdiff(matBg, matScaled, matDiff);
#endif

    float threshold = 66.0;  // 0
    float dist;
    //Mat mask(matBg.size(), CV_8UC1);
    cv::Mat mask = cv::Mat::zeros(matDiff.rows, matDiff.cols, CV_8UC1);
    for(int j = 0; j < matDiff.rows; ++j) {
        for(int i = 0; i < matDiff.cols; ++i){
            cv::Vec3b pix = matDiff.at<cv::Vec3b>(j, i);
            //int val = (pix[0] + pix[1] + pix[2]);
            dist = (pix[0]*pix[0] + pix[1]*pix[1] + pix[2]*pix[2]);
            dist = sqrt(dist);
            if(dist > threshold){
                mask.at<unsigned char>(j, i) = 255;
            }
        }
    }
    Mat res;
    bitwise_and(matScaled, matScaled, res, mask);

    // display
    //imshow("maskwhite", mask);
    //imshow("res", res);
    //cv::threshold(motion, motion, 80, 255, cv::THRESH_BINARY);
    //cv::erode(motion, motion, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3,3)));
}

int bytesToInt(QByteArray bytes) {
	int addr = bytes[0] & 0x000000FF;
	addr |= ((bytes[1] << 8) & 0x0000FF00);
	addr |= ((bytes[2] << 16) & 0x00FF0000);
	addr |= ((bytes[3] << 24) & 0xFF000000);
	return addr;
}
QByteArray  intToByte(int number)
{
	QByteArray abyte0;
	abyte0.resize(4);
	abyte0[0] = (uchar)(0x000000ff & number);
	abyte0[1] = (uchar)((0x0000ff00 & number) >> 8);
	abyte0[2] = (uchar)((0x00ff0000 & number) >> 16);
	abyte0[3] = (uchar)((0xff000000 & number) >> 24);
	return abyte0;
}

QFile g_file;
QByteArray g_byteArray;

void OpenAndReadSlidingWindowsFile(QString swInfoFileName)
{
	qDebug("OpenAndReadSlidingWindowsFile swInfoFile=%s", swInfoFileName.toLatin1().constData());

	g_file.setFileName(swInfoFileName);

	if (g_file.open(QIODevice::ReadOnly))
	{
		g_file.seek(0);
	}
}

void OpenAndWriteSlidingWindowsFile(QString swInfoFileName)
{
	qDebug("OpenAndWriteSlidingWindowsFile swInfoFile=%s", swInfoFileName);

	g_file.setFileName(swInfoFileName);

	if (g_file.open(QIODevice::WriteOnly))
	{
		g_file.seek(0);
	}
}

void SaveSlidingWindowsNumsOfLines(size_t numsOfLines)
{
	g_byteArray.append(intToByte(numsOfLines));
}

void CloseAndReadSlidingWindowsFile()
{
	g_file.close();
}
void CloseAndWriteSlidingWindowsFile()
{
	g_file.write(g_byteArray);
	g_file.close();
}


////QByteArray bArr;
////bArr.resize(sizeof(fc_adv_info_t));
////// do something to fill bArr with received data
////fc_adv_info_t* info=reinterpret_cast<fc_adv_info_t*>(bArr.data());
//QByteArray arr;
//mavlink_attitude_t* m = reinterpret_cast<mavlink_attitude_t*>(arr.data());
//void aa(Mat &src, SSlidingWindowInfo *pInfo)//int minX, int maxX, int centroid, int y)
void AddSlidingWindowsContent(
	Mat &src, vector<SSlidingWindowInfo> &vWindowInfo, int minX, int maxX, int centroid, int y)
{
	SSlidingWindowInfo info;

	info.min_x = minX;
	info.max_x = maxX;
	info.centroid = centroid;
	info.y = y;

	vWindowInfo.push_back(info);

#ifdef SLIDING_WINDOWS_BE_ERASE
    for (int j = minX; j < maxX; ++j)
    {
        src.at<Vec3b>(y, j)[0] = 255;
        src.at<Vec3b>(y, j)[1] = 255;
        src.at<Vec3b>(y, j)[2] = 255;
    }
#endif
    
#ifdef BACKUP_SWINFOFILE
	QByteArray outputdata;
	outputdata.resize(sizeof(SSlidingWindowInfo));
	//memcpy(outputdata.data(), pInfo, sizeof(SSlidingWindowInfo));
	memcpy(outputdata.data(), &info, sizeof(SSlidingWindowInfo));

	g_byteArray.append(outputdata);
#endif
}

void LoadSlidingWindows(std::vector<std::vector<SSlidingWindowInfo>> &slidingWindowInfo)
{

	//QBuffer buffer()
	//QDataStream ds(buffer);
	//QByteArray dataArray = g_file.readAll();
	//QDataStream ds(g_file.readAll());
	QDataStream ds(&g_file);
	ds.setVersion(QDataStream::Qt_4_6);
	ds.setByteOrder(QDataStream::LittleEndian);
	ds.setFloatingPointPrecision(QDataStream::SinglePrecision);

	QList< QVector<float>* > dataList;
	quint32 lineCount;
	quint32 numsOfPoints;
	quint32 minX;
	quint32 maxX;
	quint32 centroid;
	quint32 y;
	ds >> lineCount;
	//qDebug("LoadSlidingWindows lineCount=%d", lineCount);
	for (int lineIdx = 0; lineIdx < lineCount; ++lineIdx)
	{
		ds >> numsOfPoints;
		//qDebug("LoadSlidingWindows numsOfPoints=%d", numsOfPoints);
		vector<SSlidingWindowInfo> currentWindowInfoList;

		for (int ptIdx = 0; ptIdx < numsOfPoints; ++ptIdx)
		{
			SSlidingWindowInfo info;
			ds >> (info.min_x);
			ds >> (info.max_x);
			ds >> (info.centroid);
			ds >> (info.y);
			currentWindowInfoList.push_back(info);
			//qDebug("LoadSlidingWindows [%d] %u %u %u %u", ptIdx, info.min_x, info.max_x, info.centroid, info.y);
		}
		slidingWindowInfo.push_back(currentWindowInfoList);
	}
	//QVector<int> *data = new QVector<int>();
	//    for(int j = 0; j < 1; j++)
	//    {
	//        ds >> m ;
	//        qDebug("XXX m=%u", m);
	//        //data->append(m);
	//    }
	//    char buf[16];
	//    qint64 lineLength = ds.readRawData(buf, sizeof(buf));
	//    qDebug("XXX lineCount=%x %x", buf[0], buf[1]);

	//
	//
	//    qDebug("XXX lineCount=%u", lineCount);

}

void CVFindRect(int x1, int y1, int x2, int y2, int offset, int boundX, int boundY, cv::Rect &rect)
{
	int rcMinX = x1, rcMaxX, rcMinY = y1, rcMaxY;
	if (rcMinX > x2)
	{
		rcMinX = x2;
		rcMaxX = x1;
	}
	else
		rcMaxX = x2;

	if (rcMinY > y2)
	{
		rcMinY = y2;
		rcMaxY = y1;
	}
	else
		rcMaxY = y2;

	if (offset != 0)
	{
		rcMinX -= offset;
		if (rcMinX < 0) rcMinX = 0;
		rcMaxX += offset;
		if (rcMaxX > boundX) rcMaxX = boundX;

		rcMinY -= offset;
		if (rcMinY < 0) rcMinY = 0;
		rcMaxY += offset;
		if (rcMaxY > boundY) rcMaxY = boundY;
	}	

	rect.x = rcMinX;
	rect.y = rcMinY;
	rect.width = rcMaxX - rcMinX;
	rect.height = rcMaxY - rcMinY;

	//int x1, x2, y1, y2;
	//int offset = 6, boundX = 25, boundY = 305;
	//cv::Rect rect1;
	//cv::Rect rect2;
	//cv::Rect rect3;
	//cv::Rect rect4;

	//x1 = 0;
	//x2 = 20;
	//y1 = 100;
	//y2 = 300;
	//CVFindRect(x1, y1, x2, y2, offset, boundX, boundY, rect1);
	//qDebug("%d %d %d %d", rect1.x, rect1.y, rect1.width, rect1.height);
	//x1 = 20;
	//x2 = 0;
	//y1 = 100;
	//y2 = 300;
	//CVFindRect(x1, y1, x2, y2, offset, boundX, boundY, rect2);
	//qDebug("%d %d %d %d", rect2.x, rect2.y, rect2.width, rect2.height);
	//x1 = 20;
	//x2 = 0;
	//y1 = 300;
	//y2 = 100;
	//CVFindRect(x1, y1, x2, y2, offset, boundX, boundY, rect3);
	//qDebug("%d %d %d %d", rect3.x, rect3.y, rect3.width, rect3.height);
	//x1 = 0;
	//x2 = 20;
	//y1 = 300;
	//y2 = 100;
	//CVFindRect(x1, y1, x2, y2, offset, boundX, boundY, rect4);
	//qDebug("%d %d %d %d", rect4.x, rect4.y, rect4.width, rect4.height);
}

bool CVCheckPointInROI(vector<cv::Point> &contours, Point2f &pt)
{
	double r = cv::pointPolygonTest(contours, pt, true);
	//qDebug("CVCheckPointInROI (%0f, %0f) r=%f", pt.x, pt.y, r);
    return ((r >= 0) ? (true) : (false));
}

void vvv(std::vector<std::vector<SSlidingWindowInfo>> &slidingWindowInfo)
{
	int previousX, previousY;
	int minX, maxX;
	SSlidingWindowInfo info;

	//std::vector<std::vector<SSlidingWindowInfo>>::iterator it = slidingWindowInfo.begin();
	//qDebug("vvv lines=%d", slidingWindowInfo.size());

	for (int i = 0; i < slidingWindowInfo.size(); ++i)
	{
		std::vector<SSlidingWindowInfo> *pCurrentWindowInfoList = &(slidingWindowInfo[i]);
		//qDebug("vvv points=%d", pCurrentWindowInfoList->size());
		for (int j = 0; j < pCurrentWindowInfoList->size(); ++j)
		{
			SSlidingWindowInfo *pInfo = &(pCurrentWindowInfoList->at(j));
			//qDebug("[%d] %u %u %u %u", j, pInfo->min_x, pInfo->max_x, pInfo->centroid, pInfo->y);
		}
	}
}

void drawHistImg(const Mat &src, Mat &dst) {
	int histSize = 256;
	float histMaxValue = 0;
	for (int i = 0; i<histSize; i++) {
		float tempValue = src.at<float>(i);
		if (histMaxValue < tempValue) {
			histMaxValue = tempValue;
		}
	}

	float scale = (0.9 * 256) / histMaxValue;
	for (int i = 0; i<histSize; i++) {
		int intensity = static_cast<int>(src.at<float>(i)*scale);
		line(dst, Point(i, 255), Point(i, 255 - intensity), Scalar(0));
	}
}

void ShowGrayHistImage(const Mat &src)
{
	int histSize = 256;
	float range[] = { 0, 256 };
	const float* histRange = { range };
	Mat gray = src;
	cvtColor(src, gray, CV_BGR2GRAY);
	Mat histImg;
	calcHist(&gray, 1, 0, Mat(), histImg, 1, &histSize, &histRange);// , uniform, accumulate);
	Mat showHistImg(256, 256, CV_8UC1, Scalar(255));
	drawHistImg(histImg, showHistImg);

	imshow("ShowGrayHistImage", showHistImg);
}

void ShowColorfulHistImage(const Mat &src, int width, int height)
{
	vector<Mat> bgr_planes;
	split(src, bgr_planes);

	int histSize = 256;

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	bool uniform = true; bool accumulate = false;

	Mat b_hist, g_hist, r_hist;

	/// Compute the histograms:
	calcHist(&bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate);

	// Draw the histograms for B, G and R
	//int hist_w = 512; int hist_h = 400;
	//int bin_w = cvRound((double)hist_w / histSize);
	//int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound((double)width / histSize);

	Mat histImage(height, width, CV_8UC3, Scalar(0, 0, 0));

	/// Normalize the result to [ 0, histImage.rows ]
	normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

	/// Draw for each channel
	for (int i = 1; i < histSize; i++)
	{
		line(histImage, Point(bin_w*(i - 1), height - cvRound(b_hist.at<float>(i - 1))),
			Point(bin_w*(i), height - cvRound(b_hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
		line(histImage, Point(bin_w*(i - 1), height - cvRound(g_hist.at<float>(i - 1))),
			Point(bin_w*(i), height - cvRound(g_hist.at<float>(i))),
			Scalar(0, 255, 0), 2, 8, 0);
		line(histImage, Point(bin_w*(i - 1), height - cvRound(r_hist.at<float>(i - 1))),
			Point(bin_w*(i), height - cvRound(r_hist.at<float>(i))),
			Scalar(0, 0, 255), 2, 8, 0);
	}

	/// Display
	namedWindow("ShowColorfulHistImage", CV_WINDOW_AUTOSIZE);
	imshow("ShowColorfulHistImage", histImage);
}

void transform2(Point2f* src_vertices, Point2f* dst_vertices, const Mat& src, Mat &dst) {
	Mat M = getPerspectiveTransform(src_vertices, dst_vertices);
	warpPerspective(src, dst, M, dst.size(), INTER_LINEAR, BORDER_CONSTANT);
}

void main3(const Mat &src) {
	//Mat src = imread("test.png");

	Point2f src_vertices[4];
	const int line_dst_offset = 200;
	src_vertices[0] = Point(595, 452);
	src_vertices[1] = Point(685, 452);
	src_vertices[2] = Point(1120, 720);
	src_vertices[3] = Point(220, 720);
	//src_vertices[0] = Point(337, 3);
	//src_vertices[1] = Point(426, 3);
	//src_vertices[2] = Point(593, 510);
	//src_vertices[3] = Point(92, 574);
	//src_vertices[0] = Point(337 - xx, 0);
	//src_vertices[1] = Point(426 + xx, 0);
	//src_vertices[2] = Point(593 + xx, 576);
	//src_vertices[3] = Point(92 - xx, 576);
	Point2f dst_vertices[4];
	dst_vertices[0] = Point(92, 0);
	dst_vertices[1] = Point(593, 0);
	dst_vertices[2] = Point(593, 576);
	dst_vertices[3] = Point(92, 576);

	//dst_vertices[0] = Point(0, 0);
	//dst_vertices[1] = Point(768, 0);
	//dst_vertices[2] = Point(768, 576);
	//dst_vertices[3] = Point(0, 576);
	dst_vertices[0] = Point(src_vertices[3].x + line_dst_offset, 0);
	dst_vertices[1] = Point(src_vertices[2].x - line_dst_offset, 0);
	dst_vertices[2] = Point(src_vertices[2].x - line_dst_offset, 576);
	dst_vertices[3] = Point(src_vertices[3].x + line_dst_offset, 576);

	const int height = src.rows;
	const int width = src.cols;
	Mat M = getPerspectiveTransform(src_vertices, dst_vertices);
	Mat dst(height, width, CV_8UC1);

	warpPerspective(src, dst, M, dst.size(), INTER_LINEAR, BORDER_CONSTANT);
	imshow("dst1", dst);

	Mat dst2(height, width, CV_8UC1);
	transform2(src_vertices, dst_vertices, src, dst2);

	Mat result = cv::Mat::zeros(height, width, CV_8UC1);

	addWeighted(src, 1, dst, 0.3, 0, result);
	imshow("dst2", dst);
	Mat b3col(1, dst.cols, CV_64FC1, Scalar(0));
	//Mat b3col(1, dst.cols, CV_32SC1, Scalar(0));

	reduce(dst, b3col, 0, CV_REDUCE_SUM, CV_64FC1); //axis=0
	//std::cout << b3col << std::endl;

	//vector<int> graph(b3col.cols);
	//for (int c = 0; c<b3col.cols - 1; c++)
	//{
	//	Mat roi = b3col(Rect(c, 0, 1, b3col.rows));
	//	graph[c] = int(mean(roi)[0]);
	//}

	//Mat mgraph(260, b3col.cols + 10, CV_8UC3);
	//for (int c = 0; c<b3col.cols - 1; c++)
	//{
	//	line(mgraph, Point(c + 5, 0), Point(c + 5, graph[c]), Scalar(255, 0, 0), 1, CV_AA);
	//}
	//imshow("mgraph", mgraph);
	//imshow("source", src);
	//vector<Mat> planes;
	//split(dst, planes);
	////imshow("b", planes[0]);
	////imshow("g", planes[1]);
	////imshow("r", planes[2]);
	//cv::equalizeHist(planes[0], planes[0]);
	//cv::equalizeHist(planes[1], planes[1]);
	//cv::equalizeHist(planes[2], planes[2]);
	////imshow("bh", planes[0]);
	////imshow("gh", planes[1]);
	////imshow("rh", planes[2]);

	////imshow("src", src);
	//imshow("dddst", dst);
	////imshow("dst2", dst2);
	////imshow("result", result);
	////ShowColorfulHistImage(dst, dst.cols, dst.rows);
	//ShowGrayHistImage(dst);

	qDebug("cccc");
}

void a(const Mat &frame)
{

	std::cout << "what11" << std::endl;
	std::cout << "what11" << std::endl;
	std::cout << "what11" << std::endl;
	Mat bgrF = Mat::zeros(frame.size(), CV_32FC3);
	frame.convertTo(bgrF, CV_32FC3, 1.0 / 255, 0);
	Mat hlsF;
	cvtColor(bgrF, hlsF, COLOR_BGR2HLS);

	vector<Mat> chHLSF;
	//split(hlsF, chHLSF);
	//std::cout << chHLSF[2] << endl;
	//Mat s_binary = Mat::zeros(frame.size(), CV_8U);
	int hmin = 0;
	int hmax = 360;
	int lmin = 0;
	int lmin_Max = 255;
	int lmax = 0;
	int lmax_Max = 255;
	int smin = 170;
	int smin_Max = 255;
	int smax = 255;
	int smax_Max = 255;
	//Mat mask;
	//inRange(chHLSF, Scalar(hmin, lmin / float(lmin_Max), smin / float(smin_Max)), 
	//	Scalar(hmax, lmax / float(lmax_Max), smax / float(smax_Max)), mask);
	split(hlsF, chHLSF);
	std::cout << chHLSF[2] << std::endl;
	Mat s_binary;
	cv::inRange(chHLSF[2], Scalar(170.0 / 255.0), Scalar(255.0 / 255.0), s_binary);
	Mat s_binary_out;// = Mat::zeros(frame.size(), CV_8UC3);
	s_binary.convertTo(s_binary_out, CV_8U, 255.0, 0);
	imwrite("s_binary2_f1280.bmp", s_binary_out);

	std::cout << "what22" << std::endl;
	std::cout << "what22" << std::endl;
	std::cout << "what22" << std::endl;

	Mat hls;
	cvtColor(frame, hls, COLOR_BGR2HLS);
	vector<Mat> chHLS;
	split(hls, chHLS);
	std::cout << chHLS[2] << std::endl;
	Mat spec = Mat::zeros(hls.size(), CV_8U);
	for (int rowIdx = 0; rowIdx < hls.rows; ++rowIdx)
	{
		for (int colIdx = 0; colIdx < hls.cols; ++colIdx)
		{

			if ((170 <= chHLS[2].at<uchar>(rowIdx, colIdx)) &&
				(255 >= chHLS[2].at<uchar>(rowIdx, colIdx)))
			{
				spec.at<uchar>(rowIdx, colIdx) = 255;
			}
			else
				spec.at<uchar>(rowIdx, colIdx) = 0;
		}
	}
	imwrite("spec.bmp", spec);
	Mat s_binary2;// = Mat::zeros(frame.size(), CV_8U);
	cv::inRange(chHLS[2], Scalar(170), Scalar(255), s_binary2);
	std::cout << s_binary2 << std::endl;
	imwrite("s_binary2_i1280.bmp", s_binary2);
}
#include <fstream>
void writeMatToFile(cv::Mat& m, const char* filename)
{
	ofstream fout (filename, std::ofstream::out);

	if (!fout)
	{
		std::cout << "File Not Opened" << std::endl;  return;
	}

	for (int i = 0; i<m.rows; i++)
	{
		for (int j = 0; j<m.cols; j++)
		{
			fout << m.at<double>(i, j) << "\t";
		}
		fout << std::endl;
	}

	fout.close();
}
void testframe(const Mat &matImage)
{

	//imwrite("first.jpg", src);
	Mat src;
	Mat hls1;
#if 0
	CVImageResize2ByWH(matImage, src, 768, 576);
#else
	src = matImage;
#endif

	//a(matImage);

	//imwrite("first_intestframe.bmp", matImage);
	Mat srcF;
	matImage.convertTo(srcF, CV_32FC3, 1.0 / 255, 0);
	Mat hlsF;
	cvtColor(srcF, hlsF, COLOR_BGR2HLS);
	vector<Mat> chHLSF;

	//qDebug("fA0=%f %f %f", hlsF.at<Vec3f>(0, 0)[0], hlsF.at<Vec3f>(0, 0)[1], hlsF.at<Vec3f>(0, 0)[2]);
	//qDebug("fA1=%f %f %f", hlsF.at<Vec3f>(0, 1)[0], hlsF.at<Vec3f>(0, 1)[1], hlsF.at<Vec3f>(0, 1)[2]);
	//qDebug("fA2=%f %f %f", hlsF.at<Vec3f>(0, 2)[0], hlsF.at<Vec3f>(0, 2)[1], hlsF.at<Vec3f>(0, 2)[2]);
	//qDebug("fA3=%f %f %f", hlsF.at<Vec3f>(0, 3)[0], hlsF.at<Vec3f>(0, 3)[1], hlsF.at<Vec3f>(0, 3)[2]);
	//qDebug("fA4=%f %f %f", hlsF.at<Vec3f>(0, 4)[0], hlsF.at<Vec3f>(0, 4)[1], hlsF.at<Vec3f>(0, 4)[2]);
	//g_matHLSOutH = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
	//g_matHLSOutL = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
	//g_matHLSOutS = Mat::zeros(Size(matScaledOrgClone.cols, matScaledOrgClone.rows), CV_8UC1);
	split(hlsF, chHLSF);
	//qDebug("fA100=%f", chHLSF[1].at<float>(0, 0));
	//qDebug("fA101=%f", chHLSF[1].at<float>(0, 1));
	//qDebug("fA102=%f", chHLSF[1].at<float>(0, 2));
	//qDebug("fA103=%f", chHLSF[1].at<float>(0, 3));
	//qDebug("fA104=%f", chHLSF[1].at<float>(0, 4));

	//qDebug("fA200=%f", chHLSF[2].at<float>(0, 0));
	//qDebug("fA201=%f", chHLSF[2].at<float>(0, 1));
	//qDebug("fA202=%f", chHLSF[2].at<float>(0, 2));
	//qDebug("fA203=%f", chHLSF[2].at<float>(0, 3));
	//qDebug("fA204=%f", chHLSF[2].at<float>(0, 4));

	////Mat hls2;
	//cvtColor(src, hls1, COLOR_BGR2HLS);
	//qDebug("B0=%d %d %d", hls1.at<Vec3b>(0, 0)[0], hls1.at<Vec3b>(0, 0)[1], hls1.at<Vec3b>(0, 0)[2]);
	//qDebug("B1=%d %d %d", hls1.at<Vec3b>(0, 1)[0], hls1.at<Vec3b>(0, 1)[1], hls1.at<Vec3b>(0, 1)[2]);
	//qDebug("B2=%d %d %d", hls1.at<Vec3b>(0, 2)[0], hls1.at<Vec3b>(0, 2)[1], hls1.at<Vec3b>(0, 2)[2]);
	//qDebug("B3=%d %d %d", hls1.at<Vec3b>(0, 3)[0], hls1.at<Vec3b>(0, 3)[1], hls1.at<Vec3b>(0, 3)[2]);
	//qDebug("B4=%d %d %d", hls1.at<Vec3b>(0, 4)[0], hls1.at<Vec3b>(0, 4)[1], hls1.at<Vec3b>(0, 4)[2]);

	vector<Mat> chHLS;
	split(hls1, chHLS);
	//qDebug("B100=%d", chHLS[1].at<uchar>(0, 0));
	//qDebug("B101=%d", chHLS[1].at<uchar>(0, 1));
	//qDebug("B102=%d", chHLS[1].at<uchar>(0, 2));
	//qDebug("B103=%d", chHLS[1].at<uchar>(0, 3));
	//qDebug("B104=%d", chHLS[1].at<uchar>(0, 4));

	//qDebug("B110=%d", chHLS[1].at<uchar>(1, 0));
	//qDebug("B111=%d", chHLS[1].at<uchar>(1, 1));
	//qDebug("B112=%d", chHLS[1].at<uchar>(1, 2));
	//qDebug("B113=%d", chHLS[1].at<uchar>(1, 3));
	//qDebug("B114=%d", chHLS[1].at<uchar>(1, 4));

	//qDebug("B200=%d", chHLS[2].at<uchar>(0, 0));
	//qDebug("B201=%d", chHLS[2].at<uchar>(0, 1));
	//qDebug("B202=%d", chHLS[2].at<uchar>(0, 2));
	//qDebug("B203=%d", chHLS[2].at<uchar>(0, 3));
	//qDebug("B204=%d", chHLS[2].at<uchar>(0, 4));
	//qDebug("B210=%d", chHLS[2].at<uchar>(1, 0));
	//qDebug("B211=%d", chHLS[2].at<uchar>(1, 1));
	//qDebug("B212=%d", chHLS[2].at<uchar>(1, 2));
	//qDebug("B213=%d", chHLS[2].at<uchar>(1, 3));
	//qDebug("B214=%d", chHLS[2].at<uchar>(1, 4));

	vector<Mat> chHLS2;
//	imwrite("H.jpg", chHLS[0]);
//	imwrite("L.bmp", chHLS[1]);
//	imwrite("S.jpg", chHLS[2]);
	//CV_8UC1

	Mat sobel_binary = Mat::zeros(Size(chHLS[1].cols, chHLS[1].rows), CV_8U);
	Mat s_binary = Mat::zeros(Size(sobel_binary.cols, sobel_binary.rows), CV_8U);
	Mat combined_binary = Mat::zeros(Size(s_binary.cols, s_binary.rows), CV_8U);// CV_32FC3);
	//Mat H = Mat::zeros(Size(src.cols, src.rows), CV_8UC1);
	//Mat L = Mat::zeros(Size(src.cols, src.rows), CV_8UC1);
	//Mat S = Mat::zeros(Size(src.cols, src.rows), CV_8UC1);
	Mat sobelx;
	Mat sobely = cv::Mat::zeros(cv::Size(chHLS[1].cols, chHLS[1].rows), CV_64F);
	
	int sobel_kernel = 7;
	//Sobel(gray, sobelx, CV_64F, 1, 0, sobel_kernel, 1.0, 0.0, BORDER_DEFAULT);
	Sobel(chHLS[1], sobelx, CV_64F, 1, 0, sobel_kernel);
	for (int rowIdx = 0; rowIdx < sobelx.rows; ++rowIdx) //1280
	{ //48752
		for (int colIdx = 0; colIdx < sobelx.cols; ++colIdx) //720
		{
			if ((rowIdx == 37) && (colIdx > 116) && (colIdx < 120))
				qDebug("1 colIdx=%d %f", colIdx, sobelx.at<double>(rowIdx, colIdx));
		}
	}

	const char* filename = "output.txt";
	writeMatToFile(sobelx, filename);
	//imwrite("sobelx.bmp", sobelx);
	//Mat sobelxR = imread("sobelx.bmp");
	//for (int rowIdx = 0; rowIdx < sobelx.rows; ++rowIdx) //1280
	//{ //48752
	//	for (int colIdx = 0; colIdx < sobelx.cols; ++colIdx) //720
	//	{
	//		if ((rowIdx == 37) && (colIdx > 116) && (colIdx < 120))
	//			qDebug("1u colIdx=%d %f", colIdx, sobelxR.at<uchar>(rowIdx, colIdx));
	//	}
	//}

	Mat sobelx2, sobely2;
	cv::pow(sobelx, 2, sobelx2);
	for (int rowIdx = 0; rowIdx < sobelx.rows; ++rowIdx) //1280
	{ //48752
		for (int colIdx = 0; colIdx < sobelx.cols; ++colIdx) //720
		{
			if ((rowIdx == 37) && (colIdx > 116) && (colIdx < 120))
				qDebug("2 colIdx=%d %f", colIdx, sobelx2.at<double>(rowIdx, colIdx));
		}
	}
	imwrite("sobelx2.bmp", sobelx2);

	const char* filename2 = "output2.txt";
	writeMatToFile(sobelx2, filename2);

	//Mat sobelxR2 = imread("sobelx2.bmp");
	//for (int rowIdx = 0; rowIdx < sobelx.rows; ++rowIdx) //1280
	//{ //48752
	//	for (int colIdx = 0; colIdx < sobelx.cols; ++colIdx) //720
	//	{
	//		if ((rowIdx == 37) && (colIdx > 116) && (colIdx < 120))
	//			qDebug("2u colIdx=%d %f", colIdx, sobelxR2.at<uchar>(rowIdx, colIdx));
	//	}
	//}
	cv::pow(sobely, 2, sobely2);
	imwrite("sobely2.bmp", sobely2);
	Mat sobel_absa = cv::abs(sobelx2);
	imwrite("sobel_abs1a.bmp", sobel_absa);
	Mat sobel_abs = cv::abs(sobelx2 + sobely2);
	imwrite("sobel_abs2.bmp", sobel_abs);
	double v_min, v_max = 0;
	int idx_min[2] = { 255,255 }, idx_max[2] = { 255, 255 };
	minMaxIdx(sobel_abs, &v_min, &v_max, idx_min, idx_max);
	qDebug("%f", v_max);
	std::cout << "idx_min=" << idx_min[1] << ", idx_max=" << idx_max[1] << std::endl;
	Mat sobel_abs_norm = sobel_abs.mul(255.0 / v_max);
	imwrite("sobel_abs2.bmp", sobel_abs_norm);

	//Mat sobel_binary;
	cv::inRange(sobel_abs_norm, Scalar(3), Scalar(255), sobel_binary);
	imwrite("sobel_binary2.bmp", sobel_binary);
	cv::inRange(chHLS[2], Scalar(170), Scalar(255), s_binary);
	imwrite("s_binary2.bmp", s_binary);
	//qDebug("sobelx.type()=%d", sobelx.type());
	//imwrite("sobelx.jpg", sobelx);
	//cv::Mat dx, dy;
	//cv::convertScaleAbs(sobelx, dx);
	qDebug("combined_binary.rows=%d %d %d", combined_binary.type(), combined_binary.rows, combined_binary.cols);
	qDebug("sobel_binary.rows=%d %d %d", sobel_binary.type(), sobel_binary.rows, sobel_binary.cols);
	qDebug("s_binary.rows=%d %d %d", s_binary.type(), s_binary.rows, s_binary.cols);
	for (int rowIdx = 0; rowIdx < combined_binary.rows; ++rowIdx)
	{
		for (int colIdx = 0; colIdx < combined_binary.cols; ++colIdx)
		{

			if ((255 == sobel_binary.at<uchar>(rowIdx, colIdx)) ||
			    (255 == s_binary.at<uchar>(rowIdx, colIdx)))
			{
				combined_binary.at<uchar>(rowIdx, colIdx) = 255;
			}
			else
				combined_binary.at<uchar>(rowIdx, colIdx) = 0;
		}
	}
	imwrite("combined_binary1.bmp", combined_binary);
	//{

		minMaxIdx(combined_binary, &v_min, &v_max, idx_min, idx_max);
		Mat combined_binary_norm = combined_binary.mul(255.0 / v_max);
		imwrite("combined_binary2.bmp", combined_binary_norm);
	//}

	// mask_polyg
	Mat mask_img = Mat::zeros(Size(combined_binary.cols, combined_binary.rows), combined_binary.type());
	Mat mask_img2(Size(combined_binary.cols, combined_binary.rows), CV_8U, Scalar(0));
	int ignore_mask_color = 255;
	int offset = 100;
	int width = combined_binary.cols;
	int height = combined_binary.rows;

	Point root_points[1][4];

	root_points[0][0] = Point(0 + offset, height);
	root_points[0][1] = Point(width / 2.5, height / 1.65);
	root_points[0][2] = Point(width / 1.8, height / 1.65);
	root_points[0][3] = Point(width, height);

	const Point* ppt[1] = { root_points[0] };
	int npt[] = { 4 };
	//polylines(mask_img2, ppt, npt, 1, 1, Scalar(ignore_mask_color), 1, 8, 0);
	//imshow("Test", mask_img2);
	//mask_img = combined_binary.clone();
	fillPoly(mask_img, ppt, npt, 1, Scalar(ignore_mask_color));
	imwrite("mask_img.bmp", mask_img);
	Mat new_combined_binary;
	bitwise_and(combined_binary, mask_img, new_combined_binary);
	imwrite("new_combined_binary.bmp", new_combined_binary);
	//imshow("mask_img", mask_img);
	
	main3(new_combined_binary);// combined_binary_norm);
	//sobel_abs = np.abs(sobelx**2 + sobely * *2)
	//Sobel(chHLS[1], sobely, CV_64F, 0, 1, sobel_kernel);
	//cvtColor(src, hls2, COLOR_BGR2HLS);
	//Mat saveRGB1;
	//Mat saveBGR1;
	//cvtColor(hls1, saveRGB1, COLOR_HLS2RGB);
	//cvtColor(hls1, saveBGR1, COLOR_HLS2BGR);
	//imwrite("RGB2RGB.jpg", saveRGB1);
	//imwrite("RGB2BGR.jpg", saveBGR1);

	//Mat saveRGB2;
	//Mat saveBGR2;
	//cvtColor(hls2, saveRGB2, COLOR_HLS2RGB);
	//cvtColor(hls2, saveBGR2, COLOR_HLS2BGR);
	//imwrite("BGR2RGB.jpg", saveRGB2);
	//imwrite("BGR2BGR.jpg", saveBGR2);
}

void PrintMatInfo(const cv::Mat &src)
{
	qDebug("(%s) mat info: type=%d cols=%d rows=%d", __func__, src.type(), src.cols, src.rows);
}

double CrossProduct(Point v1, Point v2) {
	return v1.x*v2.y - v1.y*v2.x;
}

bool GetIntersectionPoint(Point a1, Point a2, Point b1, Point b2, Point & intPnt) {
	Point p = a1;
	Point q = b1;
	Point r(a2 - a1);
	Point s(b2 - b1);

	if (CrossProduct(r, s) == 0) { return false; }

	double t = CrossProduct(q - p, s) / CrossProduct(r, s);

	intPnt = p + t * r;
	return true;
}


cv::Point2f GetCrossPointF(cv::Vec4i LineA, cv::Vec4i LineB)
{
    double ka, kb;
    ka = (double)(LineA[3] - LineA[1]) / (double)(LineA[2] - LineA[0]); 
    kb = (double)(LineB[3] - LineB[1]) / (double)(LineB[2] - LineB[0]); 

    Point2f crossPoint;
    crossPoint.x = (ka*LineA[0] - LineA[1] - kb * LineB[0] + LineB[1]) / (ka - kb);
    crossPoint.y = (ka*kb*(LineA[0] - LineB[0]) + ka * LineB[1] - kb * LineA[1]) / (ka - kb);
    return crossPoint;
}

cv::Point2i GetCrossPoint(cv::Vec4i LineA, cv::Vec4i LineB)
{
    double ka, kb;
    ka = (double)(LineA[3] - LineA[1]) / (double)(LineA[2] - LineA[0]);
    kb = (double)(LineB[3] - LineB[1]) / (double)(LineB[2] - LineB[0]);

    Point2i crossPoint;
    crossPoint.x = (int)((ka*LineA[0] - LineA[1] - kb * LineB[0] + LineB[1]) / (ka - kb));
    crossPoint.y = (int)((ka*kb*(LineA[0] - LineB[0]) + ka * LineB[1] - kb * LineA[1]) / (ka - kb));
    return crossPoint;
}

QImage cvMat2QImage(const cv::Mat& mat)
{
    // 8-bits unsigned, NO. OF CHANNELS = 1
    if(mat.type() == CV_8UC1)
    {
        QImage image(mat.cols, mat.rows, QImage::Format_Indexed8);
        // Set the color table (used to translate colour indexes to qRgb values)
        image.setColorCount(256);
        for(int i = 0; i < 256; i++)
        {
            image.setColor(i, qRgb(i, i, i));
        }
        // Copy input Mat
        uchar *pSrc = mat.data;
        for(int row = 0; row < mat.rows; row ++)
        {
            uchar *pDest = image.scanLine(row);
            memcpy(pDest, pSrc, mat.cols);
            pSrc += mat.step;
        }
        return image;
    }
    // 8-bits unsigned, NO. OF CHANNELS = 3
    else if(mat.type() == CV_8UC3)
    {
        // Copy input Mat
        const uchar *pSrc = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
        return image.rgbSwapped();
    }
    else if(mat.type() == CV_8UC4)
    {
        DSG(1, "CV_8UC4");
        // Copy input Mat
        const uchar *pSrc = (const uchar*)mat.data;
        // Create QImage with same dimensions as input Mat
        QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_ARGB32);
        return image.copy();
    }
    else
    {
        DSG(1, "ERROR: Mat could not be converted to QImage.");
        //qDebug() << "ERROR: Mat could not be converted to QImage.";
        return QImage();
    }
}

cv::Mat QImage2cvMat(QImage image)
{
    cv::Mat mat;
    //qDebug() << image.format();
    switch(image.format())
    {
    case QImage::Format_ARGB32:
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32_Premultiplied:
        mat = cv::Mat(image.height(), image.width(), CV_8UC4, (void*)image.constBits(), image.bytesPerLine());
        break;
    case QImage::Format_RGB888:
        mat = cv::Mat(image.height(), image.width(), CV_8UC3, (void*)image.constBits(), image.bytesPerLine());
        cv::cvtColor(mat, mat, CV_BGR2RGB);
        break;
    case QImage::Format_Indexed8:
        mat = cv::Mat(image.height(), image.width(), CV_8UC1, (void*)image.constBits(), image.bytesPerLine());
        break;
    }
    return mat;
}

#if 0
float line_angle = atan2(pt1.y - pt2.y, pt1.x - pt2.x);
line_angle *= 57.19577951; // 180 / CV_PI
std::cout << line_angle << '\n';
const float deta = 5.0f;
float abslangle = abs(line_angle - deta);
if (abslangle < 0 || abslangle < 90 || abslangle < 180 || abslangle < 270)
{
    cv::LineIterator lit(binary, pt1, pt2, 8);
    for (int i = 0; i < lit.count; i++, ++lit)
    {
        cv::Point pt(lit.pos());
        //int val = binary.at<uchar>(pt.y, pt.x);
        //std::cout << pt.y << "," << pt.x << "-->" << val << std::endl;
        binary.at<uchar>(pt.y, pt.x) = 0;
    }
    continue;
}


void aaa(const Mat &src)
{
	Mat mask_img = Mat::zeros(Size(src.cols, src.rows), src.type());
	Mat mask_img2(Size(src.cols, src.rows), CV_8U, Scalar(0));
	int ignore_mask_color = 255;
	int offset = 100;
	int width = src.cols;
	int height = src.rows;

	Point root_points[1][4];

	root_points[0][0] = Point(0 + offset, height);
	root_points[0][1] = Point(width / 2.5, height / 1.65);
	root_points[0][2] = Point(width / 1.8, height / 1.65);
	root_points[0][3] = Point(width, height);

	const Point* ppt[1] = { root_points[0] };
	int npt[] = { 4 };
	//polylines(mask_img2, ppt, npt, 1, 1, Scalar(ignore_mask_color), 1, 8, 0);
	//imshow("Test", mask_img2);
	//mask_img = combined_binary.clone();
	fillPoly(mask_img, ppt, npt, 1, Scalar(ignore_mask_color));
	//imwrite("mask_img.bmp", mask_img);
	Mat new_combined_binary;
	bitwise_and(src, mask_img, new_combined_binary);
	//imwrite("new_combined_binary.bmp", new_combined_binary);
	//imshow("mask_img", mask_img);

	main3(src);// combined_binary_norm);
}


#endif
