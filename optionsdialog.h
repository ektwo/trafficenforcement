#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>
#include <QAbstractButton>
#include "traffic_enforcement.h"


typedef struct S_Vehicle_Detection_Algorithm_Tag {
	EVehicleDetectionModel mode;
	QString description;
} SVehicleDetectionAlgorithm;

static const SVehicleDetectionAlgorithm k_vehicle_detection_algorithm[] = {
	{ e_vd_model_kneron, "kneron" },
	{ e_vd_model_common_cascadeclassifier, "common cascade classifier" },

};

namespace Ui {
class OptionsDialog;
}

class OptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();

private:
    void closeEvent(QCloseEvent *bar);

	void LoadSettings();
	void SaveSettings();

private slots:
    void on_OptionsDialog_CB_LD_HowOften_currentIndexChanged(int index);

    void on_OptionsDialog_CB_VD_HowOften_currentIndexChanged(int index);

    void on_buttonBox_clicked(QAbstractButton *button);

    void on_OptionsDialog_CB_VD_1PassDiffThreshold_currentIndexChanged(int index);

    void on_OptionsDialog_CB_VD_Algorithm_currentIndexChanged(int index);

private:
	QString m_settingsFileName;
    Ui::OptionsDialog *ui;
};

#endif // OPTIONSDIALOG_H
