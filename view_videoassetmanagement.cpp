﻿#include "view_videoassetmanagement.h"
#include <QSettings>
#include "traffic_enforcement.h"
#include "ui_videoassetmanagement.h"

Q_DECLARE_METATYPE(cv::Mat)
VideoAssetManagement::VideoAssetManagement(QWidget *parent) :
    QDialog(parent),
	m_pVideoDemo(nullptr),
	m_pVideoFileDemoThread(nullptr),
    ui(new Ui::VideoAssetManagement)
{
    ui->setupUi(this);

    m_settingsFileName = QApplication::applicationDirPath() + "/TrafficEnforcement.ini";

    //this->setStyleSheet("{ background-color: blue }");
    //this->setStyleSheet("{background: 'blue'; }");
    //this->setStyleSheet("background-color: rgb(55, 135,215); border-radius:10px;");
    //this->setStyleSheet("background-color: rgb(55, 135, 215);");
	//this->setStyleSheet("background-color: rgb(0, 0, 0);");

    //this->setStyleSheet("QLineEdit { background-color: yellow }");

    LoadSettings();

	//connect(ui.vam_table_File, SIGNAL(cellClicked(int, int)), this, SLOT(myCellClicked(int, int)));
	connect(ui->vam_table_File, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(tableChangeDbl(int, int)));

	m_imageScenePreview.setParent(this);
	m_imageScenePreview.setBackgroundBrush(Qt::black);
	ui->vam_graphics_RecordFilePreview->setScene(&m_imageScenePreview);
	ui->vam_graphics_RecordFilePreview->setRenderHints(QPainter::Antialiasing);
	m_imageScenePreview.SetMode(EscortGraphicsScene::DrawLine);
	m_imageScenePreview.setSceneRect(0, 0, ui->vam_graphics_RecordFilePreview->geometry().width(), ui->vam_graphics_RecordFilePreview->geometry().height());
}

VideoAssetManagement::~VideoAssetManagement()
{
    delete ui;
}

void VideoAssetManagement::closeEvent(QCloseEvent *bar)
{
    //qDebug("OptionsDialog::closeEvnet");
	if (m_pVideoDemo)
	{
		delete m_pVideoDemo;
		m_pVideoDemo = nullptr;
	}
	if (m_pVideoFileDemoThread)
	{
		delete m_pVideoFileDemoThread;
		m_pVideoFileDemoThread = nullptr;
	}

}

void VideoAssetManagement::LoadSettings()
{

    QSettings settings(m_settingsFileName, QSettings::IniFormat);

    settings.setIniCodec("UTF-8");

    ui->vam_cb_Country->addItem(QString(u8"台灣"));

    ui->vam_cb_City->addItem(QString(u8"台北市"));
    ui->vam_cb_City->addItem(QString(u8"新北市"));

    ui->vam_cb_Village->addItem(QString(u8"中和區"));

    ui->vam_cb_Street->addItem(QString(u8"景新街"));

    ui->vam_cb_Character1->addItem(QString(u8"顏色"));
    ui->vam_cb_Character2->addItem(QString(u8"車型"));

    ui->vam_cb_TypesOfViolation->addItem(QString(u8"壓雙白線"));


    ui->vam_table_Camera->setColumnCount(5);
    ui->vam_table_Camera->setRowCount(20 + 1);

    QStringList headerInfo;
    headerInfo << tr("address") << tr("camera type") << tr("Serial No.") << tr("date of manufacture") << tr("comment") ;
    ui->vam_table_Camera->setHorizontalHeaderLabels(headerInfo);
    //ui->vam_table_Camera->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //ui->vam_table_Camera->setSelectionBehavior(QAbstractItemView::SelectRows);
    //ui->vam_table_Camera->setSelectionMode(QAbstractItemView::SingleSelection);
    //ui->vam_table_Camera->resize(250, 200);

    QString str;


    for (int currentIdx = 0; currentIdx < 20; ++currentIdx)
    {
        str.sprintf(u8"999巷");
        ui->vam_table_Camera->setItem(currentIdx, 0, new QTableWidgetItem(str));

        str.sprintf(u8"高清");
        ui->vam_table_Camera->setItem(currentIdx, 1, new QTableWidgetItem(str));

        str.sprintf(u8"117-19");
        ui->vam_table_Camera->setItem(currentIdx, 2, new QTableWidgetItem(str));

        str.sprintf(u8"2017/11/17");
        ui->vam_table_Camera->setItem(currentIdx, 3, new QTableWidgetItem(str));

        str.sprintf(u8"維修過兩次");
        ui->vam_table_Camera->setItem(currentIdx, 4, new QTableWidgetItem(str));
    }

	

    //{
    //	QTableWidgetItem *item;
    //	for (int i = 0; i < ui->trVehicleInfoWidget->columnCount(); ++i) {
    //		item = ui->trVehicleInfoWidget->itemAt(currentIdx, i);
    //		//item->setFlags(item->flags() | Qt::ItemIsEditable);
    //	}
    //}



    ////for (int i = 0; i < DIM(k_lanedetection_cycletime); ++i)
    ////	ui->OptionsDialog_CB_LD_HowOften->addItem(k_lanedetection_cycletime[i].description);
    ////for (int i = 0; i < DIM(k_vehicledetection_cycletime); ++i)
    ////	ui->OptionsDialog_CB_VD_HowOften->addItem(k_lanedetection_cycletime[i].description);

    //for (int i = 0; i < DIM(k_lanedetection_cycletime); ++i)
    //	ui->OptionsDialog_CB_LD_HowOften->insertItem(i, k_lanedetection_cycletime[i].description);
    //for (int i = 0; i < DIM(k_vehicledetection_cycletime); ++i)
    //	ui->OptionsDialog_CB_VD_HowOften->insertItem(i, k_vehicledetection_cycletime[i].description);
    //for (int i = 0; i < 101; ++i)
    //{
    //	QString str;
    //	str.sprintf("%d %%", i);
    //	ui->OptionsDialog_CB_VD_1PassDiffThreshold->insertItem(i, str);
    //}
    //for (int i = 0; i < DIM(k_vehicle_detection_algorithm); ++i)
    //	ui->OptionsDialog_CB_VD_Algorithm->insertItem(i, k_vehicle_detection_algorithm[i].description);

    //ui->OptionsDialog_CB_LD_HowOften->setCurrentIndex(
    //	settings.value(QString(k_str_tools_opt_ld_cycletime_idx), 0).toInt());
    //ui->OptionsDialog_CB_VD_HowOften->setCurrentIndex(
    //	settings.value(QString(k_str_tools_opt_vd_cycletime_idx), 0).toInt());
    //ui->OptionsDialog_CB_VD_1PassDiffThreshold->setCurrentIndex(
    //	settings.value(QString(k_str_tools_opt_vd_1pass_diffthreshold_idx), 30).toInt());
    //ui->OptionsDialog_CB_VD_Algorithm->setCurrentIndex(
    //	settings.value(QString(k_str_tools_opt_vd_model_idx), 0).toInt());

}

void VideoAssetManagement::SaveSettings()
{
    QSettings settings(m_settingsFileName, QSettings::IniFormat);

    settings.setIniCodec("UTF-8");
//
//	settings.setValue(QString(k_str_tools_opt_ld_cycletime_idx),
//		ui->OptionsDialog_CB_LD_HowOften->currentIndex());
//	settings.setValue(QString(k_str_tools_opt_vd_cycletime_idx),
//		ui->OptionsDialog_CB_VD_HowOften->currentIndex());
//	settings.setValue(QString(k_str_tools_opt_vd_1pass_diffthreshold_idx),
//		ui->OptionsDialog_CB_VD_1PassDiffThreshold->currentIndex());
//	settings.setValue(QString(k_str_tools_opt_vd_model_idx),
//		ui->OptionsDialog_CB_VD_Algorithm->currentIndex());
}


void VideoAssetManagement::on_vam_btn_Search_clicked()
{
    QString str;

    ui->vam_table_File->setRowCount(1);
    ui->vam_table_File->setColumnCount(5);


    ui->vam_table_File->setRowCount(20 + 1);

    QStringList headerFileInfo;
    headerFileInfo << tr("filename") << tr("date") << tr("time") << tr("types of violation") << tr("characters");
    ui->vam_table_File->setHorizontalHeaderLabels(headerFileInfo);
    //ui->vam_table_File->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //ui->vam_table_File->setSelectionBehavior(QAbstractItemView::SelectRows);
    //ui->vam_table_File->setSelectionMode(QAbstractItemView::SingleSelection);
    //ui->vam_table_File->resize(250, 200);


    for (int currentIdx = 0; currentIdx < 20; ++currentIdx)
    {
        if ((currentIdx % 3) == 0)
        {
            str.sprintf(u8"violation_dwl_red_van.mp4");
            ui->vam_table_File->setItem(currentIdx, 0, new QTableWidgetItem(str));

            str.sprintf(u8"2018/07/16");
            ui->vam_table_File->setItem(currentIdx, 1, new QTableWidgetItem(str));

            str.sprintf(u8"17:13");
            ui->vam_table_File->setItem(currentIdx, 2, new QTableWidgetItem(str));

            str.sprintf(u8"壓白線");
            ui->vam_table_File->setItem(currentIdx, 3, new QTableWidgetItem(str));

            str.sprintf(u8"紅色, 箱型車");
            ui->vam_table_File->setItem(currentIdx, 4, new QTableWidgetItem(str));
        }
        else if ((currentIdx % 3) == 1)
        {
            str.sprintf(u8"violation_phone_yello_normal.mp4");
            ui->vam_table_File->setItem(currentIdx, 0, new QTableWidgetItem(str));

            str.sprintf(u8"2018/07/15");
            ui->vam_table_File->setItem(currentIdx, 1, new QTableWidgetItem(str));

            str.sprintf(u8"22:22");
            ui->vam_table_File->setItem(currentIdx, 2, new QTableWidgetItem(str));

            str.sprintf(u8"開車用手機");
            ui->vam_table_File->setItem(currentIdx, 3, new QTableWidgetItem(str));

            str.sprintf(u8"黃色, 轎車");
            ui->vam_table_File->setItem(currentIdx, 4, new QTableWidgetItem(str));
        }
        else if ((currentIdx % 3) == 2)
        {
            str.sprintf(u8"violation_rturnr_blue_truck.mp4");
            ui->vam_table_File->setItem(currentIdx, 0, new QTableWidgetItem(str));

            str.sprintf(u8"2018/07/14");
            ui->vam_table_File->setItem(currentIdx, 1, new QTableWidgetItem(str));

            str.sprintf(u8"10:10");
            ui->vam_table_File->setItem(currentIdx, 2, new QTableWidgetItem(str));

            str.sprintf(u8"紅燈右轉");
            ui->vam_table_File->setItem(currentIdx, 3, new QTableWidgetItem(str));

            str.sprintf(u8"藍色, 貨車");
            ui->vam_table_File->setItem(currentIdx, 4, new QTableWidgetItem(str));
        }
    }

}

void VideoAssetManagement::tableChangeDbl(int row, int column)
{
#ifdef SKIP_ON_20181010
	//append(QString("Double Clicked %1, %2").arg(row).arg(column));
	if (!m_pVideoDemo)
		m_pVideoDemo = new TRVideoFileSource(nullptr);
	if (!m_pVideoFileDemoThread)
		m_pVideoFileDemoThread = new SafeFreeThread;

	SVideoConfig configDemo;
	configDemo.interval_ms = -1;// 33;
	strcpy(configDemo.filename, "D:\\Work\\projects\\TrafficEnforcementUbuntu\\bin\\video\\complex\\DSCF4448.MOV");
	qDebug("1"); 
	if (m_pVideoDemo->SetConfig(&configDemo))
	{
		qDebug("2");
		qRegisterMetaType<cv::Mat>();
		m_pVideoDemo->moveToThread(m_pVideoFileDemoThread);
		connect(m_pVideoDemo, &TRVideoFileSource::cvColorReady, &m_imageScenePreview, &EscortGraphicsScene::handleUpdateViewCV);
		m_pVideoFileDemoThread->start();

		QMetaObject::invokeMethod(m_pVideoDemo, "start", Qt::QueuedConnection, Q_ARG(int, CV_CAP_ANY));
	}
#endif
}