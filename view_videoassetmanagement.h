#ifndef VIDEO_ASSET_MANAGEMENT_H
#define VIDEO_ASSET_MANAGEMENT_H

#include <QDialog>
#include "trvideothread.h"
#include "trcamerathread.h"
#include "escort_graphicsscene.h"

namespace Ui {
	class VideoAssetManagement;
}

class VideoAssetManagement : public QDialog
{
	Q_OBJECT

public:
	explicit VideoAssetManagement(QWidget *parent = 0);
	~VideoAssetManagement();

private:
	void closeEvent(QCloseEvent *bar);

	void LoadSettings();
	void SaveSettings();

private slots:
    void on_vam_btn_Search_clicked();
	void tableChangeDbl(int row, int column);

private:
	TRVideoFileSource *m_pVideoDemo;
	SafeFreeThread *m_pVideoFileDemoThread;
	EscortGraphicsScene m_imageScenePreview;
	QString m_settingsFileName;
	Ui::VideoAssetManagement *ui;
};

#endif // VIDEO_ASSET_MANAGEMENT_H
