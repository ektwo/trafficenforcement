#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "detection_header.h"

using namespace std;
using namespace cv;

static const char *winRoadmark = "Roadmark";

Rect roiRoadmark;
Rect roiRoadmark2 = Rect(384, 1, 118, 118);

//parameters for landmark detection
int roadmarkROIX = 540;
int roadmarkROIY = 508;
int roadmarkROIWidth = 294;
int roadmarkROIHeight = 214;
//typedef void (*TrackbarCallback)(int pos, void* userdata);

void CreateWindow_Landmark()
{
    namedWindow(winRoadmark, CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);
}

void createTrackbar_Landmark(TrackbarCallback callback)
{
    createTrackbar("roadmark ROI X", winConfig2, &roadmarkROIX, 1000, callback);
    createTrackbar("~ Y", winConfig2, &roadmarkROIY, 1000, callback);
    createTrackbar("~ Width", winConfig2, &roadmarkROIWidth, 2000, callback);
    createTrackbar("~ Height", winConfig2, &roadmarkROIHeight, 2000, callback);
}

void onROIChange_Landmark()
{
    /// ROI of landmark detection
    roiRoadmark.x = roadmarkROIX;
    roiRoadmark.y = roadmarkROIY;
    roiRoadmark.width = roadmarkROIWidth;
    roiRoadmark.height = roadmarkROIHeight;
}
#if 0

#define NEG_DIR "/media/TOURO/opencv/neg/"


// Use IPM to detect Roadmark
void detectRoadmark2(Mat *imgInput) {
    const char *winDR2IPM = "Detect Roadmark 2 IPM";

    Mat iROI, iGray, iIPM, iHist, iThres, iGauss;

    iROI = Mat(*imgInput, roiLane);

    cvtColor(iROI, iGray, CV_RGB2GRAY);

    GaussianBlur(iGray, iGauss, Size(3, 3), 3);

    //equalizeHist(iGauss, iHist);
    adaptiveThreshold(iGauss, iHist, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 51, -10);
    threshold(iHist, iThres, 255 * 950 / 1000, 255, THRESH_TOZERO);


    /// IPM map
    warpPerspective(iThres, iIPM, matrixIPM, iROI.size());

    cutRegion(&iIPM, Rect(0, 0, iIPM.cols, iIPM.rows), "/media/TOURO/neg");

    // Use cascade classifier to identify landmark
    vector<Rect> roadmarks;
    vector<Point2f> pIn, pOut;
    static CascadeClassifier _cascade;
    static CascadeClassifier* cascade = NULL;
    int ret;

    if (cascade == NULL) {
        cascade = &_cascade;
        ret = cascade->load(ROADMARK2_CASCADE);
        if (!ret) {
            fprintf(stderr, "Could not load cascade file: `%s'\n", ROADMARK2_CASCADE);
            exit(-1);
        }
    }

    //detect landmark
    cascade->detectMultiScale(iIPM, roadmarks, 1.1, 3, 0 | CASCADE_SCALE_IMAGE, Size(100, 100) );

    fprintf(stdout, "indentify %lu roadmark\n", roadmarks.size());

    for( size_t i = 0; i < roadmarks.size(); i++ )
    {
        //draw landmark rect
        Point2f p1, p2, pc, pcT, pcR, pTxt;
        p1.x = roadmarks[i].x;
        p1.y = roadmarks[i].y;
        p2.x = p1.x + roadmarks[i].width;
        p2.y = p1.y + roadmarks[i].height;
        rectangle(iIPM, p1, p2, CV_RGB(255, 200, 255), 2);
    }

    imshow(winDR2IPM, iIPM);
}

void detectRoadmark(Mat *imgInput, Rect _roi) {
    vector<Rect> marks;
    vector<Point2f> pIn, pOut;
    static CascadeClassifier _cascade;
    static CascadeClassifier* cascade = NULL;
    int ret;

    if (cascade == NULL) {
        cascade = &_cascade;
        ret = cascade->load(ROADMARK_CASCADE);
        if (!ret) {
            fprintf(stderr, "Could not load cascade file: `%s'\n", ROADMARK_CASCADE);
            exit(-1);
        }
    }

    if (_roi.x < 0 || _roi.x >= imgInput->cols) {
        _roi.x = 0;
    }
    if (_roi.y < 0 || _roi.y >= imgInput->rows) {
        _roi.y = 0;
    }
    if (_roi.x + _roi.width >= imgInput->cols) {
        _roi.width = imgInput->cols - 1 - _roi.x;
    }
    if (_roi.y + _roi.height >= imgInput->rows) {
        _roi.height = imgInput->rows - 1 - _roi.y;
    }

    Mat iROI(*imgInput, _roi);
    Mat iGray; //, iGaussian, iHist, iThres;

    //gray map
    cvtColor(iROI, iGray, COLOR_BGR2GRAY);

    /// Gaus Blur
    /*
    GaussianBlur(iGray, iGaussian, Size(5, 5), 5);
    equalizeHist(iGaussian, iHist);
    threshold(iHist, iThres, 127, 255, THRESH_TOZERO);
    */

    /// draw area of landmark detection
    rectangle(imgOrigin, Point(_roi.x, _roi.y), Point(_roi.x + _roi.width, _roi.y + _roi.height), CV_RGB(128, 128, 255), 2);


    /// detect landmark
    cascade->detectMultiScale(iGray, marks, 1.1, 1, 0 | CASCADE_SCALE_IMAGE, Size(30, 30) );

    fprintf(stderr, "detected %lu landmarks\n", marks.size());


    for( size_t i = 0; i < marks.size(); i++ )
    {
        /// draw landmark rect
        Point2f p1, p2, pc, pcT, pcR, pTxt;
        p1.x = marks[i].x + _roi.x;
        p1.y = marks[i].y + _roi.y;
        p2.x = p1.x + marks[i].width;
        p2.y = p1.y + marks[i].height;

        rectangle(imgOrigin, p1, p2, CV_RGB(0, 0, 255), 4);
        rectangle(iGray, Point(marks[i].x, marks[i].y), Point(marks[i].x + marks[i].width, marks[i].y + marks[i].height), CV_RGB(255, 255, 255), 4);


        /// save detected area in negative path. for further training
        char negpath[1024] = {0};
        static int negi = 0;
        snprintf(negpath, sizeof(negpath) - 1, "%s/neg-%ld-%02d.png", NEG_DIR, time(NULL), ++negi % 100);
        imwrite(negpath, Mat(*imgInput, _roi));

    }

    imshow(winRoadmark, iGray);
}



#endif
