#include "trcvthread.h"
#if 0
#include "autogenerator_line.h"
#include "autogenerator_rect.h"
#include <QTimer>
#include <QElapsedTimer>

using namespace std;
using namespace cv;

#define USE_TIMER

#ifdef USE_TIMER
QTimer *timer = nullptr;
QElapsedTimer timer2;
#endif
TRCVThread::TRCVThread(QObject *parent)
    : QThread(parent)
    , m_isStopped(false)
{
#ifdef USE_TIMER
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(10);

    timer2.start();
#endif
}

void TRCVThread::AddCVTask(SMovingCVObject *object)
{
	m_movingCVObjectList.push_back(*object);
}

void TRCVThread::Stop()
{
	qDebug("TRCVThread::Stop() m_isStopped=%d", m_isStopped);
	m_isStopped = true;
}

void TRCVThread::run()
{
    while (!m_isStopped) {
        SMovingCVObject *pObject = nullptr;
        SCarAgent *pCarAgent;
        int cars;
        for (int i = 0; i < m_movingCVObjectList.count(); ++i)
        {
            pObject = &m_movingCVObjectList[i];
            if (!pObject) return;
            pCarAgent = pObject->car_agent;
            if (pCarAgent)
            {
                cars = pCarAgent->car_list.size();
                for (int j = 0; j < cars; ++j)
                {
                    (pCarAgent->autogenerator_list.at(j)).TuneCVRect(&pCarAgent->car_list[j], &pCarAgent->car_list[j]);
                }
            }
            QThread::msleep(pObject->interval_ms);
        }
        emit updateCVRectItem();

    }
#ifdef USE_TIMER
    if (timer)
        delete timer;
#endif
    qDebug("TRCVThread::Run() done");
    emit done();
}

void TRCVThread::update()
{
#ifdef USE_TIMER
    qDebug("ms=%d", timer2.elapsed());
#endif
}
#endif