#ifndef FILTER_HLS_H
#define FILTER_HLS_H

#include "filter.h"
#include "escort_graphicsscene.h"
#include "cqtopencvviewergl/cqtopencvviewergl.h"

using namespace std;
using namespace cv;

class HLSFilter : public Filter
{
    Q_OBJECT
public:
    HLSFilter(BaseGraph *parent, int profileIdx);
    ~HLSFilter();

    bool ConnectTo(Filter *pDownstreamFilter);
    void DisconnectAll();
    Q_SIGNAL void newFrame(const QImage &);
    Q_SIGNAL void update(const QImage &);
public slots:
    void handleUpstreamStarted();
    void handleUpstreamStopped();
    void handleColorFrameCV(const cv::Mat &frame);
    void handleProcessedFrameCV(const cv::Mat &frame) {}

protected:

    void PrepareImageForLaneDetection(const Mat &matScaledOrgClone);
    void ReactFromTrackbar(const Mat &in, Mat *lpMaskH, Mat *lpMaskL, Mat *lpMaskS);
    void ReactFromTrackbar();
    void UpdateOutputMat(const Mat &srcF, const Mat &maskH, const Mat &maskL, const Mat &maskS,Mat &mask, Mat &dst);
    void ShowWindow();

    void ThresholdCallback(int pos, void *userdata);
    void DoImageProcessing(const Mat &matImage);
    virtual void timerEvent(QTimerEvent *event);

    const Mat* GetHLSMaskH() { return &m_matMaskHLS_H; }
    const Mat* GetHLSMaskL() { return &m_matMaskHLS_L; }
    const Mat* GetHLSMaskS() { return &m_matMaskHLS_S; }
    const Mat* GetHLSMask() { return &m_matMaskHLS; }
    void CreateWindowHLS();

private:
    int m_thresH;
    int m_thresL;
    int m_thresS;
    int m_thresHMax;
    int m_thresLMax;
    int m_thresSMax;

    Mat m_matHLSInF;
    Mat m_matHLSOut;
    Mat m_matScaledImageF;
    vector<Mat> m_matHLSChannel;
    Mat m_matMaskHLS_H;
    Mat m_matMaskHLS_L;
    Mat m_matMaskHLS_S;
    Mat m_matMaskHLS;
    EscortGraphicsScene *m_pScene;
    //CQtOpenCVViewerGl *m_pScene;
    //QOpenGLWidget *m_pScene;
    
};

#endif // FILTER_HLS_H
