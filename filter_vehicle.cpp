#include "filter_vehicle.h"
#include "parameters_table.h"
#include "common.h"
#include "detection_lanecovered.h"


using namespace cv;
using namespace std;

extern SParametersTable k_parameters_table[2];
VehicleFilter::VehicleFilter(BaseGraph *parent, int profileIdx)
    : Filter(parent)
    , m_modelLoaded(false)
    , m_classifierScaleFactor(k_parameters_table[profileIdx].car_classifier.scale_factor)
    , m_classifierMinNeighbors(k_parameters_table[profileIdx].car_classifier.min_heighbors)
    , m_classifierFlags(k_parameters_table[profileIdx].car_classifier.flags)
    , m_classifierMinSize(k_parameters_table[profileIdx].car_classifier.min_size)
    , m_classifierMaxSize(k_parameters_table[profileIdx].car_classifier.max_size)
    , m_periodCnt(0)
    , m_pPythonWrapper(nullptr)
    , m_pPythonWrapperModel(nullptr)
{
    m_class = k_str_class_transform_name;
    m_name = k_str_vehicle_filter_name;
    DSG(1, "VehicleFilter m_class=%s", m_class);
    DSG(1, "VehicleFilter m_name=%s", m_name);
}

VehicleFilter::~VehicleFilter()
{
    //disconnect(this, &Filter::cvColorReady, 0, 0);
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if (e_vd_model_kneron == pCache->vehicledetection_model)
    {
        if (m_pPythonWrapperModel)
        {
            delete m_pPythonWrapperModel;
            m_pPythonWrapperModel = nullptr;
        }
        if (m_pPythonWrapper)
        {
            delete m_pPythonWrapper;
            m_pPythonWrapper = nullptr;
        }
    }
    else
    {
    }
}

bool VehicleFilter::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = false;
    if (pDownstreamFilter)
    {
        ret = __super::ConnectTo(pDownstreamFilter);

        connect(this, SIGNAL(iamStarted()), pDownstreamFilter, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), pDownstreamFilter, SLOT(handleUpstreamStopped()));
        //connect(this, &Filter::imgProcessedReady, pDownstreamFilter, &Filter::handleProcessedFrameCV);
    }
    return ret;
}

void VehicleFilter::DisconnectAll()
{
    __super::DisconnectAll();

    //disconnect(this, &Filter::imgProcessedReady, 0, 0);
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

void VehicleFilter::handleUpstreamStarted()
{
    ++m_startedUpstreamFilters;
    DSG(1, "VehicleFilter::handleUpstreamStarted m_startedUpstreamFilters=%d m_pUpstreamFilterList.size()=%d",
        m_startedUpstreamFilters, m_pUpstreamFilterList.size());
    if (m_startedUpstreamFilters == m_pUpstreamFilterList.size())
    {
        __super::handleUpstreamStarted();


    }
}

void VehicleFilter::handleUpstreamStopped()
{
    --m_startedUpstreamFilters;
    DSG(1, "VehicleFilter::handleUpstreamStopped m_startedUpstreamFilters=%d", m_startedUpstreamFilters);
    if (m_startedUpstreamFilters == 0)
    {
        m_imageList.clear();

        __super::handleUpstreamStopped(); // change filter state and emit signal to next filter
    }
}

void VehicleFilter::handleColorFrameCV(const cv::Mat &frame)
{
    //imshow("handleColorFrameCV", frame);
    //PrintMatInfo(frame);
    //DSG(1, "VehicleFilter::handleColorFrameCV");
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 10)
        {
            DSG(1, "queue m_imageList.size() drop = %d", m_imageList.size());
            cv::Mat image = m_imageList.dequeue();
            image.release();
        }

        m_imageList.enqueue(frame);
    }

    //if (m_readyToEmit)
    {
        if (!m_timer.isActive())
            m_timer.start(0, this);
    }
}

void 
VehicleFilter::FilterOutUnreasonableObject(std::vector<cv::Vec4i> &inVehicleArrary, 
                                           std::vector<cv::Vec4i> &outVehicleArrary,
                                           int boundingW,
                                           int boundingH)
{
    int maxAllowedW = boundingW / 4;
    int maxAllowedH = boundingH / 4;
    bool pass = false;
    //DSG(1, "inVehicleArrary.size()=%d", inVehicleArrary.size());
    for (int i = 0; i < inVehicleArrary.size(); ++i)
    {
        cv::Vec4i rc = inVehicleArrary.at(i);
        int t = rc[0];
        int l = rc[1];
        int b = rc[2];
        int r = rc[3];
        

        if (i < (inVehicleArrary.size() - 1))
        {
            //DSG(1, "GGG");
            pass = true;
            for (int j = i + 1; j < inVehicleArrary.size(); ++j)
            {
                if (i != j)
                {
                    cv::Vec4i rc2 = inVehicleArrary.at(j);
                    int t2 = rc2[0];
                    int l2 = rc2[1];
                    int b2 = rc2[2];
                    int r2 = rc2[3];
                    int total = abs(t - t2) + abs(l - l2) + abs(b - b2) + abs(r - r2);
                    //if ((t >= t2) && (l >= l2) && (b <= b2) && (r <= r2))
                    if (total < 80)
                    {
                        DSG(1, "similar %d %d %d %d %d %d %d %d", t, l, b, r, t2, l2, b2, r2);
                        pass = false;
                        break;
                    }
                }
            }
            if (!pass)
                continue;
            //DSG(1, "GGG 2");
        }

        cv::Vec4i rcDst;
        //cv::Rect rcDst;


        int x = l;
        int y = t;
        int width = r - l;
        int height = b - t;
        int goodW = 0;
        int goodH = 0;
        rcDst[0] = l;
        rcDst[1] = t;
        rcDst[2] = r;
        rcDst[3] = b;
        //rcDst.x = x;
        //rcDst.y = y;
        //rcDst.width = width;
        //rcDst.height = height;
        const float percent = 0.2;
        const float lowBound = 1.0 - percent;
        const float highBound = 1.0 + percent;
        //for 720p
        if ((y >= 0) && (y < 97))
        {
            int iVarsLinW[2] = { 0, 87 };
            float targetLinW = (width / 87.0);
            goodW = Interpolator<int>::Linear(targetLinW, iVarsLinW);

            int iVarsLinH[2] = { 0, 97 };
            float targetLinH = (height / 97.0);
            goodH = Interpolator<int>::Linear(targetLinH, iVarsLinH);

            if ((width >(int)(goodW * lowBound)) && (width < (int)(goodW * highBound)) &&
                (height >(int)(goodH * lowBound)) && (height < (int)(goodH * highBound)))
            {
                outVehicleArrary.push_back(rcDst);
            }
        }
        else if ((y >= 97) && (y < 250))
        {
            int iVarsLinW[2] = { 131, 190 };
            const float targetLinW = (width / 190.0);
            goodW = Interpolator<int>::Linear(targetLinW, iVarsLinW);

            int iVarsLinH[2] = { 151, 257 };
            const float targetLinH = (height / 257.0);
            goodH = Interpolator<int>::Linear(targetLinH, iVarsLinH);

            if ((width >(int)(goodW * lowBound)) && (width < (int)(goodW * highBound)) &&
                (height >(int)(goodH * lowBound)) && (height < (int)(goodH * highBound)))
            {
                outVehicleArrary.push_back(rcDst);
            }
        }
        else if ((y >= 250) && (y < 330))
        {
            int iVarsLinW[2] = { 190, 255 };
            const float targetLinW = (width / 255.0);
            goodW = Interpolator<int>::Linear(targetLinW, iVarsLinW);

            int iVarsLinH[2] = { 257, 390 };
            const float targetLinH = (height / 390.0);
            goodH = Interpolator<int>::Linear(targetLinH, iVarsLinH);

            if ((width >(int)(goodW * lowBound)) && (width < (int)(goodW * highBound)) &&
                (height >(int)(goodH * lowBound)) && (height < (int)(goodH * highBound)))
            {
                outVehicleArrary.push_back(rcDst);
            }
        }
        else //if (y >= 330)
        {
            //float ratio = height / (float)width;
            //if ((ratio > highBound) && (height < 390))
            if ((width >(int)(maxAllowedW * lowBound)) && (width < (int)(maxAllowedW * highBound)) &&
                (height >(int)(maxAllowedH * lowBound)) && (height < (int)(maxAllowedH * highBound)))
            {
                outVehicleArrary.push_back(rcDst);
            }
            //int iVarsLinW[2] = { 255, 255 };
            //const float targetLinW = (width / 255.0);
            //goodW = Interpolator<int>::Linear(targetLinW, iVarsLinW);

            //int iVarsLinH[2] = { 390, 720 };
            //const float targetLinH = (height / 720);
            //goodH = Interpolator<int>::Linear(targetLinH, iVarsLinH);
        }
    }
}

void VehicleFilter::PrepareImageForVehicleDetection(const Mat &matScaledOrgClone)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if (e_vd_model_kneron == pCache->vehicledetection_model)
    {
        if (!m_pPythonWrapper)
            m_pPythonWrapper = new CPythonWrapper();
        DSG(1, "VehicleFilter m_pPythonWrapper=%p", m_pPythonWrapper);
        if (!m_pPythonWrapperModel)
            m_pPythonWrapperModel = new CPythonWrapperModel();
        DSG(1, "VehicleFilter m_pPythonWrapperModel=%p", m_pPythonWrapperModel);
        if (m_pPythonWrapperModel)
        {
            if (0 == m_pPythonWrapperModel->Initialize())
                m_modelLoaded = true;
        }
    }
    else if (e_vd_model_common_cascadeclassifier == pCache->vehicledetection_model)
    {
        QString sPath = QCoreApplication::applicationDirPath().append("/casaa.xml");
        QFile::copy(":/models/opensource/cars.xml", sPath);
        m_cascade.load(sPath.toStdString());
        QFile::remove(sPath);

        cv::cvtColor(matScaledOrgClone, m_matCommonVehicleIn, COLOR_BGR2GRAY);
    }
}


void VehicleFilter::ThresholdCallbackVehicle(int pos, void *userdata)
{
    //DSG(1, "VehicleFilter::ThresholdCallbackVehicle");

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    bool isCandidate = false;
    int baseline = 0;
    int roiLeft, roiRight, roiTop, roiBottom;
    char szText[1024] = { 0 };
    Size textSize;

    //SLanelinesInfo *pLaneLinesInfo;
    //std::vector<cv::Vec4i> *pLaneArray;

    //const Mat *pLaneDetectedImage = pImageProcessing->GetLaneDetectedImage();
    //Mat *pDisplayImage = pImageProcessing->ReassignDisplayImage(*pLaneDetectedImage);
    //DSG(1, "pDisplayImage =%p", pDisplayImage);
    //if (!pDisplayImage) return;

    size_t numsOfCars = 0;

    do {

        Mat mat = m_matScaledImage;

        //vector<Rect> tmpVehicleArrary;

        //m_matDisplay = mat.clone();

        bool checkVehicle = 0;
        //checkVehicle = pImageProcessing->FindDiffBetweenMatrix(*(const_cast<Mat*>(pSrcP)), *(const_cast<Mat*>(pSrc)), pLaneLinesInfo, 0.3);
        //checkVehicle = pImageProcessing->FindDiffBetweenMatrix(pSrcList->head(), *(const_cast<Mat*>(pSrc)), pLaneLinesInfo, 0.3); 
        if ((checkVehicle) || ((0 == pos) || (userdata)))
        {
            m_vehicleArray.clear();
            m_vehicleArray.shrink_to_fit();

            if ((e_vd_model_kneron == pCache->vehicledetection_model) && (m_modelLoaded))
            {
                std::vector<cv::Vec4i> tmpVehicleArrary;
            #if 1
                if (m_pPythonWrapperModel)
                {
                    //std::vector<cv::Vec4i> tmpVehicleArrary2;
                    //PrintMatInfo(mat);
                    //imshow("vehiclemat", mat);
                    if (0 == m_periodCnt)
                    {
                        tmpVehicleArrary = m_pPythonWrapperModel->GetDetectedCars(mat);
                    }
                    else
                    {
                        m_calcTimer.ResetTimer();
                        tmpVehicleArrary = m_pPythonWrapperModel->GetDetectedCars(mat);
                        m_calcTimer.Print(0, "VehicleFilter::ThresholdCallbackVehicle ***");
                    }

                    FilterOutUnreasonableObject(tmpVehicleArrary, m_vehicleArray, mat.cols, mat.rows);
                    //DSG(1, "VehicleFilter::ThresholdCallbackVehicle oldsize=%d newsize=%d",
                    //    tmpVehicleArrary.size(), m_vehicleArray.size());
                    //for (int i = 0; i < m_vehicleArray.size(); ++i)
                    //{
                    //    DSG(1, "VehicleFilter::ThresholdCallbackVehicle (%d) %d %d %d %d",
                    //        i, m_vehicleArray[i][0], m_vehicleArray[i][1], m_vehicleArray[i][2], m_vehicleArray[i][3]);
                    //}
                }
            #else
                static std::vector<cv::Vec4i> tmpVehicleArraryStd;


                if ((m_periodCnt != 0) && ((m_periodCnt % 3) == 0))
                {
                    for (int i = 0; i < tmpVehicleArraryStd.size(); ++i)
                    {
                        tmpVehicleArrary[i][0] = tmpVehicleArraryStd[i][0];
                        tmpVehicleArrary[i][1] = tmpVehicleArraryStd[i][1];
                        tmpVehicleArrary[i][2] = tmpVehicleArraryStd[i][2];
                        tmpVehicleArrary[i][3] = tmpVehicleArraryStd[i][3];
                    }
                }
                else {
                    if (m_pPythonWrapperModel)
                    {
                        //std::vector<cv::Vec4i> tmpVehicleArrary2;
                        //PrintMatInfo(mat);
                        //imshow("vehiclemat", mat);
                        if (0 == m_periodCnt)
                        {
                            tmpVehicleArrary = m_pPythonWrapperModel->GetDetectedCars(mat);
                        }
                        else
                        {
                            m_calcTimer.ResetTimer();
                            tmpVehicleArrary = m_pPythonWrapperModel->GetDetectedCars(mat);
                            m_calcTimer.Print(0, "VehicleFilter::ThresholdCallbackVehicle ***");
                        }

                        for (int i = 0; i < tmpVehicleArraryStd.size(); ++i)
                        {
                            tmpVehicleArraryStd[i][0] = tmpVehicleArrary[i][0];
                            tmpVehicleArraryStd[i][1] = tmpVehicleArrary[i][1];
                            tmpVehicleArraryStd[i][2] = tmpVehicleArrary[i][2];
                            tmpVehicleArraryStd[i][3] = tmpVehicleArrary[i][3];
                        }

                        FilterOutUnreasonableObject(tmpVehicleArrary, m_vehicleArray);
                        DSG(1, "VehicleFilter::ThresholdCallbackVehicle oldsize=%d newsize=%d",
                            tmpVehicleArrary.size(), m_vehicleArray.size());
                    }
                }

                ++m_periodCnt;
            #endif
            }
            else if (e_vd_model_common_cascadeclassifier == pCache->vehicledetection_model)
            {
                cv::cvtColor(mat, m_matCommonVehicleIn, COLOR_BGR2GRAY);

                vector<Rect> tmpVehicleArrary;
                m_cascade.detectMultiScale(m_matCommonVehicleIn,
                    tmpVehicleArrary,
                    double(m_classifierScaleFactor + 11) / 10.0,
                    m_classifierMinNeighbors,
                    (1 << m_classifierFlags),// 0 | CV_HAAR_SCALE_IMAGE,
                    Size(m_classifierMinSize, m_classifierMinSize),
                    Size(m_classifierMaxSize, m_classifierMaxSize));

                for (size_t idx = 0; idx < tmpVehicleArrary.size(); ++idx)
                {
                    m_vehicleArray[idx][0] = tmpVehicleArrary[idx].x;
                    m_vehicleArray[idx][1] = tmpVehicleArrary[idx].y;
                    m_vehicleArray[idx][2] = tmpVehicleArrary[idx].x + tmpVehicleArrary[idx].width;
                    m_vehicleArray[idx][3] = tmpVehicleArrary[idx].y + tmpVehicleArrary[idx].height;
                }
            }

            //DSG(1, "m_vehicleArray.size=%d", m_vehicleArray);
            //emit updateVehicleInfo(m_vehicleArray);
        }
    } while (0);

    
    //numsOfCars = m_vehicleArray.size();
}

void VehicleFilter::DoImageProcessing(const cv::Mat &matImage)
{
    //DSG(1, "VehicleFilter::DoImageProcessing");

    if (0 == m_currentFrame)
        PrepareImageForVehicleDetection(matImage);

    m_matScaledImage = matImage;

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if (pCache->vehicledetection_enabled)
    {
        //m_calcTimer.ResetTimer();
        ThresholdCallbackVehicle(0, 0);
        //m_calcTimer.Print(0, "VehicleFilter::DoImageProcessing ThresholdCallback");

        emit updateVehicleInfo(m_vehicleArray);
        //emit cvColorReady(m_matHLSOut);
    }
    //else
    //    emit cvColorReady(matImage);
}

void VehicleFilter::timerEvent(QTimerEvent *event)
{
    //qDebug("VehicleFilter::timerEvent (%s) ev->timerId()=%d m_nTimerID=%d", m_name, event->timerId(), m_timer.timerId());
    if (event->timerId() != m_timer.timerId()) return;// { QObject::timerEvent(event); return; }
    //if (event->timerId() != m_timer.m_nTimerID) return;
    //qDebug("FilterThread::timerEvent (%s) 1", m_name);
    if (e_component_state_started != m_state)
        return;

    
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 0)
        {
            cv::Mat image = m_imageList.dequeue();
            m_frame = image.clone();
            DoImageProcessing(m_frame);
            image.release();
        }
    }

    //qDebug("FilterThread::timerEvent (%s) m_frame=%d %d", m_name, m_frame.cols, m_frame.rows);
    
    m_timer.stop();
    ++m_currentFrame;
    if ((m_currentFrame % 30) == 0)
        DSG(1, "VehicleFilter::DoImageProcessing m_currentFrame=%d", m_currentFrame);

    //Sleep(1);
}
