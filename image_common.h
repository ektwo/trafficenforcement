#ifndef IMAGE_COMMON_H
#define IMAGE_COMMON_H

#include <QString>
#include <opencv2/opencv.hpp>
#include <QImage>

static const double k_angel15 = 0.261799;
static const double k_angel30 = 0.523599;
static const double k_angel45 = 0.785398;
static const double k_angel60 = 1.047198;
static const double k_angel120 = 2.094395;
static const double k_angel135 = 2.356194;
static const double k_angel150 = 2.617994;


typedef struct S_SlidingWindow_Info_Tag {
	int min_x;
	int max_x;
	int centroid;
	int y;

} SSlidingWindowInfo;

typedef struct S_Lanelines_Info_Tag {
	std::vector<bool> in_roi;
	std::vector<cv::Vec4i> array;
	double offset_x;
	double offset_y;
	std::vector<cv::Point2f> sliding_win_tl;
	std::vector<cv::Point2f> sliding_win_br;
	//std::vector<cv::Rect> sliding_win_region;
	std::vector<std::vector<SSlidingWindowInfo>> sliding_win_info_list;
} SLanelinesInfo;

static void matDeleter2(void* mat) { delete static_cast<cv::Mat*>(mat); }

cv::Mat CVImageOpen(const QString &fileName);
void CVImageResize(cv::Mat& src, cv::Mat &dst, double fx, double fy);
void CVImageResize2ByWH(const cv::Mat& src, cv::Mat &dst, int width, int height);
void CVGetWindowRect( const char* name, int &x, int &y, int &width, int &height);
void CVRemoveLinesOfInconsistentOrientations(std::vector<cv::Vec4i> &lines, int width, int height);
void CVRemoveLinesOfInconsistentOrientationsAdvanced(
	std::vector<cv::Vec4f> &lines, const cv::Mat &orientations, double percentage, double delta);
void CVFindDiffBetweenMatrix(cv::Mat &matScaled);

void OpenAndReadSlidingWindowsFile(QString swInfoFileName);
void OpenAndWriteSlidingWindowsFile(QString swInfoFileName);
void SaveSlidingWindowsNumsOfLines(size_t numsOfLines);
void CloseAndReadSlidingWindowsFile();
void CloseAndWriteSlidingWindowsFile();
void AddSlidingWindowsContent(
	cv::Mat &src, std::vector<SSlidingWindowInfo> &vWindowInfo, int minX, int maxX, int centroid, int y);
void LoadSlidingWindows(std::vector<std::vector<SSlidingWindowInfo>> &slidingWindowInfo);
void CVFindRect(int x1, int y1, int x2, int y2, int offset, int boundX, int boundY, cv::Rect &rect);
bool CVCheckPointInROI(std::vector<cv::Point> &contours, cv::Point2f &pt);

void ShowGrayHistImage(const cv::Mat &src);
void ShowColorfulHistImage(const cv::Mat &src, int width, int height);
void testframe(const cv::Mat &src);

void PrintMatInfo(const cv::Mat &src);

double CrossProduct(cv::Point v1, cv::Point v2);
bool GetIntersectionPoint(cv::Point a1, cv::Point a2, cv::Point b1, cv::Point b2, cv::Point & intPnt);
cv::Point2f GetCrossPointF(cv::Vec4i LineA, cv::Vec4i LineB);
cv::Point2i GetCrossPoint(cv::Vec4i LineA, cv::Vec4i LineB);

QImage cvMat2QImage(const cv::Mat& mat);
cv::Mat QImage2cvMat(QImage image);

#endif // IMAGE_COMMON_H
