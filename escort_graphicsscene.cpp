#include "escort_graphicsscene.h"
#include <QGraphicsItem>
#include <QGraphicsView>
#include "common.h"
#include "image_cvtapi.h"
#include "image_common.h"

using namespace cv;

EscortGraphicsScene::EscortGraphicsScene(QObject* parent)
	: QGraphicsScene(parent)
	, m_sceneMode(NoMode)
    , m_pGraph(nullptr)
	, m_imagePixmap(nullptr)
{

}

EscortGraphicsScene::~EscortGraphicsScene()
{

}

void EscortGraphicsScene::SetOwner(BaseGraph *pGraph)
{
    if (pGraph)
    {
        m_pGraph = pGraph;
        connect(this, SIGNAL(videoRenderStarted()), pGraph, SLOT(handleAllComponentsAreRunning()));
        connect(this, SIGNAL(videoRenderStopped()), pGraph, SLOT(handleAllComponentsAreStopped()));
    }
    else
    {
        m_pGraph = nullptr;
        disconnect(this, SIGNAL(videoRenderStarted()), 0, 0);
        disconnect(this, SIGNAL(videoRenderStopped()), 0, 0);
    }
}

void EscortGraphicsScene::SetMode(Mode mode) 
{
	m_sceneMode = mode;
	QGraphicsView::DragMode vMode =
		QGraphicsView::NoDrag;

	if (mode == DrawLine) {
		MakeItemsControllable(false);
		vMode = QGraphicsView::NoDrag;
	}
	else if (mode == SelectObject) {
		MakeItemsControllable(true);
		vMode = QGraphicsView::RubberBandDrag;
	}

	QGraphicsView* mView = views().at(0);
	if (mView)
		mView->setDragMode(vMode);
}

void EscortGraphicsScene::handleUpstreamStarted()
{
    qDebug("EscortGraphicsScene::handleUpstreamStarted");
    emit videoRenderStarted();
}

void EscortGraphicsScene::handleUpstreamStopped()
{
    qDebug("EscortGraphicsScene::handleUpstreamStopped");
    emit videoRenderStopped();
}

#include "filter.h"
void EscortGraphicsScene::handleUpdateViewCV(const cv::Mat &matImage)
{
    
    Filter *pFilter = static_cast<Filter*>(sender());
    //DSG(1, "EscortGraphicsScene pFilter->m_name=%s", pFilter->m_name);
	//qDebug("handleUpdateViewCV aa %d %d %d", matImage.type(), matImage.cols, matImage.rows);
	//QImage image = QImage(CVS::CVMatToQImage(matImage));
	//QImage scaledImage;
	Mat matScaled;

	//qDebug("handleUpdateViewCV 2 %d %f %d %f", image.width(), this->sceneRect().width(), image.height(), this->sceneRect().height());
	//qDebug("handleUpdateViewCV 2 %d %f %d %f", image.width(), this->width(), image.height(), this->height());
	//qDebug("handleUpdateViewCV bb %d %d ", (int)(this->sceneRect().width()), (int)(this->sceneRect().height()));
	if ((matImage.cols != (int)(this->sceneRect().width())) || (matImage.rows != (int)(this->sceneRect().height())))
	{
		//qDebug("XX handleUpdateViewCV");
		CVImageResize2ByWH(matImage, matScaled, (int)(this->sceneRect().width()), (int)(this->sceneRect().height()));
		//qDebug("XX handleUpdateViewCV 2");
		//scaledImage = image.scaled(
		//	this->sceneRect().width(), this->sceneRect().height(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
			//this->width(), this->height(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
		//const QImage scaledImage(matImage.data, matImage.cols, matImage.rows, matImage.step,
		//	QImage::Format_RGB888, &matDeleter2, new cv::Mat(matImage));
	}
	else
		matScaled = matImage;
	//qDebug("XX handleUpdateViewCV 3");
    //DSG(1, "EscortGraphicsScene::handleUpdateViewCV");
    //PrintMatInfo(matScaled);
	QImage image = QImage(CVS::CVMatToQImage(matScaled));
	//qDebug("XX handleUpdateViewCV 4");
	//const QImage image(matScaled.data, matScaled.cols, matScaled.rows, matScaled.step,
	//	QImage::Format_RGB888, &matDeleter2, new cv::Mat(matScaled));
	//qDebug("WWWWWWWW mat %d %d %d", matImage.type(), matImage.cols, matImage.rows);
	//qDebug("WWWWWWWW bb %d %d", this->m_rect.width(), this->m_rect.height());
	if (!m_imagePixmap)
	{
		m_imagePixmap = addPixmap(QPixmap::fromImage(image));
	}
	else
	{
		m_imagePixmap->setPixmap(QPixmap::fromImage(image));
		//ui->imageView_center->setScene(&m_imageScene);
	}
}

void EscortGraphicsScene::MakeItemsControllable(bool areControllable) {
	foreach(QGraphicsItem* item, items()) {
		item->setFlag(QGraphicsItem::ItemIsSelectable,
			areControllable);
		item->setFlag(QGraphicsItem::ItemIsMovable,
			areControllable);
	}
}