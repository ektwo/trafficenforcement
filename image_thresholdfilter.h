#ifndef IMAGE_THREADHOLDFILTER_H
#define IMAGE_THREADHOLDFILTER_H

#include <opencv2/opencv.hpp>


cv::Mat ImageThresholdFilter(cv::Mat src);
cv::Mat ImageThresholdFilter2(cv::Mat src);

#endif // IMAGE_THREADHOLDFILTER_H
