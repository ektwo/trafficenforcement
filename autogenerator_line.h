#ifndef AUTOGENERATOR_LINE_H
#define AUTOGENERATOR_LINE_H

#include <QPointF>
#include <QLineF>
#include <QRectF>
#include <QtGlobal>
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
#   include <QRandomGenerator>
#else
//#include <QGlobal.h>
#include <QTime>
#endif

class AutoGeneratorLine
{
public:
    enum { e_weight_more_lower, e_weight_more_higher };
    enum { e_weight_rotation_none, e_weight_rotation_clockwise, e_weight_rotation_counterclockwise};

private:
    int m_count;
    int m_weightStrategyOfX1;
    int m_weightStrategyOfX2;
    int m_weightStrategyOfY1;
    int m_weightStrategyOfY2;
    int m_weightStrategyOfRotation;
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    QRandomGenerator *m_generator;
#endif
    QRectF m_rect;

public:
    AutoGeneratorLine();

    void SetBoundary(QRectF rect);
    QLineF genLine();
    QLineF tuneLine(QLineF line);
};



#endif // AUTOGENERATOR_LINE_H
