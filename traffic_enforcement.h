#ifndef TRAFFIC_ENFORCEMENT_H
#define TRAFFIC_ENFORCEMENT_H

#include <QSettings>
#include <QApplication>
#include <QRectF>
#include <QString>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "singleton.h"
#include "common.h"
#include "image_common.h"

#define TEST_TIME

#define DIM(_x_) (sizeof(_x_) / sizeof(_x_[0]))

#define GRAPHICSVIEW_SIZE_DEPENDED_ON_IMAGE

//#define USE_HSV_TO_COMPARE
#define USE_SCALED_TO_COMPARE

#define MASK_IS_OUTPUT

//#define FINDLINE_BY_DIV2PIC
#define UPDATE_SLIDINGWINDOW_CONTENT
#define SLIDING_WINDOWS_BE_ERASE
#define DRAW_LANELINE_ON_MAT_TO_TEST
#define DEMO_CAMERA
//#define USE_KNERON_VEHICLE_DETECTION
#define CHECK_LANDLINE
#define OUTPUT_DEBUG_IMG
#define FASTEST_CAMERA_INIT
#define DO_PRESCALE
#define DOWNSCALE_WHEN_LANDETECTION

#define MAIN_SCREEN_FROM_SOURCE 0
#define MAIN_SCREEN_FROM_MIXER  1
#define MAIN_SCREEN_FROM        MAIN_SCREEN_FROM_SOURCE

//#define CONNECT_SOURCE_LANEFILTER
//#define BACKUP_SWINFOFILE
#ifdef BACKUP_SWINFOFILE
    static const char *k_swinfo_filename = "swinfo.txt";
#endif

//#define UNLOCK_TEMPORARILY_0714_1
    typedef void(*FnThresholdCallback)(int, void*);

static const int32_t k_start_roiline_zvalue = 1;
static const int32_t k_start_laneline_zvalue = k_start_roiline_zvalue + 4;
static const int32_t k_start_vehicle_zvalue = k_start_laneline_zvalue + 4;



static const bool g_laneInfoFromPrevious = true;

static const int k_sliding_windows_width = 13;
static const int k_sliding_windows_radius = k_sliding_windows_width / 2;

static const quint32 k_engineering_default_nums_of_lines = 1;
static const quint32 k_engineering_default_nums_of_rects = 1;
static const int k_border_size = 4;
static const int k_tolerance_min = 10;
static const int k_tolerance_max = 50;

static const qreal k_min_ratio_of_vehicle = 0.5F;
static const qreal k_min_lengh_of_line = 48;
static const qreal k_min_width_of_vehicle = 32;
static const qreal k_min_height_of_vehicle = 32;

static const int32_t k_roi_button_size = 16;
static const int32_t k_offset_button = k_roi_button_size / 2;
static const int k_roiline_clr_r = 127;
static const int k_roiline_clr_g = 127;
static const int k_roiline_clr_b = 255;
static const int k_roiline_width = 4;
static const float k_roiline_opacity = 0.7F;


static const int k_laneline_clr_r = 250;//0;
static const int k_laneline_clr_g = 85;//255;
static const int k_laneline_clr_b = 135;
static const int k_laneline_width = k_sliding_windows_width / 2;
static const float k_laneline_opacity = 0.6F;
static const int k_cluster_distance = 50;

static const char *winOrigin = "Origin";
static const char *winROI = "ROI";
static const char *strWinROIGray = "Gray";
static const char *strWinROIGrayIPM = "IPM";
static const char *strWinROIGrayIPMGaussian = "Gaussian";
static const char *strWinROIGrayIPMGaussianHist = "Hist";
static const char *strWinROIGrayIPMGaussianHistThreshold = "Threshold";
static const char *strWinROIGrayIPMGaussianHistThreshold2 = "Threshold2";
static const char *strWinROIGrayIPM32 = "IPM-32Bits";



static const int k_car_font_face = cv::FONT_HERSHEY_SIMPLEX;
static const double k_car_font_scale_no = 0.5F;// 0.7;
static const double k_car_font_scale_detailed = 0.5F;
static const int k_car_thickness_no = 1;
static const int k_car_thickness_detailed = 1;
static const cv::Scalar k_car_color_no = CV_RGB(230, 230, 230);
static const cv::Scalar k_car_color_detailed = CV_RGB(255, 0, 0);



// QSettings
// Special
static const char *k_str_roi_horizontal_pos1 = "ROI_Horizontal_Pos1";
static const char *k_str_roi_horizontal_pos2 = "ROI_Horizontal_Pos2";
static const char *k_str_roi_location_top1 = "ROI_Location_Top1";
static const char *k_str_roi_location_top2 = "ROI_Location_Top2";
static const char *k_str_roi_location_bottom1 = "ROI_Location_Bottom1";
static const char *k_str_roi_location_bottom2 = "ROI_Location_Bottom2";

static const char *k_str_tools_roi_enabled = "Tools_ROI_Enabled";
static const char *k_str_tools_video_stability_enabled = "Tools_VideoStability_Enabled";
static const char *k_str_tools_opt_ld_cycletime_idx = "Tools_LaneDetection_CycleTime_Index";
static const char *k_str_tools_opt_vd_cycletime_idx = "Tools_VehicleDetection_CycleTime_Index";
static const char *k_str_tools_opt_vd_1pass_diffthreshold_idx = "Tools_VehicleDetection_1Pass_DiffThreshold_Index";
static const char *k_str_tools_opt_vd_model_idx = "Tools_VehicleDetection_Model_Index";

static const char *k_str_view_roiline = "View_ROILine";
static const char *k_str_view_videoassetmanagement = "View_VideoAssetManagement";
static const char *k_str_view_gpsstation = "View_GPSStation";

static const char *k_str_dwl_tolerance = "DWL_Tolerance";


static const char *k_str_lanedetection_manual_enabled = "LaneDetection_ManualEnabled";
static const char *k_str_lanedetection_manual_line1_pt1_x = "lanedetection_manual_line1_pt1_x";
static const char *k_str_lanedetection_manual_line1_pt1_y = "lanedetection_manual_line1_pt1_y";
static const char *k_str_lanedetection_manual_line1_pt2_x = "lanedetection_manual_line1_pt2_x";
static const char *k_str_lanedetection_manual_line1_pt2_y = "lanedetection_manual_line1_pt2_y";
static const char *k_str_lanedetection_manual_line2_pt1_x = "lanedetection_manual_line2_pt1_x";
static const char *k_str_lanedetection_manual_line2_pt1_y = "lanedetection_manual_line2_pt1_y";
static const char *k_str_lanedetection_manual_line2_pt2_x = "lanedetection_manual_line2_pt2_x";
static const char *k_str_lanedetection_manual_line2_pt2_y = "lanedetection_manual_line2_pt2_y";
static const char *k_str_lanedetection_max_fps = "lanedetection_max_fps";
static const char *k_str_videofile_fps = "videofile_fps";
static const char *k_str_workspace_path = "k_str_workspace_path";


static const char *k_str_lanedetection_enabled = "LaneDetection_Enabled";
static const char *k_str_vehicledetection_enabled = "VehicleDetection_Enabled";

static const char *k_str_engineeringmode = "EngineeringMode";
static const char *k_str_engineeringmode_enabled = "EngineeringMode_Enabled";
static const char *k_str_em_dwl_randomly_generate_lanelines = "EngineeringMode_DWL_Randomly_Generate_lanelines";
static const char *k_str_showtrackbar_enabled = "EngineeringMode_ShowTrackbar_Enabled";
static const char *k_str_em_sm_always_backup_info_of_slidingwindows = "EngineeringMode_AlwaysBackupInfoOfSlidingWindows";
static const char *k_str_em_sm_use_slidingwindows_info_file = "EngineeringMode_UseSlidingWindowsInfoFile";

static const char *k_str_emdialog_staticmode = "StaticMode";
static const char *k_str_emdialog_sm_baseimagefile = "BaseImageFile";
static const char *k_str_emdialog_sm_slidingwindowsfile = "SlidingWindowsFile";
static const char *k_str_emdialog_sm_baseimagefilepath = "BaseImageFilePath";
static const char *k_str_emdialog_sm_slidingwindowsfilepath = "SlidingWindowsFilePath";

static const char *k_str_emdialog_lm_lld_mode = "Laneline_Detection_Mode";



static const char *k_str_unknown_filter_name = "unknown";
static const char *k_str_class_source_name = "source";
static const char *k_str_class_transform_name = "transform";
static const char *k_str_class_render_name = "render";
static const char *k_str_video_file_filter_name = "videofile";
static const char *k_str_video_capture_filter_name = "videocapture";
static const char *k_str_hls_filter_name = "hls";
static const char *k_str_hsv_filter_name = "hsv";
static const char *k_str_sobel_filter_name = "sobel";
static const char *k_str_lane_filter_name = "lane";
static const char *k_str_vehicle_filter_name = "vehicle";
static const char *k_str_mixer_filter_name = "mixer";

typedef enum E_DeviceType_Tag {
    e_devicetype_unknown = 0,
    e_devicetype_camera_common,
    e_devicetype_camera_depth_etron
} EDeviceType;

typedef enum E_MovingApproach_Tag {
    e_move_randomly = 0,
} EMovingApproach;

typedef enum E_Operation_Mode_Tag {
    e_operation_static = 0,
    e_operation_live

} EOperationMode;

typedef enum E_ImageProcessing_State_Tag {
    e_imageprocessing_state_stopped = 0,
    e_imageprocessing_state_started,
} EImageProcessingState;

typedef enum E_ImageProcessing_Approach_Tag {
    e_imageprocessing_common_preview = 0,
    e_imageprocessing_grabber_preview,
    e_imageprocessing_grabber_capture,
    e_imageprocessing_etron_preview,
    e_imageprocessing_etron_capture
} EImageProcessingApproach;

typedef enum E_Component_State_Tag {
    e_component_state_idle = 0,
    e_component_state_initialized,
    e_component_state_stopped,
    e_component_state_started,
} EComponentState;

typedef enum E_LaneDetection_Approach_Tag {
    e_lanedetection_hls = 0,
    e_lanedetection_hsv,
    e_lanedetection_sobel,
    e_lanedetection_canny,
    e_lanedetection_mix = 10,
    e_lanedetection_mix_hls_hsv_canny,
    e_lanedetection_mix_hls_hsv_sobel_canny,

} ELaneDetectionApproach;

typedef enum E_LaneDetection_Mask_Mix_Mode_Tag {

    mask_hls_and = 0x01,
    mask_hls_or = 0x02,
    mask_hsv_and = 0x10,
    mask_hsv_or = 0x20,
    mask_sobel_and = 0x100,
    mask_sobel_or = 0x200,
    
    mask_hls_and_hsv_and_sobel = mask_hls_and | mask_hsv_and | mask_sobel_and,
    mask_hls_and_hsv_or_sobel = mask_hls_and | mask_hsv_and | mask_sobel_or,
    mask_hls_or_hsv_or_sobel = mask_hls_or | mask_hsv_or | mask_sobel_or,
    //mask_hls_and_hsv_or_gray_sobel = mask_hls_and | mask_hsv_and | mask_sobel_or,
    mask_gray_equalizehist_only = 0x1000,
} ELaneDetectionMaskMixMode;

typedef enum E_VehicleDetection_CycleTime_Tag {
    e_vehicledetection_cycletime_always = 0,
    e_vehicledetection_cycletime_after_onepass,
    e_vehicledetection_cycletime_10s,
    e_vehicledetection_cycletime_manual
} EVehicleDetectionCycleTime;

typedef enum e_VehicleDetection_Model_Tag {
    e_vd_model_kneron = 0,
    e_vd_model_common_cascadeclassifier = 1,

} EVehicleDetectionModel;

typedef enum E_LaneDetection_CalibrationApproach_Tag {
    e_lanedetection_calibration_10fps = 0,
    e_lanedetection_calibration_30fps,
    e_lanedetection_calibration_150fps,
} ELaneDetectionCalibrationApproach;

typedef enum E_LaneDetection_CycleTime_Tag {
    e_lanedetection_cycletime_always = 0,
    e_lanedetection_cycletime_10s,
    e_lanedetection_cycletime_10m,
    e_lanedetection_cycletime_6hr,
    //e_lanedetection_cycletime_manual
} ELaneDetectionCycleTime;

typedef enum E_VehicleDetection_Level_Tag {
    e_vd_level_safe = 0x0,
    e_vd_level_inroi = 0x1,
    e_vd_level_crosslane = 0x2
} EVehicleDetectionLevel;

typedef struct S_LaneDetection_CalibrationApproach_Tag {
    ELaneDetectionCalibrationApproach approach;
    int max_fps;
} SLaneDetectionCalibrationApproach;

typedef struct S_LaneDetection_CycleTime_Tag {
    ELaneDetectionCycleTime mode;
    QString description;
} SLaneDetectionCycleTime;

typedef struct S_Vehicle_Detection_CycleTime_Tag {
    EVehicleDetectionCycleTime mode;
    QString description;
} SVehicleDetectionCycleTime;

typedef struct S_Lane_Detection_Info_Tag {
    cv::Vec4i cv_array;
} SLaneDetectionInfo;

typedef struct S_Vehicle_Detection_Info_Tag {
    EVehicleDetectionLevel level;
    cv::Vec4i cv_array;
} SVehicleDetectionInfo;

typedef struct S_Camera_ImageProcessing_Config_Tag {
    int32_t owner_id;
    int32_t want_width;
    int32_t want_height;

} SCameraImageProcessingConfig;

static const SLaneDetectionCalibrationApproach k_lanedetection_k_approach[] = {
    { e_lanedetection_calibration_10fps, 10 },
    { e_lanedetection_calibration_30fps, 30 },
    { e_lanedetection_calibration_150fps, 150 },
//{ e_lanedetection_cycletime_manual, "manual" },
};

static const SLaneDetectionCycleTime k_lanedetection_cycletime[] = {
    { e_lanedetection_cycletime_always, "always" },
    { e_lanedetection_cycletime_10s, "10 seconds" },
    { e_lanedetection_cycletime_10m, "10 minutes" },
    { e_lanedetection_cycletime_6hr, "6 hours" },
    //{ e_lanedetection_cycletime_manual, "manual" },
};

static const SVehicleDetectionCycleTime k_vehicledetection_cycletime[] = {
    { e_vehicledetection_cycletime_always, "always" },
    { e_vehicledetection_cycletime_after_onepass, "after one pass" },
    { e_vehicledetection_cycletime_10s, "10 sec" },
    { e_vehicledetection_cycletime_manual, "manual" },
};


#if QT_VERSION < QT_VERSION_CHECK(5, 10, 0)
#include <QTime>
static int GetBounded(int low, int high)
{
    QTime time = QTime::currentTime();

    qsrand(time.msec());
    return (low + qrand() % ((high + 1) - low));
}
#endif


namespace TrafficEnforcement {

    class GlobalCacheData : public QObject
    {
        Q_OBJECT
    public:
        GlobalCacheData() 
            : lanedetection_paused(false)
            , operation_mode(e_operation_live)
            , lanedetection_maskmix_mode(mask_hls_and_hsv_and_sobel)
            , lanedetection_approach(e_lanedetection_mix_hls_hsv_sobel_canny)
            , lanedetection_k_approach(e_lanedetection_calibration_10fps)
            , calibration_counter(0)
            , max_fps(5)//k_lanedetection_k_approach[lanedetection_k_approach].max_fps)
        {
            application_dir = QApplication::applicationDirPath();
            settings_file_path = application_dir + "/TrafficEnforcement.ini";
        }
        void InitParameters()
        {
            QString str;

            QSettings settings(settings_file_path, QSettings::IniFormat);
            settings.setIniCodec("UTF-8");

            lanedetection_enabled = settings.value(QString(k_str_lanedetection_enabled), true).toBool();
            vehicledetection_enabled = settings.value(QString(k_str_vehicledetection_enabled), true).toBool();

            roi_enabled = settings.value(QString(k_str_tools_roi_enabled), true).toBool();
            video_stability_enabled = settings.value(QString(k_str_tools_video_stability_enabled), false).toBool();
            showtrackbar_enabled = settings.value(QString(k_str_showtrackbar_enabled), false).toBool();
            engineeringmode_enabled = settings.value(QString(k_str_engineeringmode_enabled), false).toBool();

            dwl_tolerance = settings.value(QString(k_str_dwl_tolerance), 25).toInt();
            vd_1pass_diffthreshold_idx = settings.value(QString(k_str_tools_opt_vd_1pass_diffthreshold_idx), 30).toInt();


            vehicledetection_model = 
            static_cast<EVehicleDetectionModel>(settings.value(QString(k_str_tools_opt_vd_model_idx), 0).toInt());

            lanedetecion_cycletime =
            k_lanedetection_cycletime[settings.value(QString(k_str_tools_opt_ld_cycletime_idx), 0).toInt()].mode;
            vehicledetecion_cycletime =
            k_vehicledetection_cycletime[settings.value(QString(k_str_tools_opt_vd_cycletime_idx), 0).toInt()].mode;

            lanedetection_manual_enabled = settings.value(QString(k_str_lanedetection_manual_enabled), false).toBool();
            dwl_tolerance = settings.value(QString(k_str_dwl_tolerance), 25).toInt();
            
            lanedetection_manual_enabled = settings.value(QString(k_str_lanedetection_manual_enabled), false).toBool();
            manual_leftlane[0] = settings.value(QString(k_str_lanedetection_manual_line1_pt1_x), 838).toInt();
            manual_leftlane[1] = settings.value(QString(k_str_lanedetection_manual_line1_pt1_y), 20).toInt();
            manual_leftlane[2] = settings.value(QString(k_str_lanedetection_manual_line1_pt2_x), 810).toInt();
            manual_leftlane[3] = settings.value(QString(k_str_lanedetection_manual_line1_pt2_y), 710).toInt();
            manual_rightlane[0] = settings.value(QString(k_str_lanedetection_manual_line2_pt1_x), 980).toInt();
            manual_rightlane[1] = settings.value(QString(k_str_lanedetection_manual_line2_pt1_y), 20).toInt();
            manual_rightlane[2] = settings.value(QString(k_str_lanedetection_manual_line2_pt2_x), 1208).toInt();
            manual_rightlane[3] = settings.value(QString(k_str_lanedetection_manual_line2_pt2_y), 710).toInt();
            max_fps = settings.value(QString(k_str_lanedetection_max_fps), 5).toInt();
            fps = settings.value(QString(k_str_videofile_fps), 100).toInt();
            workspace_path = settings.value(QString(k_str_workspace_path), QString("C:\\")).toString();
            

        }

        bool CheckLaneDetectionAndCalibration(int currentFrameIdx)
        {
            bool ret = false;
            //DSG(1, "CheckLaneDetectionAndCalibration");
            if (e_lanedetection_cycletime_always == lanedetecion_cycletime)
            {
                DSG(1, "e_lanedetection_cycletime_always");
                lanedetection_paused = false;
                ret = true;
            }
            else if (e_lanedetection_cycletime_10s == lanedetecion_cycletime)
            {
                if (0 == currentFrameIdx)
                {
                    DSG(1, "e_lanedetection_cycletime_10s m_currentFrame=0");
                    m_timerSlice.start();
                    lanedetection_paused = false;
                    ret = true;
                }
                else if (m_timerSlice.hasExpired(10000))
                {
                    DSG(1, "hasExpired(10000)");
                    m_timerSlice.restart();
                    lanedetection_paused = false;
                    ret = true;
                }
            }
            else if (e_lanedetection_cycletime_10m == lanedetecion_cycletime)
            {
                if (0 == currentFrameIdx)
                {
                    m_timerSlice.start();
                    lanedetection_paused = false;
                    ret = true;
                }
                else if (m_timerSlice.hasExpired(600000))
                {
                    DSG(1, "hasExpired(600000)");
                    m_timerSlice.restart();
                    lanedetection_paused = false;
                    ret = true;
                }
            }
            else if (e_lanedetection_cycletime_6hr == lanedetecion_cycletime)
            {
                if (0 == currentFrameIdx)
                {
                    m_timerSlice.start();
                    lanedetection_paused = false;
                    ret = true;
                }
                else if (m_timerSlice.hasExpired(21600000))
                {
                    DSG(1, "hasExpired(21600000)");
                    m_timerSlice.restart();
                    lanedetection_paused = false;
                    ret = true;
                }
            }
            if (!ret)
            {
                //if (calibration_counter == max_fps)
                //{
                //    calibration_counter = 0;
                //    lanedetection_paused = true;
                //}
            }

            return ret;
        }


        void InitROILineCoordinate(int borderWidth, int borderHeight)
        {
            QSettings settings(settings_file_path, QSettings::IniFormat);
            settings.setIniCodec("UTF-8");

            int valY1 = 
                settings.value(QString(k_str_roi_horizontal_pos1), (borderHeight >> 2)).toInt();

            roi_linelist[0][0] = 0;
            roi_linelist[0][1] = valY1;
            roi_linelist[0][2] = borderWidth;
            roi_linelist[0][3] = valY1;

            int valY2 =
                settings.value(QString(k_str_roi_horizontal_pos2), (borderHeight * 0.75)).toInt();

            roi_linelist[1][0] = 0;
            roi_linelist[1][1] = valY2;
            roi_linelist[1][2] = borderWidth;
            roi_linelist[1][3] = valY2;

            int valXTop1 =
                settings.value(QString(k_str_roi_location_top1), (borderWidth >> 2)).toInt();
            int valXBtm1 =
                settings.value(QString(k_str_roi_location_bottom1), (borderWidth >> 2)).toInt();

            roi_linelist[2][0] = valXTop1;
            roi_linelist[2][1] = 0;
            roi_linelist[2][2] = valXBtm1;
            roi_linelist[2][3] = borderHeight;

            int valVerTop2 =
                settings.value(QString(k_str_roi_location_top2), (borderWidth * 0.75)).toInt();
            int valVerBtm2 =
                settings.value(QString(k_str_roi_location_bottom2), (borderWidth * 0.75)).toInt();
            roi_linelist[3][0] = valVerTop2;
            roi_linelist[3][1] = 0;
            roi_linelist[3][2] = valVerBtm2;
            roi_linelist[3][3] = borderHeight;
        }

        void UpdateROILineCoordinate(cv::Vec4i line1, cv::Vec4i line2, cv::Vec4i line3, cv::Vec4i line4)
        {
            roi_linelist[0] = line1;
            roi_linelist[1] = line2;
            roi_linelist[2] = line3;
            roi_linelist[3] = line4;

            cv::Point pt0;
            GetIntersectionPoint(
                cv::Point(roi_linelist[0][0], roi_linelist[0][1]), cv::Point(roi_linelist[0][2], roi_linelist[0][3]),
                cv::Point(roi_linelist[2][0], roi_linelist[2][1]), cv::Point(roi_linelist[2][2], roi_linelist[2][3]),
                pt0); //roi_ptlist[0]);
            roi_ptlist.push_back(pt0);

            cv::Point pt1;
            GetIntersectionPoint(
                cv::Point(roi_linelist[0][0], roi_linelist[0][1]), cv::Point(roi_linelist[0][2], roi_linelist[0][3]),
                cv::Point(roi_linelist[3][0], roi_linelist[3][1]), cv::Point(roi_linelist[3][2], roi_linelist[3][3]), 
                pt1); //roi_ptlist[1]);
            roi_ptlist.push_back(pt1);

            cv::Point pt2;
            GetIntersectionPoint(
                cv::Point(roi_linelist[1][0], roi_linelist[1][1]), cv::Point(roi_linelist[1][2], roi_linelist[1][3]),
                cv::Point(roi_linelist[3][0], roi_linelist[3][1]), cv::Point(roi_linelist[3][2], roi_linelist[3][3]),
                pt2); //roi_ptlist[2]);
            roi_ptlist.push_back(pt2);

            cv::Point pt3;
            GetIntersectionPoint(
                cv::Point(roi_linelist[1][0], roi_linelist[1][1]), cv::Point(roi_linelist[1][2], roi_linelist[1][3]),
                cv::Point(roi_linelist[2][0], roi_linelist[2][1]), cv::Point(roi_linelist[2][2], roi_linelist[2][3]),
                pt3); //roi_ptlist[3]);
            roi_ptlist.push_back(pt3);
        }

        static GlobalCacheData* instance()
        {
            return Singleton<GlobalCacheData>::instance(GlobalCacheData::createInstance);
        }

    public slots:


    public:
        EOperationMode operation_mode;
        ELaneDetectionMaskMixMode lanedetection_maskmix_mode;
        ELaneDetectionApproach lanedetection_approach;
        ELaneDetectionCalibrationApproach lanedetection_k_approach;

        bool lanedetection_manual_enabled;
        bool lanedetection_enabled;
        bool lanedetection_paused;
        bool vehicledetection_enabled;
        bool roi_enabled;
        bool video_stability_enabled;
        bool showtrackbar_enabled;
        bool engineeringmode_enabled;
        int  calibration_counter;
        int fps;
        int max_fps;
        int dwl_tolerance;
        int vd_1pass_diffthreshold_idx;

        cv::Vec4i manual_leftlane;
        cv::Vec4i manual_rightlane;

        EVehicleDetectionModel vehicledetection_model;
        ELaneDetectionCycleTime lanedetecion_cycletime;
        EVehicleDetectionCycleTime vehicledetecion_cycletime;
        //int lanedetecion_cycletime_idx;
        //int vehicledetecion_cycletime_idx;
        cv::Vec4i roi_linelist[4];
        cv::vector<cv::Point> roi_ptlist;
        //cv::Point2i roi_ptlist[4];
        QElapsedTimer m_timerSlice;

        QString application_dir;
        QString settings_file_path;
        QString workspace_path;

    private:
        static GlobalCacheData* createInstance()
        {
            return new GlobalCacheData();
        }
    };
}
#endif // TRAFFICENFORCEMENT_H
