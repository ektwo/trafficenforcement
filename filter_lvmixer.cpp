#include "filter_lvmixer.h"
#include "parameters_table.h"
#include "common.h"
#include "detection_lanecovered.h"


using namespace cv;
using namespace std;

extern SParametersTable k_parameters_table[2];
LVMixerFilter::LVMixerFilter(BaseGraph *parent, int profileIdx)
    : Filter(parent)
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
    , m_pScene(k_parameters_table[profileIdx].display_scene)
#endif
{
    m_class = k_str_class_transform_name;
    m_name = k_str_mixer_filter_name;
    DSG(1, "LVMixerFilter m_class=%s", m_class);
    DSG(1, "LVMixerFilter m_name=%s", m_name);

#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
    if (m_pScene)
    {
        m_pScene->SetOwner(parent);
        connect(this, SIGNAL(iamStarted()), m_pScene, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), m_pScene, SLOT(handleUpstreamStopped()));
        connect(this, &Filter::cvColorReady, m_pScene, &DrawableGraphicsScene::handleUpdateViewCV);
    }
#endif
}

LVMixerFilter::~LVMixerFilter()
{
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
    if (m_pScene)
    {
        m_pScene->SetOwner(nullptr);
        disconnect(this, &Filter::cvColorReady, 0, 0);
    }
#endif
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

bool LVMixerFilter::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = false;
    if (pDownstreamFilter)
    {
        ret = __super::ConnectTo(pDownstreamFilter);

        //connect(this, SIGNAL(iamStarted()), pDownstreamFilter, SLOT(handleUpstreamStarted()));
        //connect(this, SIGNAL(iamStopped()), pDownstreamFilter, SLOT(handleUpstreamStopped()));
        //connect(this, &Filter::cvColorReady, pDownstreamFilter, &Filter::handleProcessedFrameCV);
    }
    return ret;
}

void LVMixerFilter::DisconnectAll()
{
    __super::DisconnectAll();

    //disconnect(this, &Filter::cvColorReady, 0, 0);
    //disconnect(this, SIGNAL(iamStarted()), 0, 0);
    //disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

void LVMixerFilter::handleUpstreamStarted()
{
    ++m_startedUpstreamFilters;
    DSG(1, "LVMixerFilter::handleUpstreamStarted m_startedUpstreamFilters=%d m_pUpstreamFilterList.size()=%d",
        m_startedUpstreamFilters, m_pUpstreamFilterList.size());
    if (m_startedUpstreamFilters == m_pUpstreamFilterList.size())
    {
        __super::handleUpstreamStarted();


    }
}

void LVMixerFilter::handleUpstreamStopped()
{
    --m_startedUpstreamFilters;
    DSG(1, "LVMixerFilter::handleUpstreamStopped m_startedUpstreamFilters=%d", m_startedUpstreamFilters);
    if (m_startedUpstreamFilters == 0)
    {
        m_imageList.clear();

        __super::handleUpstreamStopped(); // change filter state and emit signal to next filter
    }
}

void LVMixerFilter::handleColorFrameCV(const cv::Mat &frame)
{
    DSG(1, "NONONO LVMixerFilter::handleColorFrameCV");
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
    DSG(1, "LVMixerFilter::handleColorFrameCV");
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 10)
        {
            //DSG(1, "queue m_imageList.size() drop = %d", m_imageList.size());
            cv::Mat image = m_imageList.dequeue();
            image.release();
        }

        m_imageList.enqueue(frame);
    }

    ////if (m_readyToEmit)
    //{
        if (!m_timer.isActive())
            m_timer.start(0, this);
    //}
#endif
}
//void LVMixerFilter::handleUpdateLanelnfo(std::vector<cv::Vec4i> *pLanelineArray)
void LVMixerFilter::handleUpdateLanelnfo(const std::vector<cv::Vec4i> &laneArray)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    //if ((!pCache->lanedetection_manual_enabled) && (pCache->lanedetection_enabled) && (laneArray.size() > 0))
    if ((pCache->lanedetection_enabled) && (laneArray.size() > 0))
    {
        DSG(1, "YESYES LVMixerFilter::handleUpdateLanelnfo laneArray.size()=%d", laneArray.size());
        // 2: signal:updateLaneline is coming
        QMutexLocker locker(&m_mutex);
        //QMutexLocker locker(&m_mutexOp);
        m_laneArray.clear();
        m_laneArray.shrink_to_fit();
        m_laneArray.assign(laneArray.begin(), laneArray.end());

        //TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
        //if (pCache->vehicledetection_enabled)
        //{
        //    m_calcTimer.ResetTimer();
        //    ThresholdCallback(0, 0);
        //    m_calcTimer.Print(0, "LVMixerFilter::DoImageProcessing handleUpdateLanelnfo");
        //    emit cvColorReady(m_matDisplay);
        //}
        //else
        //    emit cvColorReady(m_matScaledImage);
    }
}

void LVMixerFilter::handleUpdateVehicleInfo(const std::vector<cv::Vec4i> &vehicleArray)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if ((pCache->vehicledetection_enabled) && (vehicleArray.size() > 0))
    {
        DSG(1, "YESYES LVMixerFilter::handleUpdateVehicleInfo vehicleArray.size()=%d", vehicleArray.size());
        m_vehicleArray.clear();
        m_vehicleArray.shrink_to_fit();
        m_vehicleArray.assign(vehicleArray.begin(), vehicleArray.end());
        {
            // 3: signal:updateVehicleInfo is coming
            QMutexLocker locker(&m_mutex);
            ThresholdCallbackSelf();
            //TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
            //if (pCache->vehicledetection_enabled)
            //{
            //    m_calcTimer.ResetTimer();
            //    ThresholdCallback(0, 0);
            //    m_calcTimer.Print(0, "LVMixerFilter::DoImageProcessing handleUpdateLanelnfo");
            //    emit cvColorReady(m_matDisplay);
            //}
            //else
            //    emit cvColorReady(m_matScaledImage);
        }
    }
}
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
void LVMixerFilter::DrawVehicle(int idx, int drawLevel, int x, int y, Point leftWheel, Point rightWheel, Point pt1, Point pt2, int width, int height)
{
    int baseline = 0;
    Size textSize;
    char szText[1024] = { 0 };

    if (2 == drawLevel)
    {
        //snprintf(szText, sizeof(szText) - 1, "car_%d_%d_%d_%d_%d", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
        snprintf(szText, sizeof(szText) - 1, "car_%d", idx);
        textSize = getTextSize(String(szText), k_car_font_face, k_car_font_scale_no, k_car_thickness_no, &baseline);

        rectangle(m_matDisplay, pt1, Point(pt1.x + textSize.width, pt1.y + textSize.height), Scalar(60, 60, 60), CV_FILLED, 8, 0);
        putText(m_matDisplay,
            String(szText),
            Point(leftWheel.x, pt1.y + textSize.height),//leftWheel.y + textSize.height + 4),
            k_car_font_face, k_car_font_scale_no,
            k_car_color_no);
        rectangle(m_matDisplay, pt1, pt2, Scalar(0, 255, 0), 3, 8, 0);

        //DSG(1, "x=%d y=%d width=%d height=%d", x, y, width, height);
#ifdef OUTPUT_DEBUG_IMG
        static int illegalIdx = 0;
        int newX, newY, newW, newH;
        newX = x - (width / 10);
        if (newX < 0) newX = 0;
        newY = y - (height / 10);
        if (newY < 0) newY = 0;
        newW = width * 1.2;
        if (newW > 1280) newW = 1280;
        newH = height * 1.2;
        if (newH > 720) newH = 720;
        DSG(1, "newX=%d newY=%d newW=%d newH=%d", newX, newY, newW, newH);
        if (newX + newW > m_matDisplay.cols)
        {
            newX = x; newW = width;
        }
        if (newY + newH > m_matDisplay.rows)
        {
            newY = y; newH = height;
        }
        //PrintMatInfo(m_matDisplay);
        Mat matFile = Mat(m_matScaledImage, Rect(newX, newY, newW, newH));
        char sz[256];
        sprintf(sz, "D:/storage/car_%d_illegal.bmp", illegalIdx++);
        imwrite(sz, matFile);

        //emit pImageProcessing->updateImageList();
#endif
    }
    else if (1 == drawLevel)
    {
        snprintf(szText, sizeof(szText) - 1, "car_%d_%d_%d_%d_%d", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
        //snprintf(szText, sizeof(szText) - 1, "car_%d", idx);
        textSize = getTextSize(String(szText), k_car_font_face, k_car_font_scale_no, k_car_thickness_no, &baseline);

        rectangle(m_matDisplay, pt1, Point(pt1.x + textSize.width, pt1.y + textSize.height), Scalar(60, 60, 60), CV_FILLED, 8, 0);
        putText(m_matDisplay,
            String(szText),
            Point(leftWheel.x, pt1.y + textSize.height),//leftWheel.y + textSize.height + 4),
            k_car_font_face, k_car_font_scale_no,
            k_car_color_no);

        rectangle(m_matDisplay, pt1, pt2, Scalar(0, 255, 255), 3, 8, 0);
    }
    else if (0 == drawLevel)
    {
        //snprintf(szText, sizeof(szText) - 1, "car_%d_%d_%d_%d_%d", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
        snprintf(szText, sizeof(szText) - 1, "car_%d", idx);
        textSize = getTextSize(String(szText), k_car_font_face, k_car_font_scale_no, k_car_thickness_no, &baseline);

        rectangle(m_matDisplay, pt1, Point(pt1.x + textSize.width, pt1.y + textSize.height), Scalar(60, 60, 60), CV_FILLED, 8, 0);
        putText(m_matDisplay,
            String(szText),
            Point(leftWheel.x, pt1.y + textSize.height),//leftWheel.y + textSize.height + 4),
            k_car_font_face, k_car_font_scale_no,
            k_car_color_no);

        rectangle(m_matDisplay, pt1, pt2, Scalar(150, 150, 150), 3, 8, 0);
    }
}


void LVMixerFilter::ThresholdCallback(int pos, void *userdata)
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    int drawLevel = 0;
    int roiLeft, roiRight, roiTop, roiBottom;
    int tolerance = pCache->dwl_tolerance;
    Point ptLt;
    Point ptRb;


    {
        //QMutexLocker locker(&m_mutexOp);
        std::vector<cv::Vec4i> *pLaneArray = &m_laneArray;
        std::vector<cv::Vec4i> *pVehicleArray = &m_vehicleArray;

        size_t numsOfLines = pLaneArray->size();
        size_t numsOfCars = pVehicleArray->size();
        DSG(1, "LVMixerFilter::ThresholdCallback numsOfCars=%d", numsOfCars);
        //DSG(1, "LVMixerFilter::ThresholdCallback numsOfLines=%d", numsOfLines);
        do {
#if 1
            int x, y, width, height;

            cv::Point leftWheel;
            cv::Point rightWheel;
            cv::Point2f leftWheelF, rightWheelF;
            cv::Vec4i ltrb;
            for (size_t idx = 0; idx < numsOfCars; ++idx)
            {
                Point ptLt;
                Point ptRb;
                ltrb = pVehicleArray->at(idx);
                x = ltrb[0];
                y = ltrb[1];
                width = ltrb[2] - ltrb[0];
                height = ltrb[3] - ltrb[1];
                ptLt.x = ltrb[0];
                ptLt.y = ltrb[1];
                ptRb.x = ltrb[2];
                ptRb.y = ltrb[3];

                //DSG(1, "pt1.x=%d %d %d %d", pt1.x, pt1.y, pt2.x, pt2.y);
                leftWheel.x = ptLt.x;
                leftWheel.y = ptRb.y;
                rightWheel.x = ptRb.x;
                rightWheel.y = ptRb.y;
                leftWheelF.x = (float)(ptLt.x);
                leftWheelF.y = (float)(ptLt.y);
                rightWheelF.x = (float)(ptRb.x);
                rightWheelF.y = (float)(ptRb.y);

                //qDebug("car:[%d] wheel(%d, %d) (%d, %d)", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
                //DSG(1, "numsOfLines 2 =%d", numsOfLines);
#ifdef CHECK_LANDLINE
                if (numsOfLines <= 0)
                    break;

                drawLevel = 0;
                //if ((CVCheckPointInROI(*pProcessingSetting->getROIPoints(), leftWheelF)) ||
                //	(CVCheckPointInROI(*pProcessingSetting->getROIPoints(), rightWheelF)))
                {
                    ++drawLevel;
                    //pLaneInROI = &(pLaneLinesInfo->in_roi);

                    for (size_t j = 0; j < numsOfLines; ++j)
                    {
                        if (true == CheckCoveredState(&(pLaneArray->at(j)), leftWheel, rightWheel, tolerance))
                        {
                            ++drawLevel;
                            break;
                        }
                    }
                }
                //DSG(1, "drawLevel=%d", drawLevel);
#else
                drawLevel = 2;
#endif
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
                m_matDisplay = m_matScaledImage.clone();// m_matScaledImage.clone();
                DrawVehicle(idx, drawLevel, x, y, leftWheel, rightWheel, ptLt, ptRb, width, height);
#endif
            }
            break;

#endif
        } while (0);
    }
}

void LVMixerFilter::DoImageProcessing(const cv::Mat &matImage)
{
    DSG(1, "LVMixerFilter::DoImageProcessing");

    m_matScaledImage = matImage;

#if 0
    {
        //QMutexLocker locker(&m_mutexOp);
        //QMutexLocker locker(&m_mutex);
        emit cvColorReady(m_matDisplay);
    }
#else
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if (pCache->vehicledetection_enabled)
    {
        m_calcTimer.ResetTimer();
        ThresholdCallback(0, 0);
        m_calcTimer.Print(0, "LVMixerFilter::DoImageProcessing ThresholdCallback");
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
        emit cvColorReady(m_matDisplay);// m_matDisplay);
#endif
    }
    else
    {
#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
        emit cvColorReady(matImage);
#endif
    }
#endif
}

void LVMixerFilter::timerEvent(QTimerEvent *event)
{
    //qDebug("FilterThread::timerEvent (%s) ev->timerId()=%d %d", m_name, event->timerId(), m_timer.timerId());
    //if (event->timerId() != m_nTimerID) return;
    if (event->timerId() != m_timer.timerId())  return; // { QObject::timerEvent(event); return; }

    if (e_component_state_started != m_state)
        return;

#if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
    // To Do ...
#endif
    m_timer.stop();
    {
        // 1: frame is coming
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 0)
        {
            cv::Mat image = m_imageList.dequeue();
            m_frame = image.clone();
            image.release();
        }
    }

    DoImageProcessing(m_frame);

    ++m_currentFrame;
    if ((m_currentFrame % 30) == 0)
        DSG(1, "HLSFilter::DoImageProcessing m_currentFrame=%d", m_currentFrame);
}

#else

void LVMixerFilter::ThresholdCallbackSelf()
{
    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();

    int drawLevel = 0;
    int roiLeft, roiRight, roiTop, roiBottom;
    int tolerance = pCache->dwl_tolerance;
    Point ptLt;
    Point ptRb;

    {
        std::vector<cv::Vec4i> *pLaneArray = &m_laneArray;
        std::vector<cv::Vec4i> *pVehicleArray = &m_vehicleArray;

        size_t numsOfLines = pLaneArray->size();
        size_t numsOfCars = pVehicleArray->size();
        DSG(1, "LVMixerFilter::ThresholdCallback numsOfCars=%d", numsOfCars);
        //DSG(1, "LVMixerFilter::ThresholdCallback numsOfLines=%d", numsOfLines);
        do {
            int x, y, width, height;

            cv::Point leftWheel;
            cv::Point rightWheel;
            cv::Point2f leftWheelF, rightWheelF;
            cv::Vec4i ltrb;
            for (size_t idx = 0; idx < numsOfCars; ++idx)
            {
                Point ptLt;
                Point ptRb;
                ltrb = pVehicleArray->at(idx);
                x = ltrb[0];
                y = ltrb[1];
                width = ltrb[2] - ltrb[0];
                height = ltrb[3] - ltrb[1];
                ptLt.x = ltrb[0];
                ptLt.y = ltrb[1];
                ptRb.x = ltrb[2];
                ptRb.y = ltrb[3];

                //DSG(1, "pt1.x=%d %d %d %d", pt1.x, pt1.y, pt2.x, pt2.y);
                leftWheel.x = ptLt.x;
                leftWheel.y = ptRb.y;
                rightWheel.x = ptRb.x;
                rightWheel.y = ptRb.y;
                leftWheelF.x = (float)(ptLt.x);
                leftWheelF.y = (float)(ptLt.y);
                rightWheelF.x = (float)(ptRb.x);
                rightWheelF.y = (float)(ptRb.y);

                //qDebug("car:[%d] wheel(%d, %d) (%d, %d)", idx, leftWheel.x, leftWheel.y, rightWheel.x, rightWheel.y);
                //DSG(1, "numsOfLines 2 =%d", numsOfLines);
            #ifdef CHECK_LANDLINE
                if (numsOfLines <= 0)
                    break;

                drawLevel = 0;
                if ((CVCheckPointInROI(pCache->roi_ptlist, leftWheelF)) ||
                	(CVCheckPointInROI(pCache->roi_ptlist, rightWheelF)))
                {
                    ++drawLevel;
                    //pLaneInROI = &(pLaneLinesInfo->in_roi);

                    for (size_t j = 0; j < numsOfLines; ++j)
                    {
                        if (true == CheckCoveredState(&(pLaneArray->at(j)), leftWheel, rightWheel, tolerance))
                        {
                            ++drawLevel;
                            break;
                        }
                    }
                }
                //DSG(1, "drawLevel=%d", drawLevel);
            #else
                drawLevel = 2;
            #endif
            #if MAIN_SCREEN_FROM == MAIN_SCREEN_FROM_MIXER
                m_matDisplay = m_matScaledImage.clone();// m_matScaledImage.clone();
                DrawVehicle(idx, drawLevel, x, y, leftWheel, rightWheel, ptLt, ptRb, width, height);
            #endif
            }
            break;

        } while (0);
    }
}

#endif
