#-------------------------------------------------
#
# Project created by QtCreator 2018-05-08T19:46:19
#
#-------------------------------------------------
#QT += avwidgets
QT       += core gui network opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ../../bin/traffic_enforcement
TEMPLATE = app

USE_QTAV=1
USE_SYS_QTSDK=0
#CONFIG += USE_QTAV
# USE_QTAV {
equals(USE_QTAV, "1") {
    CONFIG += av
    #QT += avwidgets
} else {
}
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0




SOURCES += \
    framelesswindow/framelesswindow.cpp \
    framelesswindow/windowdragger.cpp \
    abstractcamera.cpp \
    autogenerator_line.cpp \
    autogenerator_rect.cpp \
    common.cpp \
    DarkStyle.cpp \
    detection_car.cpp \
    detection_landmark.cpp \
    detection_lane_gaussian.cpp \
    detection_lane_kalman.cpp \
    detection_lane_particlefilter.cpp \
    detection_lanecovered.cpp \
    drawable_graphicsscene.cpp \
    emoptionsdialog.cpp \
    escort_graphicsscene.cpp \
    filter.cpp \
    filter_hls.cpp \
    filter_hsv.cpp \
    filter_lane.cpp \
    filter_lvmixer.cpp \
    filter_sobel.cpp \
    filter_vehicle.cpp \
    image_affine.cpp \
    image_birdeye.cpp \
    image_common.cpp \
    image_laneboundary.cpp \
    image_processing.cpp \
    image_processing_thread.cpp \
    image_thresholdfilter.cpp \
    image_undistortion.cpp \
    main.cpp \
    mainwindow.cpp \
    optionsdialog.cpp \
    pythonwrapper.cpp \
    quick_sort.cpp \
    roi_button.cpp \
    sobel.cpp \
    test_hsv.cpp \
    test_rgb.cpp \
    trcamerathread.cpp \
    trcvthread.cpp \
    trqtthread.cpp \
    trvideothread.cpp \
    view_videoassetmanagement.cpp \
    graph.cpp \
    abstractsource.cpp \
    parameters_table.cpp \
    base_graph.cpp \
    cqtopencvviewergl/cqtopencvviewergl.cpp

INCLUDEPATH +="framelesswindow"
HEADERS += \
    framelesswindow/framelesswindow.h \
    framelesswindow/windowdragger.h \
    abstractcamera.h \
    autogenerator_line.h \
    autogenerator_rect.h \
    common.h \
    DarkStyle.h \
    detection_header.h \
    detection_lanecovered.h \
    drawable_graphicsscene.h \
    dwl_positionoflines.h \
    emoptionsdialog.h \
    escort_graphicsscene.h \
    face_detection_CSIM.h \
    filter.h \
    filter_hls.h \
    filter_hsv.h \
    filter_lane.h \
    filter_lvmixer.h \
    filter_sobel.h \
    filter_vehicle.h \
    image_affine.h \
    image_birdeye.h \
    image_common.h \
    image_cvtapi.h \
    image_laneboundary.h \
    image_processing.h \
    image_processing_thread.h \
    image_thresholdfilter.h \
    image_undistortion.h \
    mainwindow.h \
    optionsdialog.h \
    parameters_table.h \
    pythonwrapper.h \
    quick_sort.h \
    roi_button.h \
    sobel.h \
    test_hsv.h \
    test_rgb.h \
    traffic_enforcement.h \
    trcamerathread.h \
    trcvthread.h \
    trqtthread.h \
    trvideothread.h \
    view_videoassetmanagement.h \
    graph.h \
    singleton.h \
    abstractsource.h \
    base_graph.h \
    cqtopencvviewergl/cqtopencvviewergl.h


message(OUT_PWD)
message($$OUT_PWD)




Release:DESTDIR = release
Release:OBJECTS_DIR = release/.obj
Release:MOC_DIR = release/.moc
Release:RCC_DIR = release/.rcc
Release:UI_DIR = release/.ui

Debug:DESTDIR = debug
Debug:OBJECTS_DIR = debug/.obj
Debug:MOC_DIR = debug/.moc
Debug:RCC_DIR = debug/.rcc
Debug:UI_DIR = debug/.ui

ReleaseBuild {

}

USE_PYTHON3=1

win32 {
    message(win32)
    message("QTDIR" = $(QTDIR))

    static { # everything below takes effect with CONFIG ''= static
        CONFIG += static
        # CONFIG += staticlib # this is needed if you create a static library, not a static executable
        DEFINES+= STATIC
         message("~~~ static build ~~~") # this is for information, that the static build is done

        win32: TARGET = $$join(TARGET,,,)
        QMAKE_LFLAGS_RELEASE = /NODEFAULTLIB:libcmtd.lib
        #this adds an s in the end, so you can seperate static build from non static build
    }
    BASELIB=$$PWD/lib


    # external library
    ZLIBPATH=D:/sourcecode/zlib-1.2.11-build
    ZLIBPATH_INC=$$ZLIBPATH/include
    ZLIBPATH_LIB=$$ZLIBPATH/lib
    message("ZLIBPATH_INC=" $$ZLIBPATH_INC)
    message("ZLIBPATH_LIB=" $$ZLIBPATH_LIB)

    PNGPATH=D:/sourcecode/libpng-1.6.34-build
    PNGPATH_INC=$$PNGPATH
    PNGPATH_LIB=$$PNGPATH/projects/vstudio2017/x64/ReleaseLibrary
    message("PNGPATH_INC=" $$PNGPATH_INC)
    message("PNGPATH_LIB=" $$PNGPATH_LIB)

    JPEGPATH=D:/sourcecode/jpegsr9c-build
    JPEGPATH_INC=$$JPEGPATH
    JPEGPATH_LIB=$$JPEGPATH
    message("JPEGPATH_INC=" $$JPEGPATH_INC)ilmimf.lib
    message("JPEGPATH_LIB=" $$JPEGPATH_LIB)

    INCLUDEPATH += $$ZLIBPATH_INC
    INCLUDEPATH += $$PNGPATH_INC
    INCLUDEPATH += $$JPEGPATH_INC
    DEPENDPATH += $$ZLIBPATH_INC
    DEPENDPATH += $$PNGPATH_INC
    DEPENDPATH += $$JPEGPATH_INC


    message("PYTHONDIR" = $(PYTHONDIR))
    # python section
    equals(USE_PYTHON3, 1) {
    PYTHON_PATH = $(PYTHONDIR)# D:/Application/Python/Python36
    }
    else {
    PYTHON_PATH = $(PYTHONDIR)# D:/Application/Python/Python36
    }
    message("PYTHON_PATH" = $$PYTHON_PATH)
    PYTHON_LIBS_PATH = $$PYTHON_PATH/libs

    INCLUDEPATH += $$PYTHON_PATH $$PYTHON_PATH/include
    INCLUDEPATH += $$PYTHON_PATH/Lib/site-packages/numpy/core/include/numpy
    DEPENDPATH += $$PYTHON_PATH $$PYTHON_PATH/include
    DEPENDPATH += $$PYTHON_PATH/Lib/site-packages/numpy/core/include/numpy



    # opencv section
    OPENCV_PATH = $(OPENCVDIR) # D:\sourcecode\opencv-2.4.13.6-build\install
    INCLUDEPATH += $$OPENCV_PATH/include
    DEPENDPATH += $$OPENCV_PATH/include
    OPENCV_LIBS_PATH = $$OPENCV_PATH/x64/vc15/staticlib


    INCLUDEPATH += libyuv
    DEPENDPATH += libyuv
    LIBS += -L$$BASELIB -lyuv
    # qt section
    INCLUDEPATH += L$(QTDIR)/include
    equals(USE_SYS_QTSDK, "1") {
        LIBS += -Llib
    } else {
        LIBS += -L$(QTDIR)/lib
        LIBS += -lqtharfbuzz -lqtpcre2 \
    }

    LIBS += \
    -lQt5Widgets \
    -lQt5Gui \
    -lQt5Test \
    -lQt5Concurrent \
    -lQt5Core \
    -lUxTheme \
    -lwinmm \
    -lVersion


    LIBS += -L$$OPENCV_LIBS_PATH
    LIBS += -lIlmImf \
    -llibjasper \
    -llibtiff \
    -lopencv_calib3d2413 \
    -lopencv_contrib2413 \
    -lopencv_core2413 \
    -lopencv_features2d2413 \
    -lopencv_flann2413 \
    -lopencv_gpu2413 \
    -lopencv_highgui2413 \
    -lopencv_imgproc2413 \
    -lopencv_legacy2413 \
    -lopencv_ml2413 \
    -lopencv_nonfree2413 \
    -lopencv_objdetect2413 \
    -lopencv_ocl2413 \
    -lopencv_photo2413 \
    -lopencv_stitching2413 \
    -lopencv_superres2413 \
    -lopencv_ts2413 \
    -lopencv_video2413 \
    -lopencv_videostab2413

    equals(USE_PYTHON3, 1) {
    LIBS += -L$$PYTHON_LIBS_PATH -lpython36
    }
    else {
    LIBS += -L$$PYTHON_LIBS_PATH -lpython27
    }

    LIBS += -L$$ZLIBPATH_LIB -lzlib
    LIBS += -L$$PNGPATH_LIB -llibpng
    LIBS += -L$$JPEGPATH_LIB -llibjpeg

    #-lopencv_world2413 \
    LIBS += vfw32.lib
    LIBS+=-lopengl32 -lglu32

    win32:!win32-g++: PRE_TARGETDEPS += $$BASELIB/yuv.lib
    else:win32-g++: PRE_TARGETDEPS += $$BASELIB/yuv.lib
    win32:!win32-g++: PRE_TARGETDEPS += $$ZLIBPATH_LIB/zlib.lib
    else:win32-g++: PRE_TARGETDEPS += $$ZLIBPATH_LIB/libzlib.a
    win32:!win32-g++: PRE_TARGETDEPS += $$PNGPATH_LIB/libpng.lib
    else:win32-g++: PRE_TARGETDEPS += $$PNGPATH_LIB/liblibpng.a
    win32:!win32-g++: PRE_TARGETDEPS += $$JPEGPATH_LIB/libjpeg.lib
    else:win32-g++: PRE_TARGETDEPS += $$JPEGPATH_LIB/liblibjpeg.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/IlmImf.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libIlmImf.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libjasper.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/liblibjasper.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libtiff.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/liblibtiff.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_calib3d2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_calib3d2413.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_contrib2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_contrib2413.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_core2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_core2413.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_features2d2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_features2d2413.a
    win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_flann2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_flann2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_gpu2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_gpu2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_highgui2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_highgui2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_imgproc2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_imgproc2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_legacy2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_legacy2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_ml2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_ml2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_nonfree2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_nonfree2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_objdetect2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_objdetect2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_ocl2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_ocl2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_photo2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_photo2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_stitching2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_stitching2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_superres2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_superres2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_ts2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_ts2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_video2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_video2413.a
    .win32:!win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/opencv_videostab2413.lib
    else:win32-g++: PRE_TARGETDEPS += $$OPENCV_LIBS_PATH/libopencv_videostab2413.a
    equals(USE_PYTHON3, 1) {
    win32:!win32-g++: PRE_TARGETDEPS += $$PYTHON_LIBS_PATH/python36.lib
    else:win32-g++: PRE_TARGETDEPS += $$PYTHON_LIBS_PATH/libpython36.a
    }
    else {
    win32:!win32-g++: PRE_TARGETDEPS += $$PYTHON_LIBS_PATH/python27.lib
    else:win32-g++: PRE_TARGETDEPS += $$PYTHON_LIBS_PATH/libpython27.a
    }


}
win64 {
    message(win64)
}
macx {
    message(macx)
}
unix:!macx{
    message(unix)

    PYTHONDIR = /home/ektwo/.conda/envs/tensorflow-gpu
    message(PYTHONDIR = $$PYTHONDIR)

    INCLUDEPATH += /usr/local/include
    INCLUDEPATH += $$PYTHONDIR $$PYTHONDIR/include/python3.6m
    INCLUDEPATH += $$PYTHONDIR/lib/python3.6/site-packages/numpy/core/include/numpy

    message(INCLUDEPATH = $$INCLUDEPATH)

    PYTHONLIBS = $$PYTHONDIR/lib
    #LIBS += -L$$PYTHONLIBS/python3.6/site-packages -lpython3.6m
    LIBS += -lpython3.6m
    #PRE_TARGETDEPS += $$PYTHONLIBS/libpython3.6m.a
    #LIBS += -L$$PYTHONLIBS -lpython3.6
    #LIBS += /home/ektwo/.conda/envs/tensorflow-gpu/lib/python3.6


    OPENCVDIR = /home/ektwo/.conda/envs/tensorflow-gpu
    OPENCV_LIBS_PATH = $$OPENCVDIR/lib
    #OPENCVDIR = /home/projects/opencv/opencv-3.4.2
    INCLUDEPATH += $$OPENCVDIR/include
    LIBS += `pkg-config opencv --libs`
    LIBS += -L /usr/local/lib
    LIBS += -L $$OPENCV_LIBS_PATH \
    LIBS += -lopencv_highgui \
            -lopencv_core \
            -lopencv_imgproc \
            -lopencv_imgcodecs \
            -lopencv_objdetect \
            -lopencv_videoio

    LIBS += -L$$OPENCVDIR/lib/python3.6/site-packages -lpcv2
    PRE_TARGETDEPS += $$OPENCVDIR/lib/python3.6/site-packages/cv2.so
    #/home/ektwo/.conda/envs/tensorflow-gpu/lib/libopencv_highgui.so
    #/home/projects/opencv/opencv-3.4.2/build/lib/libopencv_highgui.so
}

FORMS += \
        mainwindow.ui \
    emoptionsdialog.ui \
    optionsdialog.ui \
    videoassetmanagement.ui \
    framelesswindow/framelesswindow.ui

RESOURCES += \
    traffic_enforcement.qrc \
    darkstyle.qrc \
    framelesswindow.qrc

#unix:!macx: LIBS += -L$$PWD/../../../ektwo/.conda/envs/tensorflow-gpu/lib/ -lpython3.6m

#INCLUDEPATH += $$PWD/../../../ektwo/.conda/envs/tensorflow-gpu/lib/python3.6
#DEPENDPATH += $$PWD/../../../ektwo/.conda/envs/tensorflow-gpu/lib/python3.6

#unix:!macx: PRE_TARGETDEPS += $$PWD/../../../ektwo/.conda/envs/tensorflow-gpu/lib/libpython3.6m.a


