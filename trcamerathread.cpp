#include "trcamerathread.h"
#include <QTimerEvent>
#include <QDebug>
#include <QPainter>
#include <chrono>
#include <thread>
#include "traffic_enforcement.h"
#include "common.h"
#include "image_common.h"
#include "trvideothread.h"


using namespace std;
using namespace cv;


TRVideoCapture::TRVideoCapture(BaseGraph *parent)
    : AbstractCamera(e_devicetype_camera_common, parent)
    //, m_state(e_component_state_idle)
    //, m_timerId(-1)
{
    m_class = k_str_class_source_name;
    m_name = k_str_video_capture_filter_name;
    DSG(1, "TRVideoCapture m_class=%s", m_class);
    DSG(1, "TRVideoCapture m_name=%s", m_name);

    memset(&m_config, 0, sizeof(m_config));

    connect(this, &TRVideoCapture::started, (BaseGraph*)parent, &BaseGraph::handleAllSourceStarted);
    connect(this, &TRVideoCapture::stopped, (BaseGraph*)parent, &BaseGraph::handleAllSourceStopped);
}

TRVideoCapture::~TRVideoCapture()
{
    qDebug("~TRVideoCapture");
    Close();
    disconnect(this, &TRVideoCapture::stopped, 0, 0);
    disconnect(this, &TRVideoCapture::started, 0, 0);
}

bool TRVideoCapture::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = __super::ConnectTo(pDownstreamFilter);
    DSG(1, "TRVideoCapture::ConnectTo=%p", pDownstreamFilter);

    connect(this, &TRVideoCapture::cvColorReady, pDownstreamFilter, &Filter::handleColorFrameCV);

    return ret;
}

void TRVideoCapture::DisconnectAll()
{
    __super::DisconnectAll();

    disconnect(this, &TRVideoCapture::cvColorReady, 0, 0);
    disconnect(this, &TRVideoCapture::qColorReady, 0, 0);
}

bool TRVideoCapture::SetConfig(void *pConfig)//SCameraConfig *pConfig)
{
    bool ret = false;

    memcpy(&m_config, pConfig, sizeof(SCameraConfig));

    if (!m_videoCapture)
    {
        qDebug("TRVideoCapture::SetConfig m_videoCapture.isNull=%p", m_videoCapture.isNull());
        qDebug("TRVideoCapture::SetConfig m_config.device_idx=%d", m_config.device_idx);
        if (-1 == m_config.device_idx)
        {
            m_videoCapture.reset(new cv::VideoCapture(m_config.filename));
        }
        else
        {
            m_videoCapture.reset(new cv::VideoCapture(m_config.device_idx));
        }
    }

    if (m_videoCapture->isOpened())
    {
        m_videoCapture->set(CV_CAP_PROP_FRAME_WIDTH, m_config.want_width);
        m_videoCapture->set(CV_CAP_PROP_FRAME_HEIGHT, m_config.want_height);

        Size videoSize = Size(
            (int)m_videoCapture->get(CV_CAP_PROP_FRAME_WIDTH),
            (int)m_videoCapture->get(CV_CAP_PROP_FRAME_HEIGHT));

        SCameraCapabilities cap;
        cap.width = videoSize.width;
        cap.height = videoSize.height;
        cap.frame_cnt = m_videoCapture->get(CV_CAP_PROP_FRAME_COUNT);

        //pDeviceFilterInfoList->capability[pGlobalCacheData->devices_color_resolution_idx].height;
        //qDebug("TRVideoCapture::SetConfigcamera width=%d height=%d", cap.width, cap.height);

        double fps;
        if (-1 == m_config.interval_ms)
        {
            fps = m_videoCapture->get(CV_CAP_PROP_FPS);
            qDebug("TRVideoCapture::SetConfig 1 fps=%f", fps);
            m_videoCapture->set(CV_CAP_PROP_FPS, 30);
            fps = m_videoCapture->get(CV_CAP_PROP_FPS);
            qDebug("TRVideoCapture::SetConfig 2 fps=%f", fps);
            if (qRound(fps) != 30)
            {
                m_videoCapture->set(CV_CAP_PROP_FPS, 25);
                fps = m_videoCapture->get(CV_CAP_PROP_FPS);
            }
            cap.fps = fps;
            m_config.interval_ms = static_cast<uint32_t>(1000 / fps);
        }
        else
        {
            fps = static_cast<double>(1000 / m_config.interval_ms);
            m_videoCapture->set(CV_CAP_PROP_FPS, fps);
            cap.fps = fps;
        }

        m_capabilitiesList.push_back(cap);

        ret = true;
    }

    return ret;
}

bool TRVideoCapture::Open()
{
    if (e_component_state_idle == m_state)
    {
        if ((!m_videoCapture.isNull()) && (m_videoCapture->isOpened()))
            m_state = e_component_state_initialized;
    }

    return (e_component_state_initialized == m_state);

}

void TRVideoCapture::Close()
{
    InternalStop();
    if (m_videoCapture->isOpened())
        m_videoCapture->release();
}

void TRVideoCapture::OpenSettingDialog()
{
    if (m_videoCapture)
    {
        m_videoCapture->set(CV_CAP_PROP_SETTINGS, 1);
    }
}

bool TRVideoCapture::GetFirstFrame(cv::Mat &frame)
{
    //*m_pCapture >> frame;
    qDebug("TRVideoCapture::GetFirstFrame the problem is m_videoCapture->read  ? ");

    bool ret = false;
    int count = 0;

    while (count < 30)
    {
        Mat img;
        if (!m_videoCapture.isNull())
            ret = m_videoCapture->read(img);
        else
            qDebug("TRVideoCapture::GetFirstFrame, [number of times=%d] m_videoCapture.isNull() !!!, try it again", count);

        if (ret)
        {
            qDebug("TRVideoCapture::GetFirstFrame, [number of times=%d] success to read the frame!!!", count);
            qDebug("TRVideoCapture::GetFirstFrame, [number of times=%d] frame.cols=%d frame.rows=%d frame.type()=%d", 
                count, img.cols, img.rows, img.type());

        #ifdef DO_PRESCALE
            //DSG(1, "DDD %d %d %d %d", m_config.want_width, img.cols, m_config.want_height, img.rows);
            CQElapsedTimer timer;
            if ((m_config.want_width != img.cols) || (m_config.want_height != img.rows))
            {
                CVImageResize2ByWH(img, frame, m_config.want_width, m_config.want_height);
            }
            //namedWindow("frame", CV_WINDOW_KEEPRATIO | CV_WINDOW_NORMAL);
            //imshow("frame", frame);
            //cv::waitKey(0);

            //timer.Print(0, "width=%d height=%d", m_config.want_width, m_config.want_height);
            //imshow("camera", img);
            //PrintMatInfo(img);
        #if defined(GIVE_ME_MIRROR) && !defined(USE_HW_MIRROR)
            cv::flip(frame, frame, 1);
        #endif
            emit cvColorReady(img);
        #else
            //frame = img;
            emit cvColorReady(img);
        #endif

            

            ret = true;

            break;

		    m_videoCapture->set(CV_CAP_PROP_POS_FRAMES, 0);
        }
        else
        {
            qDebug("TRVideoCapture::GetFirstFrame, [number of times=%d] failed to read frame, will do a sleep about 200ms", count);
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        ++count;
    }
    //if (ret)
    //{
    //    imwrite("frame.bmp", frame);
    //}
    return ret;
}

void TRVideoCapture::start(int cam)
{
    qDebug("TRVideoCapture::start");
    if (InternalStart())
        emit started();
}

void TRVideoCapture::stop()
{
    qDebug("TRVideoCapture::stop");
    InternalStop();
    emit stopped();
}

bool TRVideoCapture::InternalStart()
{
    bool ret = false;
    qDebug("TRVideoCapture::InternalStart m_state=%d", m_state);
    if ((e_component_state_initialized == m_state) ||
        (e_component_state_stopped == m_state))
    {
        if (m_videoCapture->isOpened()) {

            m_timerId = startTimer(m_config.interval_ms, Qt::CoarseTimer);

            m_state = e_component_state_started;
			qDebug("TRVideoCapture::start emit started");
            ret = true;
        }
    }

    return ret;
}

void TRVideoCapture::InternalStop()
{
    m_currentFrame = 0;
    if (e_component_state_started == m_state)
    {
        if (-1 != m_timerId)
        {
            killTimer(m_timerId);
            m_timerId = -1;
        }

        m_state = e_component_state_stopped;
    }
}

void TRVideoCapture::DoImageProcessing(const cv::Mat &matImage)
{

}

void TRVideoCapture::timerEvent(QTimerEvent * ev) {

    if (ev->timerId() != m_timerId) return;
   if (e_component_state_started != m_state) return;

    cv::Mat img;
    cv::Mat frame;
    if (m_videoCapture->isOpened())
    {
        if (!m_videoCapture->read(img)) { // Blocks until a new frame is ready

            qDebug("TRVideoCapture::timerEvent bye");
            return;
        }
    #ifdef DO_PRESCALE
        if ((m_config.want_width != img.cols) || (m_config.want_height != img.rows))
        {
            CVImageResize2ByWH(img, frame, m_config.want_width, m_config.want_height);
        }
        else
            frame = img;

    #if defined(GIVE_ME_MIRROR) && !defined(USE_HW_MIRROR)
        cv::flip(frame, frame, 1);
    #endif

    #else
        frame = img;
    #endif

        TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
        pCache->CheckLaneDetectionAndCalibration(m_currentFrame);

        ++m_currentFrame;
        if ((m_currentFrame % 30) == 0)
            DSG(1, "TRVideoCapture::timerEvent, m_currentFrame=%d", m_currentFrame);

        emit cvColorReady(frame);
    }
}

vector<cv::Size> TRVideoCapture::GetSupportedResolutionsProbing()
{
    const cv::Size CommonResolutions[] = {
        cv::Size(160, 120),
        cv::Size(176, 144),
        cv::Size(320, 240),
        cv::Size(352, 288),

        cv::Size(640, 480)
    };

    vector<cv::Size> supportedVideoResolutions;
    int nbTests = sizeof(CommonResolutions) / sizeof(CommonResolutions[0]);

    for (int i = 0; i < nbTests; i++) {
        CvSize test = CommonResolutions[i];

        // try to set resolution
        m_videoCapture->set(CV_CAP_PROP_FRAME_WIDTH, test.width);
        m_videoCapture->set(CV_CAP_PROP_FRAME_HEIGHT, test.height);

        double width = m_videoCapture->get(CV_CAP_PROP_FRAME_WIDTH);
        double height = m_videoCapture->get(CV_CAP_PROP_FRAME_HEIGHT);

        if (test.width == width && test.height == height) {
            supportedVideoResolutions.push_back(test);
        }
    }

    return supportedVideoResolutions;
}
