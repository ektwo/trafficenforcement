#include "detection_lanecovered.h"
#include "traffic_enforcement.h"
#include "common.h"

using namespace cv;

//extern std::vector<cv::Vec4i> g_detectedLaneLines;

#include <algorithm>    // std::swap

#if 1

bool CheckCoveredState(cv::Vec4i *pLaneLine, Point leftWheel, Point rightWheel, int tolerance)
{
    if (!pLaneLine) return false;

    int *pLine = (int*)(&pLaneLine[0]);
    const int LL0 = pLine[0];
    const int LL1 = pLine[1];
    const int LL2 = pLine[2];
    const int LL3 = pLine[3];

    bool isCovered = false;
    bool isPossible = false;
    int Y = leftWheel.y;
    int distanceX = LL2 - LL0;
    int distanceY = LL3 - LL1;
    float fLeftWheelX = static_cast<float>(leftWheel.x);
    float fRightWheelX = static_cast<float>(rightWheel.x);
    if (fLeftWheelX > fRightWheelX)
        std::swap(fLeftWheelX, fRightWheelX);

    float gap = (k_tolerance_max - tolerance) / static_cast<float>(k_tolerance_max);
    //float gap = (tolerance) / static_cast<float>(k_tolerance_max); // 0.56
    //float dToleranceLeftWheel = fLeftWheelX * (1.0 + gap);
    //float dToleranceRightWheel = fRightWheelX * (1.0 - gap);
    float dToleranceLeftWheel = fLeftWheelX + (((fRightWheelX - fLeftWheelX) / 2.0) * gap);
    float dToleranceRightWheel = fRightWheelX - (((fRightWheelX - fLeftWheelX) / 2) * gap);
    //qDebug("CheckCoveredState %d %d %d %d", LL0, LL1, LL2, LL3);
    //DSG(1, "gap=%f", gap);
    //DSG(1, "(((fRightWheelX - fLeftWheelX) / 2.0) * gap)=%f", (((fRightWheelX - fLeftWheelX) / 2.0) * gap));
    float dPredictX;
    float ratio;

    if (distanceY > 0) // 1 - 3
    {
        if ((Y >= LL1) && (Y <= LL3))
        {
            isPossible = true;
            ratio = (Y - LL1) / static_cast<double>(distanceY);
        }
    }
    else if (distanceY < 0) // 3 - 1
    {
        if ((Y >= LL3) && (Y <= LL1))
        {
            isPossible = true;
            ratio = (Y - LL3) / static_cast<double>(abs(distanceY));
        }
    }

    if (!isPossible)
        return false;

    if (distanceX > 0)
        dPredictX = ceil(ratio * static_cast<float>(abs(distanceX)) + LL0);
    else
        dPredictX = ceil(ratio * static_cast<float>(abs(distanceX)) + LL2);
    //DSG(1, "leftWheel.x=%d rightWheel.x=%d dPredictX=%f dToleranceLeftWheel=%f dToleranceRightWheel=%f",
    //    leftWheel.x, rightWheel.x, dPredictX, dToleranceLeftWheel, dToleranceRightWheel);
    if ((dToleranceRightWheel > dPredictX) && (dPredictX > dToleranceLeftWheel))
    {
        //qDebug("CheckCoveredState %d %d %d %d", LL0, LL1, LL2, LL3);
        //DSG(1, "gap=%f", gap);
        //DSG(1, "(((fRightWheelX - fLeftWheelX) / 2.0) * gap)=%f", (((fRightWheelX - fLeftWheelX) / 2.0) * gap));

        //DSG(1, "leftWheel.x=%d rightWheel.x=%d dPredictX=%f dToleranceLeftWheel=%f dToleranceRightWheel=%f",
        //    leftWheel.x, rightWheel.x, dPredictX, dToleranceLeftWheel, dToleranceRightWheel);
        isCovered = true;
    }

    return isCovered;
}

#else

bool CheckCoveredState(cv::Vec4i *pLaneLine, Point leftWheel, Point rightWheel, int tolerance)
{
    if (!pLaneLine) return false;

    int *pLine = (int*)(&pLaneLine[0]);
    const int LL0 = pLine[0];
    const int LL1 = pLine[1];
    const int LL2 = pLine[2];
    const int LL3 = pLine[3];
//#define LL0 g_detectedLaneLines[index][0]
//#define LL1 g_detectedLaneLines[index][1]
//#define LL2 g_detectedLaneLines[index][2]
//#define LL3 g_detectedLaneLines[index][3]
//#define LL0 pLine[0]
//#define LL1 pLine[1]
//#define LL2 pLine[2]
//#define LL3 pLine[3]
//    if (index >= g_detectedLaneLines.size())
//        return false;

    bool isCovered = false;
    bool isPossible = false;
    int Y = leftWheel.y;
    int distanceX = LL2 - LL0;
    int distanceY = LL3 - LL1;
    float fLeftWheelX = static_cast<float>(leftWheel.x);
    float fRightWheelX = static_cast<float>(rightWheel.x);
    if (fLeftWheelX > fRightWheelX)
        std::swap(fLeftWheelX, fRightWheelX);

    //float fCarWidth = fRightWheelX - fLeftWheelX;
    //float carHalfWidth = static_cast<float>(rightWheelX - leftWheelX) / 2.0;
    //float dToleranceWidth = static_cast<float>(carWidth * tolerance) / k_tolerance_max;
    //float centerXOfCar = (leftWheelX + rightWheelX) / 2;
    float gap = (k_tolerance_max - tolerance) / static_cast<float>(k_tolerance_max);
    float dToleranceLeftWheel = fLeftWheelX * (1.0 + gap);
    float dToleranceRightWheel = fRightWheelX * (1.0 - gap);
    //qDebug("gap %d %d %d %f %f %f", tolerance, leftWheel.x, rightWheel.x, gap, dToleranceLeftWheel, dToleranceRightWheel);
    float dPredictX;
    float ratio;
    //qDebug("dToleranceWidth=%f", dToleranceWidth);
    if (distanceY > 0) // 1 - 3
    {
        if ((Y >= LL1) && (Y <= LL3))
        {
            isPossible = true;
            ratio = (Y - LL1) / static_cast<double>(distanceY);
        }
    }
    else if (distanceY < 0) // 3 - 1
    {
        if ((Y >= LL3) && (Y <= LL1))
        {
            isPossible = true;
            ratio = (Y - LL3) / static_cast<double>(abs(distanceY));
        }
    }

    if (!isPossible)
        return false;

//    if (distanceX > 0) // 0 - 2
//    {
//        dPredictX = ceil(ratio * static_cast<double>(distanceX) + LL0);
////qDebug("333 ratio=%f", ratio);
////qDebug("333 dPredictX=%f", dPredictX);
////qDebug("333 GGG=%f", (LL2 - ((distanceX * tolerance) / 100)));
//        if ((rightWheelX > dPredictX) && (dPredictX > leftWheelX) &&602-
//           (((static_cast<double>(rightWheelX) - dPredictX) > dToleranceWidth) ||
//            ((dPredictX - static_cast<double>(leftWheelX)) > dToleranceWidth)))
//        {
//            isCovered = true;
//        }
//    }
//    else if (distanceX < 0) // 2 - 0
    {
        qDebug("LL0 %d %d %d %d, Y=%d", LL0, LL1, LL2, LL3, Y);
        qDebug("ratio %f", ratio);
        if (distanceX > 0)
            dPredictX = ceil(ratio * static_cast<float>(abs(distanceX)) + LL0);
        else
            dPredictX = ceil(ratio * static_cast<float>(abs(distanceX)) + LL2);
        qDebug("tol left=%f right=%f pred=%f", dToleranceLeftWheel, dToleranceRightWheel, dPredictX);
        if ((dToleranceRightWheel > dPredictX) && (dPredictX > dToleranceLeftWheel))
//           (((static_cast<double>(rightWheelX) - dPredictX) > dToleranceWidth) ||
//            ((dPredictX - static_cast<double>(leftWheelX)) > dToleranceWidth)))
        {
            isCovered = true;
        }
    }

    return isCovered;
}

#endif

