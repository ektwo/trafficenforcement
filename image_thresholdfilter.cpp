#include "image_thresholdfilter.h"
#include <opencv/cv.h>

using namespace cv;


//#define USE_BLUR
#define USE_GRAY
#define THRES_MIN 80
#define THERS_MAX 255

Mat ImageThresholdFilter(Mat src)
{
    Mat srcGray;
#ifdef USE_BLUR
    GaussianBlur(src, src, Size(3,3), 0, 0);
#endif
#ifdef USE_GRAY
	cv::cvtColor(src, src, CV_BGR2GRAY );
#endif

    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    Sobel(src, grad_x, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT);
    convertScaleAbs(grad_x, abs_grad_x);
    Sobel(src, grad_y, CV_16S, 0, 1, 3, 1, 0, BORDER_DEFAULT);
    convertScaleAbs(grad_y, abs_grad_y);

    Mat dst1, dst2;
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, dst1);
    if (dst1.type() == CV_8UC1)
        threshold(dst1, dst2, THRES_MIN, THERS_MAX, THRESH_BINARY | THRESH_OTSU);
    else
        threshold(dst1, dst2, THRES_MIN, THERS_MAX, THRESH_BINARY);

    //imshow("origin", src);
    //imshow("abs_grad_x", abs_grad_x);
    //imshow("abs_grad_y", abs_grad_y);
    //imshow("dst1", dst1);
    //imshow("dst2", dst2);

    return src;
}

Mat ImageThresholdFilter2(Mat src)
{
    Mat srcGray;
#ifdef USE_BLUR
    GaussianBlur(src, src, Size(3,3), 0, 0);
#endif
#ifdef USE_GRAY
	cv::cvtColor(src, src, CV_BGR2GRAY );
#endif

    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    Sobel(src, grad_x, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT);
    convertScaleAbs(grad_x, abs_grad_x);
    Sobel(src, grad_y, CV_16S, 0, 1, 3, 1, 0, BORDER_DEFAULT);
    convertScaleAbs(grad_y, abs_grad_y);

    Mat dst1, dst2;
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, dst1);
    if (dst1.type() == CV_8UC1)
        threshold(dst1, dst2, THRES_MIN, THERS_MAX, THRESH_BINARY | THRESH_OTSU);
    else
        threshold(dst1, dst2, THRES_MIN, THERS_MAX, THRESH_BINARY);

    //imshow("origin", src);
    //imshow("abs_grad_x", abs_grad_x);
    //imshow("abs_grad_y", abs_grad_y);
    //imshow("dst1", dst1);
    //imshow("dst2", dst2);

    return src;
}
//scaled_sobel = np.uint8(255*abs_sobel/np.max(abs_sobel))
//binary_output = np.zeros_like(scaled_sobel)
//binary_output[(scaled_sobel >= thresh_min) & (scaled_sobel <= thresh_max)] = 1
