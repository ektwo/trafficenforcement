#ifndef __PYTHONWRAPPER_H__
#define __PYTHONWRAPPER_H__
#pragma once

#include <vector>
#include <opencv2/opencv.hpp>
#pragma push_macro("slots")
#undef slots
#include "Python.h"
#pragma pop_macro("slots")

class CPythonWrapper
{
public:
    CPythonWrapper();
    ~CPythonWrapper();
};

class CPythonWrapperModel
{
public:
    CPythonWrapperModel();
    ~CPythonWrapperModel();
    int  Initialize();
    void Release();

    std::vector<cv::Vec4i> GetDetectedCars(cv::Mat &matImage);

private:
    PyObject *m_pDict;
    PyObject *m_pHandle;
};

//void main0();
//void main1();
//void main2();
//int PythonWrapperInit();
//void PythonWrapperFree();
//int PythonWrapperInit2();
//void PythonWrapperFree2();
//std::vector<cv::Vec4i> GetDetectedCars(cv::Mat &matImage);

#endif
