#ifndef BASE_GRAPH_H
#define BASE_GRAPH_H

#include <QObject>


class BaseGraph : public QObject
{
    Q_OBJECT
public:
    explicit BaseGraph(QObject *parent = nullptr) : QObject(parent) {}
    ~BaseGraph() {}

public slots:
    virtual void handleAllSourceStarted() = 0;
    virtual void handleAllSourceStopped() = 0;
    virtual void handleUpdate(const QImage &);

};


#endif // BASE_GRAPH_H