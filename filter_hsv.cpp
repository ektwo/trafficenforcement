#include "filter_hsv.h"
#include "parameters_table.h"
#include "common.h"


extern SParametersTable k_parameters_table[2];
HSVFilter::HSVFilter(BaseGraph *parent, int profileIdx)
    : Filter(parent)
    , m_thresH(k_parameters_table[profileIdx].hsv.threshold_value_h)
    , m_thresS(k_parameters_table[profileIdx].hsv.threshold_value_s)
    , m_thresV(k_parameters_table[profileIdx].hsv.threshold_value_v)
    , m_thresHMax(k_parameters_table[profileIdx].hsv.threshold_value_h_max)
    , m_thresSMax(k_parameters_table[profileIdx].hsv.threshold_value_s_max)
    , m_thresVMax(k_parameters_table[profileIdx].hsv.threshold_value_v_max)
    , m_pScene(k_parameters_table[profileIdx].hsv.scene)
{
    m_class = k_str_class_transform_name;
    m_name = k_str_hsv_filter_name;
    DSG(1, "HSVFilter m_class=%s", m_class);
    DSG(1, "HSVFilter m_name=%s", m_name);

    if (m_pScene)
    {
        m_pScene->SetOwner(parent);
        connect(this, SIGNAL(iamStarted()), m_pScene, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), m_pScene, SLOT(handleUpstreamStopped()));
        connect(this, &Filter::cvColorReady, m_pScene, &EscortGraphicsScene::handleUpdateViewCV);
    }
}

HSVFilter::~HSVFilter()
{
    if (m_pScene)
    {
        m_pScene->SetOwner(nullptr);
        disconnect(this, &Filter::cvColorReady, 0, 0);
    }
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

bool HSVFilter::ConnectTo(Filter *pDownstreamFilter)
{
    bool ret = false;
    if (pDownstreamFilter)
    {
        ret = __super::ConnectTo(pDownstreamFilter);

        connect(this, SIGNAL(iamStarted()), pDownstreamFilter, SLOT(handleUpstreamStarted()));
        connect(this, SIGNAL(iamStopped()), pDownstreamFilter, SLOT(handleUpstreamStopped()));
        connect(this, &Filter::imgProcessedReady, pDownstreamFilter, &Filter::handleProcessedFrameCV);
    }
    return ret;
}

void HSVFilter::DisconnectAll()
{
    __super::DisconnectAll();

    disconnect(this, &Filter::imgProcessedReady, 0, 0);
    disconnect(this, SIGNAL(iamStarted()), 0, 0);
    disconnect(this, SIGNAL(iamStopped()), 0, 0);
}

void HSVFilter::handleUpstreamStarted()
{
    ++m_startedUpstreamFilters;
    DSG(1, "HSVFilter::handleUpstreamStarted m_startedUpstreamFilters=%d m_pUpstreamFilterList.size()=%d",
        m_startedUpstreamFilters, m_pUpstreamFilterList.size());
    if (m_startedUpstreamFilters == m_pUpstreamFilterList.size())
    {
        __super::handleUpstreamStarted();


    }
}

void HSVFilter::handleUpstreamStopped()
{
    --m_startedUpstreamFilters;
    DSG(1, "HSVFilter::handleUpstreamStopped m_startedUpstreamFilters=%d", m_startedUpstreamFilters);
    if (m_startedUpstreamFilters == 0)
    {
        m_imageList.clear();
        __super::handleUpstreamStopped(); // change filter state and emit signal to next filter
    }
}

void HSVFilter::handleColorFrameCV(const cv::Mat &frame)
{
    //DSG(1, "HSVFilter::handleProcessedFrameCV");
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 10)
        {
            DSG(1, "queue m_imageList.size() drop = %d", m_imageList.size());
            cv::Mat image = m_imageList.dequeue();
            image.release();
        }

        m_imageList.enqueue(frame);
    }

    //if (m_readyToEmit)
    {
        if (!m_timer.isActive())
            m_timer.start(0, this);
    }
}

void HSVFilter::PrepareImageForLaneDetection(const Mat &matScaledOrgClone)
{
    m_matMaskHSV_H = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matMaskHSV_S = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matMaskHSV_V = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matMaskHSV = Mat::zeros(matScaledOrgClone.size(), CV_8U);
    m_matHSVOut = Mat::zeros(matScaledOrgClone.size(), CV_8UC3);
    m_matScaledImageF = Mat::zeros(matScaledOrgClone.size(), CV_32FC3);
}

void HSVFilter::ReactFromTrackbar(const Mat &in, Mat *lpMaskH, Mat *lpMaskS, Mat *lpMaskV)
{
    vector<Mat> chHSV;
    split(in, chHSV);

    inRange(chHSV[0], Scalar(m_thresH), Scalar(m_thresHMax), *lpMaskH);
    inRange(chHSV[1], Scalar(m_thresS / float(k_hsv_s_max)), Scalar(m_thresSMax / float(k_hsv_s_max)), *lpMaskS);
    inRange(chHSV[2], Scalar(m_thresV / float(k_hsv_v_max)), Scalar(m_thresVMax / float(k_hsv_v_max)), *lpMaskV);

}

void 
HSVFilter::UpdateOutputMat(const Mat &srcF, const Mat &maskH, const Mat &maskS, const Mat &maskV, Mat &mask, Mat &dst)
{
    Mat matTmpDst = Mat::zeros(srcF.size(), CV_32FC3);
    for (int rowIdx = 0; rowIdx < srcF.rows; ++rowIdx)
    {
        for (int colIdx = 0; colIdx < srcF.cols; ++colIdx)
        {
            if ((255 == maskH.at<uchar>(rowIdx, colIdx)) &&
                (255 == maskS.at<uchar>(rowIdx, colIdx)) &&
                (255 == maskV.at<uchar>(rowIdx, colIdx)))
            {
                mask.at<uchar>(rowIdx, colIdx) = 255;
                matTmpDst.at<Vec3f>(rowIdx, colIdx) = srcF.at<Vec3f>(rowIdx, colIdx);
            }
            else
            {
                //2018.1004
                mask.at<uchar>(rowIdx, colIdx) = 0;// 255;
                matTmpDst.at<Vec3f>(rowIdx, colIdx) = 0;
            }
        }
    }

    matTmpDst.convertTo(dst, CV_8UC3, 255, 0);
}

void HSVFilter::ThresholdCallback(int pos, void *userdata)
{
    //TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    {
        m_calcTimer.ResetTimer();
        Mat hsvInF;
        cv::cvtColor(m_matScaledImageF, hsvInF, COLOR_BGR2HSV);

        vector<Mat> chHSV;
        split(m_matScaledImageF, chHSV);

        inRange(chHSV[0], Scalar(m_thresH), Scalar(m_thresHMax), m_matMaskHSV_H);
        inRange(chHSV[1], Scalar(m_thresS / float(k_hsv_s_max)), Scalar(m_thresSMax / float(k_hsv_s_max)), m_matMaskHSV_S);
        inRange(chHSV[2], Scalar(m_thresV / float(k_hsv_v_max)), Scalar(m_thresVMax / float(k_hsv_v_max)), m_matMaskHSV_V);
#ifdef TEST_TIME
        m_calcTimer.Print(0, "HSVFilter::ThresholdCallback");
        m_calcTimer.ResetTimer();
#endif
        Mat matTmpDst = m_matScaledImageF.clone();
        for (int rowIdx = 0; rowIdx < matTmpDst.rows; ++rowIdx)
        {
            for (int colIdx = 0; colIdx < matTmpDst.cols; ++colIdx)
            {
                if ((255 == m_matMaskHSV_H.at<uchar>(rowIdx, colIdx)) &&
                    (255 == m_matMaskHSV_S.at<uchar>(rowIdx, colIdx)) &&
                    (255 == m_matMaskHSV_V.at<uchar>(rowIdx, colIdx)))
                {
                    m_matMaskHSV.at<uchar>(rowIdx, colIdx) = 255;
                    matTmpDst.at<Vec3f>(rowIdx, colIdx) = m_matScaledImageF.at<Vec3f>(rowIdx, colIdx);
                }
                else
                {
                    //2018.1004
                    m_matMaskHSV.at<uchar>(rowIdx, colIdx) = 0;// 255;
                    matTmpDst.at<Vec3f>(rowIdx, colIdx) = 0;
                }
            }
        }

        matTmpDst.convertTo(m_matHSVOut, CV_8UC3, 255, 0);
#ifdef TEST_TIME
        m_calcTimer.Print(1, "HSVFilter::ThresholdCallback");
#endif
    }
}

void HSVFilter::DoImageProcessing(const Mat &matImage)
{
    static int duration = 0;

    if (0 == m_currentFrame)
        PrepareImageForLaneDetection(matImage);

    m_matScaledImageF.release();
    matImage.convertTo(m_matScaledImageF, CV_32FC3, 1.0 / 255, 0);

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    if ((pCache->lanedetection_enabled) && (!pCache->lanedetection_paused))
    //if ((!pCache->lanedetection_manual_enabled) && (pCache->lanedetection_enabled) && (!pCache->lanedetection_paused))
    {
        ThresholdCallback(0, 0);
        //imshow("m_matMaskHSV", m_matMaskHSV);
        //PrintMatInfo(m_matMaskHSV);
        emit imgProcessedReady(m_matMaskHSV);
        emit cvColorReady(m_matHSVOut);
    }
    else
        emit cvColorReady(matImage);
}

void HSVFilter::timerEvent(QTimerEvent *event)
{
    //qDebug("HSVFilter::timerEvent (%s) ev->timerId()=%d m_nTimerID=%d", m_name, event->timerId(), m_timer.timerId());
    //if (event->timerId() != m_nTimerID) return;
    if (event->timerId() != m_timer.timerId())  return; // { QObject::timerEvent(event); return; }
    //qDebug("FilterThread::timerEvent (%s) 1", m_name);
    if (e_component_state_started != m_state)
        return;

    m_timer.stop();
    {
        QMutexLocker locker(&m_mutex);
        if (m_imageList.size() > 0)
        {
            cv::Mat image = m_imageList.dequeue();
            m_frame = image.clone();
            image.release();
        }
    }

    //qDebug("FilterThread::timerEvent (%s) m_frame=%d %d", m_name, m_frame.cols, m_frame.rows);
    DoImageProcessing(m_frame);

    ++m_currentFrame;
    if ((m_currentFrame % 30) == 0)
        DSG(1, "HSVFilter::DoImageProcessing m_currentFrame=%d", m_currentFrame);

    //Sleep(1);
}