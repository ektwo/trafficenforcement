#include "sobel.h"

sobel::sobel()
{

}
#if 0
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <vector>
#include <stdio.h>
#include "linefinder.h"

using namespace cv;

int main(int argc, char* argv[])
{
    int houghVote = 200;
    string arg = argv[1];
    Mat image;
    image = imread(argv[1]);
    Mat gray;
	cv::cvtColor(image,gray,CV_RGB2GRAY);
    GaussianBlur( gray, gray, Size( 5, 5 ), 0, 0 );
    vector<string> codes;
    Mat corners;
    findDataMatrix(gray, codes, corners);
    drawDataMatrixCodes(image, codes, corners);
    //Mat image = imread("");
    //Rect region_of_interest = Rect(x, y, w, h);
    //Mat image_roi = image(region_of_interest);
    std::cout << image.cols << "\n";
    std::cout << image.rows << "\n";
    Rect roi(0,290,640,190);// set the ROI for the image
    Mat imgROI = image(roi);
    // Display the image
    imwrite("original.bmp", imgROI);
    // Canny algorithm
    Mat contours;
    Canny(imgROI, contours, 120, 300, 3);
    imwrite("canny.bmp", contours);
    Mat contoursInv;
    threshold(contours,contoursInv,128,255,THRESH_BINARY_INV);
    // Display Canny image
    imwrite("contours.bmp", contoursInv);

    /*
    Hough tranform for line detection with feedback
    Increase by 25 for the next frame if we found some lines.
    This is so we don't miss other lines that may crop up in the next frame
    but at the same time we don't want to start the feed back loop from scratch.
    */
    std::vector<Vec2f> lines;
    if (houghVote < 1 or lines.size() > 2){ // we lost all lines. reset
    houghVote = 200;
    }else{
    houghVote += 25;
    }
    while(lines.size() < 5 && houghVote > 0){
    HoughLines(contours,lines,1,PI/180, houghVote);
    houghVote -= 5;
    }
    std::cout << houghVote << "\n";
    Mat result(imgROI.size(),CV_8U,Scalar(255));
    imgROI.copyTo(result);
    // Draw the limes
    std::vector<Vec2f>::const_iterator it= lines.begin();
    Mat hough(imgROI.size(),CV_8U,Scalar(0));
    while (it!=lines.end()) {
    float rho= (*it)[0];   // first element is distance rho
    float theta= (*it)[1]; // second element is angle theta
    if ( theta > 0.09 && theta < 1.48 || theta < 3.14 && theta > 1.66 ) {
    // filter to remove    vertical and horizontal lines
    // point of intersection of the line with first row
    Point pt1(rho/cos(theta),0);
    // point of intersection of the line with last row
    Point pt2((rho-result.rows*sin(theta))/cos(theta),result.rows);
    // draw a white line
    line( result, pt1, pt2, Scalar(255), 8);
    line( hough, pt1, pt2, Scalar(255), 8);
    }
    ++it;
    }

// Display the detected line image
std::cout << "line image:"<< "\n";
namedWindow("Detected Lines with Hough");
imwrite("hough.bmp", result);

// Create LineFinder instance
LineFinder ld;

// Set probabilistic Hough parameters
ld.setLineLengthAndGap(60,10);
ld.setMinVote(4);

    // Detect lines
    std::vector<Vec4i> li= ld.findLines(contours);
    Mat houghP(imgROI.size(),CV_8U,Scalar(0));
    ld.setShift(0);
    ld.drawDetectedLines(houghP);
    std::cout << "First Hough" << "\n";
    imwrite("houghP.bmp", houghP);

    // bitwise AND of the two hough images
    bitwise_and(houghP,hough,houghP);
    Mat houghPinv(imgROI.size(),CV_8U,Scalar(0));
    Mat dst(imgROI.size(),CV_8U,Scalar(0));
    threshold(houghP,houghPinv,150,255,THRESH_BINARY_INV); // threshold and invert to black lines
    namedWindow("Detected Lines with Bitwise");
    imshow("Detected Lines with Bitwise", houghPinv);

    Canny(houghPinv,contours,100,350);
    li= ld.findLines(contours);
    // Display Canny image
    imwrite("contours.bmp", contoursInv);

    // Set probabilistic Hough parameters
    ld.setLineLengthAndGap(5,2);
    ld.setMinVote(1);
    ld.setShift(image.cols/3);
    ld.drawDetectedLines(image);

    std::stringstream stream;
    stream << "Lines Segments: " << lines.size();

    putText(image, stream.str(), Point(10,image.rows-10), 2, 0.8, Scalar(0,0,255),0);
    imwrite("processed.bmp", image);

    char key = (char) waitKey(10);
    lines.clear();
 }
#endif
