#ifndef FILTER_HSV_H
#define FILTER_HSV_H

#include "filter.h"
#include "escort_graphicsscene.h"

using namespace std;
using namespace cv;

class HSVFilter : public Filter
{
    Q_OBJECT
public:
    HSVFilter(BaseGraph *parent, int profileIdx);
    ~HSVFilter();

    bool ConnectTo(Filter *pDownstreamFilter);
    void DisconnectAll();

public slots:
    void handleUpstreamStarted();
    void handleUpstreamStopped();
    void handleColorFrameCV(const cv::Mat &frame);
    void handleProcessedFrameCV(const cv::Mat &frame) {}

protected:
    void PrepareImageForLaneDetection(const Mat &matScaledOrgClone);
    void ReactFromTrackbar(const Mat &in, Mat *lpMaskH, Mat *lpMaskS, Mat *lpMaskV);
    void UpdateOutputMat(const Mat &srcF, const Mat &maskH, const Mat &maskS, const Mat &maskV, Mat &mask, Mat &dst);
    void ThresholdCallback(int pos, void *userdata);
    void DoImageProcessing(const Mat &matImage);
    virtual void timerEvent(QTimerEvent *event);

private:
    int m_thresH;
    int m_thresS;
    int m_thresV;
    int m_thresHMax;
    int m_thresSMax;
    int m_thresVMax;

    Mat m_matHSVInF;
    Mat m_matHSVOut;
    Mat m_matScaledImageF;
    vector<Mat> m_matHSVChannel;
    Mat m_matMaskHSV_H;
    Mat m_matMaskHSV_S;
    Mat m_matMaskHSV_V;
    Mat m_matMaskHSV;

    EscortGraphicsScene *m_pScene;
    //void timerEvent(QTimerEvent * ev);
};

#endif // FILTER_HSV_H
