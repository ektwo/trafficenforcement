#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

Mat g_imgROIGrayIPMGaussian;

Mat& GetImgGaussianMatrix()
{
    return g_imgROIGrayIPMGaussian;
}

void CreateGuassianAndShow(Mat &matImgIPMFromGray, const char *strGaussianWindowName, Mat *kernelX, Mat *kernelY)
{
    /// 高斯模糊
    ///
    //sepFilter2D(matImgIPMFromGray, g_imgROIGrayIPMGaussian, matImgIPMFromGray.depth(), gaussianKernelX, gaussianKernelY);
    sepFilter2D(matImgIPMFromGray, g_imgROIGrayIPMGaussian, matImgIPMFromGray.depth(), *kernelX, *kernelY);
#if (SHOW_GAUSSIAN == 1)
    imshow(strGaussianWindowName, g_imgROIGrayIPMGaussian);
#endif
}

void drawLine_Guassian(Mat &matResp)
{
    int i;
    Point lp(0, g_imgROIGrayIPMGaussian.rows - matResp.at<float>(0, 0));
    Point cp;
    for (i = 1; i < matResp.cols; i++) {
        cp.x = i;
        cp.y = g_imgROIGrayIPMGaussian.rows - matResp.at<float>(0, i);
        line(g_imgROIGrayIPMGaussian, lp, cp, CV_RGB(255, 255, 255));
        lp = cp;
    }
}
