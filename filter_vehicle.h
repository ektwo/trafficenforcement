#ifndef FILTER_VEHICLE_H
#define FILTER_VEHICLE_H

#include "filter.h"
#include <opencv2/objdetect/objdetect.hpp>
#include "common.h"
#include "pythonwrapper.h"


class VehicleFilter : public Filter
{
    Q_OBJECT
public:
    VehicleFilter(BaseGraph *parent, int profileIdx);
    ~VehicleFilter();

    bool ConnectTo(Filter *pDownstreamFilter);
    void DisconnectAll();

signals:
    void updateVehicleInfo(const std::vector<cv::Vec4i> &vehicleArray);
    void updateImageList();

public slots:
    void handleUpstreamStarted();
    void handleUpstreamStopped();
    void handleColorFrameCV(const cv::Mat &frame);
    void handleProcessedFrameCV(const cv::Mat &frame) {}

protected:
    void FilterOutUnreasonableObject(std::vector<cv::Vec4i> &inVehicleArrary, 
                                     std::vector<cv::Vec4i> &outVehicleArrary,
                                     int boundingW,
                                     int boundingH);
    void PrepareImageForVehicleDetection(const cv::Mat &matScaledOrgClone);
    void ThresholdCallbackVehicle(int pos, void *userdata);
    void DoImageProcessing(const cv::Mat &matImage);
    virtual void timerEvent(QTimerEvent *event);


private:
    bool m_modelLoaded;
    int m_classifierScaleFactor;
    int m_classifierMinNeighbors;
    int m_classifierFlags;
    int m_classifierMinSize;
    int m_classifierMaxSize;
    int m_periodCnt;

    CPythonWrapper *m_pPythonWrapper;
    CPythonWrapperModel *m_pPythonWrapperModel;
    cv::CascadeClassifier m_cascade;
    cv::Mat m_matScaledImage;
    cv::Mat m_matCommonVehicleIn;
    //cv::Mat m_matDisplay;
    std::vector<cv::Vec4i> m_vehicleArray;
};

#endif // FILTER_VEHICLE_H
