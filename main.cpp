#include "mainwindow.h"
#include <QApplication>
#include <QTextCodec>
#include "pythonwrapper.h"
#include "DarkStyle.h"
#include "framelesswindow.h"

//#include <QProcessEnvironment>
#pragma push_macro("slots")
#undef slots
#include "Python.h"
#pragma pop_macro("slots")
FramelessWindow *g_f;
MainWindow *g_m;
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setStyle(new DarkStyle);

    TrafficEnforcement::GlobalCacheData *pCache = TrafficEnforcement::GlobalCacheData::instance();
    pCache->InitParameters();

    FramelessWindow framelessWindow;
      //framelessWindow.setWindowState(Qt::WindowMaximized);
      //framelessWindow.setWindowTitle("test title");
      //framelessWindow.setWindowIcon(a.style()->standardIcon(QStyle::SP_DesktopIcon));

      // create our mainwindow instance
    g_f = &framelessWindow;

    MainWindow *mainWindow = new MainWindow;

    // add the mainwindow to our custom frameless window
    framelessWindow.setContent(mainWindow);
    framelessWindow.show();

    //w.show();



    return app.exec();
}
