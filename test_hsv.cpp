#include "test_hsv.h"
#include <opencv2/opencv.hpp>
#include <QDebug>

using namespace cv;
Mat colorMapHV;
Mat colorMapHS;
Mat colorMapSV;

int H = 180;
int MAX_H = 360;

int S = 0;
int MAX_S = 255;

int V = 0;
int MAX_V = 255;

const char *k_str_hv = "H-V";
const char *k_str_hs = "H-S";
const char *k_str_sv = "S-V";


#if 0
          //qDebug("matScaled.type=%d", matScaled.type());
        qDebug("matScaled.at<Vec3b>[0]=%d %d %d",
               matScaled.at<Vec3b>(0, 0)[0], //B 212
                matScaled.at<Vec3b>(0, 0)[1], //G 86
                matScaled.at<Vec3b>(0, 0)[2]); //R 211

        Mat matScaledHSV_B;
		cv::cvtColor(matScaled, matScaledHSV_B, COLOR_BGR2HSV);
        qDebug("matScaledHSV_B.type=%d", matScaledHSV_B.type());
        qDebug("matScaledHSV_B.at<Vec3b>[0]=%d %d %d",
               matScaledHSV_B.at<Vec3b>(0, 0)[0], //150
                matScaledHSV_B.at<Vec3b>(0, 0)[1], //152
                matScaledHSV_B.at<Vec3b>(0, 0)[2]); //212

        Mat matScaledF;
        matScaled.convertTo(matScaledF, CV_32FC3, 1.0 / 255, 0);
        //qDebug("matScaledF.type=%d", matScaledF.type());
        qDebug("matScaledF.at<Vec3f211>[0]=%f %f %f",
               matScaledF.at<Vec3f>(0, 0)[0], // 212/255
                matScaledF.at<Vec3f>(0, 0)[1], // 86/255
                matScaledF.at<Vec3f>(0, 0)[2]); //211/255

        Mat matScaledHSV_F;
		cv::cvtColor(matScaledF, matScaledHSV_F, COLOR_BGR2HSV);
        qDebug("matScaledHSV_F.type=%d", matScaledHSV_F.type());
        qDebug("matScaledHSV_F.at<Vec3f>[0]=%f %f %f",
               matScaledHSV_F.at<Vec3f>(0, 0)[0], // 299.523804
                matScaledHSV_F.at<Vec3f>(0, 0)[1], // 0.594340
                matScaledHSV_F.at<Vec3f>(0, 0)[2]); // 0.831373
#endif


void callbackH(int, void*)
{
    const int n = (MAX_S * 2);
    for (int y = 0; y < colorMapSV.rows; ++y) //0~255
    {
        for (int x = 0; x < colorMapSV.cols; ++x) //0~511
        {
            //colorMapSV.at<Vec3f>(colorMapSV.rows -1 - y, x) = Vec3f(H, x / float(MAX_S), y / float(MAX_V));
            if (x < MAX_S)
            {
                colorMapSV.at<Vec3f>(colorMapSV.rows -1 - y, x) =
                        Vec3f(((x / float(colorMapSV.cols - 1) )*(MAX_H/2)),
                              (colorMapSV.cols - 1 - (MAX_S + x)) / float(MAX_S),
                              y / float(MAX_V));
            }
            else
                colorMapSV.at<Vec3f>(colorMapSV.rows -1 - y, x) =
                        Vec3f(((x / float(colorMapSV.cols - 1) )*(MAX_H/2)),
                              (x - MAX_S) / float(MAX_S),
                              y / float(MAX_V));

        }
    }

	cv::cvtColor(colorMapSV, colorMapSV, COLOR_HSV2BGR);

    imshow(k_str_sv, colorMapSV);
}

void callbackS(int, void*)
{
    for (int y = 0; y < colorMapHV.rows; ++y) //255
    {
        for (int x = 0; x < colorMapHV.cols; ++x) //360
        {
            colorMapHV.at<Vec3f>(colorMapHV.rows -1 - y, x) = Vec3f(x, S / float(MAX_S), y / float(MAX_V));
        }
    }

	cv::cvtColor(colorMapHV, colorMapHV, COLOR_HSV2BGR);

    imshow(k_str_hv, colorMapHV);
}

void callbackV(int, void*)
{

    for (int y = 0; y < colorMapHS.rows; ++y) //255
    {
        for (int x = 0; x < colorMapHS.cols; ++x) //360
        {
            colorMapHS.at<Vec3f>(colorMapHS.rows -1 - y, x) = Vec3f(x, y / float(MAX_S), V / float(MAX_V));
        }
    }

	cv::cvtColor(colorMapHS, colorMapHS, COLOR_HSV2BGR);

    imshow(k_str_hs, colorMapHS);
}

TestHSV::TestHSV()
{

}

void TestHSV::Run()
{
    //rows = height
    //cols = width
    colorMapHV.create(MAX_V+1, MAX_H+1, CV_32FC3); //rows, cols, ...
    colorMapHS.create(MAX_S+1, MAX_H+1, CV_32FC3); //rows, cols, ...
    colorMapSV.create(MAX_V+1, (MAX_S * 2)+1, CV_32FC3); //rows, cols, ...
qDebug("%d %d", colorMapSV.cols, colorMapSV.rows);

    namedWindow(k_str_sv, WINDOW_AUTOSIZE);
    createTrackbar("H", k_str_sv, &H, MAX_H, callbackH);

    namedWindow(k_str_hv, WINDOW_AUTOSIZE);
    createTrackbar("S", k_str_hv, &S, MAX_S, callbackS);

    namedWindow(k_str_hs, WINDOW_AUTOSIZE);
    createTrackbar("V", k_str_hs, &V, MAX_V, callbackV);
}

void TestHSV::run2()
{
    Mat src;
    Mat hsv;
    Mat dst;
    Mat r,r2,g,b;


    src=imread("D:\\Work\\projects\\TrafficEnforcementData\\DSCF4460.JPG");
    //Mat srca=imread("D:\\Work\\projects\\TrafficEnforcementData\\Images\\420.jpg");
    //Mat srcb=imread("D:\\Work\\projects\\TrafficEnforcementData\\Images\\420.png");
    //qDebug("src.type()=%d", src.type());

    Mat mask=Mat::zeros(src.rows,src.cols, CV_8U);

    cv::cvtColor(src,hsv,CV_BGR2HSV);

    inRange(hsv,Scalar(0,100,120) , Scalar(10,255,255), r);
    inRange(hsv,Scalar(170,100,120) , Scalar(180,255,255), r2);
    inRange(hsv,Scalar(50,100,120) ,Scalar(70,255,255), g);
    inRange(hsv,Scalar(110,100,120) , Scalar(130,255,255), b);

    mask=r+r2+g+b;

    src.copyTo(dst,mask );

    imshow("r",r);//show b
    imshow("r2",r2);//show b
    imshow("b",b);//show b
    imshow("mask",mask);//show mask
    imshow("src",src);
    imshow("result",dst);
}
