#ifndef ABSTRACTCAMERA_H
#define ABSTRACTCAMERA_H

#include <stdint.h>
#include <vector>
#include <QObject>
#include <qjsonobject.h>
#include "traffic_enforcement.h"
#include "abstractsource.h"


typedef struct S_Camera_Capabilities_Tag {
    int32_t width;
    int32_t height;
    int32_t frame_cnt;
    double fps;

} SCameraCapabilities;

typedef struct S_Camera_Config_Tag {
    uint32_t device_idx;
    uint32_t interval_ms;
    int32_t  want_width;
    int32_t  want_height;
    int32_t ir_value;
    char    filename[2048];
} SCameraConfig;

class AbstractCamera : public AbstractSource//QObject
{
    Q_OBJECT
public:
    AbstractCamera(EDeviceType deviceType, BaseGraph *parent = {}) : 
        AbstractSource(parent),//QObject(parent),
        m_devicetype(deviceType) {
        memset(&m_config, 0, sizeof(m_config));
    }
    virtual ~AbstractCamera(){};

    //virtual bool SetConfig(SCameraConfig *pConfig) = 0;
    //virtual bool Open() = 0;
    //virtual void Close() = 0;
    std::vector<SCameraCapabilities>& GetCameraCapabilities() {
        return m_capabilitiesList;
    }

    EDeviceType GetDeviceType() { return m_devicetype; }

protected:
    SCameraConfig m_config;
    std::vector<SCameraCapabilities> m_capabilitiesList;

public:
    EDeviceType m_devicetype;
};


#endif
