#include "optionsdialog.h"
#include <QSettings>
#include "ui_optionsdialog.h"
#include "traffic_enforcement.h"


OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);

	m_settingsFileName = QApplication::applicationDirPath() + "/TrafficEnforcement.ini";

	LoadSettings();
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

void OptionsDialog::closeEvent(QCloseEvent *bar)
{
    //qDebug("OptionsDialog::closeEvnet");


}

void OptionsDialog::LoadSettings()
{
	QSettings settings(m_settingsFileName, QSettings::IniFormat);

	settings.setIniCodec("UTF-8");

	//for (int i = 0; i < DIM(k_lanedetection_cycletime); ++i)
    //	ui->OptionsDialog_CB_LD_HowOften->addItem(k_lanedetection_cycletime[i].description);
	//for (int i = 0; i < DIM(k_vehicledetection_cycletime); ++i)
	//	ui->OptionsDialog_CB_VD_HowOften->addItem(k_lanedetection_cycletime[i].description);

	for (int i = 0; i < DIM(k_lanedetection_cycletime); ++i)
        ui->OptionsDialog_CB_LD_HowOften->insertItem(i, k_lanedetection_cycletime[i].description);
	for (int i = 0; i < DIM(k_vehicledetection_cycletime); ++i)
		ui->OptionsDialog_CB_VD_HowOften->insertItem(i, k_vehicledetection_cycletime[i].description);
	for (int i = 0; i < 101; ++i)
	{
		QString str;
		str.sprintf("%d %%", i);
		ui->OptionsDialog_CB_VD_1PassDiffThreshold->insertItem(i, str);
	}
	for (int i = 0; i < DIM(k_vehicle_detection_algorithm); ++i)
		ui->OptionsDialog_CB_VD_Algorithm->insertItem(i, k_vehicle_detection_algorithm[i].description);

    ui->OptionsDialog_CB_LD_HowOften->setCurrentIndex(
		settings.value(QString(k_str_tools_opt_ld_cycletime_idx), 0).toInt());
	ui->OptionsDialog_CB_VD_HowOften->setCurrentIndex(
		settings.value(QString(k_str_tools_opt_vd_cycletime_idx), 0).toInt());
	ui->OptionsDialog_CB_VD_1PassDiffThreshold->setCurrentIndex(
		settings.value(QString(k_str_tools_opt_vd_1pass_diffthreshold_idx), 30).toInt());
	ui->OptionsDialog_CB_VD_Algorithm->setCurrentIndex(
		settings.value(QString(k_str_tools_opt_vd_model_idx), 0).toInt());

}

void OptionsDialog::SaveSettings()
{
	QSettings settings(m_settingsFileName, QSettings::IniFormat);

	settings.setIniCodec("UTF-8");

	settings.setValue(QString(k_str_tools_opt_ld_cycletime_idx),
        ui->OptionsDialog_CB_LD_HowOften->currentIndex());
	settings.setValue(QString(k_str_tools_opt_vd_cycletime_idx),
		ui->OptionsDialog_CB_VD_HowOften->currentIndex());
	settings.setValue(QString(k_str_tools_opt_vd_1pass_diffthreshold_idx),
		ui->OptionsDialog_CB_VD_1PassDiffThreshold->currentIndex());
	settings.setValue(QString(k_str_tools_opt_vd_model_idx),
		ui->OptionsDialog_CB_VD_Algorithm->currentIndex());
}

void OptionsDialog::on_OptionsDialog_CB_LD_HowOften_currentIndexChanged(int index)
{

}

void OptionsDialog::on_OptionsDialog_CB_VD_HowOften_currentIndexChanged(int index)
{

}

void OptionsDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    if ((QPushButton *)button == ui->buttonBox->button(QDialogButtonBox::Ok))
	{
		SaveSettings();
		this->close();
	}
    else if ((QPushButton *)button == ui->buttonBox->button((QDialogButtonBox::Cancel)))
	{
		this->close();
	}
}

void OptionsDialog::on_OptionsDialog_CB_VD_1PassDiffThreshold_currentIndexChanged(int index)
{

}

void OptionsDialog::on_OptionsDialog_CB_VD_Algorithm_currentIndexChanged(int index)
{

}
