'''
Script to test traffic light localization and detection
'''

import numpy as np
import tensorflow as tf
from PIL import Image
import os
from matplotlib import pyplot as plt
import time
from glob import glob
import cv2
cwd = os.path.dirname(os.path.realpath(__file__))
#import os
#os.environ["CUDA_VISIBLE_DEVICES"]="-1"    
#tf.device({'/device:GPU:0','/device:GPU:1'})#('/device:GPU:2')
#tf.device('/device:GPU:2')
#tf.device('/device:GPU:2')
#tf.device(['/device:GPU:1', '/device:GPU:2'])
# Uncomment the following two lines if need to use the visualization_tunitls
#os.chdir(cwd+'/models')
#from object_detection.utils import visualization_utils as vis_util
class LINE(object):
    def __init__(self,l_start,t_start,l_end,t_end):
        self.l_start=l_start#x
        self.l_end =l_end#x
        self.t_start=t_start#y
        self.t_end=t_end#y

def between(value,low,high):
    if (value>=low) and (value < high) :
       return(1)
    else:
       return(0)
def line_formula(line,l,t,left_region=0):
    slope = float(line.t_end-line.t_start)/float(line.l_end-line.l_start)
    result = slope*(l-line.l_start)+line.t_start-t
    #print (result,t,l,line.l_start,line.t_start,slope)#(2.33*(l-1108)+105-t)<0
    if (result*slope)>0:#right region of line
      if left_region==1:
         print("rrrr1-0 is ",result,left_region,l,t,slope)
         return(0)
      else:
         print("rrrr2-1 is ",result,left_region,l,t,slope)
         return(1)#in choose region
    else:#left region of line
      if left_region==1:
         print("rrrr3-1 is ",result,left_region,l,t,slope)
         return(1)
      else:
         print("rrrr4-0 is ",result,left_region,l,t,slope)
         return(0)

def road_region(line_r,line_l,l,t,r,d):
    if (line_formula(line_r,l,t,left_region=1) and line_formula(line_l,r,d,left_region=0) ):
        return(1)
    else:
        return(0)


class CarDetector(object):
    def __init__(self):

        self.car_boxes = []
        #self.count=0#david
        #self.fps_list=[]#david        
        os.chdir(cwd)
        print(cwd)
        #Tensorflow localization/detection model
        # Single-shot-dectection with mobile net architecture trained on COCO
        #detect_model_name = 'ssd_mobilenet_test'#
        # dataset
        detect_model_name = 'ssd_mobilenet_v1_045_CarOnlyLabel_RZImg768_324_ssd300_300_Train3420_Test50_MixKilong_TaipeiImgs_186745_mAPP59'#bad than 044 frame is unstable
        #detect_model_name = 'ssd_mobilenet_v1_044_CarOnlyLabel_RZImg768_324_ssd300_300_Train3420_Test50_MixKilong_TaipeiImgs_58454_mAPP59'#
        #detect_model_name = 'ssd_mobilenet_v1_043_CarOnlyLabel_RZImg768_324_ssd300_300_Train3012_Test54__2382_mAPP99'#Kangli data set
        #detect_model_name = 'ssd_mobilenet_v1_042_CarOnlyLabel_RZImg768_324_ssd300_300_Train3012_Test54_classP1_locP1_dropP7_82691_mAPP59'#xxbadx
        #detect_model_name = 'ssd_mobilenet_v1_041_CarOnlyLabel_RZImg768_324_ssd300_300_Train3012_Test54_class_lossP03_loc_loss_1_109793_mAPP56'#xxbadx
        #detect_model_name = 'ssd_mobilenet_v1_040_CarOnlyLabel_RZImg768_324_ssd300_300_Train3012_Test54_class_lossP5_loc_loss_1_36468_mAPP61'#***************************** good *****
        #detect_model_name = 'Object-detection_resize_1_3_form_ver_13Category_039_CarOnlyLabel_RZImg768_324_ssd300_300_Train3012_Test54_class_loss1_loc_loss_pnt5_27548_mapP546_besetMatch_byEye'#map0.35
        #detect_model_name = 'ssd_mobilenet_v1_035_onlyCarLabel_RZ500x300_556Train502Test54_DropPnt8_327977_mAPPnt6559'#map0.35
        #detect_model_name = 'ssd_mobilenet_v1_033_onlyCarLabel_RZ500x300_SSDRZ250x250_556Train502Test54_DropPnt8_80000_mAPPnt541'#map0.35
        #detect_model_name = 'ssd_mobilenet_v1_032_onlyCarLabel_RZ500x300_SSDRZ250x250_556Train502Test54_DropPnt8_20000_CmapPn5542'#map0.35
        #detect_model_name = 'ssd_mobilenet_v1_031_onlyCarLabel_RZ500x300_SSDRz224_224_IOUPnt8_FMMindepth8__556Train502Test54_DropPnt8_NMS_THPnt8_17447'#map0.35
        #detect_model_name = 'ssd_mobilenet_v1_030_onlyCarLabel_RZ500x300_556Train502Test54_DropPnt8_80000_NMS_THPnt8'#
        #detect_model_name = 'ssd_mobilenet_v1_030_onlyCarLabel_RZ500x300_556Train502Test54_DropPnt8_16664_NMS_THPnt1'#
        #detect_model_name = 'ssd_mobilenet_v1_030_onlyCarLabel_RZ500x300_556Train502Test54_DropPnt8_16664_mAP6034 _test_for_bazel'#
        #detect_model_name = 'ssd_mobilenet_v1_030_onlyCarLabel_RZ500x300_556Train502Test54_DropPnt8_20000_mAP6063'#
        #detect_model_name = 'ssd_mobilenet_v1_030_onlyCarLabel_RZ500x300_556Train502Test54_DropPnt8_16664_mAP6034'#----------------------
        #detect_model_name = 'ssd_mobilenet_v1_028_onlyCarLabel_RZ300x300_202imgs_more43imgs_DropPnt8_99098'#
        #detect_model_name = 'ssd_mobilenet_v1_027_onlyCarLabel_RZ300x300_202imgs_more43imgs_78051'#
        #detect_model_name = 'ssd_mobilenet_v1_025_onlyCarLabel_RZ533_300_18182'#
        #detect_model_name = 'ssd_mobilenet_v1_024_from013_formallabel_onlyCarLabel_78415'#*************************************************************************current best one 
        #detect_model_name = 'ssd_mobilenet_v1_023_from012_Train_epsilonFrom1to10'#xxx unstable frame happend
        #detect_model_name = 'ssd_mobilenet_v1_022_from012_formallabel_mixeCatLabel_80000'#xxxxxx
        #detect_model_name = 'ssd_mobilenet_v1_021_from012_formallabel_onlyCarLabel_18474'#better than 20000
        #detect_model_name = 'ssd_mobilenet_v1_021_from012_formallabel_onlyCarLabel_20000'#xxxxx
        #detect_model_name = 'ssd_mobilenet_v1_020_from012_formallabel_mixeCatLabel_20000'#
        #detect_model_name = 'ssd_mobilenet_v1_020_from012_tmp_2698'#
        #detect_model_name = 'ssd_mobilenet_v1_018_optimize_ssd_from017_bazel_graph'#
        #detect_model_name = 'ssd_mobilenet_v1_017_from010_train_20000_graph'#10
        #detect_model_name = 'ssd_mobilenet_v1_016_from010_train_11081_graph'#10
        #detect_model_name = 'ssd_mobilenet_v1_015_from010_train_3578_graph'#10
        #detect_model_name = 'ssd_mobilenet_v1_014_from009_add_MoreSample'#10
        #detect_model_name = 'ssd_mobilenet_v1_013_from008_add_MoreSample_and_label_motor_grap'#9
        #detect_model_name = 'ssd_mobilenet_v1_012_13Category_012_algign_rect_to_wholecar_compare_to_005_grap'#8
        #xxxdetect_model_name = 'ssd_mobilenet_v1_011_13Category_rec_CarOnlyWithCarNo_graph'#7 xxxx #some car didn't locate correct position, but may be too few sample(due to at least half car number)
        #xxxdetect_model_name = 'ssd_mobilenet_v1_010_13Category_rec_fullcarAndbusTruckIsCar_graph'#6#xxxxmore massive so classification make rec more clear
        #detect_model_name = 'ssd_mobilenet_v1_009_13Category_align_rec_to_tires_graph'#5#the most stable one!!!!!!!!!!! butif car drive with rotating some degree can't detect #rec easily float to car's upper loc
        #xxxdetect_model_name = 'ssd_mobilenet_v1_008_13Category_13trainEx_graph'#4xxxxxxx didn't label well/precisely




        #detect_model_name = 'ssd_mobilenet_v1_007_13Category_drop50_graph'
        #detect_model_name = 'ssd_mobilenet_v1_006_multicategory_done_graph'#bad

        #detect_model_name = 'ssd_mobilenet_v1_005_13Category_graph'
        #detect_model_name = 'ssd_mobilenet_v1_004_multicategory_graph'
        #detect_model_name = 'ssd_mobilenet_v1_003_car_NO3_graph'
        #detect_model_name = 'ssd_mobilenet_v1_coco_11_06_2017'
        #detect_model_name = 'ssd_mobilenet_v1_coco_2017_11_17'#trained model
        #detect_model_name = 'ssdlite_mobilenet_v2_coco_2018_05_09'
        #detect_model_name = 'ssd_mobilenet_v2_coco_2018_03_29'

        #detect_model_name = 'ssd_inception_v2_coco_2017_11_17'#bad
        #detect_model_name = 'faster_rcnn_inception_v2_coco_2018_01_28'#too slow for i5 only
        #detect_model_name = 'faster_rcnn_resnet50_coco_2017_11_08'
        #detect_model_name = 'mask_rcnn_inception_resnet_v2_atrous_coco_2018_01_28'
        
        #PATH_TO_CKPT = detect_model_name + '/frozen_inference_graph.pb'
        model_path='../models/kneron/'
        PATH_TO_CKPT = model_path+detect_model_name + '/frozen_inference_graph.pb'
        #print("PATH_TO_CKPT=",PATH_TO_CKPT)
        # setup tensorflow graph
        self.detection_graph = tf.Graph()
        
        # configuration for possible GPU use
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        # load frozen tensorflow detection model and initialize 
        # the tensorflow graph
        print(config)
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            print(od_graph_def)
            with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
               serialized_graph = fid.read()
               od_graph_def.ParseFromString(serialized_graph)
               tf.import_graph_def(od_graph_def, name='')
               
            self.sess = tf.Session(graph=self.detection_graph, config=config)
            self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
              # Each box represents a part of the image where a particular object was detected.
            self.boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
              # Each score represent how level of confidence for each of the objects.
              # Score is shown on the result image, together with the class label.
            self.scores =self.detection_graph.get_tensor_by_name('detection_scores:0')
            self.classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
            self.num_detections =self.detection_graph.get_tensor_by_name('num_detections:0')
    
    # Helper function to convert image into numpy array    
    def load_image_into_numpy_array(self, image):
         (im_width, im_height) = image.size
         return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)).astype(np.uint8)       
    # Helper function to convert normalized box coordinates to pixels
    #def box_normal_to_pixel(self, box, dim):
    def box_normal_to_pixel(self, box, dim,score):#david
    
        height, width = dim[0], dim[1]
        #box_pixel = [int(box[0]*height), int(box[1]*width), int(box[2]*height), int(box[3]*width)]
        box_pixel = [int(box[0]*height), int(box[1]*width), int(box[2]*height), int(box[3]*width),int(score*100)]#david
        return np.array(box_pixel)       
        
    def get_localization(self, image, visual=False):  
        """Determines the locations of the traffic light in the image

        Args:
            image: camera image

        Returns:
            list of bounding boxes: coordinates [y_up, x_left, y_down, x_right]

        """
        print("----------------img--------",len(image))
        category_index={1: {'id': 1, 'name': u'person'},
                        2: {'id': 2, 'name': u'bicycle'},
                        3: {'id': 3, 'name': u'car'},
                        4: {'id': 4, 'name': u'motorcycle'},
                        5: {'id': 5, 'name': u'airplane'},
                        6: {'id': 6, 'name': u'bus'},
                        7: {'id': 7, 'name': u'train'},
                        8: {'id': 8, 'name': u'truck'},
                        9: {'id': 9, 'name': u'boat'},
                        10: {'id': 10, 'name': u'traffic light'},
                        11: {'id': 11, 'name': u'fire hydrant'},
                        13: {'id': 13, 'name': u'stop sign'},
                        14: {'id': 14, 'name': u'parking meter'}}  
        '''        
        category_index={1: {'id': 1, 'name': u'car'}}  #david
        '''
        with self.detection_graph.as_default():
              image_expanded = np.expand_dims(image, axis=0)
              #start=time.time()#david
              (boxes, scores, classes, num_detections) = self.sess.run(
                  [self.boxes, self.scores, self.classes, self.num_detections],
                  feed_dict={self.image_tensor: image_expanded})
              '''       
              end=time.time()#david
              self.count=self.count+1#david
              self.fps_list.append(round(end-start,2))#david
              print("xxxxxxxxxxxxxxxxxxxxxxxxmodel fps is ",round(end-start,2),self.fps_list)#david
              '''       
              if visual == True:
                  vis_util.visualize_boxes_and_labels_on_image_array(
                      image,
                      np.squeeze(boxes),
                      np.squeeze(classes).astype(np.int32),
                      np.squeeze(scores),
                      category_index,
                      use_normalized_coordinates=True,min_score_thresh=.4,
                      line_thickness=3)
    
                  plt.figure(figsize=(9,6))
                  plt.imshow(image)
                  plt.show()  
              
              boxes=np.squeeze(boxes)
              classes =np.squeeze(classes)
              scores = np.squeeze(scores)
    
              cls = classes.tolist()
              
              # The ID for car is 3 
              idx_vec = [i for i, v in enumerate(cls) if ((v==3) and (scores[i]>0.1))]#85))]#0.3))]#0.1))]#david0.3))]
              #david idx_vec = [i for i, v in enumerate(cls) if ((v==1) and (scores[i]>0.3))]#david0.3))]
              
              if len(idx_vec) ==0:
                  print('no detection!')
              else:
                  tmp_car_boxes=[]
                  for idx in idx_vec:
                      dim = image.shape[0:2]
                      #david box = self.box_normal_to_pixel(boxes[idx], dim)
                      box = self.box_normal_to_pixel(boxes[idx], dim,scores[idx])#david
                      box_h = box[2] - box[0]
                      box_w = box[3] - box[1]
                      t=box[0]
                      l=box[1]
                      d=box[2]
                      r=box[3]
                      ratio = box_h/(box_w + 0.01)
                      
                      #david if ((ratio < 0.8) and (box_h>20) and (box_w>20)):
                      #if (1):#((box_h>20) and (box_w<400)):
                      if ( (t>1000 and between(l,500,3000)) ):
                          print("1")
                      elif (between(t,500,1000) and between(l,750,2500)):
                          print("2")
                      elif (between(t,0,500) and between(l,1200,2250)) :
                          print("3")

                      #015 DSCF4447.MOV
                      '''
                      L1S=1108
                      T1S=105
                      L1E=1476
                      T1E=963

                      L2S=800
                      T2S=179
                      L2E=400
                      T2E=959
                      L1S, T1S, L1E, T1E =(1108, 105, 1476, 963)
                      L2S, T2S, L2E, T2E =(800, 179, 400, 959)
                      '''
                      #line_r=LINE(1108,105,1476,963)
                      #line_l=LINE(800,179,400,959)
                      #015 DSCF4448.MOV
                      L1S, T1S, L1E, T1E =(1184, 218, 1885, 665)
                      L2S, T2S, L2E, T2E =(850, 267, 360, 1065)
                      
                      line_r=LINE(L1S,T1S,L1E,T1E)#LINE(1184,218,1885,665)
                      line_l=LINE(L2S,T2S,L2E,T2E)#(850,267,360,1056)
                      #cv2.line(image,(L1S,T1S),(L1E,L1E),(0,255,0),2)
                      #cv2.line(image,(L2S,T2S),(L2E,L2E),(0,255,0),2)
                      #cv2.line(image,(100,100),(100,1000),(0,255,0),10)
                      #cv2.line(image,(100,100),(1000,100),(0,255,0),10)
                      #cv2.line(image,(100,1000),(1000,1000),(0,255,0),10)
                      #cv2.line(image,(1000,100),(1000,1000),(0,255,0),10)


                      #print ("xxxxxxxxxxxx",line_formula(line,1178,125,left_region=1))
                     #if(1):# ( (t>1000 and between(l,500,3000) and box_w>500) or (between(t,500,1000) and between(l,750,2500) and box_w>300)or (between(t,200,500) and between(l,1200,2250) and box_w>250)):
                      if(box_w> 100):#008
                      #if( l>300 and t>150 and box_w>20 and box_w<500 and box_h >200 and box_h<400  and(ratio>0.5 and box_w>box_h) ):
                      #if( t>300 and box_w> 20 and box_h >20):
                      #if( t>150 and box_w> 200 and box_h >20):#008
                      #if(box_w> 120):#009
                      #if(t>200 and box_w> 100 ):#009
                      #if(t>100 and box_w> 90 ):#011
                      #if(t>100  and box_w> 90 and road_region(line_r,line_l,l,t,r,d) ):#015 DSCF4447.MOV
                          tmp_car_boxes.append(box)
                          print("--------------------------t,l",t,l,"\n")
                          print(box, ', confidence: ', scores[idx], 'ratio:', ratio,"pppppppppp",box[4])
                         
                      else:
                          pass
                          #print('wrong ratio or wrong size, ', box, ', confidence: ', scores[idx], 'ratio:', ratio)
                          
                          
                  
                  self.car_boxes = tmp_car_boxes
             
        return self.car_boxes
        
    
    
if __name__ == '__main__':
        
        det =CarDetector()
        os.chdir(cwd)
        TEST_IMAGE_PATHS= glob(os.path.join('test_images/', '*.jpg'))
        
        for i, image_path in enumerate(TEST_IMAGE_PATHS[0:-1]):
            print('')
            print('*************************************************')
            
            img_full = Image.open(image_path)
            img_full_np = det.load_image_into_numpy_array(img_full)
            img_full_np_copy = np.copy(img_full_np)
            start = time.time()
            b = det.get_localization(img_full_np, visual=False)
            end = time.time()
            print('Localization time: ', end-start)
#            
            
