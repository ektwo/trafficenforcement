#include <stdio.h>
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::ostringstream
#include <stdlib.h> 
#include <vector>
#include <opencv2/opencv.hpp>
#include "traffic_enforcement.h"
#include "common.h"
#include "pythonwrapper.h"
#include "arrayobject.h"

#include "Lib/site-packages/numpy/core/include/numpy/arrayobject.h"
static const char* SYS_PATH = "source.zip;stdlib.zip;lib/python35";
#define PYTHON_VERION_3
#define USE_GIL_CTRL

using namespace std;


//typedef struct tagBITMAPFILEHEADER {
//        uint16_t    bfType;
//        uint32_t    bfSize;
//        uint16_t    bfReserved1;
//        uint16_t    bfReserved2;
//        uint32_t   bfOffBits;
//} BITMAPFILEHEADER;

//typedef struct tagRGBQUAD {
//        uint8_t    rgbBlue;
//        uint8_t    rgbGreen;
//        uint8_t    rgbRed;
//        uint8_t    rgbReserved;
//} RGBQUAD;

//typedef struct tagBITMAPINFOHEADER{
//        uint32_t      biSize;
//        int32_t       biWidth;
//        int32_t       biHeight;
//        uint16_t      biPlanes;
//        uint16_t      biBitCount;
//        uint32_t      biCompression;
//        uint32_t      biSizeImage;
//        int32_t       biXPelsPerMeter;
//        int32_t       biYPelsPerMeter;
//        uint32_t      biClrUsed;
//        uint32_t      biClrImportant;
//} BITMAPINFOHEADER;

//typedef struct tagBITMAPINFO {
//    BITMAPINFOHEADER    bmiHeader;
//    RGBQUAD             bmiColors[1];
//} BITMAPINFO;

void showImageInfo(const char *filename)
{
    //PyObject *pArgs = Py_BuildValue("s", filename);
    ////PyObject *pKargs = Py_BuildValue("{s:i, s:i}", "a", a, "b", b);
    //PyObject *pRes = PyObject_Call(pFunc, pArgs, 0);
}

uint16_t GetDataWORD(unsigned char **pData)
{
    unsigned char *p = *pData;

    uint16_t ret = (p[1] << 8) | (p[0]);

    *pData += 2;

    return ret;
}

uint32_t GetDataDWORD(unsigned char **pData)
{
    unsigned char *p = *pData;

    uint32_t ret = (p[3] << 24) | (p[2] << 16) | (p[1] << 8) | (p[0]);

    *pData += 4;

    return ret;
}

static const char *k_module_name = "detector2";//"test2";
static const char *k_func_name_showimage = "showImageInfo2";
static const char *k_class_name = "CarDetector";// "Person";
static const char *k_class_func_name = "get_localization";


unsigned char data2[768 * 576 * 3];
//#if PY_VERSION_HEX >= 0x03000000
//void *
//#else
//void
//#endif
//InitializeArray()
//{
//	import_array();
//}

#if PY_VERSION_HEX >= 0x03000000


CPythonWrapper::CPythonWrapper()
{
    size_t error_pos;
    DSG(1, "Py_GetProgramFullPath()=%s", Py_EncodeLocale(Py_GetProgramFullPath(), &error_pos));
    DSG(1, "Py_GetPrefix()=%s", Py_EncodeLocale(Py_GetPrefix(), &error_pos));
    DSG(1, "Py_GetExecPrefix()=%s", Py_EncodeLocale(Py_GetExecPrefix(), &error_pos));
    DSG(1, "Py_GetPath()=%s", Py_EncodeLocale(Py_GetPath(), &error_pos));

    const char *name = "traffic_enforcement";
    wchar_t* program = NULL;
    program = Py_DecodeLocale(name, NULL);
    Py_SetProgramName(program);

    Py_Initialize();
    if (!Py_IsInitialized())
    {
        DSG(1, "Py_Initialize error");
    }
    else
    {
        //if (!PyEval_ThreadsInitialized()) {
        //    PyEval_InitThreads();
        //}
#ifdef USE_GIL_CTRL
        PyEval_InitThreads();
        int nInit = PyEval_ThreadsInitialized();
        if (nInit)
        {
            DSG(1, "PyEval_SaveThread");
            PyEval_SaveThread();
            DSG(1, "PyEval_SaveThread 2");
        }
#endif
    }

    if (program)
        PyMem_RawFree(program);
}

CPythonWrapper::~CPythonWrapper()
{
#ifdef USE_GIL_CTRL
    PyGILState_STATE gstate = PyGILState_Ensure();
#endif
    Py_Finalize();
}

CPythonWrapperModel::CPythonWrapperModel()
    : m_pDict(nullptr)
    , m_pHandle(nullptr)
{
}

CPythonWrapperModel::~CPythonWrapperModel()
{
    Release();
}

int  CPythonWrapperModel::Initialize()
{
    DSG(1, "CPythonWrapperModel::Initialize()");
    int ret = -1;
    PyObject *pFile = nullptr;
    PyObject *pModule = nullptr;
    PyObject *pClass = nullptr;


#ifdef USE_GIL_CTRL
    int nHold = PyGILState_Check();
    PyGILState_STATE gstate;
    if (!nHold)
    {
        gstate = PyGILState_Ensure();
    }
#endif

    if (PyArray_API == nullptr)
    {
        import_array();
    }
#ifdef USE_GIL_CTRL    
    Py_BEGIN_ALLOW_THREADS;
    Py_BLOCK_THREADS;
#endif


    do
    {
        pFile = PyUnicode_FromString(k_module_name);
        DSG(1, "CPythonWrapperModel::Initialize() pFile =%p", pFile);
        pModule = PyImport_Import(pFile);
        Py_DECREF(pFile);
        DSG(1, "CPythonWrapperModel::Initialize() pModule=%p", pModule);
        if (nullptr == pModule)
        {
            PyErr_Print();
            break;
        }

        m_pDict = PyModule_GetDict(pModule);
        DSG(1, "CPythonWrapperModel::Initialize() m_pDict=%p", m_pDict);
        if (nullptr == m_pDict)
        {
            PyErr_Print();
            break;
        }

        pClass = PyDict_GetItemString(m_pDict, k_class_name);
        DSG(1, "CPythonWrapperModel::Initialize() pClass=%p", pClass);
        if (nullptr == pClass)
        {
            PyErr_Print();
            break;
        }

        if (!PyCallable_Check(pClass)) {
            PyErr_Print();
            break;
        }

#if 1
        m_pHandle = PyObject_CallObject(pClass, nullptr);
#else
#ifdef PYTHON_VERION_3
        m_pHandle = PyInstanceMethod_New(pClass);
#else
        m_pHandle = PyInstance_New(pClass, nullptr, nullptr);
#endif
#endif

        DSG(1, "CPythonWrapperModel::Initialize() m_pHandle=%p", m_pHandle);
        if (nullptr == m_pHandle)
        {
            DSG(1, "CPythonWrapperModel::Initialize() PyErr_Occurred()=%d", PyErr_Occurred());

            PyObject *exc_type = NULL, *exc_value = NULL, *exc_tb = NULL;
            PyErr_Fetch(&exc_type, &exc_value, &exc_tb);
            PyObject* str_exc_type = PyObject_Repr(exc_type);
            PyObject* pyStr = PyUnicode_AsEncodedString(str_exc_type, "utf-8", "Error ~");
            const char *strExcType = PyBytes_AS_STRING(pyStr);
            DSG(1, "CPythonWrapperModel::Initialize() strExcType()11 =%s", strExcType);
            Py_XDECREF(str_exc_type);
            Py_XDECREF(pyStr);

            Py_XDECREF(exc_type);
            Py_XDECREF(exc_value);
            Py_XDECREF(exc_tb);

            break;
        }

        ret = 0;

    } while (0);

    if (pClass)
        Py_DECREF(pClass);
    //if (m_pDict)
    //       Py_DECREF(m_pDict);
    if (pModule)
        Py_DECREF(pModule);
    if (pFile)
        Py_DECREF(pFile);
#ifdef USE_GIL_CTRL
    Py_UNBLOCK_THREADS;
    Py_END_ALLOW_THREADS;

    if (!nHold)
    {
        PyGILState_Release(gstate);
    }
#endif
    return ret;
}

void CPythonWrapperModel::Release()
{
#ifdef USE_GIL_CTRL
    int nHold = PyGILState_Check();
    PyGILState_STATE gstate;
    if (!nHold)
    {
        gstate = PyGILState_Ensure();
    }

    Py_BEGIN_ALLOW_THREADS;
    Py_BLOCK_THREADS;
#endif
    if (m_pHandle)
        Py_DECREF(m_pHandle);
    if (m_pDict)
        Py_DECREF(m_pDict);
#ifdef USE_GIL_CTRL
    Py_UNBLOCK_THREADS;
    Py_END_ALLOW_THREADS;
    if (!nHold)
    {
        PyGILState_Release(gstate);
    }
#endif
}

std::vector<cv::Vec4i> CPythonWrapperModel::GetDetectedCars(cv::Mat &matImage)
{
    std::vector<cv::Vec4i> rcList;

    //DSG(1, "GetDetectedCars matImage.rows=%d matImage.cols=%d matImage.channels()=%d matImage.dims=%d",
    //    matImage.rows, matImage.cols, matImage.channels(), matImage.dims);

#ifdef USE_GIL_CTRL
    int nHold = PyGILState_Check();
    PyGILState_STATE gstate;
    if (!nHold)
    {
        gstate = PyGILState_Ensure();
    }

    Py_BEGIN_ALLOW_THREADS;
    Py_BLOCK_THREADS;
#endif
    PyObject *pArrayObj = nullptr;
    PyObject *pRet = nullptr;
    do
    {
        //PyObject_CallMethod(m_pHandle, "push", "ssi", name, sex, age);
        pArrayObj = PyTuple_New(1);
        //DSG(1, "pArrayObj=%p", pArrayObj);
        if (nullptr == pArrayObj)
        {
            PyErr_Print();
            break;
        }

        npy_intp dims[3] = { matImage.rows, matImage.cols, matImage.channels() };
        PyObject *pPyArray = PyArray_SimpleNewFromData(matImage.dims + 1, (npy_intp*)&dims, NPY_UBYTE, reinterpret_cast<void*>(matImage.data));
        //DSG(1, "pPyArray=%p", pPyArray);
        if (nullptr == pPyArray)
        {
            PyErr_Print();
            break;
        }
        PyTuple_SetItem(pArrayObj, 0, pPyArray);

        pRet = PyObject_CallMethod(m_pHandle, k_class_func_name, "O", pArrayObj);
        //DSG(1, "pRet=%p", pRet);
        if (nullptr == pRet)
        {
            PyErr_Print();
            break;
        }
        if (!PyList_Check(pRet))
        {
            PyErr_Print();
            break;
        }
        size_t SizeOfList = PyList_Size(pRet);
        //DSG(1, "SizeOfList=%u", SizeOfList); //2
        for (size_t n = 0; n < SizeOfList; n++)  //columns
        {
            PyArrayObject *pListItem = (PyArrayObject *)PyList_GetItem(pRet, n);
            if (pListItem)
            {
                int rows = pListItem->dimensions[0];
                cv::Vec4i rc;
                for (int i = 0; i < rows - 1; ++i)
                {
                    rc[i] = *(int32_t *)(pListItem->data + (i * pListItem->strides[0]));
                }
                rcList.push_back(rc);
            }
        }
    } while (0);

    if (pRet)
        Py_DECREF(pRet);

    if (pArrayObj)
        Py_DECREF(pArrayObj);
#ifdef USE_GIL_CTRL
    Py_UNBLOCK_THREADS;
    Py_END_ALLOW_THREADS;

    if (!nHold)
    {
        PyGILState_Release(gstate);
    }
#endif
    return rcList;
}

#endif
