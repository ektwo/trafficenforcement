#include "autogenerator_line.h"
#include "traffic_enforcement.h"
//#include <windows.h>
//#include <Synchapi.h>

static const int k_move_step = 2;


AutoGeneratorLine::AutoGeneratorLine()
    : m_count(0)
    , m_weightStrategyOfX1(e_weight_more_lower)
    , m_weightStrategyOfX2(e_weight_more_lower)
    , m_weightStrategyOfY1(e_weight_more_lower)
    , m_weightStrategyOfY2(e_weight_more_lower)
    , m_weightStrategyOfRotation(e_weight_rotation_clockwise)
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    , m_generator(QRandomGenerator::global())
#endif
    , m_rect(QRectF(0, 0, 0, 0))
{
}

void AutoGeneratorLine::SetBoundary(QRectF rect)
{
    m_rect = rect;
}

void initLine(qreal &pos1, qreal &pos2, qreal maxSize)
{
    if (pos1 < pos2)
    {
        if (pos2 < (pos1 + k_min_width_of_vehicle))
            pos2 = pos1 + k_min_width_of_vehicle;

        //if (pos1 > (maxWidth - k_min_width_of_vehicle))
        //    pos1 = (maxWidth - k_min_width_of_vehicle);
        if (pos2 > (maxSize - k_border_size))
        {
            pos1 -= (pos2 - (maxSize - k_border_size));
            pos2 = (maxSize - k_border_size);

            //pos2 = maxWidth - k_min_width_of_vehicle;
        }
    }
    else if (pos2 < pos1)
    {
        if (pos1 < (pos2 + k_min_width_of_vehicle))
            pos1 = pos2 + k_min_width_of_vehicle;

        if (pos1 > (maxSize - k_border_size))
        {
            pos2 -= (pos1 - (maxSize - k_border_size));
            pos1 = (maxSize - k_border_size);
        }
    }
    else
    {
        pos1 = (maxSize - k_min_width_of_vehicle) / 2;
        pos2 += pos1 + k_min_width_of_vehicle;
    }
}

QLineF AutoGeneratorLine::genLine()
{
    if ((m_rect.width() <= 0) || (m_rect.height() <= 0))
        return QLineF(0, 0, 0, 0);

#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    int posX1 = m_generator->bounded(static_cast<int>(m_rect.left() + k_border_size), static_cast<int>(m_rect.right() - k_border_size + 1));
    int posX2 = m_generator->bounded(static_cast<int>(m_rect.left() + k_border_size), static_cast<int>(m_rect.right() - k_border_size + 1));
    int posY1 = m_generator->bounded(static_cast<int>(m_rect.top() + k_border_size), static_cast<int>(m_rect.bottom() - k_border_size + 1));
    int posY2 = m_generator->bounded(static_cast<int>(m_rect.top() + k_border_size), static_cast<int>(m_rect.bottom() - k_border_size + 1));
#else
    int posX1 = GetBounded(static_cast<int>(m_rect.left() + k_border_size), static_cast<int>(m_rect.right() - k_border_size));
    int posX2 = GetBounded(static_cast<int>(m_rect.left() + k_border_size), static_cast<int>(m_rect.right() - k_border_size));
    int posY1 = GetBounded(static_cast<int>(m_rect.top() + k_border_size), static_cast<int>(m_rect.bottom() - k_border_size));
    int posY2 = GetBounded(static_cast<int>(m_rect.top() + k_border_size), static_cast<int>(m_rect.bottom() - k_border_size));
#endif

    qreal maxWidth = static_cast<qreal>(m_rect.right() - m_rect.left() - (k_border_size << 1));
    qreal maxHeight = static_cast<qreal>(m_rect.bottom() - m_rect.top() - (k_border_size << 1));

    qreal x1 = static_cast<qreal>(posX1);
    qreal x2 = static_cast<qreal>(posX2);
    qreal y1 = static_cast<qreal>(posY1);
    qreal y2 = static_cast<qreal>(posY2);

    initLine(x1, x2, maxWidth);
    initLine(y1, y2, maxHeight);

    return QLineF(posX1, posY1, posX2, posY2);
}

void getLowHighBound(int &lowBound, int &highBound, int strategyLowHigh, int strategyRotation)
{
    if (AutoGeneratorLine::e_weight_more_lower == strategyLowHigh)
    {
        if (AutoGeneratorLine::e_weight_rotation_clockwise == strategyRotation)
        {
            lowBound = - (k_move_step << 2);
            highBound = 1;
        }
        else if (AutoGeneratorLine::e_weight_rotation_counterclockwise == strategyRotation)
        {
            lowBound = 0;
            highBound = (k_move_step << 2) + 1;
        }
    }
    else if (AutoGeneratorLine::e_weight_more_higher == strategyLowHigh)
    {
        if (AutoGeneratorLine::e_weight_rotation_clockwise == strategyRotation)
        {
            lowBound = 0;
            highBound = (k_move_step << 2) + 1;
        }
        else if (AutoGeneratorLine::e_weight_rotation_counterclockwise == strategyRotation)
        {
            lowBound = - (k_move_step << 2);
            highBound = 1;
        }
    }
}

static
void AdjustPoint(int &strategy1, int &strategy2, qreal &pos1, qreal &pos2, qreal border, qreal minSize, qreal maxDistance)
{
    if (pos1 < pos2)
    {
        if (pos2 > (maxDistance - k_border_size))
        {
            pos1 -= (pos2 - (maxDistance - k_border_size));//maxDistance - minSize;
            pos2 = (maxDistance - k_border_size);
            strategy1 = AutoGeneratorLine::e_weight_more_lower;
        }
        else if (pos2 < (border + minSize))
        {
            pos1 = border;
            pos2 = border + minSize;
            strategy1 = AutoGeneratorLine::e_weight_more_higher;
        }
        else
        {
            if (pos1 < border)
            {
                pos2 += border - pos1;
                pos1 = border;
                strategy1 = AutoGeneratorLine::e_weight_more_higher;
            }
            if ((pos2 - pos1) < minSize)
                pos2 = pos1 + minSize;
        }
    }
    else if (pos2 < pos1)
    {
        if (pos1 > (maxDistance - k_border_size))
        {
            pos2 -= (pos1 - (maxDistance - k_border_size));//maxDistance - minSize;
            pos1 = (maxDistance - k_border_size);
            strategy1 = AutoGeneratorLine::e_weight_more_lower;
        }
        else if (pos1 < (border + minSize))
        {
            pos2 = border;
            pos1 = border + minSize;
            strategy1 = AutoGeneratorLine::e_weight_more_higher;
        }
        else
        {
            if (pos2 < border)
            {
                pos1 += border - pos2;
                pos2 = border;
                strategy1 = AutoGeneratorLine::e_weight_more_higher;
            }
            if ((pos1 - pos2) < minSize)
                pos1 = pos2 + minSize;
        }
    }
    else
    {
        if (pos2 > (maxDistance - minSize))
        {
            pos1 -= (pos2 - (maxDistance - minSize));
            //pos1 = pos2 - minSize;
            pos2 = (maxDistance - minSize);
            strategy1 = AutoGeneratorLine::e_weight_more_lower;
        }
        else if (pos1 < border)
        {
            pos1 = border;
            pos2 += (border - pos1);//border + minSize;
            strategy1 = AutoGeneratorLine::e_weight_more_higher;
        }
        else
        {

        }
    }
}

QLineF AutoGeneratorLine::tuneLine(QLineF line)
{
    //qDebug("tuneLine (%.2f,%.2f) (%.2f,%.2f)", line.x1(), line.y1(), line.x2(), line.y2());
    if ((m_rect.width() <= 0) || (m_rect.height() <= 0))
        return QLineF(0, 0, 0, 0);

    if (e_weight_rotation_clockwise == m_weightStrategyOfRotation)
    {
        m_weightStrategyOfX1 = e_weight_more_higher;
        m_weightStrategyOfX2 = e_weight_more_lower;
    }
    else if (e_weight_rotation_counterclockwise == m_weightStrategyOfRotation)
    {
        m_weightStrategyOfX1 = e_weight_more_lower;
        m_weightStrategyOfX2 = e_weight_more_higher;
    }

    int lowBoundX1, highBoundX1, lowBoundX2, highBoundX2;
    int lowBoundY1, highBoundY1, lowBoundY2, highBoundY2;
    int offsetX1, offsetX2, offsetY1, offsetY2;

    getLowHighBound(lowBoundX1, highBoundX1, m_weightStrategyOfX1, m_weightStrategyOfRotation);
    getLowHighBound(lowBoundX2, highBoundX2, m_weightStrategyOfX2, m_weightStrategyOfRotation);
    getLowHighBound(lowBoundY1, highBoundY1, m_weightStrategyOfY1, e_weight_rotation_none);
    getLowHighBound(lowBoundY2, highBoundY2, m_weightStrategyOfY2, e_weight_rotation_none);

#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    offsetX1 = m_generator->bounded(static_cast<int>(lowBoundX1), static_cast<int>(highBoundX1));
    offsetX2 = m_generator->bounded(static_cast<int>(lowBoundX2), static_cast<int>(highBoundX2));
    offsetY1 = m_generator->bounded(static_cast<int>(lowBoundY1), static_cast<int>(highBoundY1));
    offsetY2 = m_generator->bounded(static_cast<int>(lowBoundY2), static_cast<int>(highBoundY2));
#else
    offsetX1 = GetBounded(lowBoundX1, highBoundX1);
    offsetX2 = GetBounded(lowBoundX2, highBoundX2);
    offsetY1 = GetBounded(lowBoundY1, highBoundY1);
    offsetY2 = GetBounded(lowBoundY2, highBoundY2);
#endif
    qreal maxWidth = static_cast<qreal>(m_rect.right() - m_rect.left() - (k_border_size << 1));
    qreal maxHeight = static_cast<qreal>(m_rect.bottom() - m_rect.top() - (k_border_size << 1));

    qreal posX1 = static_cast<qreal>(line.x1() + offsetX1);
    qreal posX2 = static_cast<qreal>(line.x2() + offsetX2);
    qreal posY1 = static_cast<qreal>(line.y1() + offsetY1);
    qreal posY2 = static_cast<qreal>(line.y2() + offsetY2);

    AdjustPoint(m_weightStrategyOfX1, m_weightStrategyOfX2, posX1, posX2, k_border_size, k_min_width_of_vehicle, maxWidth);
    AdjustPoint(m_weightStrategyOfY1, m_weightStrategyOfY2, posY1, posY2, k_border_size, k_min_height_of_vehicle, maxHeight);

    if (m_count > 320)
    {
        m_count = 0;
        if (e_weight_rotation_clockwise == m_weightStrategyOfRotation)
            m_weightStrategyOfRotation = e_weight_rotation_counterclockwise;
        else if (e_weight_rotation_counterclockwise == m_weightStrategyOfRotation)
            m_weightStrategyOfRotation = e_weight_rotation_clockwise;
    }
    else
        ++m_count;

    return QLineF(posX1, posY1, posX2, posY2);
}
