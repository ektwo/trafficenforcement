#ifndef TRVIDEOTHREAD_H
#define TRVIDEOTHREAD_H


#include <QThread>
#include <QObject>
#include <QTimer>
#include <QElapsedTimer>
#include <QMetaType>
#include <QImage>
#include <QWidget>
#include "traffic_enforcement.h"
#include "abstractsource.h"
//#include <QtAV/QtAV.h>
#include <QtAV/FrameReader.h>

typedef struct S_Video_Config_Tag {
	uint32_t interval_ms;
    int32_t  want_width;
    int32_t  want_height;
	char filename[256];

} SVideoConfig;

typedef struct S_Video_Capabilities_Tag {
    int32_t width;
    int32_t height;
    int32_t frame_cnt;

} SVideoCapabilities;

class TRVideoFileSource : public AbstractSource//QObject
{
	Q_OBJECT
public:
	TRVideoFileSource(BaseGraph *parent = {});
	~TRVideoFileSource();

    bool ConnectTo(Filter *pDownstreamFilter);
    void DisconnectAll();

    SVideoConfig* GetConfig() { return &m_config; }
	bool SetConfig(void *pConfig);
    bool Open();
    void Close();

	bool GetFirstFrame(cv::Mat &frame);

signals:
	void started();
	void stopped();
    void cvColorReady(const cv::Mat &matImage);
    void qColorReady(const QImage &qImage);

public slots:
    void start(int cam = {});
	void stop();
    void handleColorFrameCV(const cv::Mat &frame) {}
    void handleColorFrameQ(const QImage &image) {}
    void handleProcessedFrameCV(const cv::Mat &frame) {}

protected:
    bool InternalStart();
    void InternalStop();
    void SendFrameFast(const QtAV::VideoFrame &videoFrame);
    void SendFrame(const QtAV::VideoFrame &videoFrame);


    void DoImageProcessing(const cv::Mat &matImage);
	void timerEvent(QTimerEvent *event);



private:
    //EComponentState m_state;
	//int m_timerId;
	int m_currentFrame;
	//QTimer *m_pTimer;
    uint8_t *m_pInBuffer;
    uint8_t *m_pOutBuffer;
	SVideoConfig m_config;
    SVideoCapabilities m_capability;
    QElapsedTimer m_timerCalc;
	QScopedPointer<cv::VideoCapture> m_videoCapture;
    QtAV::FrameReader m_qtavReader;

};

#endif // TRVIDEOTHREAD_H
