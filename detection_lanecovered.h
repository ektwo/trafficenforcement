#ifndef DETECTION_LANECOVERED_H
#define DETECTION_LANECOVERED_H

#include <qglobal.h>
#include <opencv2/opencv.hpp>

bool CheckCoveredState(cv::Vec4i *pLaneLine, cv::Point leftWheel, cv::Point rightWheel, int tolerance);

#endif // DETECTION_LANECOVERED_H
